﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using App;
using App.BbTemplate;
using App.Bundle;
using App.Extension;
using App.Extension.Configuration;
using App.Framework.Security;
using App.Menu;
using App.Properties;
using App.Resources;
using ECabinet.Data.Properties;
using ECabinetApp.Controllers;
using ECabinetApp.Controllers.Cabinet;
using ECabinetApp.Controllers.Managment;
using ECabinetApp.Controllers.SM;

namespace ECabinetApp {
	public class Plugin : AppPlugin<Plugin> {
		public static IHtmlString ECabinetResources() { return AppJsResourceManager.GetResources(Plugin.Current, @"Resources\js\resources\eCabinet.js", new AppJsToResxSetting("_CS", "", typeof(Strings))); }

		public Plugin(AppPackage package, AppExtensionElement config) : base(package, config) {

		}

		public override List<AppBbTemplateBase> GetBbTemplates() {
			return null;
		}

		public override Type GetAuditEventsEnum() {
			return null;
		}

		public override Type GetMonitoringEventsEnum() {
			return null;
		}

		public override Type GetDefautResxType() {
			return typeof(AppStrings);
		}

		public override Guid Gid {
			get { return Guid.Parse("7a7e9312-81c5-4706-1111-a166cea4ec5e"); }
		}

		public override string GetPluginKey() {
			return "eCabinet.App";
		}

		public override Dictionary<string, AppMenuItemInfo> GetMenuItems() {
			return MenuInitializer.GetMenuItems();
		}

		public static AppScriptBundle Script = null;

		public override void RegisterBundles(BundleCollection bundles) {
			Script = new AppScriptBundle(Current, "~/res/js/System").IncludeDirectory("~/Resources/js/System", "*.js", false);
			bundles.Add(Script);
			// Add Managment content
			bundles.Add(EntryManagerController.Script);
			bundles.Add(SertViewController.Script);
			bundles.Add(OpsStoreController.Script);
			bundles.Add(UserStoreController.Script);
			bundles.Add(ClientHistoryController.Script);
			// Add SM content
			bundles.Add(SmClientsController.Script);
			bundles.Add(SmEmployesController.Script);
			bundles.Add(SmEntryController.Script);
			bundles.Add(SmInspectController.Script);
			bundles.Add(SmOpsProfileController.Script);
			bundles.Add(SmReportsController.Script);
			bundles.Add(NcaReportsController.Script);
			bundles.Add(SmSertificateController.Script);
			bundles.Add(SmTransferController.Script);
			// Add Main Page content
			bundles.Add(CabinetMainPageController.Script);
			bundles.Add(CabinetMainPageController.Style);

			base.RegisterBundles(bundles);
		}

		public override void RegisterRoutes(RouteCollection routes, ref Route defaultRoute) {
			base.RegisterRoutes(routes, ref defaultRoute);
			routes.Ignore("Resources");
		}
	}
}