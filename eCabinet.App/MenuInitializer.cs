﻿using System.Collections.Generic;
using App;
using App.Data.Xpo.Menu;
using App.Enums;
using App.Framework.Security;
using App.Menu;
using App.Properties;
using ECabinet.Core.Helpers;
using ECabinet.Data.Properties;

namespace ECabinetApp {
	/// <summary> инициализация меню </summary>
	public class MenuInitializer : AppMenuInitializerBase {
		public enum MenuItems {
			MngReestrOps,
			MngReestrUsers,
			MngEntries,
			MngSerts,
			MngClientHistory,

			SertProfile,
			SertEmployes,
			SertClients,
			SertEntries,
			SertSertificats,
			SertInspection,
			SertReports,
			SertNcaReports,
			Transfer,

			RefCountryes
		}

		public override AppMenuInitType InitType { get { return AppMenuInitType.AtEveryRunForChange; } }

		public override int MenuVersion { get { return 21; } }

		public override List<AppMenuItem> GetMenu() {
			var menu = new List<AppMenuItem>();
			var administration = new AppMenuItem("Administration", "", () => AppStrings.Administration, Icons1.Cog);
			administration.AddChild("DefaultAdminPlugin", "DefaultMonitoring", () => AppStrings.Monitoring, "fa-bar-chart");
			administration.Childs.Add(new AppMenuItem("DefaultAdminPlugin", "DefaultAudit", () => AppStrings.Audit, "fa-area-chart"));
			administration.Childs.Add(new AppMenuItem("DefaultAdminPlugin", "DefaultExceptionLog", () => AppStrings.ExceptionLog, "fa-bug"));
			administration.Childs.Add(new AppMenuItem("DefaultRootPlugin", "BbTemplates", () => AppStrings.BbTemplates, "fa-code"));
			administration.Childs.Add(new AppMenuItem("DefaultRootPlugin", "RootSettings", () => AppStrings.Settings, "fa-cogs"));

			var manage = new AppMenuItem("manage", "", () => Strings.Common_Managment, "fa-keyboard-o");
			manage.AddChild(Plugin.Current.GetPluginKey(), MenuItems.MngReestrOps.ToString());
			manage.AddChild(Plugin.Current.GetPluginKey(), MenuItems.MngReestrUsers.ToString());
			manage.AddChild(Plugin.Current.GetPluginKey(), MenuItems.MngEntries.ToString());
			manage.AddChild(Plugin.Current.GetPluginKey(), MenuItems.MngSerts.ToString());
			manage.AddChild(Plugin.Current.GetPluginKey(), MenuItems.MngClientHistory.ToString());

			var sert = new AppMenuItem("sertSM", "", () => Strings.MIG_SertSM, "fa-yelp");
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertProfile.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertEmployes.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertClients.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertEntries.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertSertificats.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertInspection.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertReports.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.SertNcaReports.ToString());
			sert.AddChild(Plugin.Current.GetPluginKey(), MenuItems.Transfer.ToString());

			var reference = new AppMenuItem("reference", "", () => Strings.MIG_Reference, "fa-leanpub");
			reference.AddChild(Plugin.Current.GetPluginKey(), MenuItems.RefCountryes.ToString());

			menu.Add(manage);
			menu.Add(sert);
			// menu.Add(administration);
			menu.Add(reference);

			return menu;
		}

		public static Dictionary<string, AppMenuItemInfo> GetMenuItems() {
			var list = new Dictionary<string, AppMenuItemInfo>();
			list.Add(MenuItems.MngReestrOps.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.MI_MngReestrOps, "fa-building-o", "OpsStore", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.MngReestrOps.ToString())));
			list.Add(MenuItems.MngReestrUsers.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.MI_MngReestrUsers, "fa-group", "UserStore", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.MngReestrUsers.ToString())));
			list.Add(MenuItems.MngEntries.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.Common_Managment, "fa-server", "EntryManager", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.MngEntries.ToString())));
			list.Add(MenuItems.MngClientHistory.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.Common_Clients, "fa-pencil-square", "ClientHistory", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.MngClientHistory.ToString())));
			// list.Add(MenuItems.MngSerts.ToString(), new AppMenuItemInfo(Plugin.Current, () => "Админка", "fa-server", "MngSerts", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.MngSerts.ToString())));

			list.Add(MenuItems.SertProfile.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.MI_SertProfile, "fa-list-alt", "SmOpsProfile", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.SertProfile.ToString())));
			list.Add(MenuItems.SertEmployes.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.Common_Employees, "fa-group", "SmEmployes", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.SertEmployes.ToString())));
			list.Add(MenuItems.SertClients.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.Common_Clients, "fa-user-secret", "SmClients", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.SertClients.ToString())));
			list.Add(MenuItems.SertEntries.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.Common_Applications, "fa-list", "SmEntry", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.SertEntries.ToString())));
			list.Add(MenuItems.SertSertificats.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.Common_Certifications, "fa-file-text-o", "SmSertificate", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.SertSertificats.ToString())));
			list.Add(MenuItems.SertInspection.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.SmInspect_Inspections, "fa-edit", "SmInspect", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.SertInspection.ToString())));
			list.Add(MenuItems.SertReports.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.MI_SertReports, "fa-clone", "SmReports", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.SertReports.ToString())));
			list.Add(MenuItems.SertNcaReports.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.MI_SertNcaReports, "fa-clone", "NcaReports", "Index", null));
			list.Add(MenuItems.Transfer.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.MI_Transfers, "fa-server", "SmTransfer", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.Transfer.ToString())));

			list.Add(MenuItems.RefCountryes.ToString(), new AppMenuItemInfo(Plugin.Current, () => Strings.MI_RefCountryes, "fa-sun-o", "Actions", "Index", new MenuItemAccess(new PMainMenu(), MenuItems.RefCountryes.ToString())));

			list.Add(AppConsts.HomePageKey, new AppMenuItemInfo(Plugin.Current, () => Strings.MI_RefCountryes, "fa-pencil-square", "CabinetMainPage", "index", null));
			return list;
		}
	}

	public class PMainMenu : FwPrivilegeBase { }

	public class MenuItemAccess : FwRuleBase {
		public MenuItemAccess(FwPrivilegeBase privilege, string key)
			: base(privilege, key) {

		}
	}
}
