﻿using System.Collections.Generic;
using System.Text;
using ECabinet.Data.Properties;

namespace ECabinetApp.Views.Shared {
	public class StingsManager {
		private static StingsManager _instance = null;
		private Dictionary<string, StringsDictionary> _cacheDictionaries = new Dictionary<string, StringsDictionary>();
		
		public static StingsManager Instance {
			get { return _instance ?? (_instance = new StingsManager()); }
		}

		private StingsManager() {
			_cacheDictionaries.Add(Common.ToString(), Common);
		}

		public StringsDictionary Common {
			get {
				return new StringsDictionary("$_CS") {
					{ "ops_TypeOPS", Strings.OpsStore_TypeOPS },
				};
			}
		}

		public string Render(string prefix = "") {
			return "";
		}
	}

	public class StringsDictionary : Dictionary<string, string> {
		private string _cache = null;
		private string _prefix = null;

		public StringsDictionary(string prefix) {
			_prefix = prefix;
		}

		public string Render {get{ return _cache ?? (_cache = BuildSource());}}

		private string BuildSource() {
			StringBuilder sb = new StringBuilder();
			foreach (var tranclateString in this) {
				
			}
			return sb.ToString();
		}
	}
}