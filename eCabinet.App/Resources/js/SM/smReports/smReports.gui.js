﻿// #region Выдано сертификатов

Ext.define('App.smReportSerts.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smReportSerts.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.SmSertificate.StatusDisplay.name, type: 'string' },
				{ name: MDL.SmSertificate.SertifikateSmNumber.name, type: 'string' },
				{ name: MDL.SmSertificate.BlankNumber.name, type: 'string' },
				{ name: MDL.SmSertificate.Declarant.name, type: 'string' },
				{ name: MDL.SmSertificate.StartDate.name, type: 'date' },
				{ name: MDL.SmSertificate.FinishDate.name, type: 'date' },
				{ name: MDL.SmSertificate.AreaSertification.name, type: 'string' },
				{ name: MDL.SmSertificate.Auditor.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadSmSerts"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		// { text: $_CS.smkrep_Status, dataIndex: MDL.SmSertificate.StatusDisplay.name, width: 110 },
		{ text: 'Регистрация заявки', dataIndex: MDL.SmSertificate.RegistrationEntryDate.name, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: 'Дата заявки', dataIndex: MDL.SmSertificate.EntryDate.name, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: 'Дата обследования', dataIndex: MDL.SmSertificate.ResultReseachDate.name, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: $_CS.SmReports.SertBlankNumber, dataIndex: MDL.SmSertificate.SertifikateSmNumber.name, width: 170 },
		// { text: $_CS.smkrep_BlankNumber, dataIndex: MDL.SmSertificate.BlankNumber.name, width: 90 },
		{ text: 'Действует с', dataIndex: MDL.SmSertificate.StartDate.name, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: 'Окончание', dataIndex: MDL.SmSertificate.FinishDate.name, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: 'Система', dataIndex: MDL.SmSertificate.System.name, flex: 1 },
		{ text: $_CS.SmReports.AreaSertificationName, dataIndex: MDL.SmSertificate.AreaSertification.name, flex: 3 },
		{ text: $_CS.SmReports.DeclarantNameAdres, dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ text: 'Аудитор', dataIndex: MDL.SmSertificate.Auditor.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.SmReports.CertificatesIssuanceInform
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'OpsField',
				reference: 'OpsField',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.smReportSerts.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadSmOps"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			},
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			},{
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowSmSerts'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onSmSertGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Выдано сертификатов

// #region Зарегистрировано заявок

Ext.define('App.smReportEntries.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smReportEntries.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.SmEntry.StatusDisplay.name, type: 'string' },
				{ name: MDL.SmEntry.EntryDate.name, type: 'date' },
				{ name: MDL.SmEntry.Declarant.name, type: 'string' },
				{ name: MDL.SmEntry.Autor.name, type: 'string' },
				{ name: MDL.SmEntry.DeclarantEmail.name, type: 'string' },
				{ name: MDL.SmEntry.AreaSertification.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadSmEntryes"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Status, dataIndex: MDL.SmEntry.StatusDisplay.name, width: 110 },
		{ text: $_CS.SmReports.StartDate, dataIndex: MDL.SmEntry.EntryDate.name, xtype: 'datecolumn', width: 110, format: 'd.m.Y' },
		{ text: $_CS.Common.Declarant, dataIndex: MDL.SmEntry.DeclarantDisplay.name, width: 170, flex: 2 },
		{ text: App.$STR.Email, dataIndex: MDL.SmEntry.DeclarantEmail.name, width: 100 },
		{ text: $_CS.Common.AreaSertification, dataIndex: MDL.SmEntry.AreaSertification.name, flex: 3 },
		{ text: App.$STR.Autor, dataIndex: MDL.SmEntry.Autor.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.SmEntry.RegisteredApplicationsSmk
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'OpsField',
				reference: 'OpsField',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.smEntrySerts.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadSmOps"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			},
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowSmEntryes'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onSmSertGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Зарегистрировано заявок

// #region Статистика по СМ [ОПС]
Ext.define('App.smStatisticSerts.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smStatisticSerts.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadStatisticSert"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.OPS, dataIndex: MDL.OpsUnit.Id.name, flex: 1 },
		{ text: App.$STR.Count, dataIndex: MDL.OpsUnit.Name.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.SmReports.StatisticsOnSmk,
		title: $_CS.SmReports.StatisticsOnSmkByops
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			}, {
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowStatisticSert'
			}, {
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Статистика по СМ [ОПС]

// #region Статистика по заявкам [ОПС]
Ext.define('App.smStatisticEntries.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		model: Ext.define('App.smStatisticEntries.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadsmStatisticEntries"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.OPS, dataIndex: MDL.OpsUnit.Id.name, flex: 1 },
		{ text: App.$STR.Count, dataIndex: MDL.OpsUnit.Name.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.StatisticsOnApplications
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowStatisticEntries'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Статистика по заявкам [ОПС]

// #region Динамика оформления СМ
Ext.define('App.smReportDinamicSert.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smReportDinamicSert.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadDinamicSert"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Date, dataIndex: MDL.OpsUnit.Id.name, flex: 1 },
		{ text: App.$STR.Count, dataIndex: MDL.OpsUnit.Name.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.StatisticsOnApplications
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowDinamicSert'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Статистика по заявкам [ОПС]

// #region Статистика типы СМ [ОПС]

Ext.define('App.smStatisticSertTypes.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smStatisticSertTypes.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadSmSertTypes"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.SmReports.CertificationTypeSmk, dataIndex: MDL.OpsUnit.Id.name, flex: 1 },
		{ text: App.$STR.Count, dataIndex: MDL.OpsUnit.Name.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.CertificateTypesByRegion,
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'OpsField',
				reference: 'OpsField',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.smReportSertTypes.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadSmOps"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			},
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowSmSertTypes'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Типы сертификатов

// #region Сертификаты [Регионы]
Ext.define('App.smReportSertRegion.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		model: Ext.define('App.smReportSertRegion.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadSertFromRegion"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.SmReports.RegionData, dataIndex: MDL.OpsUnit.Id.name, flex: 1 },
		{ text: App.$STR.Count, dataIndex: MDL.OpsUnit.Name.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.CertificatesByRegion
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.InSection,
				name: MDL.OpsUnit.Location.name,
				reference: MDL.OpsUnit.Location.name,
				editable: false,
				labelWidth: 100,
				mode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					fields: [
						{ name: 'Id', type: 'int' },
						{ name: 'Name', type: 'string' }
					],
					data: [
						{ 'Id': 0, 'Name': $_CS.Common.Region },
						{ 'Id': 1, 'Name': App.$STR.City }
					]
				}),
				valueField: 'Id',
				displayField: 'Name'
			},
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowSertRegionCount'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Организации по регионам

// #region Клиенты по регионам ОПС
Ext.define('App.smOrgsFromAreasOPS.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smOrgsFromAreasOPS.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadOrgsFromAreasOPS"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Organisation, dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ text: $_CS.Common.OPS, dataIndex: MDL.SmSertificate.Auditor.name, flex: 2 },
		{ text: $_CS.SmReports.SertificationDate, dataIndex: MDL.SmSertificate.StartDate.name, flex: 1, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: $_CS.Common.CertificationType, dataIndex: 'SertType', flex: 1 },
		{ text: App.$STR.Status, dataIndex: MDL.SmSertificate.Status.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.ClientByRegionOps
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'Areald',
				reference: 'Areald',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.smOrgsFromAreasOPS.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadLocalRegions"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			},
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowOrgsFromAreasOPS'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Клиенты по регионам ОПС

// #region Клиенты по регионам
Ext.define('App.smOrgsFromAreas.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smOrgsFromAreas.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadOrgsFromAreas"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Organisation, dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ text: $_CS.Common.OPS, dataIndex: MDL.SmSertificate.Auditor.name, flex: 2 },
		{ text: $_CS.SmReports.OpsRegion, dataIndex: 'Location', flex: 1 },
		{ text: $_CS.SmReports.SertificationDate, dataIndex: MDL.SmSertificate.StartDate.name, flex: 1, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: $_CS.Common.CertificationType, dataIndex: 'SertType', flex: 1 },
		{ text: App.$STR.Status, dataIndex: MDL.SmSertificate.Status.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.ClientByRegion
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'Areald',
				reference: 'Areald',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.smOrgsFromAreas.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadLocalRegions"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			},
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowOrgsFromAreas'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Клиенты по регионам

// #region Организации [Регионы]
Ext.define('App.smReportOpsRegion.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		model: Ext.define('App.smReportOpsRegion.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadOpsFromRegion"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.SmReports.RegionData, dataIndex: MDL.OpsUnit.Id.name, flex: 1 },
		{ text: App.$STR.Count, dataIndex: MDL.OpsUnit.Name.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.OrgByRegion
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.InSection,
				name: MDL.OpsUnit.Location.name,
				reference: MDL.OpsUnit.Location.name,
				editable: false,
				labelWidth: 100,
				mode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					fields: [
						{ name: 'Id', type: 'int' },
						{ name: 'Name', type: 'string' }
					],
					data: [
						{ 'Id': 0, 'Name': $_CS.Common.Region },
						{ 'Id': 1, 'Name': App.$STR.City }
					]
				}),
				valueField: 'Id',
				displayField: 'Name'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowRegionCount'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Организации по регионам

// #region Типы сертификатов [ОПС]
Ext.define('App.smReportTypeSertRegion.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smReportTypeSertRegion.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [

			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadSertFromType"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.OPS, dataIndex: 'Ops', flex: 3 },
		{ text: $_CS.SmReports.RegionData, dataIndex: 'Region', flex: 2 },
		{ text: $_CS.Common.Total, dataIndex: 'Count', flex: 1 },
		{ text: '9001', dataIndex: 's9001', flex: 1 },
		{ text: '14001', dataIndex: 's14001', flex: 1 },
		{ text: '18001', dataIndex: 's18001', flex: 1 },
		{ text: '22000', dataIndex: 's22000', flex: 1 },
		{ text: '29001', dataIndex: 's29001', flex: 1 },
		{ text: '50001', dataIndex: 's50001', flex: 1 },
		{ text: '27001', dataIndex: 's27001', flex: 1 },
		{ text: '1352', dataIndex: 's1352', flex: 1 },
		{ text: '1179', dataIndex: 's1179', flex: 1 },
		{ text: '1614', dataIndex: 's1614', flex: 1 },
		{ text: '1617', dataIndex: 's1617', flex: 1 },
		{ text: '120203', dataIndex: 's120203', flex: 1 },
		{ text: '20000', dataIndex: 's20000', flex: 1 },
		{ text: '14001 2016', dataIndex: 's14001_16', flex: 1 },
		{ text: '9001 2016', dataIndex: 's9001_16', flex: 1 },
		{ text: '27001 2015', dataIndex: 's27001_15', flex: 1 },
		{ text: '20000 2016', dataIndex: 's20000_16', flex: 1 },

		{ text: '13485-2017', dataIndex: 's13485_17', flex: 1 },
		{ text: '45001:2018', dataIndex: 's45001_18', flex: 1 },
		{ text: '45001-2019', dataIndex: 's45001_19', flex: 1 },
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		title: $_CS.SmReports.SmkStatByCertificationTypes,
		fileName: $_CS.SmReports.SmkStatByCertificationTypes
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowSmOutSerts'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Организации по регионам

// #region Мониоринг аудиторов

Ext.define('App.smAuditorsMonitoring.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		model: Ext.define('App.smAuditorsMonitoring.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.OpsUnit.Id.name, type: 'string' },
				{ name: MDL.OpsUnit.Name.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("SmMonitoringAuditors"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.Auditor, dataIndex: MDL.OpsUnit.Id.name, flex: 1 },
		{ text: App.$STR.Organisation, dataIndex: MDL.OpsUnit.Name.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.SmReports.MonitoringOfAuditors,
		title: $_CS.SmReports.MonitoringOfAuditors
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Show,
				handler: 'onShowSmMonitoringAuditors'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Мониоринг аудиторов

// #region Выгрузка

Ext.define('App.smOutReport.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smOutReport.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.SmSertificate.StatusDisplay.name, type: 'string' },
				{ name: MDL.SmSertificate.SertifikateSmNumber.name, type: 'string' },
				{ name: MDL.SmSertificate.BlankNumber.name, type: 'string' },
				{ name: MDL.SmSertificate.Declarant.name, type: 'string' },
				{ name: MDL.SmSertificate.StartDate.name, type: 'date' },
				{ name: MDL.SmSertificate.FinishDate.name, type: 'date' },
				{ name: MDL.SmSertificate.AreaSertification.name, type: 'string' },
				{ name: MDL.SmSertificate.Auditor.name, type: 'string' },
				{ name: MDL.SmSertificate.Signatory.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadSmOutSerts"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: 'KOD_OPS', dataIndex: MDL.SmSertificate.Counter.name, flex: 1 },
		{ text: 'NAME_OPS', dataIndex: MDL.SmSertificate.Signatory.name, flex: 3 },
		{ text: 'DATE_R', dataIndex: MDL.SmSertificate.StartDate.name, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: 'DATE_O', dataIndex: MDL.SmSertificate.FinishDate.name, xtype: 'datecolumn', width: 100, format: 'd.m.Y' },
		{ text: 'NSERT', dataIndex: MDL.SmSertificate.SertifikateSmNumber.name, width: 170 },
		{ text: 'NBLSERT', dataIndex: MDL.SmSertificate.BlankNumber.name, width: 85 },
		{ text: 'NM', dataIndex: MDL.SmSertificate.AreaSertification.name, flex: 3 },
		{ text: 'ZAYV', dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ text: 'SYS', dataIndex: MDL.SmSertificate.System.name, flex: 2 },
		{ text: 'TREB', dataIndex: MDL.SmSertificate.Requirements.name, flex: 3 },
		{ text: 'EXP', dataIndex: MDL.SmSertificate.Auditor.name, flex: 2 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: { fileName: $_CS.SmReports.IssuedCertificatesSmk },
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			},{
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowSmOutSerts'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Выдано сертификатов

// #region Controller

Ext.define('App.organization.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.smReports',

	onShowSmOutSerts: function () {
		this.view.store.proxy.extraParams = {
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate:this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};
		this.view.store.load(/*{
			params: {
				startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
				finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
			}
		}*/);
	},

	onShowDinamicSert: function () {
		this.view.store.proxy.extraParams = {
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowOrgsFromAreasOPS: function () {
		this.view.store.proxy.extraParams = {
			areald: this.view.lookupReference('Areald').getValue(),
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowOrgsFromAreas: function () {
		this.view.store.proxy.extraParams = {
			areald: this.view.lookupReference('Areald').getValue(),
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowStatisticSert: function () {
		this.view.store.proxy.extraParams = {
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowStatisticEntries: function () {
		this.view.store.proxy.extraParams = {
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowRegionCount: function () {
		this.view.store.proxy.extraParams = {
			opsType: this.view.lookupReference(MDL.OpsUnit.Location.name).getValue()
		};

		this.view.store.load();
	},

	onShowSmSerts: function () {
		this.view.store.proxy.extraParams = {
			opsId: this.view.lookupReference('OpsField').getValue(),
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowSmEntryes: function () {
		this.view.store.proxy.extraParams = {
			opsId: this.view.lookupReference('OpsField').getValue(),
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onSmSertGridPrint: function () {
		this.view.file.save({
			remote: true,
			title: $_CS.SmReports.CertIssuanceInformByPeriod + Ext.Date.format(this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(), 'd.m.Y') + ' ' + App.$STR.To + ' '
				+ Ext.Date.format(this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue(), 'd.m.Y'),
			title2: $_CS.Reports.OrgNameByCertififcation + this.view.lookupReference('OpsField').getRawValue() + '\r\n' +
				$_CS.SmReports.RegNumberInStateReg
		});
	},

	onShowSertRegionCount: function () {
		this.view.store.proxy.extraParams = {
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue(),
			opsType: this.view.lookupReference(MDL.OpsUnit.Location.name).getValue()
		};

		this.view.store.load();
	},

	onShowSmSertTypes: function () {
		this.view.store.proxy.extraParams = {
			opsId: this.view.lookupReference('OpsField').getValue(),
			startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowSmMonitoringAuditors: function () {
		this.view.store.load();
	},

	onGridPrint: function () {
		this.view.file.save({ remote: true });
		//this.view.file.savePages(1, { remote: true });
		// this.view.file.save();
	},

	onCancel: function () {
		this.view.close();
	}
});

// #endregion Controller

Ext.define('App.smReports.view.Panel', {
	extend: 'Ext.panel.Panel',
	requires: ['Ext.layout.container.Border'],
	layout: 'fit',
	bodyBorder: false,
	items: [
		new App.smReports.view.TabReports({
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.smReports.view.Panel')
		]
	});
});