﻿Ext.define('App.smSert.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.smSert',

	onSelectionChange: function (g, rs) {
		this.view.lookupReference('btnSert').setDisabled(rs.length < 1);
		var isDisable = true;
		if (rs.length > 0) {
			var status = rs[0].get('Status');
			isDisable = status == 1 || status == 7;
		}

		this.disableButtons(isDisable);
	},

	disableButtons: function (isDisable) {
		this.view.lookupReference('btnPrint').setDisabled(isDisable);
		this.view.lookupReference('btnNewPrint').setDisabled(isDisable);
		this.view.lookupReference('btnDublicate').setDisabled(isDisable);
		this.view.lookupReference('btnDelete').setDisabled(isDisable);
		this.view.lookupReference('btnAuditor').setDisabled(isDisable);
		this.view.lookupReference('btnProfile').setDisabled(isDisable);
		this.view.lookupReference('txtFind').setDisabled(isDisable);
		this.view.lookupReference('btnFind').setDisabled(isDisable);
	},

	onDeleteSert: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length !== 1) return;
		var store = this.view.store;

		this.confirm($_CS.SmSertificate.ConfirmDeleteCertificate, null, function () {
			var prms = {
				oid: rs[0].get('Id')
			},
				fn = function (d, res) {
					this.toast.info(res.message);
					store.reload();
				};
			this.ajax.post('DeleteSert', prms, fn, App.$STR.Saving, null, 200);
		}, null, this);
	},

	onEdit: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length !== 1) return;
		this.showEditor(false, rs[0]);
	},

	onAdd: function () {
		this.showEditor(true);
	},

	onDublicate: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		// this.showEditor(false, rs[0]);
		var store = this.view.store;
		var dlg = Ext.create('App.smSert.view.Editor', {
			record: rs[0],
			isNew: true,
			scope: store,
			callback: store.reload
		});

		dlg.show();
		// dlg.lookupReference(MDL.SmSertificate.SmSertificationType.name).setDisabled(true);
		// dlg.lookupReference(MDL.SmSertificate.FinishDate.name).setDisabled(true);
		dlg.lookupReference(MDL.SmSertificate.DublicateReason.name).setHidden(false);
		dlg.lookupReference(MDL.SmSertificate.BlankNumber.name).setValue('0');
	},

	onProfile: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length !== 1) return;
		var record = null;

		this.ajax.post('LoadClientProfile', { sert: rs[0].get('Id') }, function (res) {
			record = res;
			var dlg = Ext.create('App.smClientProfile.view.Editor', {
				record: record,
			});
			dlg.show();
		}, $_CS.Common.LoadingData, null, 200);
	},

	onSaveClientProfile: function () {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues())
			},
			fn = function (d, res) {
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveClientProfile', prms, fn, App.$STR.Saving, null, 200);
	},

	onChangeAuditor: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length !== 1) return;

		var store = this.view.store;
		var dlg = Ext.create('App.changeAuditor.view.Editor', {
			record: rs[0].data,
			scope: store,
			callback: store.reload
		});

		dlg.show();
	},

	onSaveAuditor: function () {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues())
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveAuditor', prms, fn, App.$STR.Saving, null, 200);
	},

	showEditor: function (isNew, r) {
		var store = this.view.store;
		var dlg = Ext.create('App.smSert.view.Editor', {
			record: r,
			isNew: isNew,
			scope: store,
			callback: store.reload
		});

		dlg.show();
		if (isNew || r.get(MDL.SmSertificate.BlankNumber.name) === "") {
			var d = r.get(MDL.SmSertificate.ResultReseachDate.name);
			dlg.lookupReference(MDL.SmSertificate.StartDate.name).setMinValue(d);
			dlg.lookupReference(MDL.SmSertificate.StartDate.name).setValue(d);
			dlg.lookupReference(MDL.SmSertificate.InspectionDate.name).setMinValue(d);
			dlg.lookupReference(MDL.SmSertificate.InspectionDate.name).setValue(d.addYears(1));
			dlg.lookupReference(MDL.SmSertificate.FinishDate.name).setMinValue(d);
			dlg.lookupReference(MDL.SmSertificate.FinishDate.name).setValue(d.addYears(3));
			// dlg.lookupReference(MDL.SmSertificate.SmSertificationType.name).setDisabled(false);
			dlg.lookupReference(MDL.SmSertificate.FinishDate.name).setDisabled(false);
			dlg.lookupReference('DublicateReason').setHidden(true);

			var requimentRu = dlg.lookupReference(MDL.SmSertificate.Requirements.name);
			var requimentKz = dlg.lookupReference(MDL.SmSertificate.RequirementsKz.name);
			var requimentEn = dlg.lookupReference(MDL.SmSertificate.RequirementsEn.name);
			var combo = dlg.lookupReference(MDL.SmSertificate.SmSertificationType.name);
			var smType = combo.findRecord(combo.valueField, combo.getValue());
			//dlg.lookupReference(MDL.SmSertificate.SmSertificationType.name).getValue();
			requimentRu.setValue(smType.get('NameRu') + ' ' + smType.get('ValueRu'));
			requimentKz.setValue(smType.get('NameKz') + ' ' + smType.get('ValueKz'));
			requimentEn.setValue(smType.get('NameEn') + ' ' + smType.get('ValueEn'));

			var systemRu = dlg.lookupReference(MDL.SmSertificate.System.name);
			var systemKz = dlg.lookupReference(MDL.SmSertificate.SystemKz.name);
			var systemEn = dlg.lookupReference(MDL.SmSertificate.SystemEn.name);
			systemRu.setValue(smType.get('SystemRu'));
			systemKz.setValue(smType.get('SystemKz'));
			systemEn.setValue(smType.get('SystemEn'));
		}
	},

	onValidate: function (f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},
	onCancel: function () {
		this.view.close();
	},
	onSave: function () {
		var d1 = this.view.lookupReference(MDL.SmSertificate.StartDate.name);
		if (d1 !== null)
			if (d1.getValue() > this.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()) {
				this.toast.error($_CS.SmSertificate.StartDateLessThanEndDate);
				return;
			} else if (d1.getValue() > this.lookupReference(MDL.SmSertificate.InspectionDate.name).getValue()) {
				this.toast.error($_CS.SmSertificate.StartDateLessThanInspectDate);
				return;
			}

		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveSmSert', prms, fn, App.$STR.Saving, null, 200);
	},
	onGridPrint: function () {
		//		this.view.file.save({title: ''});
		this.view.file.save();
	},

	onSertTypeSelected: function (r, s) {
		var requimentRu = this.lookupReference(MDL.SmSertificate.Requirements.name);
		var requimentKz = this.lookupReference(MDL.SmSertificate.RequirementsKz.name);
		var requimentEn = this.lookupReference(MDL.SmSertificate.RequirementsEn.name);
		requimentRu.setValue(s.get('NameRu') + ' ' + s.get('ValueRu'));
		requimentKz.setValue(s.get('NameKz') + ' ' + s.get('ValueKz'));
		requimentEn.setValue(s.get('NameEn') + ' ' + s.get('ValueEn'));

		var systemRu = this.lookupReference(MDL.SmSertificate.System.name);
		var systemKz = this.lookupReference(MDL.SmSertificate.SystemKz.name);
		var systemEn = this.lookupReference(MDL.SmSertificate.SystemEn.name);
		systemRu.setValue(s.get('SystemRu'));
		systemKz.setValue(s.get('SystemKz'));
		systemEn.setValue(s.get('SystemEn'));
	},

	onSertFilterSelected: function (r, s) {
		this.view.store.currentPage = 1;
		this.view.store.proxy.extraParams = { sertType: s.get('Id') };
		findPatern: this.lookupReference('txtFind').setValue('');
		this.view.lookupReference('txtFind').setDisabled(false);
		this.view.lookupReference('btnFind').setDisabled(false);

		this.view.store.load();
	},

	onFindCertificate: function () {
		this.view.store.currentPage = 1;
		this.view.store.proxy.extraParams = { sertType: this.lookupReference('cbSelectFilter').getValue(), findPatern: this.lookupReference('txtFind').getValue() };
		this.view.store.load();
	},

	onPrint: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length !== 1) return;

		Ext.create('Ext.window.Window', {
			title: 'Печать сертификата',
			height: 600,
			width: 1000,
			modal: true,
			maximizable: true,
			bodyBorder: false,
			border: false,
			//			maximized: true,
			layout: 'fit',
			loader: {
				url: App.ajax.buildurl('../../SmPrintBlank?sertId=' + rs[0].get('Id'), null),
				renderer: 'frame'
			}
		}).show();
	},

	onPrintNew: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length !== 1) return;

		Ext.create('Ext.window.Window', {
			title: 'Печать сертификата [Новый бланк]',
			height: 600,
			width: 1000,
			modal: true,
			maximizable: true,
			bodyBorder: false,
			border: false,
			//			maximized: true,
			layout: 'fit',
			loader: {
				url: App.ajax.buildurl('../../SmPrintBlankNew?sertId=' + rs[0].get('Id'), null),
				renderer: 'frame'
			}
		}).show();
	},
});

Ext.define('App.smSert.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'smSert',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.smSert.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.smSert.view.Panel')
		]
	});
});