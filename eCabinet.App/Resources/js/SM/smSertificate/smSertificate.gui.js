﻿Ext.define('App.smSert.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smSert',
	xtype: 'widget.smSertgrid',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: App.defineModel('SmSertModel', App.MDL.SmSertificate),
				proxy: {
					type: 'ajax',
					url: window.buildurl("LoadSmSert"),
					reader: {
						type: 'json',
						rootProperty: 'data'
					}
				}
	}),
	file: {
		remote: true,
		title: $_CS.SmSertificate.CreateSM
	},
	columns: [
		{ text: App.$STR.Status, dataIndex: MDL.SmSertificate.StatusDisplay.name, width: 100 },
		{ text: $_CS.Common.Declarant, dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ xtype: 'datecolumn', text: $_CS.Common.Acts, dataIndex: MDL.SmSertificate.StartDate.name, width: 90, format: 'd.m.Y' },
		{ xtype: 'datecolumn', text: $_CS.SmSertificate.DateBefor, dataIndex: MDL.SmSertificate.FinishDate.name, width: 90, format: 'd.m.Y' },
		{ text: $_CS.SertView.SertNumber, dataIndex: MDL.SmSertificate.CounterDisplay.name, width: 175 },
		{ text: $_CS.SmSertificate.BlankNumber, dataIndex: MDL.SmSertificate.BlankNumber.name, width: 80 },
		{ text: $_CS.Common.Expert, dataIndex: MDL.SmSertificate.Auditor.name, flex: 1 },
		{ text: $_CS.Common.SertType, dataIndex: MDL.SmSertificate.SmSertificationTypeDisplay.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'container',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							defaultType: 'button',
							defaults: {
								style: 'margin-right:5px;'
							},
							style: 'margin-bottom:5px;',
							items: [
								{
									xtype: 'combobox',
									reference: 'cbSelectFilter',
									labelWidth: 60,
									fieldLabel: App.$STR.Filter,
									editable: false,
									mode: 'local',
									forceSelection: true,
									store: Ext.create('Ext.data.Store', {
										fields: [
											{ name: 'Id', type: 'int' },
											{ name: 'Name', type: 'string' }
										],
										data: [
											{ 'Id': 1, 'Name': $_CS.Common.Application },
											{ 'Id': 2, 'Name': $_CS.Common.Certificate },
											{ 'Id': 3, 'Name': $_CS.Common.Undo },
											{ 'Id': 4, 'Name': $_CS.Common.Pause },
											{ 'Id': 5, 'Name': $_CS.Common.Renewed },
											{ 'Id': 6, 'Name': $_CS.Common.Reassigned },
											{ 'Id': 7, 'Name': $_CS.Common.Removed }
										]
									}),
									listeners: {
										select: 'onSertFilterSelected',
										delay: 1
									},
									valueField: 'Id',
									displayField: 'Name'
								},
								{
									iconCls: Ext.icon.pagetext,
									text: $_CS.SmSertificate.CreateSM,
									reference: 'btnSert',
									disabled: true,
									handler: 'onEdit'
								},
								{
									iconCls: Ext.icon.dublicate,
									text: $_CS.SmSertificate.Dublicate,
									reference: 'btnDublicate',
									disabled: true,
									handler: 'onDublicate'
								},
								{
									iconCls: Ext.icon.printer,
									text: $_CS.Common.Certificate,
									reference: 'btnPrint',
									disabled: true,
									handler: 'onPrint'
								},
								{
									iconCls: Ext.icon.printer,
									text: App.$STR.Print,
									reference: 'btnNewPrint',
									disabled: true,
									handler: 'onPrintNew'
								},
							]
						},
						{
							xtype: 'container',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							defaults: {
								style: 'margin-right:5px;'
							},
							defaultType: 'button',
							items: [
								{
									xtype: 'textfield',
									labelWidth: 60,
									fieldLabel: App.$STR.Find,
									reference: 'txtFind',
									disabled: true,
								},
								{
									text: App.$STR.Find,
									reference: 'btnFind',
									handler: 'onFindCertificate',
									disabled: true
								},
								{
									text: 'Аудитор',
									reference: 'btnAuditor',
									handler: 'onChangeAuditor',
									disabled: true
								},
								{
									text: 'Доп. информация',
									reference: 'btnProfile',
									handler: 'onProfile',
									disabled: true
								},
								{
									iconCls: Ext.icon.delete,
									text: App.$STR.Delete,
									reference: 'btnDelete',
									handler: 'onDeleteSert',
									disabled: true
								},
								{
									iconCls: Ext.icon.excel,
									text: App.$STR.Excel,
									reference: 'btnPrintGrid',
									handler: 'onGridPrint'
								}
							]
						}
					]
				},
			],
	}],
	listeners: {
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

Ext.define('KitchenSink.view.tab.HeaderTabs', {
	extend: 'Ext.container.Container',
	xtype: 'header-tabs',

	layout: {
		type: 'hbox',
		align: 'middle'
	},
	viewModel: true,

	initComponent: function () {
		Ext.apply(this, {
			width: 800,
			items: [ ]
		});

		this.callParent();
	}
});

/* Editors for SmkSertification */
Ext.define('App.smSert.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'smSert',
	layout: 'fit',
	frame: true,
	height: 480,
	width: 750,
	modal: true,
	constrain: true,
	title: $_CS.Common.Certificate,

	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 145,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'combobox',
					fieldLabel: $_CS.SmSertificate.Dublicate,
					name: MDL.SmSertificate.DublicateReason.name,
					reference: MDL.SmSertificate.DublicateReason.name,
					editable: false,
					labelWidth: 145,
					flex: 1,
					mode: 'local',
					allowBlank: true,
					hidden: true,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.smSert.combo.SmDublicateReason', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("RefSmDublicateReason"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					defaults: {
						anchor: '100%',
						msgTarget: 'side'
					},
					style: 'margin-bottom:5px;',
					items: [
						{
							xtype: 'textfield',
							labelWidth: 35,
							fieldLabel: 'KCC',
							width: 135,
							padding: '0 20 0 0',
							maxLength: 7,
							minLength: 7,
							name: MDL.SmSertificate.BlankNumber.name,
							reference: MDL.SmSertificate.BlankNumber.name,
							allowBlank: false
						}, {
							xtype: 'textfield',
							name: MDL.SmSertificate.Status.name,
							hidden: true,
							allowBlank: true
						}, {
							xtype: 'textfield',
							labelWidth: 25,
							fieldLabel: '№',
							padding: '0 10 0 0',
							flex: 1,
							name: MDL.SmSertificate.CounterDisplay.name,
							readOnly: true,
							allowBlank: false
						}, {
							xtype: 'datefield',
							labelWidth: 80,
							fieldLabel: $_CS.Common.Acts,
							padding: '0 10 0 0',
							width: 200,
							name: MDL.SmSertificate.StartDate.name,
							reference: MDL.SmSertificate.StartDate.name,
							value: new Date(),
							allowBlank: false
						}, {
							xtype: 'datefield',
							labelWidth: 25,
							fieldLabel: $_CS.SmSertificate.DateBefor,
							//padding: '0 0 0 10',
							width: 150,
							value: new Date().addDays(365),
							name: MDL.SmSertificate.FinishDate.name,
							reference: MDL.SmSertificate.FinishDate.name,
							allowBlank: false
						}
					]
				},
				{
					fieldLabel: $_CS.Common.Declarant,
					name: MDL.SmSertificate.Declarant.name,
					readOnly: true,
					allowBlank: false
				},
				{
					xtype: 'container',
					layout: {
						type: 'hbox',
					},
					defaults: {
						anchor: '100%',
						msgTarget: 'side'
					},
					style: 'margin-bottom:5px;',
					items: [
						{
							xtype: 'combobox',
							fieldLabel: $_CS.Common.CertificationType,
							name: MDL.SmSertificate.SmSertificationType.name,
							reference: MDL.SmSertificate.SmSertificationType.name,
							editable: false,
							labelWidth: 145,
							flex: 1,
							mode: 'remote',
							allowBlank: false,
							disabled: true,
							forceSelection: true,
							store: Ext.create('Ext.data.Store', {
								autoLoad: true,
								model: Ext.define('App.smSert.view.SmSertificationType', {
									extend: 'Ext.data.Model',
									fields: [
										{ name: 'Id', type: 'int' },
										{ name: 'Name', type: 'string' },
										{ name: 'ValueKz', type: 'string' },
										{ name: 'ValueRu', type: 'string' },
										{ name: 'SystemKz', type: 'string' },
										{ name: 'SystemRu', type: 'string' }
									]
								}),
								proxy: {
									type: 'ajax',
									url: window.buildurl("LoadSmSertType"),
									reader: {
										type: 'json',
										rootProperty: 'data'
									}
								}
							}),
							listeners: {
								select: 'onSertTypeSelected',
								delay: 1
							},
							valueField: 'Id',
							displayField: 'Name'
						},
						{
							xtype: 'datefield',
							labelWidth: 70,
							width: 195,
							fieldLabel: $_CS.SmSertificate.DateInspection,
							padding: '0 0 0 10',
							value: new Date().addDays(365),
							name: MDL.SmSertificate.InspectionDate.name,
							reference: MDL.SmSertificate.InspectionDate.name,
							allowBlank: false
						}
					]
				}, {
					fieldLabel: $_CS.SmSertificate.Signature,
					name: MDL.SmSertificate.Signatory.name,
					allowBlank: false
				},
				{
					xtype: 'tabpanel',
					flex: 1,
					height: 290,
					reference: 'tabpanel',
					plain: true,
					defaults: {
						autoScroll: true,
						border: false
					},
					items: [
						{
							title: $_CS.Common.Certificate,
							iconCls: App.ICONS.flag_ru,
							items: [
								{
									xtype: 'container',
									flex: 1,
									layout: {
										type: 'vbox',
										align: 'stretch'
									},

									items: [
										{
											//labelAlign: 'top',
//											height: 80,
											flex: 1,
											xtype: 'textfield',
											fieldLabel: App.$STR.System,
											name: MDL.SmSertificate.System.name,
											reference: MDL.SmSertificate.System.name,
											allowBlank: false
										}, {
											labelAlign: 'top',
											flex: 1,
											height: 100,
											xtype: 'textareafield',
											fieldLabel: $_CS.Common.AreaSertification,
											name: MDL.SmSertificate.AreaSertification.name,
											allowBlank: false
										}, {
											//labelAlign: 'top',
											height: 80,
											xtype: 'textareafield',
											flex: 1,
											fieldLabel: $_CS.SmSertificate.Requiments,
											name: MDL.SmSertificate.Requirements.name,
											reference: MDL.SmSertificate.Requirements.name,
											allowBlank: false
										}
									]
								}
							]
						}, {
							title: $_CS.Common.Certificate,
							iconCls: App.ICONS.flag_kk,
							items: [
								{
									xtype: 'container',
									flex: 1,
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									anchorHorizontal: '100%',
									items: [
										{
											//labelAlign: 'top',
//											height: 60,
											xtype: 'textfield',
											fieldLabel: App.$STR.System,
											name: MDL.SmSertificate.SystemKz.name,
											reference: MDL.SmSertificate.SystemKz.name,
											allowBlank: false
										}, {
											labelAlign: 'top',
											height: 100,
											xtype: 'textareafield',
											fieldLabel: $_CS.Common.AreaSertification,
											name: MDL.SmSertificate.AreaSertificationKz.name,
											allowBlank: false
										}, {
											//labelAlign: 'top',
											height: 85,
											xtype: 'textareafield',
											fieldLabel: $_CS.SmSertificate.Requiments,
											name: MDL.SmSertificate.RequirementsKz.name,
											reference: MDL.SmSertificate.RequirementsKz.name,
											allowBlank: false
										}
									]
								}
							]
						},{
							title: $_CS.Common.Certificate,
							iconCls: App.ICONS.flag_gb,
							items: [
								{
									xtype: 'container',
									flex: 1,
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									anchorHorizontal: '100%',
									items: [
										{
											//labelAlign: 'top',
//											height: 60,
											xtype: 'textfield',
											fieldLabel: App.$STR.System,
											name: MDL.SmSertificate.SystemEn.name,
											reference: MDL.SmSertificate.SystemEn.name,
											allowBlank: false
										}, {
											labelAlign: 'top',
											height: 100,
											xtype: 'textareafield',
											fieldLabel: $_CS.Common.AreaSertification,
											name: MDL.SmSertificate.AreaSertificationEn.name,
											allowBlank: false
										}, {
											//labelAlign: 'top',
											height: 85,
											xtype: 'textareafield',
											fieldLabel: $_CS.SmSertificate.Requiments,
											name: MDL.SmSertificate.RequirementsEn.name,
											reference: MDL.SmSertificate.RequirementsEn.name,
											allowBlank: false
										}
									]
								}
							]
						}, {
							title: 'Прил',
							iconCls: App.ICONS.flag_ru,
							items: [
								{
									xtype: 'container',
									flex: 1,
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									items: [
										{
											labelAlign: 'top',
											height: 240,
											xtype: 'textareafield',
											fieldLabel: $_CS.Common.AdvancedInfo,
											name: MDL.SmSertificate.AreaSertificationAppendix.name,
											allowBlank: true
										}
									]
								}
							]
						},
						{
							title: 'Прил',
							iconCls: App.ICONS.flag_kk,
							items: [
								{
									xtype: 'container',
									flex: 1,
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									items: [
										{
											labelAlign: 'top',
											height: 240,
											xtype: 'textareafield',
											fieldLabel: $_CS.Common.AdvancedInfo,
											name: MDL.SmSertificate.AreaSertificationAppendixKz.name,
											allowBlank: true
										}
									]
								}
							]
						},
						{
							title: 'Прил',
							iconCls: App.ICONS.flag_gb,
							items: [
								{
									xtype: 'container',
									flex: 1,
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									items: [
										{
											labelAlign: 'top',
											height: 240,
											xtype: 'textareafield',
											fieldLabel: $_CS.Common.AdvancedInfo,
											name: MDL.SmSertificate.AreaSertificationAppendixEn.name,
											allowBlank: true
										}
									]
								}
							]
						}
					]
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'small',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'small',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function() {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function() {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function(f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});

/* Editor for ClientProfile */
Ext.define('App.smClientProfile.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'smSert',
	layout: 'fit',
	frame: true,
	width: 550,
	modal: true,
	constrain: true,
	title: 'Настройки клиента и первичной регистрации',

	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 80,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'multilangfields',
					fieldLabel: 'Название',
					name: MDL.Clients.Name.name,
					allowBlank: false
				},
				{
					xtype: 'multilangfields',
					fieldLabel: App.$STR.Adress,
					name: MDL.Clients.Adress.name,
					allowBlank: false
				},
				{
					xtype: 'datefield',
					fieldLabel: 'Дата первичной регистрации',
					name: MDL.Clients.RegistrationDate.name,
					reference: MDL.Clients.RegistrationDate.name,
					allowBlank: false
				},
				{
					xtype: 'hiddenfield',
					name: MDL.Clients.Id.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSaveClientProfile'
		},
		{
			text: App.$STR.Cancel,
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').setValues(this.record);
			this.record = null;
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});

/* Editor for ChangeAuditor */
Ext.define('App.changeAuditor.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'smSert',
	layout: 'fit',
	frame: true,
	// height: 210,
	width: 550,
	modal: true,
	constrain: true,
	title: 'Изменить аудитора',

	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaults: {
				anchor: '100%',
				labelWidth: 80,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'combobox',
					fieldLabel: $_CS.Common.Auditor,
					name: MDL.SmSertificate.AuditorId.name,
					reference: MDL.SmSertificate.AuditorId.name,
					editable: false,
					mode: 'remote',
					allowBlank: false,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.organization.view.LocationModel', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadOpsAuditors"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					xtype: 'hiddenfield',
					name: MDL.SmSertificate.Id.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSaveAuditor'
		},
		{
			text: App.$STR.Cancel,
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').setValues(this.record);
			this.record = null;
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});