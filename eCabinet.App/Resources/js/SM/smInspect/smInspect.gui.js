﻿Ext.define('App.smInspection.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smInspection',
	xtype: 'widget.smInspectiongrid',
	bodyBorder: false,

	//#region [GRID MODEL]
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		groupField: MDL.SmInspection.SertId.name,
		model: App.defineModel('SmInspectModel', App.MDL.SmInspection),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadSmInspections"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	features: [{
		ftype: 'grouping',
		groupHeaderTpl: '{[values.rows[0].get("SertNumber")]}, КСС {[values.rows[0].get("BlankNumber")]}, {[values.rows[0].get("StartDate")]} - {[values.rows[0].get("FinishDate")]} [{[values.rows[0].get("Declarant")]}] ({rows.length})',
		hideGroupedHeader: true,
		startCollapsed: true,
		id: 'idSertGrouping'
	}],
	//#endregion [GRID MODEL]
	//#region [GRID COLUMS]
	columns: [
		{ text: App.$STR.Status, dataIndex: MDL.SmInspection.StatusDisplay.name, width: 130 },
		{ text: App.$STR.Date, xtype: 'datecolumn', dataIndex: MDL.SmInspection.DateInspection.name, width: 120, format: 'd.m.Y' },
		{ text: $_CS.Common.Auditor, dataIndex: MDL.SmInspection.Auditor.name, width: 200 },
		{ text: App.$STR.Description, dataIndex: MDL.SmInspection.Result.name, flex: 1 }
	],
	//#endregion
	//#region [GRID BUTTONS]
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					labelWidth: 70,
					xtype: 'numberfield',
					fieldLabel: App.$STR.Find,
					reference: 'SearchSM',
					emptyText: '№ сертификата/бланка',
					padding: '0 0 0 0',
					regex: new RegExp('\\d*'),
					regexText: 'Неудалось распознать номер сертификата\\бланка. Вводите значение без КСС и KZ.0000000.00.00',
					allowBlank: true
				},
				{
					xtype: 'button',
					margin: '0 8 0 -8',
					iconCls: App.ICONS.search,
					handler: 'onSearchSM'
				},
				{
					xtype: 'button',
					iconCls: Ext.icon.pageadd,
					text: $_CS.SmInspect.Inspection,
					reference: 'btnInspest',
					disabled: true,
					handler: 'onInspection'
				},
				{
					xtype: 'button',
					iconCls: Ext.icon.excel,
					text: App.$STR.Excel,
					reference: 'btnPrintGrid',
					handler: 'onGridPrint'
				}],
		}
	],
	//#endregion [BUTTONS]
	//#region [GRID EVENTS]
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	listeners: {
		afterrender: function () {
			var view = this.view;
			this.store.reload({
				callback: function () {
					view.refresh();
				}
			});
		},
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
	//#endregion [EVENTS]
});

//#region [INSPECTION Editor]
Ext.define('App.smInspection.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'smInspection',
	layout: 'fit',
	frame: true,
	height: 360,
	width: 550,
	modal: true,
	constrain: true,
	title: $_CS.SmInspect.Inspection,

	//#region [FORM GUI]
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 145,
				msgTarget: 'side',
			},
			items: [
				{
					fieldLabel: $_CS.Common.Declarant,
					name: MDL.SmInspection.Declarant.name,
					editable: false,
					allowBlank: false
				},
				{
					xtype: 'datefield',
					fieldLabel: $_CS.SmInspect.DateInspection,
					name: MDL.SmInspection.DateInspection.name,
					reference: MDL.SmInspection.DateInspection.name,
					editable: false,
					allowBlank: false
				},
				{
					xtype: 'combobox',
					fieldLabel: App.$STR.Status,
					name: MDL.SmInspection.Status.name,
					reference: MDL.SmInspection.Status.name,
					editable: false,
					mode: 'local',
					allowBlank: false,
					forceSelection: true,
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					height: 170,
					xtype: 'textareafield',
					fieldLabel: $_CS.SmInspect.Result,
					name: MDL.SmInspection.Result.name,
					reference: MDL.SmInspection.Result.name,
					allowBlank: false,
					labelAlign: 'top'
				},
				{
					xtype: 'hiddenfield',
					name: MDL.SmInspection.Id.name,
					allowBlank: false
				},
				{
					xtype: 'hiddenfield',
					name: MDL.SmInspection.SertId.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	//#endregion [FORM GUI]
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});
//#endregion [INSPECTION Editor]