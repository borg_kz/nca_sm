﻿Ext.define('App.smInspection.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.smInspection',

	onSelectionChange: function (g, rs) {
		this.view.lookupReference('btnInspest').setDisabled(rs.length < 1);
	},

	onGridPrint: function () {
		this.view.file.save({title: $_CS.SmInspect.Inspections});
//		this.view.file.save();
	},

	onInspection: function (r) {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;

		var store = this.view.store;
		var dlg = Ext.create('App.smInspection.view.Editor', {
			record: rs[0],
			isNew: true,
			scope: store,
			callback: store.reload
		});

		var storedata = [];

		// 2 - Действителен, 3 - Анулирован, 4 - Приостановлен, 5 - Возобновлён
		var status = rs[0].get(MDL.SmInspection.Status.name);
		if (status == 2 || status == 5 || status == 6) {
			storedata.push({ 'Id': 2, 'Name': $_CS.Common.Extend });
			storedata.push({ 'Id': 3, 'Name': $_CS.Common.UndoAction });
			storedata.push({ 'Id': 4, 'Name': $_CS.Common.Pause });
		}
		if (status == 3)
			storedata.push({ 'Id': 5, 'Name': $_CS.Common.Recommence });
		if (status == 4)
			storedata.push({ 'Id': 3, 'Name': $_CS.Common.UndoAction }, { 'Id': 5, 'Name': $_CS.Common.Recommence });

		dlg.show();

		var states = Ext.create('Ext.data.Store', {
			fields: ['Id', 'Name'],
			data: storedata
//						data: [
//							{ 'Id': 3, 'Name': 'Анулирован' },
//							{ 'Id': 4, 'Name': 'Приостановлен' },
//							{ 'Id': 5, 'Name': 'Возобнавлён' }
//						]
		});

		dlg.lookupReference(MDL.SmInspection.Status.name).setStore(states);
		dlg.lookupReference(MDL.SmInspection.Result.name).setValue('');
		dlg.lookupReference(MDL.SmInspection.DateInspection.name).setValue(new Date());
		// dlg.lookupReference(MDL.SmInspection.DateInspection.name).setMinValue(new Date().addDays(-5));
	},

	onSearchSM: function () {
		this.view.store.proxy.extraParams = { searchId: this.view.lookupReference('SearchSM').getValue() }
		this.view.store.load();
	},

	onSave: function () {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveInspection', prms, fn, App.$STR.Saving, null, 200);
	},

	onCancel: function () {
		this.view.close();
	},

	onDublicate: function () {
		var dlg = Ext.create('App.smDublicateSert.view.Editor', {
			record: r,
			isNew: isNew,
			scope: store,
			callback: store.reload
		});

		dlg.show();
	},

	onValidate: function (f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},
});

Ext.define('App.smInspection.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'smInspection',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.smInspection.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.smInspection.view.Panel')
		]
	});
});
