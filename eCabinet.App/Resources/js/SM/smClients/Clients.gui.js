﻿Ext.define('App.clients.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'clients',
	xtype: 'widget.clientsgrid',
	alias: 'widget.clientsgrid',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		remoteSort: true,
		model: App.defineModel('OpsStoreModel', App.MDL.Clients),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadClients"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.SystemRegDate, dataIndex: MDL.Clients.RegistrationDate.name, xtype: 'datecolumn', format: 'd.m.Y', width: 110 },
		{ text: App.$STR.Name, dataIndex: MDL.Clients.Name.name, flex: 2, xtype: 'mcolumn' },
		{ text: $_CS.Common.Location, dataIndex: MDL.Clients.LocationDisplay.name, flex: 2 },
		{ text: App.$STR.Adress, dataIndex: MDL.Clients.Adress.name, flex: 3, xtype: 'mcolumn' },
		{ text: App.$STR.Email, dataIndex: MDL.Clients.Email.name, flex: 1 },
		{ text: $_CS.Common.Phones, dataIndex: MDL.Clients.Phone.name, flex: 1 },
		{ text: $_CS.Common.Boss, dataIndex: MDL.Clients.FioBoss.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
	{
		xtype: 'toolbar',
		dock: 'top',
		defaults: {
			xtype: 'button'
		},
		items: [
			{
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Add,
				handler: 'onAdd'
			},{
				iconCls: Ext.icon.pageedit,
				text: App.$STR.Edit,
				reference: 'btnEdit',
				disabled: true,
				handler: 'onEdit'
			},{
				iconCls: Ext.icon.pagedelete,
				text: App.$STR.Delete,
				reference: 'btnDelete',
				disabled: true,
				handler: 'onDelete'
			},{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Excel,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		],
	}],
	listeners: {
		afterrender: function () {
			var view = this.view;
			this.store.reload({
				callback: function () {
					view.refresh();
				}
			});
		},
		rowdblclick: 'onEdit',
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

/* Editor for Clients */
Ext.define('App.clients.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'clients',
	layout: 'fit',
	frame: true,
	title: $_CS.SmClients.NewClient,
	width: 550,
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 120,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					style: 'margin-bottom:5px;',
					anchorHorizontal: '100%',
					items: [
						{
							xtype: 'textfield',
							labelWidth: 120,
							fieldLabel: $_CS.SmClients.NumberBin,
							name: MDL.Clients.Bin.name,
							reference: MDL.Clients.Bin.name,
							maxLength: 12,
							minLength: 12,
							regex: new RegExp('\\d{12}'),
							regexText: $_CS.SmClients.BinErrorMessage,
							flex: 1,
							allowBlank: false
						},
						{
							style: 'margin-left:5px;',
							xtype: 'button',
							text: App.$STR.Check,
							width: 100,
							disabled: true,
							reference: 'btnBin',
							handler: 'checkBin'
						}
					]
				},
				{
					xtype: 'multilangfields',
					fieldLabel: App.$STR.Name,
					name: MDL.Clients.Name.name,
					reference: MDL.Clients.Name.name,
					isDisable: true,
					disabled: true,
					editable: false,
					allowBlank: false
				},
				{
					xtype: 'combobox',
					fieldLabel: $_CS.Common.Location,
					name: MDL.Clients.Location.name,
					editable: false,
					queryMode: 'local',
					isDisable: true,
					disabled: true,
					allowBlank: false,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.SmClients', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadAllRegions"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					fieldLabel: App.$STR.Email,
					name: MDL.Clients.Email.name,
					allowBlank: true,
					isDisable: true,
					disabled: true,
					vtype: 'email'
				},
				{
					fieldLabel: $_CS.Common.Phones,
					name: MDL.Clients.Phone.name,
					isDisable: true,
					disabled: true,
					allowBlank: true
				},
				{
					xtype: 'multilangfields',
					fieldLabel: App.$STR.Adress,
					name: MDL.Clients.Adress.name,
					isDisable: true,
					disabled: true,
					allowBlank: false
				},
				{
					fieldLabel: $_CS.Common.Boss,
					name: MDL.Clients.FioBoss.name,
					isDisable: true,
					disabled: true,
					allowBlank: false
				},
				{
					xtype: 'hiddenfield',
					name: MDL.Clients.Id.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			},
			setDisable: function(v) {
				this.getForm().getFields().each(function(f) {
					if (f.isDisable === true)
						f.setDisabled(v);
				});
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
//			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	initComponent: function() {
		Ext.apply(this, {
			title: !this.isEdit ? $_CS.SmClients.NewClient : $_CS.SmClients.ClientProfile
		});
		this.callParent();
	},
	listeners: {
		afterrender: function() {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function() {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function(f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});
