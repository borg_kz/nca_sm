﻿var clientChecked = false;

Ext.define('App.clients.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.clients',

	onSelectionChange: function (g, rs) {
		this.record = rs[0];
		this.view.lookupReference('btnEdit').setDisabled(rs.length < 1);
		this.view.lookupReference('btnDelete').setDisabled(rs.length < 1);
	},

	onAdd: function () {
		this.showEditor();
	},

	checkBin: function () {
		var formPanel = this.lookupReference('form');
		var btnsBin = this.lookupReference('btnBin');
		var fieldBin = this.lookupReference(MDL.Clients.Bin.name);

		if (clientChecked) {
			Ext.Msg.show({
				title: $_CS.SmClients.NewClient,
				message: $_CS.SmClients.AddNewClient,
				buttons: Ext.Msg.YESNO,
				icon: Ext.Msg.QUESTION,
				fn: function (btn) {
					if (btn === 'yes') {
						btnsBin.setText(App.$STR.Check);
						formPanel.getForm().getFields().each(function (f) {
							f.setValue('');
						});
						formPanel.setDisable(true);
						fieldBin.setReadOnly(false);
					} else if (btn === 'no') {
						return;
					}
				}
			});

			return;
		}

		var fn = function (d) {
			if (d != null)
				formPanel.getForm().setValues(d);

			clientChecked = true;
			btnsBin.setText(App.$STR.New);
			fieldBin.setReadOnly(true);
			formPanel.setDisable(false);
		}

		var bin = fieldBin.getValue();
		this.ajax.post('BtnCheckBinClick', { bin: bin }, fn, $_CS.SmClients.CheckBin, null, 200);
	},

	onEdit: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		this.showEditor(rs[0]);
	},

	onGridPrint: function () {
		this.view.file.save({ title: $_CS.Common.Clients });
		//		this.view.file.save();
	},

	showEditor: function (r) {
		var store = this.view.store;
		var isNew = r == undefined;
		r = r || this.view.store.createModel({
			Id: 0,
			Name: String.empty,
			NameKz: String.empty,
			NameRu: String.empty,
			Bin: String.empty,
			Email: String.empty,
			FioBoss: String.empty,
			Adress: String.empty,
			Location: 0,
			LocationDisplay: String.empty,
			SystemRegistrationDate: null
		});
		var dlg = Ext.create('App.clients.view.Editor', {
			record: r,
			isNew: isNew,
			scope: store,
			callback: store.reload
		});
		clientChecked = false;

		if (!isNew) {
			dlg.lookupReference(MDL.Clients.Bin.name).setDisabled(true);
			dlg.lookupReference('btnBin').setDisabled(true);
		}
		dlg.lookupReference('form').setDisable(isNew);
		dlg.title = (isNew ? "Новый клиент" : "Редактировнаие");
		dlg.show();
	},

	onSave: function () {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveClient', prms, fn, App.$STR.Saving, null, 200);
	},

	onCancel: function () {
		this.view.close();
	},

	onDelete: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		var store = this.view.store;
		this.confirm($_CS.SmClients.ConfirmDeleteClient, null, function () {
			var prms = { clientId: rs[0].get('Id') },
				fn = function (d, res) {
					this.toast.info(res.message);
					store.reload();
				};
			this.ajax.post('DeleteClient', prms, fn, App.$STR.Deleting, null, 200);
		}, null, this);
	},

	onValidate: function (f, v) {
		if (this.view.isNew)
			this.lookupReference('btnBin').setDisabled(this.lookupReference(MDL.Clients.Bin.name).getValue().length != 12);
		this.lookupReference('btnSave').setDisabled(!v);
	},
});

Ext.define('App.clients.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'clients',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.clients.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.clients.view.Panel')
		]
	});
});