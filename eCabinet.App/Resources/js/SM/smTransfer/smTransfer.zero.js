﻿var clientChecked = false;

Ext.define('App.clients.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.transfer',

	onTransfer: function() {
		var store = this.view.store;
		var dlg = Ext.create('App.transfer.view.Editor',
			{
				scope: store,
				callback: store.reload
			});
		dlg.show();
	},

	onChangeOpsForBlank: function () {
		var th = this;
		var dlg = this.view,
			prms = {
				opsUnitId: dlg.lookupReference('OpsField').getValue(),
				blankNumber: dlg.lookupReference(MDL.SmTransfer.BlankNumber.name).getValue(),
				date: dlg.lookupReference(MDL.SmTransfer.Date.name).getValue(),
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};

		Ext.Msg.show({
			title: 'Перевести сертификат',
			message: 'Внимание! Переведённый сертификат больше не будет доступен вашему ОПС!',
			buttons: Ext.Msg.YESNO,
			icon: Ext.Msg.QUESTION,
			fn: function (btn) {
				if (btn === 'yes') {
					th.ajax.post('ChangeOpsForBlank', prms, fn, App.$STR.Saving, null, 200);
				}
			}
		});
	},

	onGridPrint: function () {
		this.view.file.save({ title: $_CS.Common.Clients });
		//		this.view.file.save();
	},

	onCancel: function () {
		this.view.close();
	},

	onValidate: function (f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},
});

Ext.define('App.transfers.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'transfer',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.transfers.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.transfers.view.Panel')
		]
	});
});