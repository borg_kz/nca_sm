﻿Ext.define('App.transfers.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'transfer',
	xtype: 'widget.clientsgrid',
	alias: 'widget.clientsgrid',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		remoteSort: true,
		model: App.defineModel('OpsStoreModel', App.MDL.Clients),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadTransfers"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Date, dataIndex: MDL.SmTransfer.Date.name, xtype: 'datecolumn', format: 'd.m.Y', width: 110 },
		{ text:  $_CS.Common.OPS, dataIndex: MDL.SmTransfer.Recipient.name, flex: 2, xtype: 'mcolumn' },
		{ text: $_CS.SmSertificate.SmkCertificate, dataIndex: MDL.SmTransfer.Sertificate.name, flex: 2 },
		{ text:  $_CS.SmSertificate.BlankNumber, dataIndex: MDL.SmTransfer.BlankNumber.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
	{
		xtype: 'toolbar',
		dock: 'top',
		defaults: {
			xtype: 'button'
		},
		items: [
			{
				iconCls: Ext.icon.pageadd,
				text: 'Перевести',
				handler: 'onTransfer'
			},{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Excel,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		],
	}],
	listeners: {
		afterrender: function () {
			var view = this.view;
			this.store.reload({
				callback: function () {
					view.refresh();
				}
			});
		},
		rowdblclick: 'onEdit',
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

/* Editor for Clients */
Ext.define('App.transfer.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'transfer',
	layout: 'fit',
	frame: true,
	width: 500,
	title: 'Трансфер сертификата СМ',
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 120,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'combobox',
					fieldLabel: 'Куда (ОПС)',
					name: 'OpsField',
					reference: 'OpsField',
					mode: 'local',
					flex: 1,
					allowBlank: false,
					minChars: 3,
					queryMode: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.ChangeOps.view.LocationModel', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadListSmOps"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},{
					xtype: 'numberfield',
					fieldLabel: $_CS.EntryManager.NumberBlank,
					reference: MDL.SmTransfer.BlankNumber.name,
					allowBlank: false,
					maxLength: 7,
					minLength: 4,
					labelWidth: 120
				}, {
					xtype: 'datefield',
					labelWidth: 120,
					width: 195,
					fieldLabel: 'Дата трансфера',
					padding: '0 0 0 10',
					value: new Date(),
					reference: MDL.SmTransfer.Date.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Execute,
			iconCls: Ext.icon.save,
			disabled: true,
			reference: 'btnSave',
			handler: 'onChangeOpsForBlank'
		},
		{
			text: App.$STR.Cancel,
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});
