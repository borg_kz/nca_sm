﻿Ext.define('App.view.entryForm.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'entryForm',
	xtype: 'widget.entryFormgrid',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: App.defineModel('SmEntryModel', App.MDL.SmEntry),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadEntryes"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Status, dataIndex: MDL.SmEntry.StatusDisplay.name, width: 105 },
		{ text: $_CS.SmEntry.DateEntry, xtype: 'datecolumn', dataIndex: MDL.SmEntry.EntryDate.name, width: 110, format: 'd.m.Y' },
		{ text: $_CS.Common.SystemRegDate, dataIndex: MDL.SmEntry.RegistrationDate.name, width: 110, xtype: 'datecolumn', format: 'd.m.Y' },
		{ text: $_CS.Common.Declarant, dataIndex: MDL.SmEntry.DeclarantDisplay.name, flex: 1 },
		{ text: $_CS.Common.AreaSertification, dataIndex: MDL.SmEntry.AreaSertification.name, flex: 2 },
		{ text: $_CS.Common.Auditor, dataIndex: MDL.SmEntry.AuditorDisplay.name, width: 150 },
		{ text: App.$STR.Autor, dataIndex: MDL.SmEntry.Autor.name, width: 150 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'combobox',
					labelWidth: 60,
					fieldLabel: App.$STR.Filter,
					reference: 'entryFilter',
					editable: false,
					queryMode: 'local',
					allowBlank: true,
					store: Ext.create('Ext.data.Store', {
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						],
						data: [
							{ 'Id': 1, 'Name': App.$STR.New },
							{ 'Id': 2, 'Name': $_CS.Common.Adopted },
							{ 'Id': 3, 'Name': $_CS.Common.Denied },
							{ 'Id': 4, 'Name': $_CS.Common.Processed },
							{ 'Id': 6, 'Name': $_CS.Common.Revoked },
							{ 'Id': 7, 'Name': $_CS.Common.Erroneous },
							{ 'Id': 5, 'Name': $_CS.Common.Certificate }
						]
					}),
					listeners: {
						select: 'onEntryFilterSelected',
						delay: 1
					},
					valueField: 'Id',
					displayField: 'Name'
				},{
					xtype: 'button',
					iconCls: Ext.icon.pageadd,
					text: App.$STR.Add,
					handler: 'onAdd'
				},{
					xtype: 'button',
					iconCls: Ext.icon.pageedit,
					text: App.$STR.Edit,
					reference: 'btnEdit',
					disabled: true,
					handler: 'onEdit'
				},{
					xtype: 'button',
					iconCls: Ext.icon.pageedit,
					text: App.$STR.Delete,
					reference: 'btnDelete',
					hidden: true,
					disabled: true,
					handler: 'onBlock'
				},{
					xtype: 'button',
					iconCls: Ext.icon.pagecheck,
					text: $_CS.SmEntry.ReviewEntry,
					reference: 'btnReview',
					disabled: true,
					handler: 'onReviewEntry'
				},{
					xtype: 'button',
					iconCls: Ext.icon.pagesearch,
					text: $_CS.SmEntry.Research,
					reference: 'btnResult',
					hidden: false,
					disabled: true,
					handler: 'onResultEntry'
				},{
					xtype: 'button',
					iconCls: Ext.icon.excel,
					text: App.$STR.Excel,
					reference: 'btnPrintGrid',
					handler: 'onGridPrint'
				}],
		}
	],
	listeners: {
		rowdblclick: 'onEdit',
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

/* Editor for Add/Edit Entry */
Ext.define('App.entryForm.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'entryForm',
	layout: 'fit',
	frame: true,
	height: 490,
	width: 600,
	modal: true,
	constrain: true,
	title: $_CS.SmEntry.CreateEntry,

	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 140,
				msgTarget: 'side',
			},
			items: [
				{
					xtype: 'datefield',
					fieldLabel: $_CS.SmEntry.DateEntry,
					name: MDL.SmEntry.EntryDate.name,
					reference: MDL.SmEntry.EntryDate.name,
					editable: false,
					value: new Date(),
					maxValue: new Date(),
					minValue: new Date().addDays(-countDayEntry),
					allowBlank: false
				},
				{
					xtype: 'loadcombo',
					fieldLabel: $_CS.Common.Declarant,
					name: MDL.SmEntry.Declarant.name,
					reference: MDL.SmEntry.Declarant.name,
					queryMode: 'local',
					autoLoad: true,
					editable: true,
					allowBlank: false,
					minChars: 2,
					action: 'LoadMyClients',
				},
				{
					xtype: 'combobox',
					fieldLabel: $_CS.Common.CertificationType,
					name: MDL.SmEntry.SmTypeSert.name,
					reference: MDL.SmEntry.SmTypeSert.name,
					editable: false,
					triggerAction: 'all',
					allowBlank: false,
					forceSelection: true,
					multiSelect: true,
					queryMode: 'local',
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.entrytypesForm.Model', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadSmSertType"),
							reader: {
								type: 'json',
								rootProperty: 'data',
								idProperty: 'Id'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					fieldLabel: $_CS.SmEntry.DeclarantFIO,
					name: MDL.SmEntry.DeclarantFio.name,
					allowBlank: false
				},
				{
					fieldLabel: $_CS.SmEntry.DeclarantPhone,
					name: MDL.SmEntry.DeclarantPhone.name,
					allowBlank: true
				},
				{
					fieldLabel: $_CS.SmEntry.DeclarantEmail,
					name: MDL.SmEntry.DeclarantEmail.name,
					vtype: 'email',
					allowBlank: true
				},
				{
					xtype: 'numberfield',
					fieldLabel: $_CS.SmEntry.CountEmployes,
					name: MDL.SmEntry.CountEmployes.name,
					allowBlank: false
				},
				{
					xtype: 'numberfield',
					fieldLabel: $_CS.SmEntry.CountYards,
					name: MDL.SmEntry.CountYards.name,
					allowBlank: false
				},
				{
					height: 125,
					xtype: 'textareafield',
					fieldLabel: $_CS.Common.AreaSertification,
					name: MDL.SmEntry.AreaSertification.name,
					allowBlank: false
				},
				{
					xtype: 'loadcombo',
					fieldLabel: $_CS.Common.Auditor,
					name: MDL.SmEntry.Auditor.name,
					queryMode: 'local',
					autoLoad: true,
					allowBlank: false,
					minChars: 2,
					action: 'LoadAuditors',
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	setReadOnly: function (v) {
		this.lookupReference('form').getForm().getFields().each(function (f) {
			f.setReadOnly(v);
		});
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});

/* Editor for Review Entry */
Ext.define('App.entryReviewForm.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'entryForm',
	layout: 'fit',
	frame: true,
	height: 400,
	width: 520,
	modal: true,
	constrain: true,
	title: $_CS.SmEntry.ReviewEntry,

	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 150,
				msgTarget: 'side'
			},
			items: [
				{
					fieldLabel: $_CS.Common.Declarant,
					name: MDL.SmEntry.DeclarantDisplay.name,
					editable: false,
					allowBlank: true
				},
				{
					xtype: 'textareafield',
					fieldLabel: $_CS.Common.AreaSertification,
					name: MDL.SmEntry.AreaSertification.name,
					height: 125,
					editable: false,
					allowBlank: false
				},
				{
					xtype: 'combobox',
					fieldLabel: App.$STR.Status,
					name: MDL.SmEntry.Status.name,
					reference: MDL.SmEntry.Status.name,
					editable: false,
					mode: 'local',
					allowBlank: false,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							],
							data: [
								{ 'Id': 2, 'Name': $_CS.Common.Adopted },
								{ 'Id': 3, 'Name': $_CS.Common.Denied },
								{ 'Id': 6, 'Name': $_CS.SmEntry.ApplicationCancellingByClient },
								{ 'Id': 7, 'Name': $_CS.SmEntry.MistakeCreated }
							]
						
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					xtype: 'textareafield',
					height: 100,
					fieldLabel: $_CS.SmEntry.ReviewDescription,
					name: MDL.SmEntry.ReviewDescription.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSaveReview'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});