﻿var iinChecked = false;
var idEntry = 0;

Ext.define('App.entryForm.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.entryForm',

	init: function() {
	},

	onSelectionChange: function(g, rs) {
		var form = this.view;
		var status = rs.length == 0 ? 0 : rs[0].get('Status');
		if (rs.length < 1 || status > 5) {
			form.lookupReference('btnEdit').setDisabled(true);
			form.lookupReference('btnReview').setDisabled(true);
			form.lookupReference('btnResult').setDisabled(true);
			return;
		}
		// Новая
		if (status == 1) {
			form.lookupReference('btnEdit').setDisabled(false);
			form.lookupReference('btnDelete').setDisabled(false);
			form.lookupReference('btnReview').setDisabled(false);
			form.lookupReference('btnResult').setDisabled(true);
		}
		// Принята
		if (status == 2) {
			form.lookupReference('btnEdit').setDisabled(false);
			form.lookupReference('btnReview').setDisabled(false);
			form.lookupReference('btnResult').setDisabled(false);
		}
		// Отказана / Отозвана / Ошибочная
		if (status == 3 || status == 6 || status == 7) {
			form.lookupReference('btnEdit').setDisabled(false);
			form.lookupReference('btnReview').setDisabled(false);
			form.lookupReference('btnResult').setDisabled(true);
		}
		// Обработана / Сертификат
		if (status == 4 || status == 5) {
			form.lookupReference('btnEdit').setDisabled(false);
			form.lookupReference('btnReview').setDisabled(false);
			form.lookupReference('btnResult').setDisabled(false);
		}
	},

	onAdd: function() {
		this.showEditor();
		this.view.store.currentPage = 1;
		this.view.store.proxy.extraParams = { entryType: 1 }
		this.view.lookupReference('entryFilter').setValue('1');
	},

	onEdit: function() {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) {
			this.toast.info($_CS.SmEntry.SelectApplication);
			return;
		}
		this.showEditor(rs[0]);
	},

	onGridPrint: function() {
		this.view.file.save({ title: $_CS.SmEntry.Applications });
	},

	showEditor: function(r) {
		var store = this.view.store;
		var isNew = r == undefined;
		r = r || this.view.store.createModel({
			Id: 0,
			Name: String.empty,
			NameKz: String.empty,
			NameRu: String.empty,
			Bin: String.empty,
			Email: String.empty,
			FioBoss: String.empty,
			Adress: String.empty,
			Location: null,
			LocationDisplay: String.empty,
			SystemRegistrationDate: null
		});

		var dlg = Ext.create('App.entryForm.view.Editor', {
			record: r,
			isNew: isNew,
			scope: store,
			callback: store.reload
		});

		dlg.lookupReference(MDL.SmEntry.EntryDate.name).setDisabled(!isNew);
		dlg.lookupReference(MDL.SmEntry.Declarant.name).setDisabled(!isNew);

		dlg.setReadOnly(r.get('Status') == 3);
		dlg.show();
	},

	onReviewEntry: function() {
		var store = this.view.store;
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		if (rs.length != 1) {
			this.toast.info($_CS.SmEntry.SelectApplication);
			return;
		}
		var dlg = Ext.create('App.entryReviewForm.view.Editor', {
			record: rs[0],
			isNew: false,
			scope: store,
			filter: this.view.lookupReference('entryFilter'),
			parent: this.view,
			callback: store.reload
		});
		if (rs[0].get(MDL.SmEntry.Status.name) == 5) {
			//dlg.lookupReference(MDL.SmEntry.Status.name)
			dlg.lookupReference(MDL.SmEntry.Status.name).setHidden(rs[0].get(MDL.SmEntry.Status.name) == 5);
		}
		dlg.show();
	},

	onResultEntry: function() {
		var store = this.view.store;
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) {
			this.toast.info($_CS.SmEntry.SelectApplication);
			return;
		}
		idEntry = rs[0].get(MDL.SmEntry.Id.name);

		var dlg = Ext.create('App.view.entryResultForm.Editor', {
			record: rs[0],
			idEntry: idEntry,
			isNew: false,
			scope: store,
			filter: this.view.lookupReference('entryFilter'),
			callback: store.reload
		});
		dlg.lookupReference(MDL.SmEntry.StartResearch.name).setMinValue(rs[0].get(MDL.SmEntry.EntryDate.name));
		dlg.lookupReference(MDL.SmEntry.FinishResearch.name).setMinValue(rs[0].get(MDL.SmEntry.EntryDate.name));
		dlg.lookupReference('entryTeamGrid').store.proxy.extraParams = { entryId: idEntry }
		dlg.lookupReference('entryTeamGrid').store.reload();
		dlg.show();
	},

	onSave: function() {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function(d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveEntry', prms, fn, App.$STR.Saving, null, 200);
	},

	onSaveReview: function() {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function(d, res) {
				var val = this.view.lookupReference(MDL.SmEntry.Status.name).getValue();
				this.view.scope.currentPage = 1;
				this.view.scope.proxy.extraParams = { entryType: val }
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.filter.setValue(val);
				this.view.parent.lookupReference('btnResult').setDisabled(val != 2);
				this.view.close();
			};
		this.ajax.post('ReviewEntry', prms, fn, App.$STR.Saving, null, 200);
	},

	onSaveResult: function() {
		var d1 = this.view.lookupReference(MDL.SmEntry.StartResearch.name);
		if (d1 != null)
			if (d1.getValue() > this.lookupReference(MDL.SmEntry.FinishResearch.name).getValue()) {
				this.toast.error($_CS.SmEntry.Err_WrongDate);
				return;
			}
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function(d, res) {
				// var val = this.view.lookupReference(MDL.SmEntry.Status.name).getValue();
				// var teamForm = this.lookupReference('teamForm');
				this.view.scope.currentPage = 1;
				this.view.scope.proxy.extraParams = { entryType: 4 }
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.filter.setValue(4);
				this.view.close();
			};
		this.ajax.post('ResultEntry', prms, fn, App.$STR.Saving, null, 200);
	},

	onCancel: function() {
		this.view.close();
	},

	onValidate: function(f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},

	onTeamValidate: function (f, v) {
		var old = true;
		if (!old && this.view.isNew) {
			var iin = this.lookupReference(MDL.Employe.Iin.name).getValue();
			this.lookupReference('btnIin').setDisabled(iin.length != 12);
		}
		this.lookupReference('btnSave').setDisabled(!v);
	},

	onTeamSelectionChange: function (g, rs) {
		var form = this.view;
		form.lookupReference('btnDelete').setDisabled(rs.length < 1);
		form.lookupReference('btnEdit').setDisabled(rs.length < 1);
	},

	checkIin: function () {
		var formPanel = this.lookupReference('teamForm');
		var butn = this.lookupReference('btnIin');
		var field = this.lookupReference(MDL.Employe.Iin.name);

		if (iinChecked) {
			Ext.Msg.show({
				title: $_CS.SmClients.NewClient,
				message: $_CS.SmClients.AddNewClient,
				buttons: Ext.Msg.YESNO,
				icon: Ext.Msg.QUESTION,
				fn: function (btn) {
					if (btn === 'yes') {
						butn.setText(App.$STR.Check);
						formPanel.getForm().getFields().each(function (f) {
							f.setValue('');
						});
						formPanel.setDisable(true);
						field.setReadOnly(false);
						iinChecked = false;
					} else if (btn === 'no') return;
				}
			});
			return;
		}

		var fn = function (d) {
			if (d != null)
				formPanel.getForm().setValues(d);
			iinChecked = true;
			butn.setText(App.$STR.New);
			field.setReadOnly(true);
			formPanel.setDisable(false);
		}

		var iin = field.getValue();
		this.ajax.post('CheckIin', { iin: iin }, fn, $_CS.emp_CheckIin, null, 200);
	},

	onAddTeam: function () {
		this.showTeamEditor();
	},

	onEditTeam: function () {
		var rs = this.lookupReference('entryTeamGrid').getSelectionModel().getSelection();
		if (rs.length != 1) return;
		this.showTeamEditor(rs[0]);
	},

	onDeleteTeam: function () {
		var entry = this.view.idEntry;
		var rs = this.lookupReference('entryTeamGrid').getSelectionModel().getSelection();
		if (rs.length != 1) return;
		this.confirm($_CS.SmEntry.ConfirmDeleteTeam, null, function () {
			var prms = { id: rs[0].get('Id'), entryId: entry },
				fn = function (d, res) {
					this.toast.info(res.message);
					this.lookupReference('entryTeamGrid').store.reload();
				};
			this.ajax.post('DeleteTeam', prms, fn, App.$STR.Deleting, null, 200);
		}, null, this);
	},

	onTeamSave: function () {
		var dlg = this.view,
		prms = {
			json: Ext.JSON.encode(dlg.getValues()),
			isNew: dlg.isNew,
			entryId: idEntry
		},
		fn = function (d, res) {
			if (Ext.isFunction(this.view.callback))
				this.view.callback.call(this.view.scope || this);
			this.toast.info(res.message);
			this.view.close();
		};
		this.ajax.post('AddTeamEntry', prms, fn, App.$STR.Saving, null, 200);
	},

	showTeamEditor: function (r) {
		var store = this.lookupReference('entryTeamGrid').store;
		var isNew = r == undefined;
		r = r || store.createModel({
			Id: 0,
			Iin: String.empty,
			Name: String.empty,
			SecondName: String.empty,
			ThirdName: String.empty,
			Position: String.empty
		});
		//entryId = this.view.entryId;
		var dlg = Ext.create('App.entryTeam.view.Editor', {
			record: r,
			isNew: true,
			scope: store,
			callback: store.reload
		});

		dlg.lookupReference(MDL.SmEntry.SmTypeSertId.name).store.proxy.extraParams = { entryId: idEntry };
		//dlg.lookupReference('entryTeamGrid').store.proxy.extraParams = { entryId: idEntry }
		if (!isNew) {
			dlg.lookupReference(MDL.Employe.Iin.name).setDisabled(true);
			dlg.lookupReference('btnIin').setDisabled(true);
		}

		dlg.lookupReference('teamForm').setDisable(isNew);
		iinChecked = false;
		dlg.show();
	},

	onEntryFilterSelected: function (r, s) {
		this.view.lookupReference('btnEdit').setDisabled(true);
		this.view.lookupReference('btnDelete').setDisabled(true);
		this.view.lookupReference('btnReview').setDisabled(true);
		this.view.lookupReference('btnResult').setDisabled(true);

		this.view.store.currentPage = 1;
		this.view.store.proxy.extraParams = { entryType: s.get('Id') }
		this.view.store.load();
	}
});

Ext.define('App.entryForm.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'entryForm',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.view.entryForm.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.entryForm.view.Panel')
		]
	});
});