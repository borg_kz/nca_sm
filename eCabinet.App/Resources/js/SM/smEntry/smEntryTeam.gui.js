﻿Ext.define('App.entryTeamForm.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	xtype: 'widget.entryTeamFormid',
	reference: 'entryTeamGrid',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		model: Ext.define('App.entryTeamForm.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.Employe.Id, type: 'int' },
				{ name: MDL.Employe.Iin, type: 'string' },
				{ name: MDL.SmEntry.TeamFIO, type: 'string' },
				{ name: MDL.Employe.FirstName, type: 'string' },
				{ name: MDL.Employe.SecondName, type: 'string' },
				{ name: MDL.Employe.ThirdName, type: 'string' },
				{ name: MDL.Employe.Status, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadTeamResearch"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.CertificationType, dataIndex: MDL.SmEntry.SmTypeSert.name, flex: 1 },
		{ text: App.$STR.FIO, dataIndex: MDL.SmEntry.TeamFIO.name, flex: 1 },
		{ text: $_CS.SmEntry.StartResearch, dataIndex: MDL.SmEntry.StartResearch.name, width: 100, xtype: 'datecolumn', format: 'd.m.Y' },
		{ text: $_CS.SmEntry.FinishResearch, dataIndex: MDL.SmEntry.FinishResearch.name, width: 100, xtype: 'datecolumn', format: 'd.m.Y' }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					iconCls: Ext.icon.pageadd,
					text: App.$STR.Add,
					handler: 'onAddTeam'
				},
//				{
//					xtype: 'button',
//					iconCls: Ext.icon.pageedit,
//					text: App.$STR.Edit,
//					reference: 'btnEdit',
//					disabled: true,
//					handler: 'onEditTeam'
//				},
				{
					xtype: 'button',
					iconCls: Ext.icon.pagedelete,
					text: App.$STR.Delete,
					reference: 'btnDelete',
					disabled: true,
					handler: 'onDeleteTeam'
				}],
		}
	],
	listeners: {
		afterrender: function () {
			var view = this.view;
			this.store.reload({
				callback: function () {
					view.refresh();
				}
			});
		},
		selectionchange: 'onTeamSelectionChange'
	}
});
var entryId = -1;
Ext.define('App.entryTeam.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'entryForm',
	layout: 'fit',
	frame: true,
	height: 250,
	width: 440,
	modal: true,
	constrain: true,
	title: $_CS.SmEntry.AddExpertToEntry,


	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'teamForm',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 100,
				msgTarget: 'side',
			},
			items: [
				{
					xtype: 'loadcombo',
					fieldLabel: $_CS.Common.Auditor,
					name: MDL.SmEntry.Auditor.name,
					reference: MDL.SmEntry.Auditor.name,
					queryMode: 'local',
					autoLoad: true,
					allowBlank: false,
					minChars: 2,
					action: 'LoadAuditors',
				}, {
					xtype: 'loadcombo',
					fieldLabel: $_CS.Common.CertificationType,
					name: MDL.SmEntry.SmTypeSertId.name,
					reference: MDL.SmEntry.SmTypeSertId.name,
					queryMode: 'local',
					autoLoad: true,
					allowBlank: false,
					minChars: 2,
					action: 'LoadSertTypes',
				}, {
					xtype: 'datefield',
					fieldLabel: $_CS.SmEntry.StartResearch,
					name: MDL.SmEntry.StartResearch.name,
					reference: MDL.SmEntry.StartResearch.name,
					value: new Date(),
					allowBlank: false
				}, {
					xtype: 'datefield',
					fieldLabel: $_CS.SmEntry.FinishResearch,
					name: MDL.SmEntry.FinishResearch.name,
					reference: MDL.SmEntry.FinishResearch.name,
					value: new Date(),
					allowBlank: false
				},

			],
			listeners: {
				validitychange: 'onTeamValidate'
			},
			setDisable: function (v) {
				this.getForm().getFields().each(function (f) {
					if (f.isDisable === true)
						f.setDisabled(v);
				});
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onTeamSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('teamForm').loadRecord(this.record);
		}
	},
	setReadOnly: function (v) {
		this.lookupReference('teamForm').getForm().getFields().each(function (f) {
			f.setReadOnly(v);
		});
	},
	getValues: function () {
		var fields = this.lookupReference('teamForm').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});

Ext.define('App.entryResultForm.view.TabReports', {
	extend: 'Ext.panel.Panel',
	xtype: 'advanced-tabs',
	height: '100%',
	layout: 'fit',
	viewModel: true,
	padding: 5,

	initComponent: function () {
		Ext.apply(this, {
			width: '100%',
			closeAction: 'destroy',
			items: [
			{
				xtype: 'tabpanel',
				reference: 'tabpanel',
				border: false,
				tabPosition: 'left',
				plain: true,
				tabRotation: 0,

				defaults: {
					bodyPadding: 5,
					autoScroll: true,
					border: true
				},
				items: [
				{
					title: $_CS.SmEntry.Research,
					layout: 'fit',
					items: [
						{
							xtype: 'form',
							layout: 'anchor',
							reference: 'form',
							defaultType: 'textfield',
							defaults: {
								anchor: '100%',
								labelWidth: 100,
								msgTarget: 'side'
							},
							items: [
								{
									fieldLabel: $_CS.Common.Declarant,
									name: MDL.SmEntry.DeclarantDisplay.name,
									editable: false,
									allowBlank: true
								},
								{
									fieldLabel: $_CS.Common.AreaSertification,
									xtype: 'textareafield',
									height: 100,
									name: MDL.SmEntry.AreaSertification.name,
									editable: false,
									allowBlank: true
								},
								{
									xtype: 'datefield',
									fieldLabel: $_CS.SmEntry.StartResearch,
									name: MDL.SmEntry.StartResearch.name,
									reference: MDL.SmEntry.StartResearch.name,
									value: new Date(),
									allowBlank: false
								},
								{
									xtype: 'datefield',
									fieldLabel: $_CS.SmEntry.FinishResearch,
									name: MDL.SmEntry.FinishResearch.name,
									reference: MDL.SmEntry.FinishResearch.name,
									value: new Date(),
									allowBlank: false
								},
								{
									xtype: 'textareafield',
									height: 120,
									fieldLabel: $_CS.SmEntry.ResultResearch,
									name: MDL.SmEntry.ResultResearch.name,
									allowBlank: false
								}
							],
							listeners: {
								validitychange: 'onValidate'
							}
						}
					]
				}, {
					title: $_CS.SmEntry.TeamHeader,
					xtype: 'form',
					layout: 'fit',
					items: [new App.entryTeamForm.view.GridPanel({})]
				}]
			}]
		});

		this.callParent();
	}
});

/* Editor for Result Entry */
Ext.define('App.view.entryResultForm.Editor', {
	extend: 'Ext.window.Window',
	controller: 'entryForm',
	layout: 'fit',
	frame: true,
	closeAction: 'hide',
	height: 430,
	width: 650,
	modal: true,
	constrain: true,
	title: $_CS.SmEntry.Research,
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	},

	initComponent: function() {
		var me = this;


		Ext.apply(me, {
			items: [
				new App.entryResultForm.view.TabReports({})
			],
			buttons: [
				{
					text: App.$STR.Save,
					disabled: true,
					scale: 'medium',
					iconCls: Ext.icon.save,
					reference: 'btnSave',
					handler: 'onSaveResult'
				},
				{
					text: App.$STR.Cancel,
					scale: 'medium',
					iconCls: Ext.icon.cancel,
					handler: 'onCancel'
				}
			]
		});

		this.callParent(arguments);
		me.lookupReference('form').loadRecord(me.record);
	}
});