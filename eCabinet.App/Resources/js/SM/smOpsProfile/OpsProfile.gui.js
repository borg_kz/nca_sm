﻿Ext.define('App.view.orgProfile.Panel', {
	extend: 'Ext.form.Panel',
	controller: 'orgProfile',
	bodyBorder: true,
	layout: 'anchor',
	bodyPadding: 15,
	collapsible: false,
	jsonSubmit: true,

	defaults: {
		anchor: '100%',
		labelWidth: 180,
		msgTarget: 'side'
	},

	initComponent: function () {
		var me = this;
		Ext.apply(this, {
			defaultType: 'textfield',
			url: window.buildurl("LoadOpsInfo"),
			reader:Ext.create('Ext.data.reader.Json', {
				model: App.defineModel('orgProfileModel', App.MDL.OpsUnit),
				record: 'data',
				successProperty: 'success'
			}),
			dockedItems: {
				xtype: 'toolbar',
				items: [
					{
						xtype: 'button',
						iconCls: Ext.icon.disk,
						text: App.$STR.Save,
						reference: 'btnSave',
						disabled: true,
						handler: 'onSave'
					}
				]
			},
			items: [
				{
					xtype: 'datefield',
					fieldLabel: $_CS.SmOpsProfile.DateRegistration,
					editable: false,
					readOnly: true,
					name: MDL.OpsUnit.SystemRegistrationDate.name
				},
				{
					xtype: 'multilangfields',
					fieldLabel: App.$STR.Name,
					allowBlank: false,
					name: MDL.OpsUnit.Name.name
				},
				{
					fieldLabel: $_CS.Common.Bin,
					name: MDL.OpsUnit.Bin.name,
					maxLength: 12,
					minLength: 12,
					allowBlank: false,
				},
				{
					xtype: 'combobox',
					fieldLabel: $_CS.Common.Location,
					name: MDL.OpsUnit.Location.name,
					editable: false,
					mode: 'local',
					allowBlank: false,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.organization.view.LocationModel', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadLocalCityes"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					xtype: 'multilangfields',
					fieldLabel: App.$STR.Adress,
					name: MDL.OpsUnit.Adress.name,
					allowBlank: false,
				},
				{
					fieldLabel: $_CS.OpsStore.PostCode,
					name: MDL.OpsUnit.PostCode.name,
					allowBlank: true,
				},
				{
					fieldLabel: $_CS.SmOpsProfile.RegNumber,
					maxLength: 9,
					minLength: 9,
					value: 'O.00.0000',
					regex: new RegExp('\\D\\.\\d{2}\\.\\d{4}'),
					regexText: $_CS.SmOpsProfile.FormatOfRegNumber,
					name: MDL.OpsUnit.AttestateNumber.name,
					readOnly: true,
					allowBlank: false,
				},
				{
					fieldLabel: $_CS.SmOpsProfile.SertSM,
					maxLength: 16,
					minLength: 16,
					value: 'KZ.0000000.00.00',
					readOnly: true,
					regex: new RegExp('KZ\\.\\d{7}\\.\\d{2}\\.\\d{2}'),
					regexText: $_CS.SmOpsProfile.FormatOfCertNumber,
					name: MDL.OpsUnit.SmSertNumber.name,
					allowBlank: false,
				},
				{
					xtype: 'numberfield',
					fieldLabel: $_CS.SmOpsProfile.LastSertNumber,
					maxLength: 7,
					name: MDL.OpsUnit.LastSertNumber.name,
					allowBlank: false,
				},
				{
					fieldLabel: $_CS.Common.Boss,
					allowBlank: false,
					name: MDL.OpsUnit.FioBoss.name,
				},
				{
					fieldLabel: $_CS.SmOpsProfile.Sing,
					name: MDL.OpsUnit.Singer.name,
				},
				{
					fieldLabel: $_CS.Common.Phones,
					name: MDL.OpsUnit.Phone.name,
				},
				{
					fieldLabel: App.$STR.Email,
					name: MDL.OpsUnit.Email.name,
					allowBlank: false
				},
				{
					fieldLabel: $_CS.Common.Site,
					name: MDL.OpsUnit.Site.name,
					allowBlank: true
				},
				{
					xtype: 'hiddenfield',
					name: MDL.OpsUnit.Id.name,
					allowBlank: false
				}
			],
			listeners: {
				scope: me.controller,
				validitychange: 'onValidate'
			},
			getValues: function () {
//				return this.getForm().getValues();
				var fields = this.getForm().getFields();
				var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
				fields.items.each(function(f) {
					if (f.name)
						res[f.name] = f.getValue();
				});
				return res;
			}
		});
		this.callParent();
		me.getForm().load({
			waitMsg: App.$STR.Loading
		});
	}
});

