﻿var iinChecked = false;

Ext.define('App.employes.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.employes',

	onSelectionChange: function (g, rs) {
		this.record = rs[0];
		this.view.lookupReference('btnEdit').setDisabled(rs.length < 1);
		this.view.lookupReference('btnBlock').setDisabled(rs.length < 1);
	},

	onGridPrint: function () {
		this.view.file.save({ title: $_CS.Common.Employees });
		//		this.view.file.save();
	},
	
	onEdit: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		this.showEditor(rs[0]);
	},

	checkIin: function () {
		var formPanel = this.lookupReference('form');
		var butn = this.lookupReference('btnIin');
		var field = this.lookupReference(MDL.Employe.Iin.name);

		if (iinChecked) {
			Ext.Msg.show({
				title: $_CS.SmEmployes.NewClient,
				message: $_CS.SmEmployes.AddNewClient,
				buttons: Ext.Msg.YESNO,
				icon: Ext.Msg.QUESTION,
				fn: function (btn) {
					if (btn === 'yes') {
						butn.setText(App.$STR.Check);
						formPanel.getForm().getFields().each(function (f) {
							f.setValue('');
						});
						formPanel.setDisable(true);
						field.setReadOnly(false);
						iinChecked = false;
					} else if (btn === 'no') {
						return;
					}
				}
			});

			return;
		}

		var iin = field.getValue();
		var fn = function (d) {
			if (d == null) return;

			formPanel.getForm().findField(MDL.Employe.Login.name).setValue(iin);
			formPanel.getForm().setValues(d);
			iinChecked = true;
			butn.setText(App.$STR.New);
			field.setReadOnly(true);
			formPanel.setDisable(false);
		}
		var ffn = function (d, response) {
			if (d != null)
				formPanel.getForm().setValues(d);
			// return true;
		}

		this.ajax.post('CheckIin', { iin: iin }, fn, App.$STR.IinValidating, null, 99999999, ffn);
	},

	onAdd: function () {
		this.showEditor();
	},

	showEditor: function (r) {
		var store = this.view.store;
		var isNew = r == undefined;
		r = r || this.view.store.createModel({
			Id: 0,
			Name: String.empty,
			NameKz: String.empty,
			NameRu: String.empty,
			Email: String.empty,
			Login: String.empty,
			Password: String.empty,
			Location: 0,
			LocationDisplay: String.empty,
			SystemRegistrationDate: null,
			RegistryRegistrationDate: null
		});
		var dlg = Ext.create('App.employes.view.Editor', {
			record: r,
			isNew: isNew,
			scope: store,
			callback: store.reload
		});

		if (!isNew) {
			dlg.lookupReference(MDL.Employe.Iin.name).setDisabled(true);
			dlg.lookupReference('btnIin').setDisabled(true);
		}

		dlg.lookupReference('form').setDisable(isNew);
		iinChecked = false;
		dlg.show();
	},

	onValidate: function (f, v) {
		if (this.view.isNew) {
			var iin = this.lookupReference(MDL.Employe.Iin.name).getValue();
			this.lookupReference('btnIin').setDisabled(iin.length != 12);
		}
		this.lookupReference('btnSave').setDisabled(!v);
	},

	onCancel: function () {
		this.view.close();
	},

	onSave: function () {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveEmploye', prms, fn, $_CS.SmEmployes.SaveMsg, null, 200);
	},

	onDelete: function() {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		var store = this.view.store;
		this.confirm($_CS.SmClients.ConfirmDeleteClient, null, function () {
			var prms = { id: rs[0].get('Id') },
				fn = function (d, res) {
					this.toast.info(res.message);
					store.reload();
				};
			this.ajax.post('DeleteEmploye', prms, fn, App.$STR.Deleting, null, 200);
		}, null, this);
	},
});

Ext.define('App.employes.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'employes',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,

	initComponent: function () {
		Ext.apply(this, {
			items: [
				{
					xtype: 'employesgrid',
					region: 'center'
				},
				//{
				//	title: $_CS.emp_statistic,
				//	collapsible: true,
				//	xtype: 'area-basic',
				//	region: 'south'
				//}
			]
		});
		this.callParent();
	}
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.employes.view.Panel')
		]
	});
});