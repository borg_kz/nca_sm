﻿Ext.define('App.view.employes.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'employes',
	xtype: 'widget.employesgrid',
	alias: 'widget.employesgrid',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		model: App.defineModel('EmployesModel', App.MDL.Employe),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadEmployes"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Status, dataIndex: MDL.Employe.Status.name, width: 90 },
		{ text: App.$STR.Role, dataIndex: MDL.Employe.RoleDisplay.name, flex: 1 },
		{ text: $_CS.Common.Employe, dataIndex: MDL.Employe.FullName.name, flex: 2 },
		{ text: App.$STR.Login, dataIndex: MDL.Employe.Iin.name, flex: 1 },
		{ text: App.$STR.Email, dataIndex: MDL.Employe.Email.name, flex: 1 },
		{ text: $_CS.Common.Phones, dataIndex: MDL.Employe.HomeTel.name, flex: 1 },
		{ text: $_CS.SmEmployes.MobTel, dataIndex: MDL.Employe.MobTel.name, flex: 1 },
		{ text: $_CS.SmEmployes.LastActivity, xtype: 'datecolumn', format: 'd.m.Y', dataIndex: MDL.Employe.LastActivity.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
	{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'button',
				iconCls: Ext.icon.pageadd,
				text: App.$STR.Add,
				handler: 'onAdd'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.peopleprofile,
				text: App.$STR.Profile,
				reference: 'btnEdit',
				disabled: true,
				handler: 'onEdit'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.pagedelete,
				text: App.$STR.Delete,
				reference: 'btnBlock',
				disabled: true,
				handler: 'onDelete'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Excel,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		],
	}],
	listeners: {
		afterrender: function () {
			var view = this.view;
			this.store.reload({
				callback: function () {
					view.refresh();
				}
			});
		},
		rowdblclick: 'onEdit',
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

/* Editors for Employe */
Ext.define('App.employes.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'employes',
	layout: 'fit',
	frame: true,
	height: 420,
	width: 500,
	modal: true,
	constrain: true,
	title: $_CS.SmEmployes.ProfileEspert,

	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 130,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					style: 'margin-bottom:5px;',
					anchorHorizontal: '100%',
					items: [
					{
						xtype: 'textfield',
						labelWidth: 130,
						fieldLabel: App.$STR.Iin,
						name: MDL.Employe.Iin.name,
						reference: MDL.Employe.Iin.name,
						regex: new RegExp('\\d{12}'),
						regexText: $_CS.SmEmployes.IinShouldContainOnlyDigits,
						maxLength: 12,
						minLength: 12,
						msgTarget: 'side',
						flex: 1,
						allowBlank: false
					},
					{
						xtype: 'button',
						text: App.$STR.Check,
						reference: 'btnIin',
						disabled: true,
						width: 100,
						handler: 'checkIin'
					}]
				},
				{
					xtype: 'combobox',
					fieldLabel: App.$STR.Role,
					name: MDL.Employe.Role.name,
					editable: false,
					mode: 'local',
					allowBlank: false,
					isDisable: true,
					disabled: true,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.Employes.view.TypeSmRoles', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadSmRoles"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					fieldLabel: App.$STR.SecondName,
					name: MDL.Employe.SecondName.name,
					isDisable: true,
					disabled: true,
					allowBlank: false
				},
				{
					fieldLabel: App.$STR.FirstName,
					name: MDL.Employe.FirstName.name,
					isDisable: true,
					disabled: true,
					allowBlank: false
				},
				{
					fieldLabel: App.$STR.ThirdName,
					name: MDL.Employe.ThirdName.name,
					isDisable: true,
					disabled: true,
					allowBlank: true
				},
				{
					xtype: 'combobox',
					fieldLabel: App.$STR.Sex,
					name: MDL.Employe.Gender.name,
					editable: false,
					mode: 'local',
					isDisable: true,
					disabled: true,
					allowBlank: false,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						],
						data: [
							{ 'Id': 1, 'Name': App.$STR.Male },
							{ 'Id': 2, 'Name': App.$STR.Female }
						]

					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					fieldLabel: App.$STR.Email,
					name: MDL.Employe.Email.name,
					allowBlank: false,
					isDisable: true,
					disabled: true,
					vtype: 'email'
				},
				{
					fieldLabel: $_CS.Common.Phones,
					name: MDL.Employe.HomeTel.name,
					isDisable: true,
					disabled: true,
					allowBlank: true,
				},
				{
					fieldLabel: $_CS.SmEmployes.MobTel,
					name: MDL.Employe.MobTel.name,
					isDisable: true,
					disabled: true,
					allowBlank: true,
				},
				{
					fieldLabel: App.$STR.Login,
					name: MDL.Employe.Login.name,
					isDisable: true,
					disabled: true,
					editable: false,
					allowBlank: false
				},
				{
					fieldLabel: App.$STR.Password,
					name: MDL.Employe.Password.name,
					isDisable: true,
					disabled: true,
					minLength: 5,
					inputType: 'password',
					allowBlank: false
				},
				{
					xtype: 'hiddenfield',
					name: MDL.Employe.Id.name,
					allowBlank: true
				}
			],
			listeners: {
				validitychange: 'onValidate'
			},
			setDisable: function(v) {
				this.getForm().getFields().each(function(f) {
					if (f.isDisable === true)
						f.setDisabled(v);
				});
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});
