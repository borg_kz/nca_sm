﻿// #region IAF MD

Ext.define('App.ncaReportIafMD.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smReportSerts.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.SmSertificate.StatusDisplay.name, type: 'string' },
				{ name: MDL.SmSertificate.SertifikateSmNumber.name, type: 'string' },
				{ name: MDL.SmSertificate.BlankNumber.name, type: 'string' },
				{ name: MDL.SmSertificate.Declarant.name, type: 'string' },
				{ name: MDL.SmSertificate.StartDate.name, type: 'date' },
				{ name: MDL.SmSertificate.FinishDate.name, type: 'date' },
				{ name: MDL.SmSertificate.AreaSertification.name, type: 'string' },
				{ name: MDL.SmSertificate.Auditor.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadIafMD"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.Indicator, dataIndex: 'Index', flex: 1 },
		{ text: $_CS.Common.Value, dataIndex: 'Value' },
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.Reports.ReportsByForm_IafMd
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'OpsField',
				reference: 'OpsField',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.smReportSerts.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadSmOps"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			},{
				xtype: 'datefield',
				fieldLabel: App.$STR.Date,
				labelWidth: 30,
				width: 150,
				value: new Date(),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			},{
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowIafMD'
			},{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onIAFMDPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion IAF MD

// #region Sertificates All OPS

Ext.define('App.ncaReportSertificates.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.smReportEntries.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.SmEntry.StatusDisplay.name, type: 'string' },
				{ name: MDL.SmEntry.EntryDate.name, type: 'date' },
				{ name: MDL.SmEntry.Declarant.name, type: 'string' },
				{ name: MDL.SmEntry.Autor.name, type: 'string' },
				{ name: MDL.SmEntry.DeclarantEmail.name, type: 'string' },
				{ name: MDL.SmEntry.AreaSertification.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadSertificates"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.OPS, dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ text: $_CS.Common.Location, dataIndex: MDL.OpsUnit.LocationDisplay.name, flex: 1 },
		{ text: $_CS.Reports.ActionsSmk, dataIndex: MDL.SmSertificate.Counter.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.Reports.ActionsSmkByAllOps
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: App.$STR.Date,
				value: new Date(),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowSertificates'
			},
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onSertsPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Sertificates All OPS

// #region Auditors All OPS

Ext.define('App.ncaReportAuditors.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.AuditorsEntries.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.SmEntry.StatusDisplay.name, type: 'string' },
				{ name: MDL.SmEntry.EntryDate.name, type: 'date' },
				{ name: MDL.SmEntry.Declarant.name, type: 'string' },
				{ name: MDL.SmEntry.Autor.name, type: 'string' },
				{ name: MDL.SmEntry.DeclarantEmail.name, type: 'string' },
				{ name: MDL.SmEntry.AreaSertification.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadAuditors"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.OPS, dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ text: $_CS.Common.Location, dataIndex: MDL.OpsUnit.LocationDisplay.name, flex: 1 },
		{ text: $_CS.Reports.OfAuditors, dataIndex: MDL.SmSertificate.Counter.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.Reports.ActionsAuditorsByAllOps
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: App.$STR.Date,
				value: new Date(),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			},{
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowAuditors'
			},{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onAuditorsPrint'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion Auditors All OPS

// #region AuditDays

Ext.define('App.ncaReportAuditDay.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'smReports',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: Ext.define('App.ncaReportAuditDay.view.GridModel', {
			extend: 'Ext.data.Model',
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadAuditDays"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.Indicator, dataIndex: 'Index', flex: 1 },
		{ text: $_CS.Reports.WorkHours, dataIndex: 'Value', flex: 1 },
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		fileName: $_CS.Reports.ReportsByForm_IafMd
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'OpsField',
				reference: 'OpsField',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.ncaReportAuditDay.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadSmOps"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			}, {
				xtype: 'datefield',
				fieldLabel: App.$STR.Date,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				fieldLabel: App.$STR.To,
				labelWidth: 30,
				width: 150,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			}, {
				xtype: 'button',
				iconCls: Ext.icon.change,
				text: App.$STR.Show,
				handler: 'onShowAuditDays'
			}, {
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onPrintAuditDays'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

// #endregion AuditDays


// #region Controller

Ext.define('App.organization.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.smReports',

	onShowIafMD: function () {
		this.view.store.proxy.extraParams = {
			opsId: this.view.lookupReference('OpsField').getValue(),
			date: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowAuditDays: function () {
		this.view.store.proxy.extraParams = {
			opsId: this.view.lookupReference('OpsField').getValue(),
			startDay: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finish: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowSertificates: function () {
		this.view.store.proxy.extraParams = {
			date: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue()
		};

		this.view.store.load();
	},

	onShowAuditors: function () {
		this.view.store.proxy.extraParams = {
			date: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue()
		};

		this.view.store.load();
	},

	onSertsPrint: function () {
		this.view.file.save({
			remote: true,
			title: $_CS.Reports.SmkCertificatesByAllOpsSmkOn + Ext.Date.format(this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(), 'd.m.Y'),
		});
	},

	onAuditorsPrint: function () {
		this.view.file.save({
			remote: true,
			title: $_CS.Reports.AuditorsByAllOpsSmkOn + Ext.Date.format(this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(), 'd.m.Y'),
		});
	},

	onIAFMDPrint: function () {
		this.view.file.save({
			remote: false,
			title: $_CS.OpsStore.OpsSmk + ': ' + this.view.lookupReference('OpsField').getRawValue(),
			title2: $_CS.Reports.OrgNameByCertififcation + Ext.Date.format(this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(), 'd.m.Y')
		});
	},
	
	onGridPrint: function () {
		this.view.file.save({ remote: true });
		//this.view.file.savePages(1, { remote: true });
		// this.view.file.save();
	},

	onCancel: function () {
		this.view.close();
	}
});

// #endregion Controller

Ext.define('App.smReports.view.Panel', {
	extend: 'Ext.panel.Panel',
	requires: ['Ext.layout.container.Border'],
	layout: 'fit',
	bodyBorder: false,
	items: [
		new App.smReports.view.TabReports({
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.smReports.view.Panel')
		]
	});
});