﻿Ext.define('App.view.OpsStore.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'organization',
	bodyBorder: false,
	store: Ext.create('App.data.Store', {
		autoLoad: false,
		remoteSort: true,
		model: App.defineModel('OpsStoreModel', App.MDL.OpsUnit),
		proxy: {
			type: 'ajax',
			url: window.buildurl("GridLoadOpsUnits"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.OpsStore.TypeOPS, dataIndex: MDL.OpsUnit.OpsTypeDisplay.name },
		{ text: App.$STR.Name, dataIndex: MDL.OpsUnit.Name.name, flex: 2, xtype: 'mcolumn' },
		{ text: $_CS.Common.Location, dataIndex: MDL.OpsUnit.LocationDisplay.name, flex: 1 },
		{ text: $_CS.OpsStore.AttNumber, dataIndex: MDL.OpsUnit.AttestateNumber.name },
		{ text: $_CS.Common.SystemRegDate, dataIndex: MDL.OpsUnit.SystemRegistrationDate.name, xtype: 'datecolumn', format: 'd.m.Y' },
		{ text: $_CS.Common.Phone, dataIndex: MDL.OpsUnit.Phone.name, flex: 2 },
		{ text: App.$STR.Email, dataIndex: MDL.OpsUnit.Email.name, flex: 1 },
		{ text: App.$STR.Login, dataIndex: MDL.OpsUnit.Login.name, flex: 1 }
	],
	file: {
		remote: true,
		fileName: $_CS.OpsStore.OpsList
	},
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'button',
					iconCls: Ext.icon.pageadd,
					text: App.$STR.Add,
					handler: 'onAdd'
				},
				{
					xtype: 'button',
					iconCls: Ext.icon.peopleprofile,
					text: App.$STR.Profile,
					reference: 'btn_edit',
					disabled: true,
					handler: 'onEdit'
				},
				{
					xtype: 'button',
					iconCls: Ext.icon.pagedelete,
					text: App.$STR.Delete,
					reference: 'btn_delete',
					disabled: true,
					handler: 'onDelete'
				},
				{
					xtype: 'button',
					iconCls: Ext.icon.excel,
					text: App.$STR.Print,
					reference: 'btnPrintGrid',
					handler: 'onGridPrint'
				}
			]
		}
	],
	listeners: {
		afterrender: function() {
			var view = this.view;
			this.store.reload({
				callback: function() {
					view.refresh();
				}
			});
		},
		rowdblclick: 'onEdit',
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

/* Editor for Add Organisation */
Ext.define('App.organization.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'organization',
	layout: 'fit',
	frame: true,
	title: $_CS.OpsStore.AddOps,
	height: 355,
	width: 530,
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 170,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'combobox',
					fieldLabel: $_CS.OpsStore.TypeOPS,
					name: MDL.OpsUnit.OpsType.name,
					editable: false,
					mode: 'local',
					allowBlank: false,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						],
						data: [
							{ 'Id': 1, 'Name': $_CS.OpsStore.OpsSmk }
						]
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					xtype: 'multilangfields',
					name: MDL.OpsUnit.Name.name,
					allowBlank: false,
					fieldLabel: App.$STR.Name
				},
				{
					fieldLabel: $_CS.OpsStore.AttNumber,
					name: MDL.OpsUnit.AttestateNumber.name,
					regex: new RegExp('\\D\\.\\d{2}\\.\\d{4}'),
					regexText: $_CS.OpsStore.NumberFormat,
					allowBlank: false
				},
				{
					xtype: 'combobox',
					fieldLabel: $_CS.Common.Location,
					name: MDL.OpsUnit.Location.name,
					editable: false,
					mode: 'local',
					allowBlank: false,
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.organization.view.LocationModel', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadLocalCityes"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					minChars: 2,
					queryMode: 'local',
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					fieldLabel: App.$STR.Email,
					name: MDL.OpsUnit.Email.name,
					allowBlank: false,
					vtype: 'email'
				},
				{
					fieldLabel: App.$STR.Login,
					name: MDL.OpsUnit.Login.name,
					allowBlank: false
				},
				{
					fieldLabel: App.$STR.Password,
					name: MDL.OpsUnit.Password.name,
					inputType: 'password',
					minLength: 6,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSaveNewOps'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});

/* Профайл организации */
Ext.define('App.opsProfile.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'organization',
	layout: 'fit',
	title: $_CS.OpsStore.ProfileOPS,
	frame: true,
	height: 540,
	width: 530,
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 150,
				msgTarget: 'side'
			},
			items: [
				{
					fieldLabel: App.$STR.Login,
					name: MDL.OpsUnit.Login.name,
					editable : false,
					allowBlank: false
				},
				{
					xtype: 'datefield',
					fieldLabel: $_CS.Common.SystemRegDate,
					editable: false,
					readOnly: true,
					name: MDL.OpsUnit.SystemRegistrationDate.name
				},
				{
					xtype: 'multilangfields',
					name: MDL.OpsUnit.Name.name,
					allowBlank: false,
					fieldLabel: App.$STR.Name
				},
				{
					fieldLabel: $_CS.Common.Bin,
					name: MDL.OpsUnit.Bin.name,
					maxLength: 12,
					minLength: 12,
					allowBlank: false,
				},
				{
					fieldLabel: $_CS.OpsStore.PostCode,
					name: MDL.OpsUnit.PostCode.name,
					allowBlank: true,
				},
				{
					xtype: 'multilangfields',
					name: MDL.OpsUnit.Adress.name,
					allowBlank: false,
					fieldLabel: App.$STR.Adress
				},
				{
					fieldLabel: $_CS.OpsStore.AttNumber,
					maxLength: 9,
					minLength: 9,
					value: 'O.00.0000',
					regex: new RegExp('\\D\\.\\d{2}\\.\\d{4}'),
					name: MDL.OpsUnit.AttestateNumber.name,
					// readOnly: true,
					allowBlank: false,
				},
				{
					fieldLabel: $_CS.OpsStore.SertSMNumber,
					maxLength: 16,
					minLength: 16,
					value: 'KZ.0000000.00.00',
					readOnly: true,
					regex: new RegExp('KZ\\.\\d{7}\\.\\d{2}\\.\\d{2}'),
					name: MDL.OpsUnit.SmSertNumber.name,
					allowBlank: false,
				},
				{
					xtype: 'combobox',
					fieldLabel: $_CS.Common.Location,
					name: MDL.OpsUnit.Location.name,
					editable: false,
					mode: 'local',
					allowBlank: false,
					minChars: 3,
					queryMode: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.opsProfile.view.LocationModel', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadLocalCityes"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},
				{
					fieldLabel: $_CS.Common.Boss,
					allowBlank: true,
					name: MDL.OpsUnit.FioBoss.name,
				},
				{
					fieldLabel: $_CS.Common.Phones,
					name: MDL.OpsUnit.Phone.name,
				},
				{
					fieldLabel: App.$STR.Email,
					name: MDL.OpsUnit.Email.name,
					allowBlank: false
				},
				{
					fieldLabel: $_CS.Common.Site,
					name: MDL.OpsUnit.Site.name,
				},
				{
					xtype: 'hiddenfield',
					name: MDL.OpsUnit.Id.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			//scale: 'medium',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			//scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});

// Старый способ инициализации главной панели на странице. сейчас: isBasePanel: true,
Ext.define('App.view.OpsStore.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'organization',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.view.OpsStore.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.view.OpsStore.Panel')
		]
	});
});