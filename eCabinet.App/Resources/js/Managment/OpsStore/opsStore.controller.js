﻿Ext.define('App.organization.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.organization',

	onSelectionChange: function (g, rs) {
		this.record = rs[0];
		this.view.lookupReference('btn_edit').setDisabled(rs.length < 1);
		this.view.lookupReference('btn_delete').setDisabled(rs.length < 1);
	},

	onAdd: function () {
		this.showEditor();
	},

	onGridPrint: function () {
		this.view.file.save({ title: $_CS.OpsStore.ListOpsSmk });
	},

	onEdit: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		var store = this.view.store;
		var dlg = Ext.create('App.opsProfile.view.Editor', {
			record: rs[0],
			isNew: false,
			scope: store,
			callback: store.reload
		});
		dlg.show();
	},

	onDelete: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		var store = this.view.store;
		this.confirm($_CS.OpsStore.ConfirmDelete, null, function () {
			var prms = { id: rs[0].get('Id') },
				fn = function(d, res) {
					this.toast.info(res.message);
					store.reload();
				};
			this.ajax.post('DeleteOpsUnit', prms, fn, App.$STR.Deleting, null, 200);
		}, null, this);
	},

	showEditor: function (r) {
		var store = this.view.store;
		var isNew = r == undefined;
		r = r || this.view.store.createModel({
			Id: 0,
			Name: String.empty,
			NameKz: String.empty,
			NameRu: String.empty,
			Email: String.empty,
			Login: String.empty,
			Password: String.empty,
			Location: 0,
			LocationDisplay: String.empty,
			OpsType: 0,
			OpsTypeDisplay: String.empty,
			SystemRegistrationDate: null,
			RegistryRegistrationDate: null
		});
		var dlg = Ext.create('App.organization.view.Editor', {
			record: r,
			isNew: isNew,
			scope: store,
			callback: store.reload

//			function () {
//				store.reload({ params: { opsType : 1 } });
//			}
		});
		dlg.show();
	},

	onValidate: function (f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},

	onSaveNewOps: function () {
		var dlg = this.view,
		prms = {
			json: Ext.JSON.encode(dlg.getValues()),
			isNew: dlg.isNew
		},
		fn = function (d, res) {
			if (Ext.isFunction(this.view.callback))
				this.view.callback.call(this.view.scope || this);
			this.toast.info(res.message);
			this.view.close();
		};
		this.ajax.post('AddOpsUnit', prms, fn, App.$STR.Saving, null, 200);
	},

	onSave: function () {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveOpsUnit', prms, fn, App.$STR.Saving, null, 200);
	},

	onCancel: function () {
		this.view.close();
	}
});