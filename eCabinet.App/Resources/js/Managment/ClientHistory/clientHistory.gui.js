﻿Ext.define('App.clients.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'clients',
	xtype: 'widget.clientsgrid',
	alias: 'widget.clientsgrid',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		remoteSort: true,
		model: App.defineModel('OpsStoreModel', App.MDL.Clients),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadClients"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: $_CS.Common.SystemRegDate, dataIndex: MDL.Clients.RegistrationDate.name, xtype: 'datecolumn', format: 'd.m.Y', width: 110 },
		{ text: $_CS.Common.OpsUnit, dataIndex: MDL.OpsUnit.OpsTypeDisplay.name, flex: 2 },
		{ text: App.$STR.New, dataIndex: MDL.Clients.NameNew.name, flex: 2, xtype: 'mcolumn' },
		{ text: "Старый", dataIndex: MDL.Clients.Name.name, flex: 2, xtype: 'mcolumn' },
		{ text: App.$STR.New, dataIndex: MDL.Clients.BinNew.name, width: 110 },
		{ text: "Старый", dataIndex: MDL.Clients.Bin.name, width: 110 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
	{
		xtype: 'toolbar',
		dock: 'top',
		defaults: {
			xtype: 'button'
		},
		items: [
			{
				xtype: 'button',
				iconCls: Ext.icon.excel,
				text: App.$STR.Excel,
				reference: 'btnPrintGrid',
				handler: 'onGridPrint'
			}
		],
	}],
	listeners: {
		afterrender: function () {
			var view = this.view;
			this.store.reload({
				callback: function () {
					view.refresh();
				}
			});
		},
		rowdblclick: 'onEdit',
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});