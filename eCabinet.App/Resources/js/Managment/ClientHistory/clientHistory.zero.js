﻿var clientChecked = false;

Ext.define('App.clients.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.clients',

	onGridPrint: function () {
		this.view.file.savePages(1, { remote: true, title: $_CS.Common.Clients });
		// this.view.file.save({ title: $_CS.Common.Clients });
	}
});

Ext.define('App.clients.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'clients',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.clients.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.clients.view.Panel')
		]
	});
});