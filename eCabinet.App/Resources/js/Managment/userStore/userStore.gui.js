﻿Ext.define('App.userStore.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'userStore',
	xtype: 'widget.userStore',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		remoteSort: true,
		pageSize: 40,
		model: Ext.define('App.userStore.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.User.Id.name, type: 'int' },
				{ name: MDL.User.Login.name, type: 'string' },
				{ name: MDL.User.ShortName.name, type: 'string' },
				{ name: MDL.User.Iin.name, type: 'string' },
				{ name: MDL.User.LastActivityDate.name, type: 'date' },
				{ name: MDL.User.OnLine.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadUsers"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Login, dataIndex: MDL.User.Login.name, flex: 1 },
		{ text: $_CS.Common.ShortName, dataIndex: MDL.User.ShortName.name, flex: 2 },
		{ text: App.$STR.Iin, dataIndex: MDL.User.Iin.name, flex: 1 },
		{ text: $_CS.UserStore.LastActivityDate, dataIndex: MDL.User.LastActivityDate.name, flex: 1, xtype: 'datecolumn', format: 'd.m.Y H:i:s' },
		{ text: $_CS.UserStore.OnLine, dataIndex: MDL.User.OnLine.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
				{
					xtype: 'combobox',
					fieldLabel: App.$STR.Role,
					name: MDL.Employe.Role.name,
					reference: MDL.Employe.Role.name,
					editable: false,
					mode: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: false,
						model: Ext.define('App.userStore.view.TypeSmRoles', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadSmRoles"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				}, {
					xtype: 'button',
					iconCls: Ext.icon.people,
					text: App.$STR.Show,
					reference: 'btnShow',
					handler: 'onShowUsers'
				},{
					xtype: 'button',
					iconCls: Ext.icon.peopleprofile,
					text: App.$STR.Edit,
					disabled: true,
					reference: 'btnEdit',
					handler: 'onEdit'
				}, {
					xtype: 'button',
					iconCls: Ext.icon.excel,
					text: App.$STR.Print,
					reference: 'btnPrintGrid',
					handler: 'onGridPrint'
				}
			]
		}],
	listeners: {
		rowdblclick: 'onEdit',
		selectionchange: 'onSelectionChange'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true,
			})
		});
		this.callParent();
	}
});

/* Editor for User set Password */
Ext.define('App.userStore.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'userStore',
	layout: 'fit',
	frame: true,
	title: App.$STR.Profile,
	height: 200,
	width: 500,
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 180,
				msgTarget: 'side'
			},
			items: [
				{
					fieldLabel: $_CS.Common.ShortName,
					name: MDL.User.ShortName.name,
					editable: false
				},
				{
					fieldLabel: App.$STR.Login,
					name: MDL.User.Login.name,
					editable: false
				},
				{
					fieldLabel: $_CS.Common.Password,
					name: MDL.User.Password.name,
					inputType: 'password',
					minLength: minPasswordLength,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: App.ICONS.floppy_o,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: App.ICONS.ban,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});