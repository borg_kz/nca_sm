﻿Ext.define('App.userStore.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.userStore',

	onGridPrint: function () {
		this.view.file.save({ title: $_CS.Common.UsersList, remote: true });
	},

	onEdit: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		var store = this.view.store;
		var dlg = Ext.create('App.userStore.view.Editor', {
			record: rs[0],
			isNew: false,
			scope: store,
			callback: store.reload
		});
		dlg.show();
	},

	onSelectionChange: function (g, rs) {
		this.record = rs[0];
		this.view.lookupReference('btnEdit').setDisabled(rs.length < 1);
	},

	onShowUsers: function () {
		this.view.store.currentPage = 1;
		this.view.store.proxy.extraParams = { roleId: this.view.lookupReference(MDL.Employe.Role.name).getValue() }
		this.view.store.load();
//		this.view.store.load({
//			params: {
//					roleId: this.view.lookupReference(MDL.Employe.Role).getValue()
//			}
//		});
	},

	onValidate: function (f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},

	onSave: function () {
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function (d, res) {
//				if (Ext.isFunction(this.view.callback))
//					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('SaveUser', prms, fn, App.$STR.Saving, null, 200);
	},

	onCancel: function () {
		this.view.close();
	}
});

Ext.define('App.userStore.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'userStore',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.userStore.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.userStore.view.Panel')
		]
	});
});