﻿Ext.define('App.sertView.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'sertView',
	xtype: 'widget.sertView',
	bodyBorder: false,
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		pageSize: 50,
		model: App.defineModel('SmSertModel', App.MDL.SmSertificate),
		proxy: {
			type: 'ajax',
			url: window.buildurl("LoadSmSert"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Status, dataIndex: MDL.SmSertificate.StatusDisplay.name, width: 100 },
		{ text: $_CS.Common.OPS, dataIndex: MDL.SmSertificate.CounterDisplay.name, flex: 2 },
		{ text: $_CS.Common.Declarant, dataIndex: MDL.SmSertificate.Declarant.name, flex: 2 },
		{ text: $_CS.Common.AreaSertification, dataIndex: MDL.SmSertificate.AreaSertification.name, flex: 3, hidden: true },
		{ text: $_CS.Common.From, xtype: 'datecolumn', dataIndex: MDL.SmSertificate.StartDate.name, width: 90, format: 'd.m.Y' },
		{ text: $_CS.Common.To, xtype: 'datecolumn', dataIndex: MDL.SmSertificate.FinishDate.name, width: 90, format: 'd.m.Y' },
		{ text: $_CS.SertView.SertNumber, dataIndex: MDL.SmSertificate.SertifikateSmNumber.name, width: 180 },
		{ text: $_CS.Common.KSS, dataIndex: MDL.SmSertificate.BlankNumber.name, width: 80 },
		{ text: $_CS.Common.CertificationType, dataIndex: MDL.SmSertificate.SmSertificationTypeDisplay.name, width: 175 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	dockedItems: [
		{
			xtype: 'toolbar',
			dock: 'top',
			items: [
			{
				xtype: 'container',
				layout: {
					type: 'vbox'
				},
				defaults: {
					anchor: '100%',
					labelWidth: 250,
					msgTarget: 'side'
				},
				items: [
					{
						xtype: 'combobox',
						fieldLabel: $_CS.SertView.OrganFromSertification,
						name: MDL.SmSertificate.Declarant.name,
						reference: MDL.OpsUnit.Name.name,
						editable: false,
						width: 700,
						mode: 'local',
						forceSelection: true,
						store: Ext.create('Ext.data.Store', {
							autoLoad: false,
							model: Ext.define('App.userStore.view.TypeSmRoles', {
								extend: 'Ext.data.Model',
								fields: [
									{ name: 'Id', type: 'int' },
									{ name: 'Name', type: 'string' }
								]
							}),
							proxy: {
								type: 'ajax',
								url: window.buildurl("LoadListSmOps"),
								reader: {
									type: 'json',
									rootProperty: 'data'
								}
							}
						}),
						valueField: 'Id',
						displayField: 'Name'
					},
					{
						xtype: 'combobox',
						fieldLabel: $_CS.Common.CertificationType,
						name: MDL.SmSertificate.SmSertificationType.name,
						reference: MDL.SmSertificate.SmSertificationType.name,
						editable: false,
						width: 700,
						mode: 'remote',
						forceSelection: true,
						store: Ext.create('Ext.data.Store', {
							autoLoad: true,
							model: Ext.define('App.smSert.view.SmSertificationType', {
								extend: 'Ext.data.Model',
								fields: [
									{ name: 'Id', type: 'int' },
									{ name: 'Name', type: 'string' },
									{ name: 'ValueKz', type: 'string' },
									{ name: 'ValueRu', type: 'string' },
									{ name: 'SystemKz', type: 'string' },
									{ name: 'SystemRu', type: 'string' }
								]
							}),
							proxy: {
								type: 'ajax',
								url: window.buildurl("LoadSmSertType"),
								reader: {
									type: 'json',
									rootProperty: 'data'
								}
							}
						}),
						listeners: {
							select: 'onSertTypeSelected',
							delay: 1
						},
						valueField: 'Id',
						displayField: 'Name'
					},
					{
						xtype: 'container',
						layout: {
							type: 'hbox'
						},
						defaults: {
							anchor: '100%',
							labelWidth: 250,
							msgTarget: 'side'
						},
						style: 'margin-bottom:5px;',
						items: [
							{
								xtype: 'datefield',
								labelWidth: 250,
								fieldLabel: $_CS.SertView.DateSertificationFrom,
								name: MDL.SmSertificate.StartDate.name,
								reference: MDL.SmSertificate.StartDate.name
							},
							{
								xtype: 'datefield',
								labelWidth: 70,
								width: 250,
								fieldLabel: $_CS.Common.To,
								padding: '0 0 0 25',
								name: MDL.SmSertificate.FinishDate.name,
								reference: MDL.SmSertificate.FinishDate.name
							}
						]
					}, {
						xtype: 'textfield',
						fieldLabel: $_CS.SertView.NumberBlankWithoutKSS,
						name: MDL.SmSertificate.BlankNumber.name,
						reference: MDL.SmSertificate.BlankNumber.name
					}, {
						xtype: 'textfield',
						fieldLabel: $_CS.SertView.SertNumber,
						name: MDL.SmSertificate.SertifikateSmNumber.name,
						reference: MDL.SmSertificate.SertifikateSmNumber.name
					}, {
						xtype: 'textfield',
						fieldLabel: $_CS.Common.Declarant,
						width: 700,
						name: MDL.SmSertificate.Declarant.name,
						reference: MDL.SmSertificate.Declarant.name
					}
				]
			},
			{
				xtype: 'container',
				layout: {
					type: 'vbox'
				},
				defaults: {
					anchor: '100%',
					labelWidth: 250,
					msgTarget: 'side'
				},
				style: 'margin-left:15px;',
				items: [
					{
						xtype: 'button',
						//iconCls: Ext.icon.pagesearch,
						text: App.$STR.Filtering,
						reference: 'btnShow',
						scale: 'medium',
						style: 'margin-bottom:5px;',
						width: 150,
						handler: 'onLoadSerts'
					}, {
						xtype: 'button',
						// iconCls: App.ICONS.ban,
						text: App.$STR.Clean,
						reference: 'btnShow',
						style: 'margin-bottom:5px;',
						scale: 'medium',
						width: 150,
						handler: 'onClearForm'
					}, {
						xtype: 'button',
						//iconCls: App.ICONS.file_excel_o,
						text: App.$STR.Print,
						scale: 'medium',
						reference: 'btnPrintGrid',
						style: 'margin-bottom:5px;',
						width: 150,
						handler: 'onGridPrint'
					}, {
						xtype: 'button',
						//iconCls: Ext.icon.profile,
						text: $_CS.Common.Certificate,
						scale: 'medium',
						reference: 'btnPrintGrid',
						style: 'margin-bottom:5px;',
						width: 150,
						handler: 'onShowSert'
					}
				]
			}]
		}],
	listeners: {
		rowdblclick: 'onShowSert'
	},
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true
			})
		});
		this.callParent();
	}
});

/* Editor for SertificateProfile */
Ext.define('App.sertView.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'sertView',
	layout: 'fit',
	frame: true,
	title: $_CS.SertView.DataReviewOnCertificate,
	height: 580,
	width: 700,
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				allowBlank: true,
				labelWidth: 170,
				labelStyle: 'font-weight:bold;padding:0;color: black;'
			},
			items: [
				{
					xtype: 'displayfield',
					fieldLabel: App.$STR.Status,
					name: MDL.SmSertificate.StatusDisplay.name
				},{
					xtype: 'displayfield',
					fieldLabel: $_CS.Common.OPS,
					name: MDL.SmSertificate.CounterDisplay.name
				},{
					xtype: 'displayfield',
					fieldLabel: $_CS.Common.Declarant,
					name: MDL.SmSertificate.Declarant.name
				},{
					xtype: 'displayfield',
					fieldLabel: App.$STR.Adress,
					name: MDL.Clients.Adress.name
				},{
					xtype: 'datefield',
					fieldLabel: $_CS.EntryManager.EntryDate,
					name: MDL.SmSertificate.InspectionDate.name,
					editable: false,
					readOnly: true
				},
				{
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					defaults: {
						anchor: '100%',
						msgTarget: 'side',
						labelWidth: 170,
						labelStyle: 'font-weight:bold;padding-top:5;padding-bottom:10;color: black;'
					},
					
					items: [
						{
							xtype: 'datefield',
							fieldLabel: 'Начало обследования',
							name: MDL.SmSertificate.StartReseachDate.name,
							editable: false,
							style: 'margin-right:15px;',
							readOnly: true
						}, {
							xtype: 'datefield',
							fieldLabel: 'окончание',
							labelWidth: 80,
							name: MDL.SmSertificate.ResultReseachDate.name,
							editable: false,
							readOnly: true
						}
					]
				},
				//{
				//	xtype: 'menuseparator',
				//	width: '100%',
				//	style: 'padding-top:5;color: black;'
				//},
				{
					xtype: 'displayfield',
					fieldLabel: $_CS.EntryManager.NumberBlank,
					name: MDL.SmSertificate.BlankNumber.name
				}, {
					xtype: 'displayfield',
					fieldLabel: $_CS.SertView.CertificateNumb,
					name: MDL.SmSertificate.SertifikateSmNumber.name
				},
				{
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					defaults: {
						//anchor: '100%',
						msgTarget: 'side',
						labelWidth: 170,
						labelStyle: 'font-weight:bold;padding-bottom:10;color: black;'
					},
					items: [{
							xtype: 'datefield',
							fieldLabel: 'Действует с',
							name: MDL.SmSertificate.StartDate.name,
							style: 'margin-right:15px;',
							editable: false,
							readOnly: true
						}, {
							xtype: 'datefield',
							fieldLabel: ' до ',
							labelWidth: 80,
							name: MDL.SmSertificate.FinishDate.name,
							editable: false,
							readOnly: true
						}
					]
				}, {
					xtype: 'displayfield',
					fieldLabel: App.$STR.System,
					name: MDL.SmSertificate.System.name
				}, {
					xtype: 'displayfield',
					fieldLabel: $_CS.Common.AreaSertification,
					name: MDL.SmSertificate.AreaSertification.name
				}, {
					xtype: 'displayfield',
					fieldLabel: $_CS.SertView.MeetsRequirements,
					name: MDL.SmSertificate.Requirements.name
				}, {
					xtype: 'displayfield',
					fieldLabel: $_CS.SertView.ExpertAuditor,
					name: MDL.SmSertificate.Auditor.name
				}
			]
		}
	],
	buttons: [
		{
			text: App.$STR.Cancel,
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});