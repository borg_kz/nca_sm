﻿Ext.define('App.sertView.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.sertView',

	onGridPrint: function () {
		this.view.file.save({ title: $_CS.SertView.SmkCertificationsSearching, remote: true });
	},

	onClearForm: function () {
		this.view.lookupReference(MDL.OpsUnit.Name.name).setValue(),
		this.view.lookupReference(MDL.SmSertificate.SmSertificationType.name).setValue(),
		this.view.lookupReference(MDL.SmSertificate.StartDate.name).setValue(),
		this.view.lookupReference(MDL.SmSertificate.FinishDate.name).setValue(),
		this.view.lookupReference(MDL.SmSertificate.BlankNumber.name).setValue(),
		this.view.lookupReference(MDL.SmSertificate.SertifikateSmNumber.name).setValue(),
		this.view.lookupReference(MDL.SmSertificate.Declarant.name).setValue();
		this.view.store.currentPage = 1;
		this.view.store.proxy.extraParams = { ops: -1111 }
		this.view.store.load();
	},

	onLoadSerts: function () {
		this.view.store.currentPage = 1;
		this.view.store.proxy.extraParams = {
			ops: this.view.lookupReference(MDL.OpsUnit.Name.name).getValue(),
			sertType: this.view.lookupReference(MDL.SmSertificate.SmSertificationType.name).getValue(),
			startdate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
			finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue(),
			blank: this.view.lookupReference(MDL.SmSertificate.BlankNumber.name).getValue(),
			sert: this.view.lookupReference(MDL.SmSertificate.SertifikateSmNumber.name).getValue(),
			declarant: this.view.lookupReference(MDL.SmSertificate.Declarant.name).getValue()
		}
		this.view.store.load();
	},

	onShowSert: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		var store = this.view.store;
		var dlg = Ext.create('App.sertView.view.Editor', {
			record: rs[0],
			isNew: false,
			scope: store,
			callback: store.reload
		});
		dlg.show();
	},

	onCancel: function () {
		this.view.close();
	}
});

Ext.define('App.sertView.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'sertView',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.sertView.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.sertView.view.Panel')
		]
	});
});