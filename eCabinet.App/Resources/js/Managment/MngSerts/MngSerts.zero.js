﻿Ext.define('App.orgProfile.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.orgProfile',

	onSave: function () {
		// this.toast.warning('Внимание! Ваша организация не активна. После сохранения данных активируйте.');
		// App.msg.toast.warning('Внимание! Ваша организация не активна. После сохранения данных активируйте.');
		var dlg = this.view,
			prms = {
				json: Ext.JSON.encode(dlg.getValues()),
				isNew: dlg.isNew
			},
			fn = function (d, res) {
				if (Ext.isFunction(this.view.callback))
					this.view.callback.call(this.view.scope || this);
				this.toast.info(res.message);
			};
		this.ajax.post('SaveOpsInfo', prms, fn, App.$STR.Saving, null, 200);
	},

	onActivate: function () {
	},
	onSubmit: function () {
		var form = this.view.getForm();
		if (!form.isValid()) {
			form.submit({
				clientValidation: false,
				jsonSubmit : true,
//				standardSubmit: false,
				url: this.ajax.buildurl('SaveOpsInfo'),
//				success: function (form, action) {
//				
//					Ext.Msg.alert('Success', action.result.message);
//				},
//				failure: function(form, action) {
//					Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
//				}
			});
		} else {
			Ext.Msg.alert( "Error!", "Your form is invalid!" );
		}
	},
	onValidate: function (f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},

	onClick: function () {
		this.ajax.post('LoadOrganizations', { name: 'dfdfdfd' }, function (d) {
			var tt = [{ name: 'sdsds' }];
			tt.select(function (item) {
				return item.name;
			});
			this.toast.warning('cjvh xkcvjbxckjvxbcjkvl');
		}, $_CS.Common.LoadingData, null, 200);
	}
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.view.orgProfile.Panel')
		]
	});
});