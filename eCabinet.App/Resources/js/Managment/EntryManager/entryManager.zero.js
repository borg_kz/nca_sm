﻿Ext.define('App.EntryManager.controller.Index', {
	extend: 'App.Controller',
	alias: 'controller.EntryManager',

	onGridPrint: function () {
		this.view.file.save({ title: $_CS.SmEntry.ApplicationsList + ' ' + this.view.lookupReference('OpsField').getValue() });
	},

	onShowSmEntryes: function () {
		this.view.store.load({
			params: {
				startDate: this.view.lookupReference(MDL.SmSertificate.StartDate.name).getValue(),
				finishDate: this.view.lookupReference(MDL.SmSertificate.FinishDate.name).getValue(),
				opsId: this.view.lookupReference('OpsField').getValue()
			}
		});
	},

	onChangeOps: function() {
		var dlg = Ext.create('App.ChangeOps.view.Editor', {});
		dlg.show();
	},

	onChangeOpsForBlank: function () {
		var dlg = this.view,
			prms = {
				opsUnitId: dlg.lookupReference('OpsField').getValue(),
				blankNumber: dlg.lookupReference(MDL.SmEntry.RegistrationDate.name).getValue(),
				recipientId: dlg.lookupReference('Recipientld').getValue(),
				date: dlg.lookupReference('TransferDate').getValue()
			},
			fn = function (d, res) {
				this.toast.info(res.message);
				// this.view.close();
			};
		this.ajax.post('ChangeOpsForBlank', prms, fn, App.$STR.Saving, null, 200);
	},

	onEdit: function () {
		var rs = this.view.getSelectionModel().getSelection();
		if (rs.length != 1) return;
		var store = this.view.store;
		var dlg = Ext.create('App.EntryManager.view.Editor', {
			record: rs[0],
			isNew: false,
			scope: store,
			callback: store.reload
		});
		dlg.show();
	},

	onSelectionChange: function (g, rs) {
		this.record = rs[0];
		this.view.lookupReference('btnEdit').setDisabled(rs.length < 1);
	},

	onValidate: function (f, v) {
		this.lookupReference('btnSave').setDisabled(!v);
	},

	onExecuteXmlData: function () {
		this.ajax.post('ExecuteXmlData', null, null, App.$STR.Saving, null, 200);
	},

	onSave: function () {
		var dlg = this.view,
			prms = {
				id: dlg.lookupReference(MDL.SmEntry.Id.name).getValue(),
				entryDate: dlg.lookupReference(MDL.SmEntry.EntryDate.name).getValue(),
				regDate: dlg.lookupReference(MDL.SmEntry.RegistrationDate.name).getValue()
			},
			fn = function (d, res) {
				this.toast.info(res.message);
				this.view.close();
			};
		this.ajax.post('EditEntry', prms, fn, App.$STR.Saving, null, 200);
	},

	onCancel: function () {
		this.view.close();
	}
});

Ext.define('App.EntryManager.view.Panel', {
	extend: 'Ext.panel.Panel',
	controller: 'EntryManager',
	requires: ['Ext.layout.container.Border'],
	layout: 'border',
	bodyBorder: false,
	items: [
		new App.EntryManager.view.GridPanel({
			region: 'center'
		})
	]
});

Ext.onReady(function () {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			Ext.create('App.EntryManager.view.Panel')
		]
	});
});