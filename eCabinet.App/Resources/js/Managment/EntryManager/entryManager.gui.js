﻿Ext.define('App.EntryManager.view.GridPanel', {
	extend: 'Ext.grid.Panel',
	controller: 'EntryManager',
	bodyBorder: false,
	layout: 'fit',
	store: Ext.create('Ext.data.Store', {
		autoLoad: false,
		model: Ext.define('App.smReportEntries.view.GridModel', {
			extend: 'Ext.data.Model',
			fields: [
				{ name: MDL.SmEntry.StatusDisplay.name, type: 'string' },
				{ name: MDL.SmEntry.EntryDate.name, type: 'date' },
				{ name: MDL.SmEntry.RegistrationDate.name, type: 'date' },
				{ name: MDL.SmEntry.Declarant.name, type: 'string' },
				{ name: MDL.SmEntry.Autor.name, type: 'string' },
				{ name: MDL.SmEntry.DeclarantEmail.name, type: 'string' },
				{ name: MDL.SmEntry.AreaSertification.name, type: 'string' }
			]
		}),
		proxy: {
			type: 'ajax',
			autoLoad: false,
			url: window.buildurl("LoadSmEntryes"),
			reader: {
				type: 'json',
				rootProperty: 'data'
			}
		}
	}),
	columns: [
		{ text: App.$STR.Status, dataIndex: MDL.SmEntry.StatusDisplay.name, width: 110 },
		{ text: $_CS.EntryManager.EntryDate, dataIndex: MDL.SmEntry.EntryDate.name, xtype: 'datecolumn', width: 110, format: 'd.m.Y' },
		{ text: $_CS.EntryManager.RegistrationDate, dataIndex: MDL.SmEntry.RegistrationDate.name, xtype: 'datecolumn', width: 110, format: 'd.m.Y' },
		{ text: $_CS.Common.Declarant, dataIndex: MDL.SmEntry.DeclarantDisplay.name, width: 170, flex: 2 },
		{ text: $_CS.Common.CertificationType, dataIndex: 'SmTypes', flex: 2 },
		{ text: $_CS.Common.AreaSertification, dataIndex: MDL.SmEntry.AreaSertification.name, flex: 3 },
//		{ text: App.$STR.Autor, dataIndex: MDL.SmEntry.Autor.name, flex: 1 }
	],
	viewConfig: {
		deferEmptyText: false,
		emptyText: App.$STR.Nodata
	},
	file: {
		remote: true,
		fileName: $_CS.SmEntry.RegisteredApplicationsSmk
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		items: [
			{
				xtype: 'combobox',
				fieldLabel: $_CS.SmReports.ChooseOPS,
				name: 'OpsField',
				reference: 'OpsField',
				mode: 'local',
				flex: 1,
				allowBlank: false,
				minChars: 3,
				queryMode: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					autoLoad: true,
					model: Ext.define('App.smEntrySerts.view.LocationModel', {
						extend: 'Ext.data.Model',
						fields: [
							{ name: 'Id', type: 'int' },
							{ name: 'Name', type: 'string' }
						]
					}),
					proxy: {
						type: 'ajax',
						url: window.buildurl("LoadListSmOps"),
						reader: {
							type: 'json',
							rootProperty: 'data'
						}
					}
				}),
				valueField: 'Id',
				displayField: 'Name'
			}, {
				xtype: 'datefield',
				fieldLabel: $_CS.Common.From,
				labelWidth: 30,
				width: 150,
				value: new Date().addDays(-30),
				name: MDL.SmSertificate.StartDate.name,
				reference: MDL.SmSertificate.StartDate.name
			}, {
				xtype: 'datefield',
				labelWidth: 30,
				width: 150,
				fieldLabel: $_CS.Common.To,
				value: new Date(),
				name: MDL.SmSertificate.FinishDate.name,
				reference: MDL.SmSertificate.FinishDate.name
			}, {
				xtype: 'button',
				iconCls: App.ICONS.dedent,
				text: App.$STR.Show,
				handler: 'onShowSmEntryes'
			}, {
				xtype: 'button',
				iconCls: App.ICONS.edit,
				text: App.$STR.Edit,
				handler: 'onEdit'
			}, {
				xtype: 'button',
				iconCls: App.ICONS.level_down,
				text: $_CS.SmEntry.DbfImport,
				handler: 'onExecuteXmlData'
			}, {
				xtype: 'button',
				iconCls: App.ICONS.file_excel_o,
				text: App.$STR.Print,
				reference: 'btnPrintGrid',
				handler: 'onSmSertGridPrint'
			}, {
				xtype: 'button',
				iconCls: App.ICONS.hospital_o,
				text: $_CS.SmEntry.ChangeOps,
				reference: 'btnChangeOps',
				handler: 'onChangeOps'
			}
		]
	}],
	initComponent: function () {
		Ext.apply(this, {
			bbar: Ext.create('Ext.PagingToolbar', {
				store: this.store,
				displayInfo: true
			})
		});
		this.callParent();
	}
});

/* Editor for User set Password */
Ext.define('App.EntryManager.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'EntryManager',
	layout: 'fit',
	frame: true,
	title: App.$STR.Edit,
	height: 150,
	width: 500,
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 180,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'datefield',
					fieldLabel: $_CS.EntryManager.EntryDate,
					name: MDL.SmEntry.EntryDate.name,
					reference: MDL.SmEntry.EntryDate.name,
					labelWidth: 130,
					width: 150,
				},{
					xtype: 'datefield',
					fieldLabel: $_CS.EntryManager.RegistrationDate,
					name: MDL.SmEntry.RegistrationDate.name,
					reference: MDL.SmEntry.RegistrationDate.name,
					labelWidth: 130,
					width: 150,
				}, {
					xtype: 'hiddenfield',
					name: MDL.SmEntry.Id.name,
					reference: MDL.SmEntry.Id.name,
					allowBlank: false
				}
			],
			listeners: {
				validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Save,
			disabled: true,
			scale: 'medium',
			iconCls: App.ICONS.floppy_o,
			reference: 'btnSave',
			handler: 'onSave'
		},
		{
			text: App.$STR.Cancel,
			scale: 'medium',
			iconCls: App.ICONS.ban,
			handler: 'onCancel'
		}
	],
	listeners: {
		afterrender: function () {
			this.lookupReference('form').loadRecord(this.record);
		}
	},
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});

/* Editor for ChangeOps */
Ext.define('App.ChangeOps.view.Editor', {
	extend: 'Ext.window.Window',
	controller: 'EntryManager',
	layout: 'fit',
	frame: true,
	title: 'Change Ops',
	width: 500,
	modal: true,
	constrain: true,
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			layout: 'anchor',
			reference: 'form',
			defaultType: 'textfield',
			defaults: {
				anchor: '100%',
				labelWidth: 130,
				msgTarget: 'side'
			},
			items: [
				{
					xtype: 'combobox',
					fieldLabel: $_CS.SmReports.ChooseOPS,
					name: 'OpsField',
					reference: 'OpsField',
					mode: 'local',
					flex: 1,
					allowBlank: false,
					minChars: 3,
					queryMode: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.ChangeOps.view.LocationModel', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadListSmOps"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},{
					fieldLabel: $_CS.EntryManager.NumberBlank,
					name: MDL.SmEntry.RegistrationDate.name,
					reference: MDL.SmEntry.RegistrationDate.name,
					labelWidth: 130,
					width: 150,
				}, {
					xtype: 'combobox',
					fieldLabel: 'ОПС куда',
					name: 'Recipientld',
					reference: 'Recipientld',
					mode: 'local',
					flex: 1,
					allowBlank: false,
					minChars: 3,
					queryMode: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						autoLoad: true,
						model: Ext.define('App.Recipient.view.LocationModel', {
							extend: 'Ext.data.Model',
							fields: [
								{ name: 'Id', type: 'int' },
								{ name: 'Name', type: 'string' }
							]
						}),
						proxy: {
							type: 'ajax',
							url: window.buildurl("LoadListSmOps"),
							reader: {
								type: 'json',
								rootProperty: 'data'
							}
						}
					}),
					valueField: 'Id',
					displayField: 'Name'
				},{
					xtype: 'datefield',
					fieldLabel: 'Дата перевода',
					name: 'TransferDate',
					reference: 'TransferDate',
					labelWidth: 130,
					width: 150,
				}
			],
			listeners: {
				//validitychange: 'onValidate'
			}
		}
	],
	buttons: [
		{
			text: App.$STR.Execute,
			scale: 'small',
			iconCls: Ext.icon.save,
			reference: 'btnSave',
			handler: 'onChangeOpsForBlank'
		},
		{
			text: App.$STR.Cancel,
			scale: 'small',
			// scale: 'medium',
			iconCls: Ext.icon.cancel,
			handler: 'onCancel'
		}
	],
	getValues: function () {
		var fields = this.lookupReference('form').getForm().getFields();
		var res = { Id: this.record != undefined ? this.record.get('Id') : 0 };
		fields.items.each(function (f) {
			if (f.name)
				res[f.name] = f.getValue();
		});
		return res;
	}
});