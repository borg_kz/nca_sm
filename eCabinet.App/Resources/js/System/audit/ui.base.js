﻿Ext.define('App.audit.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.auditgrid',
	controller: 'audit',
	initComponent: function() {
		var me = this,
			ln = window.LN;
		var store = Ext.create('Ext.data.Store', {
			autoLoad: false,
			pageSize: 150,
			model: 'App.audit.Model',
			proxy: {
				type: 'ajax',
				reader: {
					type: 'json',
					idProperty: 'Id',
					rootProperty: 'data',
					successProperty: 'success',
					messageProperty: 'message'
				},
				url: window.buildurl('GetMessages'),
				actionMethods: { read: 'POST' }
			}
		});
		Ext.apply(me, {
			store: store,
			columns: [
				{ xtype: 'rownumberer' },
				{ xtype: 'datecolumn', text: ln.ll_col_datetime, dataIndex: 'Date', width: 150, format: 'd.m.Y H:i:s' },
				{ text: ln.ll_col_login, dataIndex: 'Login', width: 100, flex: 1 },
				{ text: ln.ll_col_type, dataIndex: 'TypeDisplay', flex: 1 },
				{ text: ln.ll_col_message, dataIndex: 'Message', flex: 5 }
			],
			selModel: {
				xtype: 'rowmodel',
				mode: 'SINGLE',
//				listeners: {
//					selectionchange: 'onSelectionRole'
//				}
			},
			viewConfig: {
				emptyText: ln.ll_messages_empty,
				loadMask: true,
				loadingText: 'll_loading',
				enableTextSelection: true
			},
			dockedItems: [{
				xtype: 'pagingtoolbar',
				store: store,
				dock: 'bottom',
				displayInfo: true
			}],
			tbar: [
				me.from = Ext.create('Ext.form.field.Date', {
					text: ln.ll_filter_from,
					listeners: {
						select: 'onFromDateSelect'
					}
				}),
				me.to = Ext.create('Ext.form.field.Date', {
					text: ln.ll_filter_to,
					listeners: {
						select: 'onToDateSelect'
					}
				}),
				'  ',
				{
					xtype: 'combobox',
					filedLabel: ln.ll_filter_type,
					multiSelect: true,
					emptyText: ln.ll_filter_type_empty,
					store: Ext.create('Ext.data.Store', {
						model: 'App.audit.Combomodel',
						proxy: {
							type: 'ajax',
							reader: {
								type: 'json',
								idProperty: 'Id',
								rootProperty: 'data',
								successProperty: 'success',
								messageProperty: 'message'
							},
							url: window.buildurl('GetTypes'),
							actionMethods: { read: 'POST' }
						}
					}),
					valueField: 'Id',
					displayField: 'Name',
					mode: 'remote',
					listeners: {
						change: 'onTypeSelect'
					}
				},
				'',
				'-',
				'',
				{
					text: ln.ll_filter_btn,
					handler: 'onFilter'
				}
			]
		});
		me.callParent();
		var date = new Date();
		me.from.setValue(date);
		me.from.fireEvent('select', me.form, date);
		me.to.setValue(date);
		me.to.fireEvent('select', me.to, date);
	}
});
Ext.define('App.audit.Model', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'Id', type: 'int' },
		{ name: 'Message', type: 'string' },
		{ name: 'Date', type: 'date' },
		{ name: 'Login', type: 'string' },
		{ name: 'Type', type: 'int' },
		{ name: 'TypeDisplay', type: 'string' }
	]
});
Ext.define('App.audit.Combomodel', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'Id', type: 'int' },
		{ name: 'Name', type: 'string' }
	]
});

Ext.onReady(function() {
	Ext.QuickTips.init();
	return new Ext.Viewport({
		renderTo: Ext.getBody(),
		layout: 'fit',
		items: [
			{
				xtype: 'auditgrid'
			}
		]
	});
});
