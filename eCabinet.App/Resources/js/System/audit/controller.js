﻿Ext.define('App.audit.Controller', {
	extend: 'App.ControllerBase',
	alias: 'controller.audit',

	onFilter: function() {
		this.view.store.reload();
	},

	onToDateSelect: function(f, d) {
		this.view.from.setMaxValue(d);
		this.setGridParams('toDate', d);
	},

	onFromDateSelect: function(f, d) {
		this.view.to.setMinValue(d);
		this.setGridParams('fromDate', d);
	},

	onTypeSelect: function(f, ids) {
		this.setGridParams('types', Ext.JSON.encode(ids));
	},

	setGridParams: function(name, value) {
		if (!this.view.store.proxy.extraParams)
			this.view.store.proxy.extraParams = {};
		this.view.store.proxy.extraParams[name] = value;
	}

});