﻿Ext.ns('App.MDL');
var MDL = App.MDL;
App.MDL.OpsUnit = {
	Id: { name: 'Id', type: 'string' },
	Name: { name: 'Name', type: 'mstring' },
	Adress: { name: 'Adress', type: 'mstring' },
	FioBoss: { name: 'FioBoss', type: 'string' },
	SystemRegistrationDate: { name: 'SystemRegistrationDate', type: 'date' },
	ActivateDate: { name: 'ActivateDate', type: 'date' },
	Location: { name: 'Location', type: 'int' },
	LocationDisplay: { name: 'LocationDisplay', type: 'string' },
	OpsType: { name: 'OpsType', type: 'int' },
	OpsTypeDisplay: { name: 'OpsTypeDisplay', type: 'string' },
	Email: { name: 'Email', type: 'string' },
	Phone: { name: 'Phone', type: 'string' },
	Singer: { name: 'Singer', type: 'string' },
	Bin: { name: 'Bin', type: 'string' },
	Site: { name: 'Site', type: 'string' },
	SmSertNumber: { name: 'SmSertNumber', type: 'string' },
	AttestateNumber: { name: 'AttestateNumber', type: 'string' },
	SertAkriditationDate: { name: 'SertAkriditationDate', type: 'date' },
	Login: { name: 'Login', type: 'string' },
	PostCode: { name: 'PostCode', type: 'string' },
	Blocked: { name: 'Blocked', type: 'boolean' },
	LastSertNumber: { name: 'LastSertNumber', type: 'int' },
	Password: { name: 'Password', type: 'string' }
}
App.MDL.User = {
	Id: { name: 'Id', type: 'string' },
	ShortName: { name: 'ShortName', type: 'string' },
	Iin: { name: 'Iin', type: 'string' },
	Password: { name: 'Password', type: 'string' },
	LastActivityDate: { name: 'LastActivityDate', type: 'date' },
	Login: { name: 'Login', type: 'string' },
	OnLine: { name: 'OnLine', type: 'string' }
}
App.MDL.Employe = {
	Id: { name: 'Id', type: 'string' },
	FullName: { name: 'FullName', type: 'string' },
	FirstName: { name: 'FirstName', type: 'string' },
	SecondName: { name: 'SecondName', type: 'string' },
	ThirdName: { name: 'ThirdName', type: 'string' },
	Iin: { name: 'Iin', type: 'string' },
	Role: { name: 'Role', type: 'int' },
	RoleDisplay: { name: 'RoleDisplay', type: 'string' },
	Email: { name: 'Email', type: 'string' },
	HomeTel: { name: 'HomeTel', type: 'string' },
	MobTel: { name: 'MobTel', type: 'string' },
	Status: { name: 'Status', type: 'int' },
	Gender: { name: 'Gender', type: 'string' },
	LastActivity: { name: 'LastActivity', type: 'date' },
	Login: { name: 'Login', type: 'string' },
	Password: { name: 'Password', type: 'string' }
}
App.MDL.SmEntry = {
	Id: { name: 'Id', type: 'string' },
	Status: { name: 'Status', type: 'int' },
	StatusDisplay: { name: 'StatusDisplay', type: 'string' },
	Declarant: { name: 'Declarant', type: 'int' },
	DeclarantDisplay: { name: 'DeclarantDisplay', type: 'string' },
	SmTypeSertId: { name: 'SmTypeSertId', type: 'int' },
	SmTypeSert: { name: 'SmTypeSert'},
	Auditor: { name: 'Auditor', type: 'int' },
	AuditorDisplay: { name: 'AuditorDisplay', type: 'string' },
	Autor: { name: 'Autor', type: 'string' },
	TeamFIO: { name: 'TeamFIO', type: 'string' },
	AreaSertification: { name: 'AreaSertification', type: 'string' },
	DeclarantFio: { name: 'DeclarantFio', type: 'string' },
	DeclarantEmail: { name: 'DeclarantEmail', type: 'string' },
	DeclarantPhone: { name: 'DeclarantPhone', type: 'string' },
	CountYards: { name: 'CountYards', type: 'int' },
	CountEmployes: { name: 'CountEmployes', type: 'int' },
	RegistrationDate: { name: 'RegistrationDate', type: 'date' },
	EntryDate: { name: 'EntryDate', type: 'date' },
	StartResearch: { name: 'StartResearch', type: 'date' },
	FinishResearch: { name: 'FinishResearch', type: 'date' },
	ResultResearch: { name: 'ResultResearch', type: 'string' },
	ResultResearchDate: { name: 'ResultResearchDate', type: 'date' },
	ReviewDescription: { name: 'ReviewDescription', type: 'string' }
}
App.MDL.Clients = {
	Id: { name: 'Id', type: 'string' },
	Name: { name: 'Name', type: 'mstring' },
	NameNew: { name: 'NameNew', type: 'mstring' },
	Location: { name: 'Location', type: 'int' },
	LocationDisplay: { name: 'LocationDisplay', type: 'string' },
	RegistrationDate: { name: 'RegistrationDate', type: 'date' },
	Email: { name: 'Email', type: 'string' },
	Phone: { name: 'Phone', type: 'string' },
	Adress: { name: 'Adress', type: 'mstring' },
	Bin: { name: 'Bin', type: 'string' },
	BinNew: { name: 'BinNew', type: 'string' },
	FioBoss: { name: 'FioBoss', type: 'string' },
}
App.MDL.SmSertificate = {
	Id: { name: 'Id', type: 'string' },
	Status: { name: 'Status', type: 'int' },
	StatusDisplay: { name: 'StatusDisplay', type: 'string' },
	StartReseachDate: { name: 'StartReseachDate', type: 'date' },
	ResultReseachDate: { name: 'ResultReseachDate', type: 'date' },
	SertifikateSmNumber: { name: 'SertifikateSmNumber', type: 'string' },
	SystemKz: { name: 'SystemKz', type: 'string' },
	SystemEn: { name: 'SystemEn', type: 'string' },
	System: { name: 'System', type: 'string' },
	AreaSertificationKz: { name: 'AreaSertificationKz', type: 'string' },
	AreaSertificationEn: { name: 'AreaSertificationEn', type: 'string' },
	AreaSertification: { name: 'AreaSertification', type: 'string' },
	RequirementsKz: { name: 'RequirementsKz', type: 'string' },
	RequirementsEn: { name: 'RequirementsEn', type: 'string' },
	Requirements: { name: 'Requirements', type: 'string' },
	AreaSertificationAppendixKz: { name: 'AreaSertificationAppendixKz', type: 'string' },
	AreaSertificationAppendixEn: { name: 'AreaSertificationAppendixEn', type: 'string' },
	AreaSertificationAppendix: { name: 'AreaSertificationAppendix', type: 'string' },
	Declarant: { name: 'Declarant', type: 'string' },
	Signatory: { name: 'Signatory', type: 'string' },
	StartDate: { name: 'StartDate', type: 'date' },
	FinishDate: { name: 'FinishDate', type: 'date' },
	InspectionDate: { name: 'InspectionDate', type: 'date' },
	RegistrationEntryDate: { name: 'RegistrationEntryDate', type: 'date' },
	EntryDate: { name: 'EntryDate', type: 'date' },
	BlankNumber: { name: 'BlankNumber', type: 'string' },
	Counter: { name: 'Counter', type: 'int' },
	CounterDisplay: { name: 'CounterDisplay', type: 'string' },
	BlankNumberAppendix: { name: 'BlankNumberAppendix', type: 'mstring' },
	Auditor: { name: 'Auditor', type: 'string' },
	AuditorId: { name: 'AuditorId', type: 'int' },
	SertificationType: { name: 'SertificationType', type: 'int' },
	SertificationTypeDisplay: { name: 'SertificationTypeDisplay', type: 'string' },
	SmSertificationType: { name: 'SmSertificationType', type: 'int' },
	SmSertificationTypeDisplay: { name: 'SmSertificationTypeDisplay', type: 'string' },
	DublicateReason: { name: 'DublicateReason', type: 'int' },
	SertDate: { name: 'SertDate', type: 'date' },
	FirstRegDate: { name: 'FirstRegDate', type: 'date' },
}
App.MDL.SmInspection = {
	Id: { name: 'Id', type: 'string' },
	SertId: { name: 'SertId', type: 'int' },
	StartDate: { name: 'StartDate', type: 'string' },
	FinishDate: { name: 'FinishDate', type: 'string' },
	DateInspection: { name: 'DateInspection', type: 'date' },
	Status: { name: 'Status', type: 'int' },
	StatusDisplay: { name: 'StatusDisplay', type: 'string' },
	StatusInsp: { name: 'StatusInsp', type: 'int' },
	StatusInspDisplay: { name: 'StatusInspDisplay', type: 'string' },
	Auditor: { name: 'Auditor', type: 'string' },
	Result: { name: 'Result', type: 'string' },
	Declarant: { name: 'Declarant', type: 'string' },
	BlankNumber: { name: 'BlankNumber', type: 'string' },
	SertNumber: { name: 'SertNumber', type: 'string' }
}
App.MDL.SmTransfer = {
	Id: { name: 'Id', type: 'string' },
	Date: { name: 'Date', type: 'date' },
	Auditor: { name: 'Auditor', type: 'string' },
	AuditorFio: { name: 'AuditorFio', type: 'string' },
	Recipient: { name: 'Recipient', type: 'string' },
	Sertificate: { name: 'Sertificate', type: 'string' },
	BlankNumber: { name: 'BlankNumber', type: 'string' },
}