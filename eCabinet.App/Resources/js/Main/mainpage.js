﻿Ext.define('App.view.MainPage', {
	extend: 'Ext.panel.Panel',
	cls: 'panel-base',
	initComponent: function () {
		var me = this;
		me.tmp = Ext.create('App.view.MainPage.MainPageContainer', { region: 'center' });

		Ext.apply(this, {
			layout: 'border',
			items: [me.tmp]
		});
		this.callParent();
	}
});

var timers = [];


Ext.onReady(function () {
	var mui = Ext.create('App.view.MainPage');
	Ext.create('Ext.Viewport', {
		layout: 'fit',
		items: [mui]
	});
});

Ext.define('App.view.MainPage.MainPageContainer', {
	extend: 'Ext.panel.Panel',
	xtype: 'widgets',

	viewModel: {
		type: 'widgets'
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	cls: 'inocards-container base-panel',
	//	style: "background-color: #f6f6f6;",
	defaults: {
		xtype: 'panel'
	},
	items: [
				{
					xtype: 'panel',
					cls: 'inocards-container',
					id: "infoPanel",
					layout: {
						type: 'hbox',
						pack: 'start',
						align: 'stretchmax'
					},
					defaults: {
						flex: 1,
					},
					listeners: {
						afterRender: function () {
							App.ajax.post("GetInformers", {controller: "MainPage"}, function (resp) {
								if (!resp) return;
								var intrv = [];
								for (var i = 0; i < resp.length; i++) {
									var item = {
										xtype: 'widget-info',
										data: {
											amount: resp[i].Count,
											type: resp[i].Title,
											icon: resp[i].Icon,
											color: resp[i].Color,
											id: resp[i].Id,
										}
									};
									Ext.getCmp("infoPanel").add(item);
									var io = { interval: resp[i].Interval, id: resp[i].Provider }
									if (checkArr(intrv, io.id)) continue;
									intrv.push(io);
								}
								for (var j = 0; j < intrv.length; j++) {
									timers.push(setInterval(UpdateInformers, intrv[j].interval, intrv[j].id));
								}

							}, null, null, null, function () { }, null, true);
						}
					},
				},
				{
					xtype: 'panel',
					cls: 'inocards-container ',
					flex: 1,
					layout: {
						type: 'hbox',
						pack: 'start',
						align: 'stretch',
					},
					defaults: {
						flex: 1,
					},
					items: [
					{
						xtype: 'mainPagePanel',

					},
					{
						xtype: 'notifyPanel',
					},
					]
				},
	]
});

function UpdateInformers(provider) {
//	Ext.Ajax.request({
//		url: this.buildurl("GetInformersByProvider"),
//		timeout: (1000 * 60) * 1,
//		method: 'GET',
//		params: { id: provider },
//		action: "GetInformersByProvider",
//		completed: false,
//		success: function (resp, opt) {
//			if (resp.status == 200) {
//				var res = {};
//				try {
//					res = Ext.JSON.decode(resp.responseText);
//				} catch (e) {
//					return this.onError(resp, opt, false);
//				}
//				if (!res) return;
//				for (var i = 0; i < res.data.length; i++) {
//					var item = firstItem(Ext.getCmp("infoPanel").items.items, res.data[i].Id);
//					item.update({
//						amount: res.data[i].Count,
//						type: res.data[i].Title,
//						icon: res.data[i].Icon,
//						color: res.data[i].Color,
//						id: res.data[i].Id
//					});
//				}
//				
//			}
//			return null;
//		}
//	});

	App.ajax.post("GetInformersByProvider", { id: provider, controller: "SmMainPage" }, function (resp) {
		if (!resp) return;
		for (var i = 0; i < resp.length; i++) {
			var item = firstItem(Ext.getCmp("infoPanel").items.items, resp[i].Id);
			if (item) {
				item.update({
					amount: resp[i].Count,
					type: resp[i].Title,
					icon: resp[i].Icon,
					color: resp[i].Color,
					id: resp[i].Id
				});
			} else {
				item = {
					xtype: 'widget-info',
					data: {
						amount: resp[i].Count,
						type: resp[i].Title,
						icon: resp[i].Icon,
						color: resp[i].Color,
						id: resp[i].Id,
					}
				};
				Ext.getCmp("infoPanel").add(item);
			}
		}
	}, null, null, null, function() {
		for (var i = 0; i < timers.length; i++) {
			clearInterval(timers[i]);
		}
		return true;
	}, null, true);
}

function firstItem(arr, id) {
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].data.id === id) return arr[i];
	}
	return null;
}

function checkArr(arr, id) {
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].id === id) return true;
	}
	return false;
}

Ext.define('App.view.MainPage.WidgetInfo', {
	extend: 'Ext.panel.Panel',
	xtype: 'widget-info',
	height: 160,
	cls: 'admin-widget info-card-item info-card-large-wrap',
	tpl: '<div style="border-top: 3px solid {color}" ><span style="background-color: {color}" class="x-fa {icon}"></span><h2>{amount}</h2><div class="infodiv">{type}</div></div>',
	});

Ext.define('KitchenSink.model.Restaurant', {
	//    extend: 'KitchenSink.model.Base',
	fields: ['name', 'cuisine', 'time', 'index']
});

Ext.define('KitchenSink.view.grid.GroupedGrid', {

});

Ext.define('App.view.MainPage.widgets.WidgetA', {
	extend: 'Ext.panel.Panel',
	xtype: 'widget-a',
	//	draggable: true,
	cls: 'admin-widget shadow-panel',

	data: {
		nameHtml: '',
		roleHtml: '',
		imgSrc: '',
		color: ''
	},

	initComponent: function () {
		var me = this;

		Ext.apply(me, {
			items: [{
				xtype: 'image',
				cls: 'widget-top-container-first-img',
				height: 66,
				width: 66,
				alt: 'profile-image',
				src: me.config.data.imgSrc
			},

			{
				xtype: 'component',
				cls: 'widget-top-first-container postion-class',

				style: String.format("background-color: {0};", me.config.data.color != undefined ? me.config.data.color : '#468fd3'),
				height: 135
			},

			{
				xtype: 'container',
				cls: 'widget-bottom-first-container postion-class',
				height: 135,
				padding: '30 0 0 0',
				layout: {
					type: 'vbox',
					align: 'center'
				},
				items: [
					{
						xtype: 'label',
						cls: 'widget-name-text',
						html: me.config.data.nameHtml
					},
					{
						xtype: 'label',
						html: me.config.data.roleHtml
					},
					{
						xtype: 'toolbar',
						cls: 'widget-tool-button',
						flex: 1,
						items: [
							{
								ui: 'soft-green',
								text: 'Follow'
							},
							{
								ui: 'soft-blue',
								text: 'Message'
							}
						]
					}
				]
			}]
		});

		me.callParent(arguments);
	}
});

Ext.define('App.view.MainPage.widgets.WidgetB', {
	extend: 'Ext.panel.Panel',
	xtype: 'widget-b',
	//	draggable: true,
	cls: 'admin-widget shadow-panel',

	items: [
        {
        	xtype: 'image',
        	cls: 'widget-top-container-first-img',
        	height: 66,
        	width: 66,
        	alt: 'profile-image',
        	src: userPhoto
        },
        {
        	xtype: 'component',
        	cls: 'widget-top-second-container postion-class',
        	height: 135
        },
        {
        	xtype: 'container',
        	cls: 'widget-bottom-first-container postion-class',
        	height: 135,
        	padding: '30 0 0 0',
        	layout: {
        		type: 'vbox',
        		align: 'center'
        	},
        	items: [
                {
                	xtype: 'label',
                	cls: 'widget-name-text',
                	html: 'Lucy Moon'
                },
                {
                	xtype: 'label',
                	html: 'Web and Graphic designer'
                },
                {
                	xtype: 'toolbar',
                	flex: 1,
                	items: [
                        {
                        	ui: 'blue',
                        	iconCls: 'x-fa fa-facebook'
                        },
                        {
                        	ui: 'soft-cyan',
                        	iconCls: 'x-fa fa-twitter'
                        },
                        {
                        	ui: 'soft-red',
                        	iconCls: 'x-fa fa-google-plus'
                        },
                        {
                        	ui: 'soft-purple',
                        	iconCls: 'x-fa fa-envelope'
                        }
                	]
                }
        	]
        }
	]
});

Ext.define('App.view.MainPage.widgets.WidgetC', {
	extend: 'Ext.panel.Panel',
	xtype: 'widget-c',
	//	draggable: true,
	cls: 'admin-widget shadow-panel',

	items: [
        {
        	xtype: 'image',
        	cls: 'widget-top-container-first-img',
        	height: 66,
        	width: 66,
        	alt: 'profile-image',
        	src: userPhoto
        },
        {
        	xtype: 'component',
        	cls: 'widget-top-first-third-container postion-class',
        	height: 135
        },
        {
        	xtype: 'container',
        	cls: 'widget-bottom-first-container postion-class',
        	height: 135,
        	padding: '30 0 0 0',
        	layout: {
        		type: 'vbox',
        		align: 'center',
        		pack: 'center'
        	},
        	items: [
                {
                	xtype: 'label',
                	cls: 'widget-name-text',
                	html: 'Donald Brown'
                },
                {
                	xtype: 'label',
                	html: 'Art Designer'
                },
                {
                	xtype: 'toolbar',
                	flex: 1,
                	cls: 'widget-follower-toolbar',
                	width: '100%',
                	margin: '20 0 0 0',
                	defaults: {
                		xtype: 'displayfield',
                		flex: 1,
                		labelAlign: 'top'
                	},
                	items: [
                        {
                        	value: '<div class="label">Following</div><div>1,345</div>'
                        },
                        {
                        	cls: 'widget-follower-tool-label',
                        	value: '<div class="label">Followers</div><div>23,456</div>'
                        },
                        {
                        	value: '<div class="label">Likes</div><div>52,678</div>'
                        }
                	]
                }
        	]
        }
	]
});

Ext.define('App.view.MainPage.widgets.WidgetD', {
	extend: 'Ext.panel.Panel',
	xtype: 'widget-d',
	//	draggable: true,
	cls: 'admin-widget shadow-panel',

	items: [
        {
        	xtype: 'image',
        	cls: 'widget-top-container-first-img',
        	height: 66,
        	width: 66,
        	alt: 'profile-image',
        	src: 'resources/images/user-profile/2.png'
        },
        {
        	xtype: 'component',
        	cls: 'widget-top-first-fourth-container postion-class',
        	height: 135
        },
        {
        	xtype: 'container',
        	cls: 'widget-bottom-first-container postion-class',
        	height: 135,
        	padding: '30 0 0 0',
        	layout: {
        		type: 'vbox',
        		align: 'center',
        		pack: 'center'
        	},
        	items: [
                {
                	xtype: 'label',
                	cls: 'widget-name-text',
                	html: 'Goff Smith'
                },
                {
                	xtype: 'label',
                	html: 'Project manager'
                },
                {
                	xtype: 'toolbar',
                	flex: 1,
                	cls: 'widget-tool-button',
                	items: [
                        {
                        	ui: 'soft-green',
                        	text: 'Follow'
                        },
                        {
                        	ui: 'soft-blue',
                        	text: 'Message'
                        }
                	]
                }
        	]
        }
	]
});


Ext.define('App.view.MainPage.notifyPanel', {
	extend: 'Ext.panel.Panel',
	xtype: 'notifyPanel',
	title: $_CS.Common.Notifications,
	width: '100%',
	layout: 'fit',
	cls: 'widget-notify',
	loader: {
		url: "/notification/dashboard/index/1",
		renderer: 'frame',
		loadMask: true,
		loadOnRender: true,
		autoLoad: true
	}
});


