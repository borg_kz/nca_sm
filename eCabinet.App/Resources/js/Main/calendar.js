﻿var updatePeriod = 60000;
var calendarTimerId = 0;
var calendarTimeoutId = 0;
Ext.define('KitchenSink.store.Restaurants', {
	extend: 'Ext.data.Store',
	storeId: 'restaurants',
	groupField: 'Order',
	autoLoad: true,
	proxy: {
		type: 'ajax',
		url: App.ajax.buildurl('GetCurrentUserEvents', 'CabinetMainPage'),
		reader: {
			type: 'json',
			rootProperty: 'data'
		}
	},
	model: new Ext.data.Model({
		fields: ['Oid', 'Name', 'StartTime', 'EndTime', 'IconCls', 'Group', 'TimeToChange', 'Order']
	}),
	sorters:[
		{
			property: "Order",
			direction: "ASC"
		},
		{
			property: "StartTime",
			direction: "ASC"
		}
	],
//	sorters: ['Order', 'StartTime', 'EndTime'],
});
Ext.define('Ext.ux.IFrame', {
	extend: 'Ext.Component',
	xtype: 'mainPagePanel',
	alias: 'widget.uxiframe',
	title: $_CS.Common.News,
	loadMask: App.$STR.Loading,
	layout: 'fit',
	cls: 'widget-chart .x-panel-body-default',
	src: 'about:blank',
	renderTpl: [
		'<iframe src="../News" id="{id}-iframeEl" data-ref="iframeEl" name="{frameName}" width="100%" height="100%" frameborder="0"></iframe>'
	],
	childEls: ['iframeEl']
});


Ext.define('App.view.MainPage.Calendar', {
	
	layout: 'fit',
	emptyText: $_CS.Common.NoEvents,
	id: "MyCalendar",
	extend: 'Ext.grid.Panel',
	cls: 'widget-chart .x-panel-body-default',

	requires: [
        'Ext.grid.feature.Grouping'
	],
	collapsible: false,
	//frame: true,
//	width: 600,
//	height: 400,
	minHeight: 200,
	title: $_CS.MainPage.MyEventsCalendar,
	resizable: true,

		features: [{
			ftype: 'grouping',
//			groupHeaderTpl: '<div style="background:#EDEDED;height:100%"><b>{[values.name == 1 ? "Завтра1" : (values.name == 2 ? "Сегодня" : "Вчера")]} ({rows.length} шт.)</b></div>',
			groupHeaderTpl: '<div style="background:#EDEDED;height:100%"><b>{[values.name == 1 ? ' + App.$STR.Tomorrow + ' : (values.name == 2 ? ' + App.$STR.Today + ' : ' + App.$STR.Yesterday + ')]} ({rows.length} ' + $_CS.Common.Pcs + ')</b></div>',
			//		groupHeaderTpl: '<b>{columnName}</b>',
			hideGroupedHeader: true,
			startCollapsed: false,
			id: 'restaurantGrouping'
		}],

		viewConfig: {
			xtype: "gridview",
			listeners: {
				refresh: {
					fn: function (view) {

						var nodes = view.getNodes();
						var minTimer = 0;
						for (var i = 0; i < nodes.length; i++) {
							var node = nodes[i];
							var record = view.getRecord(node);
							var cells = Ext.get(node).query('td');

							for (var j = 0; j < cells.length; j++) {
								console.log(cells[j]);
								Ext.fly(cells[j]).setStyle('background-color', record.data.Color);
							}
							if (record.data.TimeToChange > 0) {
								if (record.data.TimeToChange * 1000 < updatePeriod && (minTimer === 0 || minTimer > record.data.TimeToChange)) minTimer = record.data.TimeToChange;
							}
						}
						if (timers.indexOf(calendarTimerId) <= -1) {
							calendarTimerId = setInterval(function() {
								Ext.getCmp("MyCalendar").store.reload();
							}, updatePeriod);
							timers.push(calendarTimerId);
						}
						if (minTimer > 0 && calendarTimeoutId === 0)
							calendarTimeoutId = setTimeout(function () {
								Ext.getCmp("MyCalendar").store.reload();
								calendarTimeoutId = 0;
							}, minTimer * 1000);
					}
				}
			}
		},

	initComponent: function () {
		this.store = new KitchenSink.store.Restaurants();
		this.columns = [{
			text: App.$STR.Events,
			flex: 1,
			dataIndex: 'Name'
		}, {
			text: 'C',
			width: 60,
			dataIndex: 'StartTime'
		}, {
			text: $_CS.Common.To,
			width: 60,
			dataIndex: 'EndTime'
		}];

		this.callParent();

		var store = this.getStore(),
            toggleMenu = [];

		this.groupingFeature = this.view.getFeature('restaurantGrouping');

		// Create checkbox menu items to toggle associated group
		//		store.getGroups().each(function (group) {
		//			toggleMenu.push({
		//				xtype: 'menucheckitem',
		//				text: group.getGroupKey(),
		//				handler: this.toggleGroup,
		//				scope: this
		//			});
		//		}, this);
		//
		//		this.addDocked([{
		//			xtype: 'toolbar',
		//			items: ['->', {
		//				text: 'Toggle groups...',
		//				destroyMenu: true,
		//				menu: toggleMenu
		//			}]
		//		}, {
		//			xtype: 'toolbar',
		//			ui: 'footer',
		//			dock: 'bottom',
		//			items: ['->', {
		//				text: 'Clear Grouping',
		//				iconCls: 'icon-clear-group',
		//				scope: this,
		//				handler: this.onClearGroupingClick
		//			}]
		//		}]);
		//
		//		this.mon(this.store, 'groupchange', this.onGroupChange, this);
		//		this.mon(this.view, {
		//			groupcollapse: this.onGroupCollapse,
		//			groupexpand: this.onGroupExpand,
		//			scope: this
		//		});
	},

	onClearGroupingClick: function () {
		this.groupingFeature.disable();
	},

	toggleGroup: function (item) {
		var groupName = item.text;
		if (item.checked) {
			this.groupingFeature.expand(groupName, true);
		} else {
			this.groupingFeature.collapse(groupName, true);
		}
	},

	onGroupChange: function (store, grouper) {
		var grouped = store.isGrouped(),
            groupBy = grouper ? grouper.getProperty() : '',
            toggleMenuItems, len, i = 0;

		// Clear grouping button only valid if the store is grouped
		this.down('[text=Clear Grouping]').setDisabled(!grouped);

		// Sync state of group toggle checkboxes
		if (grouped && groupBy === 'cuisine') {
			toggleMenuItems = this.down('button[text=Toggle groups...]').menu.items.items;
			for (len = toggleMenuItems.length; i < len; i++) {
				toggleMenuItems[i].setChecked(
                    this.groupingFeature.isExpanded(toggleMenuItems[i].text)
                );
			}
			this.down('[text=Toggle groups...]').enable();
		} else {
			this.down('[text=Toggle groups...]').disable();
		}
	},

	onGroupCollapse: function (v, n, groupName) {
		if (!this.down('[text=Toggle groups...]').disabled) {
			this.down('menucheckitem[text=' + groupName + ']').setChecked(false, true);
		}
	},

	onGroupExpand: function (v, n, groupName) {
		if (!this.down('[text=Toggle groups...]').disabled) {
			this.down('menucheckitem[text=' + groupName + ']').setChecked(true, true);
		}
	}
});