﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Configuration;
using App.Data.Xpo.Menu;
using App.Data.Xpo.Provider;
using App.Framework;
using App.Framework.Initializers;
using App.Framework.Providers;
using App.Framework.Xpo;
using App.Properties;
using App.Provider;
using App.Settings;
using App.UI;
using App.UI.Button;
using App.UI.MainPageStruct;
using App.UI.ToolBar;
using App.Web;
using DevExpress.Xpo;
using ECabinet.Core.Initializers;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Properties;

namespace ECabinetApp {
	public class ECabinetApp : AppHttpApplication {
		protected override void RegistrationInitializers(FwInitializerCollection initializers) {
			initializers.Add(new FwXpoInitializer(WebConfigurationManager.ConnectionStrings["ECabinetApp_ConnectionString"].ConnectionString));
			initializers.Add(new DataInitializer(Path.Combine(FwGlobal.CurrentDomain.BaseDirectory, "init.xml")));
			initializers.Add(new MenuInitializer());
			base.RegistrationInitializers(initializers);
		}

		public override AppUiMpStruct MainPageStructure {
			get {
				return new AppUiMpStruct {
					Layout = new AppUiLayout {
						ActionBars = new List<AppUiToolBarBase> {
									new AppUiToolBar {
										Items = new List<AppUiBase> {
												new AppUiToolBarFill(), new AppUiLangSelector(), new AppUiToolBarSpacer(), new AppUiToolBarSeparator(), new AppUiToolBarSpacer(),
												new AppUiButton {
													IconCls = "fa-question-circle", ToolTip = AppStrings.Help,
													Items = new List<AppUiBase> {new AppUiButton {Text = AppStrings.UserManual, IconCls = "fa-list"}, new AppUiToolBarSeparator(),
														new AppUiButton {Text = AppStrings.Feedback, IconCls = "fa-rss", JsHandler = "App.msg.toast.info('Данная функция еще не реализована')"},
														new AppUiButton {Text = AppStrings.WriteToAdministrator, IconCls = "fa-envelope", JsHandler = "App.msg.toast.info('Данная функция еще не реализована')"}
													}
												},
												new AppUiToolBarSpacer(), new AppUiToolBarSeparator(), new AppUiToolBarSpacer(),
												new AppUiUserMenu { Disabled = false, Hidden = false,
													Items = new List<AppUiBase> {
															new AppUiButton { Text = AppStrings.Theme, IconCls = "fa-eye", ValidateExpression = () => false,
																Items = new List<AppUiBase> {
																		new AppUiButton {Text = AppStrings.Theme_Neptune, ThemeName = "neptune", JsHandler = "var me = Ext.getCmp('base-top-panel'); me.fireEvent('onThemeChange', me, this);"},
																		new AppUiButton {Text = AppStrings.Theme_Crisp, ThemeName = "crisp", JsHandler = "var me = Ext.getCmp('base-top-panel'); me.fireEvent('onThemeChange', me, this)"},
																		new AppUiButton {Text = AppStrings.Theme_Classic, ThemeName = "default", JsHandler = "var me = Ext.getCmp('base-top-panel'); me.fireEvent('onThemeChange', me, this);", Disabled = true},
																		new AppUiButton {Text = AppStrings.Theme_Gray, ThemeName = "gray", JsHandler = "var me = Ext.getCmp('base-top-panel'); me.fireEvent('onThemeChange', me, this);", Disabled = true},
																		new AppUiButton {Text = AppStrings.Theme_Triton, ThemeName = "triton", JsHandler = "var me = Ext.getCmp('base-top-panel'); me.fireEvent('onThemeChange', me, this);"},
																}
															},
//															new AppUiToolBarSeparator(), new AppUiButton {Text = "Сменить пароль", IconCls = "fa-asterisk", JsHandler = "me.fireEvent('onPasswordChange', me, this);"},
															new AppUiButton {Text = AppStrings.ChangeRole, IconCls = "fa-user", JsHandler = "var me = Ext.getCmp('base-top-panel'); me.fireEvent('onRoleChange', me, this);"},
													}
												},
												new AppUiToolBarSpacer(), new AppUiButton {IconCls = "fa-sign-out", ToolTip = AppStrings.LogOut, JsHandler = "var me = Ext.getCmp('base-top-panel'); me.fireEvent('onLogOut', me, this);"}
											}
									}
								}
					}
				};
			}
		}

		protected override void RegistrationProviders(FwProviderCollection providers) {
			providers.Add(new AppResourceProvider());
			providers.Add(new AppSettingsProvider());
			providers.Add(new AppAuditProvider());
			providers.Add(new AppExceptionProvider());
			providers.Add(new AppMonitoringProvider());
			providers.Add(new AppMenuProvider());
			providers.Add(new AppSecurityProvider());
			// providers.Add(new AppLocalFileProvider(FwGlobal.CurrentDomain.BaseDirectory));
			providers.Add(new InfoPanelsProvider());

			base.RegistrationProviders(providers);
		} 

		protected override IAppSystemSettings SystemSettings {
			get { return new Settings(); }
		}
	}

	public class InfoPanelsProvider : IAppInformationPanelProvider {
		public void Dispose() {

		}

		public void Init() {
		}

		public List<AppInfoPanelObject> GetData() {
			using (var uow = new FwXpoSession()) {
				var list = new List<AppInfoPanelObject>();
				list.Add(new AppInfoPanelObject("myToday", Strings.Common_Certifications, "#FFB222", "fa-file-text-o", new XPQuery<MSmSertificate>(uow).Count(), 1));
				list.Add(new AppInfoPanelObject("myAll", Strings.Common_Applications, "#00A316", "fa-street-view", new XPQuery<MSmEntry>(uow).Count(), 2));
				list.Add(new AppInfoPanelObject("systemToday", Strings.Common_SmkToday, "#CEA112", "fa-calendar-check-o", new XPQuery<MSmSertificate>(uow).Count(a=> a.CreateTime.Date == DateTime.Now.Date), 3));
				list.Add(new AppInfoPanelObject("systemAll", Strings.Common_ApplicationsToday, "#0088B5", "fa-calendar", new XPQuery<MSmEntry>(uow).Count(a => a.RegistrationDate.Date == DateTime.Now.Date), 4));
				return list;
			}
		}

		public int UpdatePeriodInMiliseconds {
			get { return 60 * 10 * 1000; }//каждые 10 минут

		}
	}
}
