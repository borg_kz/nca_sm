﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Framework;
using App.Framework.Utils;
using App.Framework.Web.Mvc.Results;
using App.Web.MVC.Controllers;
using ECabinet.Core.Managers;
using ECabinet.Data.Model.Entities.OrgStructure;
using App.Data.Proxy;
using App.Data.Xpo.Extensions;
using App.Data.Xpo.Model.Entities.Auth.Security;
using ECabinet.Core;

namespace ECabinetApp.Controllers {
	public class CommonController : AppAuthorizeController {
		protected CommonManager CommonManager { get { return Managers.Find<CommonManager>(); } }
		
		public MOpsUnit CurrentOps {
			get { return CabinetApp.GetCurrentOps(CommonManager.Uow); }
		} 

		public JResult LoadAllCityes() {
			return JResult.Success(CommonManager.LoadCities(false).Items.ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}

		public JResult LoadLocalCityes() {
			var city = CommonManager.LoadCities();
			return JResult.Success(city.Items.ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}

		public JResult LoadLocalRegions() {
			return JResult.Success(CommonManager.LoadRegions().Items.ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}

		public JResult LoadAllRegions() {
			return JResult.Success(CommonManager.LoadRegions(false).Items.ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}
		public JResult RefSmDublicateReason() {
			return JResult.Success(CommonManager.RefSmDublicateReason().Items.ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}

		[HttpPost]
		public JResult LoadAreas() {
			return JResult.Success(CommonManager.LoadAreas(false).Items.ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}
		
		/// <summary> Загрузка справочника полов </summary>
		[HttpPost]
		public JResult LoadGenders() {
			return JResult.Success(new List<AppProxyIdName> {
				new AppProxyIdName((int)EGender.Female, EGender.Female.GetLocalizedField()),
				new AppProxyIdName((int)EGender.Male, EGender.Male.GetLocalizedField()),
				new AppProxyIdName((int)EGender.Unknown, EGender.Unknown.GetLocalizedField())
			});
		}

		[Obsolete("Временное решение до появление нормльаного мониторинга.")]
		public void MonitoringWrite(string message) { }

		public MAppUserRole CurrentUserRole {
			get { return CurrentUser.GetUser(CommonManager.Uow).CurrentRole; }
		}
	}
}