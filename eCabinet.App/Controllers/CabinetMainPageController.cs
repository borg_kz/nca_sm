﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Bundle;
using App.Framework.Web.Mvc.Results;
using App.Web.MVC.Controllers;
using DocumentFormat.OpenXml.ExtendedProperties;

namespace ECabinetApp.Controllers {
	public class CabinetMainPageController : AppAuthorizeController {
		public ActionResult Index() {
			return View("CabinetMainPage");
		}

		#region Bundles

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/Main").IncludeDirectory("~/Resources/js/Main", "*.js", true);
		public static AppStyleBundle Style = new AppStyleBundle(Plugin.Current, "~/res/css/Main").IncludeDirectory("~/Resources/css/Main", "*.css", true);

		#endregion

		/// <summary> Менеджер </summary>
		//public MainManager Manager { get { return Managers.Find<MainManager>(); } }

		/// <summary> Взять события для текущего пользователя </summary>
		public JResult GetCurrentUserEvents() {
//			var ee = Manager.GetAllEvents();
//			var yest = DateTime.Now.AddDays(-1).Date;
//			var tod = DateTime.Now.Date;
//			var tom = DateTime.Now.AddDays(1).Date;
//			ee = ee.Where(a => a.IsNameOnly == false && ((a.StartDate.Date <= yest && a.EndDate.Date >= yest) ||
//				(a.StartDate.Date <= tom && a.EndDate.Date >= tom) ||
//				(a.StartDate.Date <= tod && a.EndDate.Date >= tod)) && a.TopParent.IsPublished);
//			ee = ee.Where(a => a.Participants.Any(b => b.Person.Oid == CurrentUser.PersonId));
//			string grp;
//			var ret = new List<object>();
//			foreach (var a in ee) {
//				if (a.StartDate.Date <= tom && a.EndDate.Date >= tom) {
//					grp = "Завтра";
//					AddEv(ret, grp, a, 1);
//				}
//				if (a.StartDate.Date <= tod && a.EndDate.Date >= tod) {
//					grp = "Сегодня";
//					AddEv(ret, grp, a, 2, true);
//				}
//				if (a.StartDate.Date <= yest && a.EndDate.Date >= yest) {
//					grp = "Вчера";
//					AddEv(ret, grp, a, 3, false, true);
//				}
//			}
			return JResult.Success();
		}
	}
}