﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Bundle;
using App.Framework.Web.Mvc.Results;
using App.Framework.Xpo;
using DevExpress.Xpo;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Properties;

namespace ECabinetApp.Controllers.Cabinet {
	public class NcaReportsController : CommonController {
		//
		// GET: /NcaReports/
		public ActionResult Index() {
			return View("NcaReports");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/ncaReports")
			.IncludeDirectory("~/Resources/js/SM/ncaReports", "*.js", false);

		public JResult LoadSmOps() {
			using (var uow = new FwXpoSession()) {
				if (CurrentUserRole.Role.Type == (int)ERoleType.Admin || CurrentUserRole.Role.Type == (int)ERoleType.GlobalAdmin)
					return
						JResult.Success(new XPQuery<MOpsTypes>(uow)
							.Where(a => a.OpsType == EOpsType.OpsSm)
							.OrderBy(a => a.OpsUnit.Name.L2)
							.ToList()
							.Select(r => new { Id = r.Oid, Name = r.OpsUnit.Name.ToString() }));
				return JResult.Success(new { Id = CurrentOps.Oid, Name = CurrentOps.Name.ToString() });
			}
		}

		public JResult LoadIafMD(int? start, int? limit, DateTime date, int? opsId = null) {
			//new AuditManager().Write("Отчёты СМ: Организации по регионам", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			using (var uow = new FwXpoSession()) {
				if (opsId == null)
					return JResult.Failure(Strings.Reports_OpsNotSelected);

				var validSerts = new XPQuery<MSmSertificate>(uow).Count(a => a.OpsUnit.Oid == opsId && a.StartDate <= date && a.FinishDate >= date 
					&& (a.Status == ESmSertStatus.Valid || a.Status == ESmSertStatus.Renewed || a.Status == ESmSertStatus.Suspended));
				
				var ops = new XPQuery<MOpsUnit>(uow).First(a => a.Oid == opsId);
				var auditors = new XPQuery<MEmploye>(uow).Count(a => a.OpsUnit.Oid == opsId && a.Role.Type == (int)ERoleType.SmAuditor);
				var serts = new XPQuery<MSmSertificate>(uow).Count(a => a.OpsUnit.Oid == opsId && a.Status != ESmSertStatus.Deleted);
				// var clients = new XPQuery<MClientUnit>(uow).Count(a => a.OpsUnits.Contains(ops));

				// var transfers = new XPQuery<MClientUnit>(uow).Count(a => a.OpsUnits.Count > 1 && a.OpsUnits.Contains(ops));
				var transfers = new XPQuery<MSmTransfers>(uow).Count(a => a.Recipient.Oid == ops.Oid && a.Date <= date);
				var transferOut = new XPQuery<MSmTransfers>(uow).Count(a => a.From.Oid == ops.Oid && a.Date <= date);


				var result = new List<Object>();
				result.Add(new { Index = Strings.Reports_OfValidCertificates, Value = validSerts + Strings.Common_Of + serts });
				result.Add(new { Index = Strings.Reports_OfAuditors, Value = auditors });
				result.Add(new { Index = "Трансферы входящие", Value = transfers });
				result.Add(new { Index = "Трансферы исходящие", Value = transferOut });
				result.Add(new { Index = Strings.Reports_OfOverdueAudits, Value = 0 });
				return JResult.Success(result);
			}
		}

		public JResult LoadAuditDays(int? start, int? limit, DateTime startDay, DateTime finish, long? opsId = null) {
			//new AuditManager().Write("Отчёты СМ: Организации по регионам", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			using (var uow = new FwXpoSession()) {
				if (opsId == null)
					return JResult.Failure(Strings.Reports_OpsNotSelected);

				var result = new List<Object>();
				var employes = new XPQuery<MEmploye>(uow).Where(a => a.OpsUnit.Oid == opsId && a.Role.Type == (int)ERoleType.SmAuditor).ToList();
				foreach (var employe in employes) {
					int count = 0;
					var teems = new XPQuery<MSmEntryTeam>(uow).Where(e => e.Person == employe.User.Person 
						&& e.StartResearch >= startDay && e.StartResearch <= finish && e.PositionType == EMemberType.Auditor).ToList();
					foreach (var team in teems) {
						count += (team.FinishResearch.Value - team.StartResearch.Value).Days + 1;
					}

					result.Add(new { Index = employe.User.Person.ShortName, Value = count });
				}
				
				return JResult.Success(result);
			}
		}

		public JResult LoadSertificates(int? start, int? limit, DateTime date) {
			//new AuditManager().Write("Отчёты СМ: Организации по регионам", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			using (var uow = new FwXpoSession()) {
				var sql = new XPQuery<MSmSertificate>(uow).Where(a => a.StartDate < date && a.FinishDate > date
				                                                      &&
				                                                      (a.Status == ESmSertStatus.Valid ||
				                                                       a.Status == ESmSertStatus.Renewed ||
				                                                       a.Status == ESmSertStatus.Suspended))
					.GroupBy(a => a.OpsUnit).ToArray();
				var list = new List<TmpNcaSertReport>();
				foreach (var g in sql) {
					list.Add(new TmpNcaSertReport { Declarant = g.Key.Name, LocationDisplay = g.Key.Location.Name, Counter = g.Count() });
				}
				return JResult.Success(list.OrderBy(a => a.Declarant));

			}
		}

		public JResult LoadAuditors(int? start, int? limit, DateTime date) {
			//new AuditManager().Write("Отчёты СМ: Организации по регионам", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			using (var uow = new FwXpoSession()) {
				var sql = new XPQuery<MEmploye>(uow).Where(a => a.Role.Type == (int)ERoleType.SmAuditor)
					.GroupBy(a => a.OpsUnit).ToArray();
				var list = new List<TmpNcaSertReport>();
				foreach (var g in sql) {
					if (g.Key != null)
						list.Add(new TmpNcaSertReport { Declarant = g.Key.Name, LocationDisplay = g.Key.Location.Name, Counter = g.Count() });
				}
				return JResult.Success(list.OrderBy(a => a.Declarant));

			}
		}
	}

	public class TmpNcaSertReport {
		public string Declarant { get; set; }
		public string LocationDisplay { get; set; }
		public int Counter { get; set; }

	}
}