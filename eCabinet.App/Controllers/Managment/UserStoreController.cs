﻿using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework.Web.Mvc.Results;
using DevExpress.Xpo;
using ECabinet.Core.Managers.Managment;
using ECabinet.Data.Model.Proxy.OrgStructure;
using Newtonsoft.Json;
using ECabinet.Data;
using ECabinet.Data.Properties;

namespace ECabinetApp.Controllers.Managment {
	public class UserStoreController : CommonController {
		public ActionResult Index() {
			return View("UserStore");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/Managment/UserStore")
			.IncludeDirectory("~/Resources/js/Managment/UserStore", "*.js", false);

		public UserManager Manager { get { return Managers.Find<UserManager>(); } }

		public JResult LoadSmRoles() {
			return JResult.Success(new XPQuery<MAppRole>(Manager.Uow).Where(a => a.Type > 1 && a.Type < 200).ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}

		public JResult LoadUsers(int? start, int? limit, int? roleId) {
			var extRes = Manager.GetUsersByRole(start, limit, roleId);
			return JResult.Success(extRes.Items.Select(a => new ProxyUser(a.User)), extRes.Count);
		}

		public JResult SaveUser(bool? isNew, string json) {
			if (CurrentUserRole.Role.Type != (int)ERoleType.Admin && CurrentUserRole.Role.Type != (int)ERoleType.GlobalAdmin)
				return JResult.Failure(Strings.Msg_HaventWritePrivilages);

			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Msg_JResultIsEmpty);
			var obj = JsonConvert.DeserializeObject<ProxyUser>(json);

			if (string.IsNullOrEmpty(obj.Password))
				return JResult.Failure(Strings.Msg_PasswordNotSecurity);

			Manager.SetPassword(obj.Id, obj.Password);
			return JResult.Success(Strings.Common_DataSuccessSaved);
		}
	}
}