﻿using System;
using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Xpo.Model.Proxy;
using App.Framework.Web.Mvc.Results;
using ECabinet.Core.Managers.Managment;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Properties;
using Newtonsoft.Json;

namespace ECabinetApp.Controllers.Managment {
	// [Privilege("OpsStore", PrivilegeGroups.Management, AccessType.AccessDenied | AccessType.Read | AccessType.Write | AccessType.Admin)]
	public class OpsStoreController : CommonController {
		public OpsManager Manager {
			get { return Managers.Find<OpsManager>(); }
		}

		public ActionResult Index() {
			return View("OpsStore");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/Managment/OpsStore")
			.IncludeDirectory("~/Resources/js/Managment/OpsStore", "*.js", false);

		/// <summary> Загрузка списка организаций </summary>
		public JResult GridLoadOpsUnits(int? start, int? limit, string sort, int? opsType) {
			var result = Manager.GridLoadOpsUnits(start, limit, opsType, ParseJsonSort<MOpsUnit, ProxyOpsUnit>(sort));
			return JResult.Success(result.Items.ToList().Select(a => new ProxyOpsUnit(a, EOpsType.OpsSm)), result.Count);
		}

		/// <summary> Создание нового ОПС </summary>
		public JResult AddOpsUnit(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json))
				return JResult.Failure(Strings.Msg_JResultIsEmpty);
			var obj = JsonConvert.DeserializeObject<ProxyOpsUnit>(json, new MultiLanguageConverter());
			Manager.AddOpsUnit(isNew.HasValue && isNew.Value ? (long?)null : Int32.Parse(obj.Id), obj);
			return JResult.Success(Strings.Common_DataSuccessSaved);
		}

		/// <summary> Изменение даных организации </summary>
		public JResult SaveOpsUnit(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json))
				return JResult.Failure(Strings.Msg_JResultIsEmpty);
			var obj = JsonConvert.DeserializeObject<ProxyOpsUnit>(json, new MultiLanguageConverter());
			Manager.SaveFullOpsInfo(isNew.HasValue && isNew.Value ? (long?)null : Int32.Parse(obj.Id), obj);
			return JResult.Success(Strings.Common_DataSuccessSaved);
		}

		/// <summary> Удаление организации </summary>
		public JResult DeleteOpsUnit(int? id) {
			if (!id.HasValue)
				return JResult.Failure(Strings.Msg_JResultIsEmpty);

			Manager.DeleteOpsUnit(id.Value);
			return JResult.Success(Strings.OpsStore_OrganizationDeleted);
		}
	}
}