﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Xml;
using App.Bundle;
using App.Data.Proxy;
using App.Framework.Web.Mvc.Results;
using DevExpress.Xpo;
using ECabinet.Core.Managers;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Properties;

namespace ECabinetApp.Controllers.Managment {
	// [Privilege("EntryManager", PrivilegeGroups.Management, AccessType.AccessDenied | AccessType.Admin)]
	public class EntryManagerController : CommonController {
		//
		// GET: /EntryManager/
		public ActionResult Index() {
			return View("EntryManager");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/Managment/EntryManager")
			.IncludeDirectory("~/Resources/js/Managment/EntryManager", "*.js", false);

		/// <summary> Заявки СМ по выбранному ОПС за определённый период дат. </summary>
		public JResult LoadSmEntryes(int? start, int? limit, DateTime? startDate, DateTime? finishDate, int? opsId) {
			if (CurrentUserRole.Role.Type > 2)
				return JResult.Failure(Strings.EntryManager_FunctionalOnlyForAdmin);
			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			if (opsId == null)
				return JResult.Failure(Strings.EntryManager_OpsNotFount_Select);

			MOpsUnit ops = null;
			switch ((ERoleType)CurrentUserRole.Role.Type) {
				case ERoleType.SmAuditor:
				case ERoleType.SmSecretary:
				case ERoleType.SmAdmin:
					ops = CurrentOps;
					break;
				default: ops = new XPQuery<MOpsUnit>(CommonManager.Uow).FirstOrDefault(q => q.Oid == opsId.Value);
					break;
			}

			if (ops == null)
				return JResult.Failure(Strings.Common_OpsNotFound);

			int count = new XPQuery<MSmSertificate>(CommonManager.Uow).Count(a => a.OpsUnit == ops && a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1));

			return JResult.Success(new XPQuery<MSmEntry>(CommonManager.Uow)
				.Where(a => a.OpsUnit == ops && a.EntryDate >= startDate.Value && a.EntryDate <= finishDate.Value.AddDays(1)).OrderBy(a => a.Status)
				.ThenByDescending(a => a.EntryDate).Skip(start.Value).Take(limit.Value)
				.Select(r => new ProxySmEntry(r)), count);
		}

		public JResult ChangeOpsForBlank(int opsUnitId, string blankNumber, int recipientId, DateTime date) {
			if (!Int32.TryParse(blankNumber, out var blank) || blankNumber.Trim().Count() > 7) {
				return JResult.Failure(Strings.EntryManager_InvalidEnteredSequence);
			}
			if (opsUnitId == recipientId)
				return JResult.Failure("Выбирите ОПС откуда был выполнен перевод и ОПС куда был выполнен перевод.");
			/**/

			var sert = new XPQuery<MSmSertificate>(CommonManager.Uow).SingleOrDefault(a => a.BlankNumber == blank);
			if (sert == null)
				return JResult.Failure(Strings.EntryManager_CertificateNotFoundInDB);

			var currentOps = new XPQuery<MOpsUnit>(CommonManager.Uow).SingleOrDefault(a => a.Oid == opsUnitId);
		
			var recipient = new XPQuery<MOpsUnit>(CommonManager.Uow).SingleOrDefault(a => a.Oid == recipientId);
			if (recipient?.Oid != sert.OpsUnit.Oid)
				return JResult.Failure("Создание данных о переводе не возможно. ОПС в сертификате не совпадает с ОПС реципиентом!");

			var transfer = new MSmTransfers(CommonManager.Uow) {
				Date = date,
				SmSertificate = sert,
				Recipient = recipient,
				From = currentOps
			};

			var inspection = new MSmInspection(CommonManager.Uow) {
				SmSertificate = sert,
				InspectionDate = date,
				Status = ESmSertStatus.Transfered,
				Result = $"Выполнен трансфер {recipient?.Name} => {currentOps.Name}"
			};
			inspection.Save();
			transfer.Save();

			CommonManager.Uow.CommitChanges();

			return JResult.Success(string.Format(Strings.EntryManager_OpsBlankChanged, blankNumber, recipient?.Name));

			/**/

			sert = new XPQuery<MSmSertificate>(CommonManager.Uow).SingleOrDefault(a => a.BlankNumber == blank);
			if (sert == null) 
				return JResult.Failure(Strings.EntryManager_CertificateNotFoundInDB);
			recipient = new XPQuery<MOpsUnit>(CommonManager.Uow).SingleOrDefault(a => a.Oid == opsUnitId);
			sert.OpsUnit = recipient;
			sert.SmEntry.OpsUnit = recipient;
			sert.SmEntry.Declarant.OpsUnits.Add(recipient);
			sert.Save();
			sert.SmEntry.Save();

			CommonManager.Uow.CommitChanges();

			return JResult.Success(string.Format(Strings.EntryManager_OpsBlankChanged, blankNumber));
		}

		public JResult LoadListSmOps() {
			return JResult.Success(new XPQuery<MOpsTypes>(CommonManager.Uow).Where(a => a.OpsType == EOpsType.OpsSm).OrderBy(a => a.OpsUnit.Name.L2).ToList()
				.Select(r => new { Id = r.Oid, Name = r.OpsUnit.Name.ToString() }));
		}

		public JResult EditEntry(string id, DateTime? entryDate, DateTime? regDate) {
			if (CurrentUserRole.Role.Type > 2)
				return JResult.Failure(Strings.EntryManager_FunctionalOnlyForAdmin);
			if (entryDate == null || regDate == null)
				return JResult.Failure(Strings.EntryManager_ApplicationDateIsNotSet);


			var entry = new XPQuery<MSmEntry>(CommonManager.Uow).FirstOrDefault(q => q.Oid == Int64.Parse(id));
//			var entry = AppProxySecureBase.GetObjectById<MSmEntry>(Guid.Parse(id));
			if (entry == null)
				return JResult.Failure(Strings.EntryManager_ApplicationNotFound);

			entry.EntryDate = entryDate.Value;
			entry.RegistrationDate = regDate.Value;
			entry.Save();
			CommonManager.Uow.CommitChanges();
			return JResult.Success(Strings.EntryManager_ApplicationDateIsChanged);
		}

		public JResult ExecuteXmlData() {
			//IEnumerable<string> records = OpenSqlFile("resources\\kaz40_13.sql");
			//ExecSqlRecords(records);
			if (global::System.IO.File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources\\report.sql"))) {
				IEnumerable<string> records = OpenSqlFile("resources\\report.sql");
				ExecSqlRecords(records);
			}

			if (global::System.IO.File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources\\report.xml"))) {
				var xml = OpenFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources\\report.xml"));
				ParseXml(xml, "Output1.xml");
			}
			//xml = OpenFile("L:\\Temp\\222.xml");
			//ParseXml(xml, "Output2.xml");
			//xml = OpenFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources\\333.xml"));
			//ParseXml(xml, "Output3.xml");
			
			CreateSerts();
			return JResult.Success(Strings.Common_Successfully);
		}

		private static int counter = 0;
		private static void ExecSqlRecords(IEnumerable<string> records) {
			counter = 0;
			foreach (string rec in records) {
				var record = rec.Trim().TrimStart('(').TrimEnd(',', ')');
				Sert sert = CreateSertFromSql(record);
				if (sert != null) {
					if (sert.NBLSERT == 28292) {
						
					}
					if (_serts.ContainsKey(sert.NBLSERT))
						_sertsDublecat.Add(new List<Sert>() { sert, _serts[sert.NBLSERT] });
					else if (sert.NBLSERT == 0) {
						_sertsZero.Add(countZero++, sert);
					} else _serts.Add(sert.NBLSERT, sert);
				}
				counter++;
			}
		}

		static List<int> _noRecord = new List<int>();
		private static Sert CreateSertFromSql(string record) {
			var rec = record.Replace(", '", "$");
			var fields = rec.Split('$');
			if (fields.Length != 11) {
				_noRecord.Add(counter);
				return null;
			}
			// Debug.Assert(fields.Length == 11, "FIELDS " + counter);

			var nbSert = _regWord.Replace(fields[5], String.Empty).TrimStart('0');
			var zayv = fields[7].Replace("\" ", "\"$").Replace("\",", "\"$").Split('$');

			List<string> zayvAdres = new List<string>();

			if (zayv.Length != 2) {
				zayvAdres.Add(fields[7]);
				zayvAdres.Add(fields[7]);
			} else {
				zayvAdres.Add(zayv[0]);
				zayvAdres.Add(zayv[1]);
			}

			Sert sert = new Sert() {
				KOD_OPS = Int32.Parse(fields[0]),
				NAME_OPS = fields[1],
				// DATE_R = DateTime.Parse(fields[2].Replace(" 00:00:00", "")),
				DATE_R = DateTime.ParseExact(NormalizeDateLong(fields[2]), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None),
				DATE_O = DateTime.ParseExact(NormalizeDateLong(fields[3]), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None),
				NSERT = Int32.Parse(_reg.Match(fields[4]).Groups[2].Value),
				NBLSERT = Int32.Parse(string.IsNullOrEmpty(nbSert) ? "0" : nbSert),
				NM = fields[6] == null ? string.Empty : fields[6],
				ZAYV = zayvAdres[0],
				ADRES = zayvAdres[1],
				SYS = fields[8] == null ? string.Empty : fields[8],
				TREB = fields[9] == null ? string.Empty : fields[9],
				TREB_ID = GetSertType(fields[9] == null ? string.Empty : fields[9]),
				EXP = fields[10] == null ? string.Empty : fields[10]
			};

			return sert;
		}

		private static IEnumerable<string> OpenSqlFile(string fileName) {
			return global::System.IO.File.ReadLines(fileName);
			//File.ReadLines(fileName);
			//FileStream fs = new FileStream(fileName, FileMode.Open);
		}

		private int count = 0;
		private void CreateSerts() {
			count = 0;
			foreach (Sert sert in _serts.Values) {
				CommonManager.CreateSert(sert);
				count++;
			}
		}

		private static XmlNode OpenFile(string file) {
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(global::System.IO.File.ReadAllText(file, Encoding.UTF8));
			return doc.GetElementsByTagName("RECORDS")[0];
		}
		static int countZero = 0;
		private static void ParseXml(XmlNode records, string outFIle) {
			Sert sert = null;

			foreach (XmlNode record in records.ChildNodes) {
				sert = ParseRocord(record.FirstChild);
				if (sert != null) {
					if (_serts.ContainsKey(sert.NBLSERT))
						_sertsDublecat.Add(null);
					else if (sert.NBLSERT == 0) {
						_sertsZero.Add(countZero++, sert);
					} else _serts.Add(sert.NBLSERT, sert);
				}
			}
		}

		private static Regex _reg = new Regex("\\d{6,7}\\S(07|06)\\S?\\d{2}\\W?(\\d+)");
		private static Regex _regWord = new Regex("\\D+");
		private static Dictionary<int, Sert> _serts = new Dictionary<int, Sert>();
		private static List<List<Sert>> _sertsDublecat = new List<List<Sert>>();
		private static Dictionary<int, Sert> _sertsZero = new Dictionary<int, Sert>();
		private static Sert ParseRocord(XmlNode record) {
			var nbSert = _regWord.Replace(record.Attributes["NBLSERT"].Value, String.Empty).TrimStart('0');
			return new Sert() {
				KOD_OPS = Int32.Parse(record.Attributes["KOD_OPS"].Value),
				// NAME_OPS = record.Attributes["NAME_OPS"].Value,
				DATE_R = DateTime.ParseExact(NormalizeDate(record.Attributes["DATE_R"].Value), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None),
				DATE_O = DateTime.ParseExact(NormalizeDate(record.Attributes["DATE_O"].Value), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None),
				NSERT = Int32.Parse(_reg.Match(record.Attributes["NSERT"].Value).Groups[2].Value),
				NBLSERT = Int32.Parse(string.IsNullOrEmpty(nbSert) ? "0" : nbSert),
				NM = record.Attributes["SYS1"] == null ? LoadXmlNm(record, "") : LoadXmlNm1(record, ""),
				NM_K = record.Attributes["SYS1"] == null ? LoadXmlNm(record, "K") : LoadXmlNm1(record, "K"),
				ZAYV = LoadXmlZAYV(record, ""),
				ZAYV_K = LoadXmlZAYV(record, "K"),
				SYS = record.Attributes["SYS1"] == null ? (record.Attributes["NM1"] == null ? string.Empty : record.Attributes["NM1"].Value) : record.Attributes["SYS1"].Value,
				TREB = record.Attributes["TREB1"] == null ? (record.Attributes["ISKL"] == null ? string.Empty : record.Attributes["ISKL"].Value) : LoadXmlTREB(record,  ""),
				TREB_ID = GetSertType(record.Attributes["TREB1"] == null ? (record.Attributes["ISKL"] == null ? string.Empty : record.Attributes["ISKL"].Value) : LoadXmlTREB(record, "")),
				EXP = record.Attributes["EXP"] == null ? string.Empty : record.Attributes["EXP"].Value
			};
		}

		private static string LoadXmlTREB(XmlNode record, string lang) {
			string result = string.Empty;

			if (record.Attributes["TREB1" + lang] != null)
				result += " " + record.Attributes["TREB1" + lang].Value.Trim();
			if (record.Attributes["TREB2" + lang] != null)
				result += " " + record.Attributes["TREB2" + lang].Value.Trim();

			return result;
		}

		private static string LoadXmlZAYV(XmlNode record, string lang) {
			string result = string.Empty;

			if (record.Attributes["ZAYV1" + lang] != null)
				result += " " + record.Attributes["ZAYV1" + lang].Value.Trim();
			if (record.Attributes["ZAYV2" + lang] != null)
				result += " " + record.Attributes["ZAYV2" + lang].Value.Trim();
			if (record.Attributes["ZAYV3" + lang] != null)
				result += " " + record.Attributes["ZAYV3" + lang].Value.Trim();

			return result;
		}

		private static string LoadXmlNm(XmlNode record, string lang) {
			string result = string.Empty;

			if (record.Attributes["NM2" + lang] != null)
				result += " " + record.Attributes["NM2" + lang].Value.Trim();
			if (record.Attributes["NM3" + lang] != null)
				result += " " + record.Attributes["NM3" + lang].Value.Trim();
			if (record.Attributes["NM4" + lang] != null)
				result += " " + record.Attributes["NM4" + lang].Value.Trim();
			if (record.Attributes["NM5" + lang] != null)
				result += " " + record.Attributes["NM5" + lang].Value.Trim();

			return result;
		}

		private static string LoadXmlNm1(XmlNode record, string lang) {
			string result = string.Empty;

			if (record.Attributes["NM1" + lang] != null)
				result += " " + record.Attributes["NM1" + lang].Value.Trim();
			if (record.Attributes["NM2" + lang] != null)
				result += " " + record.Attributes["NM2" + lang].Value.Trim();
			if (record.Attributes["NM3" + lang] != null)
				result += " " + record.Attributes["NM3" + lang].Value.Trim();
			if (record.Attributes["NM4" + lang] != null)
				result += " " + record.Attributes["NM4" + lang].Value.Trim();

			return result;
		}

		private static int GetSertType(string treb) {
			if (treb.Contains("18001")) return 1;
			if (treb.Contains("9001")) return 2;
			if (treb.Contains("14001")) return 3;
			if (treb.Contains("22000")) return 4;
			if (treb.Contains("29001")) return 5;
			if (treb.Contains("50001")) return 6;
			if (treb.Contains("27001")) return 7;
			if (treb.Contains("1352")) return 8;
			if (treb.Contains("1179")) return 9;
			if (treb.Contains("1614")) return 10;
			if (treb.Contains("1617")) return 11;
			if (treb.Contains("12.0.230")) return 12;
			if (treb.Contains("20000")) return 13;

			return 2;
		}

		private static string NormalizeDate(string date) {
			return date.Substring(6, 2) + "." + date.Substring(4, 2) + "." + date.Substring(0, 4);
		}
		private static string NormalizeDateLong(string date) {
			return date.Substring(8, 2) + "." + date.Substring(5, 2) + "." + date.Substring(0, 4);
		}
	}

}