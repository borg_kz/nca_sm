﻿using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Framework.Manager;
using App.Framework.Web.Mvc.Results;
using ECabinet.Core.Managers.SM;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Proxy.OrgStructure;

namespace ECabinetApp.Controllers.Managment {
	public class ClientHistoryController  : CommonController {
		// GET: ClientHistory
		public ActionResult Index() {
			return View("ClientHistory");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/Managment/ClientHistory")
			.IncludeDirectory("~/Resources/js/Managment/ClientHistory", "*.js", false);

		public SmClientManager Manager => Managers.Find <SmClientManager>();

		/// <summary>
		/// Загрузка списка изменений
		/// </summary>
		public JResult LoadClients(int? start, int? limit, string sort) {
			var sorting = ParseJsonSort<MClientHistory, ProxyClientHistory>(sort);
			FwQueryResult<MClientHistory> clients = null;
			clients = Manager.GetClientsHistory(start, limit, sorting);
			return JResult.Success(clients.Items.Select(a => new ProxyClientHistory(a)), clients.Count);
		}
	}
}