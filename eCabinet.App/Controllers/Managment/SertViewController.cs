﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using App.Bundle;
using App.Framework;
using App.Framework.Web.Mvc.Results;
using App.Web.MVC.Controllers;
using DevExpress.Xpo;
using ECabinet.Core.Managers.SM;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Model.Reference;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ECabinetApp.Controllers.Managment {
	public class SertViewController : AppController {
		// GET: /SertView/
		public ActionResult Index() {
			return View("SertView");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/Managment/SertView")
			.IncludeDirectory("~/Resources/js/Managment/SertView", "*.js", false);

		public SmSertificateManager Manager => Managers.Find<SmSertificateManager>();

		public JResult LoadSmSertType() {
			return JResult.Success(new XPQuery<MSertificationStore>(Manager.Uow).ToArray().Select(a => new {
				Id = a.Oid,
				Name = a.Name.ToString(),
				NameRu = a.Name.ToString(FwCultures.Rus),
				NameKz = a.Name.ToString(FwCultures.Kaz),
				ValueKz = a.Equriments.ToString(FwCultures.Kaz),
				ValueRu = a.Equriments.ToString(FwCultures.Rus),
				SystemKz = a.System.ToString(FwCultures.Kaz),
				SystemRu = a.System.ToString(FwCultures.Rus)
			}));
		}

		public JResult LoadListSmOps() {
			return JResult.Success(new XPQuery<MOpsTypes>(Manager.Uow).Where(a => a.OpsType == EOpsType.OpsSm).OrderBy(a => a.OpsUnit.Name.L2).ToList()
				.Select(r => new { Id = r.Oid, Name = r.OpsUnit.Name.ToString() }));
		}

		public JResult LoadSmSert(long? ops, string sertType, DateTime? startdate, DateTime? finishDate, string blank, string sert, string declarant, int? start, int? limit) {
			// startdate = null;
			long? defaultLong = null;
			if (ops == null && sertType == null) {
				var par = Request.Params["json"];
				if (par == null) return JResult.Success();
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startdate = string.IsNullOrEmpty(qwer["startdate"].ToString()) ? (DateTime?)null : DateTime.Parse(qwer["startdate"].ToString());
				finishDate = string.IsNullOrEmpty(qwer["finishDate"].ToString()) ? (DateTime?)null : DateTime.Parse(qwer["finishDate"].ToString());
				ops = string.IsNullOrEmpty(qwer["ops"]?.ToString()) ? defaultLong : Int64.Parse(qwer["ops"].ToString());
				sertType = qwer["sertType"].ToString();
				blank = qwer["blank"].ToString();
				sert = qwer["sert"].ToString();
				declarant = qwer["declarant"].ToString();
			}

			var serts = Manager.FindSertificates(
				ops,
				string.IsNullOrEmpty(sertType) ? (int?)null : Int32.Parse(sertType),
				startdate, 
				finishDate, 
				string.IsNullOrEmpty(blank) ? (int?)null : Int32.Parse(blank), 
				string.IsNullOrEmpty(sert) ? (int?)null : Int32.Parse(sert), 
				declarant,
				start, limit);

			return JResult.Success(serts.Items.ToList().Select(a => new ProxySmSertificate(a)), serts.Count);
		}
	}
}