﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Bundle;
using eCabinet.Core.Managers.Managment;
using ECabinet.Core.Managers.Managment;

namespace ECabinetApp.Controllers.Managment {
	public class MngSertsController : CommonController {
		public AdminSertManager Manager {
			get { return Managers.Find<AdminSertManager>(); }
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/Managment/MngSerts")
			.IncludeDirectory("~/Resources/js/Managment/MngSerts", "*.js", false);

		public ActionResult Index() {
			return View("MngSerts");
		}
	}
}