﻿using System;
using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Proxy;
using App.Data.Xpo.Extensions;
using App.Framework.Web.Mvc.Results;
using DevExpress.Xpo;
using ECabinet.Core.Managers.Managment;
using ECabinet.Core.Managers.SM;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Model.Reference;
using ECabinet.Data.Properties;
using Newtonsoft.Json;

namespace ECabinetApp.Controllers.SM {
	// [Privilege("SmSertificate", PrivilegeGroups.SmSertification, AccessType.AccessDenied | AccessType.Read | AccessType.Write | AccessType.Admin)]
	public class SmSertificateController : CommonController {
		public ActionResult Index() {
			return View("SmSertificate");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/SmSertificate")
			.IncludeDirectory("~/Resources/js/SM/SmSertificate", "*.js", false);

		public SmSertificateManager Manager { get { return Managers.Find<SmSertificateManager>(); } }

		public JResult LoadSmSertType() {
			return JResult.Success(new XPQuery<MSertificationStore>(Manager.Uow).ToArray().Select(a => new {
				Id = a.Oid,
				Name = a.Name.ToString(),
				NameRu = a.Name.ToString("ru-RU"),
				NameKz = a.Name.ToString("kk-KZ"),
				NameEn = a.Name.ToString("en-US"),
				ValueKz = a.Equriments.ToString("kk-KZ"),
				ValueRu = a.Equriments.ToString("ru-RU"),
				ValueEn = a.Equriments.ToString("en-US"),
				SystemKz = a.System.ToString("kk-KZ"),
				SystemRu = a.System.ToString("ru-RU"),
				SystemEn = a.System.ToString("en-US")
			}));
		}

		/// <summary> Загрузка заявок организации </summary>
		public JResult LoadSmSert(int sertType, string findPatern, int? start, int? limit) {
			Int32.TryParse(findPatern, out int number);
			if (sertType == (int) ESmSertStatus.Entry) {
				var entryes = Manager.GetUserDoneEntry(CurrentUserRole, CurrentOps, start, limit);
				return JResult.Success(entryes.Items.ToList().Select(a => new ProxySmSertificate(a, CurrentOps)), entryes.Count);
			}

			var serts = Manager.GetUserSertificates(CurrentUserRole, CurrentOps, sertType, number, start, limit);

			return JResult.Success(serts.Items.ToList().Select(a => new ProxySmSertificate(a, CurrentOps)), serts.Count);
		}
		
		/// <summary> Загрузка данных о клиенте </summary>
		public JResult LoadClientProfile(int sert) {
			var client = Manager.GetClientProfile(sert);

			return JResult.Success(new ProxyClient(client));
		}

		public JResult SaveSmSert(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);

			var obj = JsonConvert.DeserializeObject<ProxySmSertificate>(json);

			if (false && (obj.Status == ESmSertStatus.Entry || isNew.Value) && Manager.CheckBlancNumber(Int32.Parse(obj.BlankNumber)))
				return JResult.Failure(Strings.SmSertificate_ThisCertAlreadyExists);

			if (isNew.HasValue && isNew.Value) {
				Manager.DublicateSertificate(CurrentOps, obj, CurrentUser.GetUser(Manager.Uow));
				return JResult.Success(Strings.SmSertificate_CertRedesigned);
			}

			Manager.SaveSertificate(isNew.Value, CurrentOps, obj, CurrentUser.GetUser(Manager.Uow));

			return JResult.Success(Strings.SmSertificate_CertSaved);
		}

		public JResult SaveClientProfile(string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);

			var obj = JsonConvert.DeserializeObject<ProxyClient>(json);

			Manager.SaveClientProfile(obj);

			return JResult.Success(Strings.Common_DataSuccessSaved);
		}

		public JResult DeleteSert(string oid) {
			Manager.DeleteSert(AppProxySecureBase.GetObjectById<MSmSertificate>(new Guid(oid)));
			return JResult.Success(Strings.SmSertificate_CertDeleted);
		}

		public JResult LoadOpsAuditors() {
			var result = new OpsManager().GridLoadOpsEmployes(CurrentOps.Oid, 0, 50, ERoleType.SmAuditor);
			return JResult.Success(result.Items.ToList().Select(a => new { Id = a.User.Person.Oid, Name = a.User.Person.FullName }));
		}

		public JResult SaveAuditor(string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);

			var obj = JsonConvert.DeserializeObject<ProxySmSertificate>(json);

			Manager.SaveAuditor(obj);

			return JResult.Success(Strings.Common_DataSuccessSaved);
		}

	}
}