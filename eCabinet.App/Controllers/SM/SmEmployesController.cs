﻿using System;
using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Proxy;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework.Utils;
using App.Framework.Web.Mvc.Results;
using App.Framework.Xpo;
using DevExpress.Xpo;
using ECabinet.Core;
using ECabinet.Core.Managers.Managment;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Properties;
using Newtonsoft.Json;

namespace ECabinetApp.Controllers.SM {
	// [Privilege("SmEmployes", PrivilegeGroups.ManageOps, AccessType.AccessDenied | AccessType.Read | AccessType.Write | AccessType.Admin)]
	public class SmEmployesController : CommonController {
		private OpsManager Manager { get { return Managers.Find<OpsManager>(); } }

		public ActionResult Index() {
			return View("SmEmployes");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/SmEmployes")
			.IncludeDirectory("~/Resources/js/SM/SmEmployes", "*.js", false);

		public JResult LoadEmployes(int? start, int? limit, string sort) {
			// new AuditManager().Write("Загрузка модуля: Cотрудники ораганизации", AuditMode.Important, AuditType.Monitoring, CurrentUser.ShortName);
			var result = Manager.GridLoadOpsEmployes(CabinetApp.GetCurrentOps(new FwXpoSession()).Oid, start, limit, null, 
				ParseJsonSort<MEmploye, ProxyEmploye>(sort));
			return JResult.Success(result.Items.ToList().Select(a => new ProxyEmploye(a)), result.Count);
		}

		public JResult LoadSmRoles() {
			return JResult.Success(new XPQuery<MAppRole>(Manager.Uow).Where(a => a.Type > 99 && a.Type < 200).ToArray().Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}

		public JResult SaveEmploye(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);
			var obj = JsonConvert.DeserializeObject<ProxyEmploye>(json);

			if (!FwIinValidator.IsValid(obj.Iin))
				return JResult.Failure(Strings.Msg_InvalidIinFormat); //todo(maksim): Локализовать

			Manager.AddSaveEmploye(CabinetApp.GetCurrentOps(Manager.Uow), isNew.Value, obj);

			string fullName = string.Format("{0} {1} {2}", obj.SecondName, obj.FirstName, obj.ThirdName).TrimAndEscape();

//			if (isNew.HasValue && isNew.Value)
//				new AuditManager().Write((string.Format("В организацию {0} добавлен пользователь {1} с ролью {2}", CabinetApp.GetCurrentOps(DataProvider).Name, fullName,
//					DataProvider.DataQuery<MRole>().Where(a => a.Oid == obj.Role).FirstOrDefault().Name)), AuditMode.Critical, AuditType.Admin, CurrentUser.ShortName);
//			else
//				new AuditManager().Write((string.Format("Изменены данные сотрудника: {0}, {1}", fullName, obj.Iin)), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);

			return JResult.Success(Strings.SmEmployes_EmployesDataSuccesSaved);
		}

		public JResult CheckIin(string iin) {
			if (!FwIinValidator.IsValid(iin))
				return JResult.Failure(Strings.Msg_InvalidIinFormat); //todo(maksim): Локализовать
			
			var res = Manager.CheckEmployeIin(iin);
			var result = res == null ? new ProxyEmploye { Id = "0", Iin = iin, Login = iin, Role = -1, RoleDisplay = string.Empty } :
				new ProxyEmploye {Id = res.Oid.ToString(), Iin = iin, Login = iin, Role = -1, RoleDisplay = string.Empty, FirstName = res.FirstName, SecondName = res.SecondName, ThirdName = res.ThirdName, Email = res.Email };
			return JResult.Success(result);
		}

		public JResult DeleteEmploye(long? id) {
			if (id == null)
				return JResult.Failure(Strings.SmEmployes_EmployeNotFound);
			Manager.DeleteEmploye(id.Value, CabinetApp.GetCurrentOps(Manager.Uow));
			return JResult.Success(Strings.SmEmployes_EmployeDeletedFromYourOrg);
		}
	}
}