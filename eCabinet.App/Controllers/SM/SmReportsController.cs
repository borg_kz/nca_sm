﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using App.Bundle;
using App.Framework.Utils;
using App.Framework.Web.Mvc.Results;
using App.Framework.Xpo;
using DevExpress.Xpo;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Model.Reference;
using ECabinet.Data.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ECabinetApp.Controllers.SM {
	// [AppPrivilege("SmReports", PrivilegeGroups.SmSertification, AccessType.AccessDenied | AccessType.Read | AccessType.Admin)]
	public class SmReportsController : CommonController {
		public ActionResult Index() {
			return View("SmReports");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/SmReports")
			.IncludeDirectory("~/Resources/js/SM/SmReports", "*.js", false);

		public JResult LoadOpsFromRegion(int? start, int? limit, int? opsType = null) {
			//new AuditManager().Write("Отчёты СМ: Организации по регионам", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			using (var uow = new FwXpoSession()) {
				MonitoringWrite("Загружен модуль: Заявки СМК");
				if (opsType == null)
					return JResult.Failure(Strings.Common_OpsTypeNotSelected);

				if (opsType == (int) EAreaUnitType.City)
					return
						JResult.Success(new XPQuery<MOpsTypes>(uow).Where(
							a => a.OpsType == EOpsType.OpsSm && a.OpsUnit.Location.Type == EAreaUnitType.City)
							.GroupBy(a => a.OpsUnit.Location)
							.ToArray().Select(g => new {Id = g.Key.Name.ToString(), Name = g.Count()}));

				var res = new XPQuery<MOpsTypes>(uow)
					.Where(a => a.OpsType == EOpsType.OpsSm && (a.OpsUnit.Location.Status == EAreaUnitStatus.Region))
					.GroupBy(a => a.OpsUnit.Location).ToArray().Select(g => new KeyValuePair<string, int>(g.Key.Name, g.Count()));
				var res1 = new XPQuery<MOpsTypes>(uow)
					.Where(a => a.OpsType == EOpsType.OpsSm && a.OpsUnit.Location.Status == EAreaUnitStatus.City)
					.GroupBy(a => a.OpsUnit.Location)
					.ToArray()
					.Select(g => new KeyValuePair<string, int>(g.Key.Parent.Name, g.Count()));

				var result = res.ToDictionary(area => area.Key, area => area.Value);

				foreach (KeyValuePair<string, int> area in res1) {
					if (result.ContainsKey(area.Key))
						result[area.Key] = result[area.Key] + area.Value;
					else result.Add(area.Key, area.Value);
				}
				return JResult.Success(result.Select(r => new {Id = r.Key, Name = r.Value}));
			}
		}

		/// <summary>
		/// Колличество сертификатов в разрезе регионов
		/// </summary>
		public JResult LoadSertFromRegion(int? start, int? limit, DateTime? startDate, DateTime? finishDate, int? opsType = null) {
			//new AuditManager().Write("Отчёты СМ: Колличество сертификатов в разрезе регионов", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			MonitoringWrite("Загружен модуль: Заявки СМК");
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				opsType = Int32.Parse(qwer["opsType"].ToString());
			}
			
			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			if (opsType == null)
				return JResult.Failure(Strings.Common_OpsTypeNotSelected);

			using(var uow = new FwXpoSession()) {
				if (opsType == (int) EAreaUnitType.City)
					return JResult.Success(
						new XPQuery<MSmSertificate>(uow).Where(a =>
								a.OpsUnit.Location.Type == EAreaUnitType.City && a.StartDate >= startDate.Value &&
								a.Status != ESmSertStatus.Deleted && a.Status != ESmSertStatus.Cancelled &&
								a.StartDate <= finishDate.Value.AddDays(1))
							.GroupBy(a => a.OpsUnit.Location).ToArray().Select(g => new {Id = g.Key.Name.ToString(), Name = g.Count()}));

				var res = new XPQuery<MSmSertificate>(uow)
					.Where(a => a.OpsUnit.Location.Status == EAreaUnitStatus.Region && a.StartDate >= startDate.Value &&
						a.Status != ESmSertStatus.Deleted && a.Status != ESmSertStatus.Cancelled &&
						a.StartDate <= finishDate.Value.AddDays(1))
					.GroupBy(a => a.OpsUnit.Location).ToArray().Select(g => new KeyValuePair<string, int>(g.Key.Name, g.Count()));
				var res1 = new XPQuery<MSmSertificate>(uow)
					.Where(a => a.OpsUnit.Location.Status == EAreaUnitStatus.City && a.StartDate >= startDate.Value &&
						a.Status != ESmSertStatus.Deleted && a.Status != ESmSertStatus.Cancelled &&
						a.StartDate <= finishDate.Value.AddDays(1))
					.GroupBy(a => a.OpsUnit.Location)
					.ToArray()
					.Select(g => new KeyValuePair<string, int>(g.Key.Parent.Name, g.Count()));

				var result = res.ToDictionary(area => area.Key, area => area.Value);

				foreach (KeyValuePair<string, int> area in res1) {
					if (result.ContainsKey(area.Key))
						result[area.Key] = result[area.Key] + area.Value;
					else result.Add(area.Key, area.Value);
				}
				return JResult.Success(result.Select(r => new {Id = r.Key, Name = r.Value}));
			}
		}

		/// <summary>
		/// Статистика сертификатов в разрезе ОПС
		/// </summary>
		public JResult LoadStatisticSert(int? start, int? limit, DateTime? startDate, DateTime? finishDate) {
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
			}

			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);

			return JResult.Success(new XPQuery<MSmSertificate>(new FwXpoSession()).Where(a => a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted).ToList()
				.GroupBy(a => a.OpsUnit.Name.ToString()).Select(g => new { Id = g.Key.ToString(), Name = g.Count() }));
		}

		/// <summary>
		/// Список организаций сертифицировшихся по региону ОПС
		/// </summary>
		public JResult LoadOrgsFromAreasOPS(int? start, int? limit, DateTime? startDate, DateTime? finishDate,
			int? areald = null) {
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject) JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				areald = Int32.Parse(qwer["areald"].ToString());
			}

			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			using(var uow = new FwXpoSession()) {
				var area = new XPQuery<MAreaUnit>(uow)
					.FirstOrDefault(a => a.Oid == areald);

				if (area.Type == EAreaUnitType.Region)
					return JResult.Success(
						new XPQuery<MSmSertificate>(uow)
							.Where(a => a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) &&
									a.Status != ESmSertStatus.Deleted && a.Status != ESmSertStatus.Cancelled &&
									a.OpsUnit.Location.Parent.Oid == areald)
							.ToList()
							.Select(g => new {
										Declarant = g.SmEntry.Declarant.Name.ToString(),
										Auditor = g.OpsUnit.Name.ToString(),
										g.StartDate,
										SertType = g.SertificationStore.ShortName,
										Status = g.Status.GetLocalizedField()
									}));

					return JResult.Success(
						new XPQuery<MSmSertificate>(uow)
							.Where(a =>
									a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) &&
									a.Status != ESmSertStatus.Deleted &&
									a.OpsUnit.Location.Oid == areald)
							.ToList()
							.Select(g => new {
										Declarant = g.SmEntry.Declarant.Name.ToString(),
										Auditor = g.OpsUnit.Name.ToString(),
										g.StartDate,
										SertType = g.SertificationStore.ShortName,
										Status = g.Status.GetLocalizedField()
									}));
			}
		}

		/// <summary>
		/// Список организаций сертифицировшихся по региону самой организации
		/// </summary>
		public JResult LoadOrgsFromAreas(int? start, int? limit, DateTime? startDate, DateTime? finishDate, int? areald = null) {
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject) JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				areald = Int32.Parse(qwer["areald"].ToString());
			}

			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			using(var uow = new FwXpoSession()) {
				return JResult.Success(
					new XPQuery<MSmSertificate>(uow).Where(a =>
								a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) &&
								a.Status != ESmSertStatus.Deleted &&
								a.SmEntry.Declarant.Location.Oid == areald).OrderBy(a => a.OpsUnit.Location)
						.ToList()
						.Select(g =>
								new {
									Declarant = g.SmEntry.Declarant.Name.ToString(),
									Auditor = g.OpsUnit.Name.ToString(),
									g.StartDate,
									Location = g.OpsUnit.Location.Name.ToString(),
									SertType = g.SertificationStore.ShortName,
									Status = g.Status.GetLocalizedField()
								}));
			}
		}

		/// <summary>
		/// Статистика заявок в разрезе ОПС
		/// </summary>
		public JResult LoadsmStatisticEntries(int? start, int? limit, DateTime? startDate, DateTime? finishDate) {
			MonitoringWrite("Загружен модуль: Заявки СМК");
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
			}

			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			using(var uow = new FwXpoSession()) {
				return JResult.Success(new XPQuery<MSmEntry>(uow)
						.Where(a => a.EntryDate >= startDate.Value && a.EntryDate <= finishDate.Value.AddDays(1))
						.ToList()
						.GroupBy(a => a.OpsUnit.Name.ToString())
						.Select(g => new {Id = g.Key.ToString(Thread.CurrentThread.CurrentCulture), Name = g.Count()}));
			}
		}

		/// <summary> Колличество типов сертификатов в разрезе ОПС </summary>
		public JResult LoadSmSertTypes(int? start, int? limit, DateTime? startDate, DateTime? finishDate, int? opsId = null) {
			MonitoringWrite("Загружен модуль: Заявки СМК");
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				opsId = Int32.Parse(qwer["opsId"].ToString());
			}

			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			if (opsId == null)
				return JResult.Failure(Strings.Reports_OpsNotSelected);
			using(var uow = new FwXpoSession()) {
				if (opsId == -1)
					return
						JResult.Success(new XPQuery<MSmSertificate>(uow)
							.Where(a => a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted)
							.GroupBy(a => a.SertificationStore).ToArray().Select(g => new {Id = g.Key.Name.ToString(), Name = g.Count()}));

				return
					JResult.Success(new XPQuery<MSmSertificate>(uow)
						.Where(a => a.OpsUnit.Oid == opsId && a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted)
						.GroupBy(a => a.SertificationStore).ToArray().Select(g => new {Id = g.Key.Name.ToString(), Name = g.Count()}));
			}
		}
		
		/// <summary> Колличество типов сертификатов в разрезе ОПС </summary>
		public JResult LoadDinamicSert(int? start, int? limit, DateTime? startDate, DateTime? finishDate) {
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
			}

			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			using(var uow = new FwXpoSession()) {
				return
					JResult.Success(new XPQuery<MSmSertificate>(uow)
						.Where(a => a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted)
						.GroupBy(a => a.StartDate).ToArray().Select(g => new {Id = g.Key.ToString("dd.MM.yyyy"), Name = g.Count()}));
			}
		}

		/// <summary> ВЫГРУЗКА. Выданные сертификаты СМ по выбранному ОПС за определённый период дат. </summary>
		public JResult LoadSmOutSerts(int? start, int? limit, DateTime? startDate, DateTime? finishDate) {
			if (startDate == null || finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				if (startDate == null || finishDate == null)
					return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			}
			using(var uow = new FwXpoSession()) {
				if (start == null || limit == null) {
					return JResult.Success(new XPQuery<MSmSertificate>(uow).Where(a =>
								a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) &&
								a.Status != ESmSertStatus.Deleted && a.FinishDate > DateTime.Now && a.Status != ESmSertStatus.Cancelled)
						.OrderBy(a => a.StartDate).Select(r => new ProxySmSertificate(r, true)));
				}

				int count = new XPQuery<MSmSertificate>(uow).Count(a =>
								a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) &&
								a.Status != ESmSertStatus.Deleted && a.FinishDate > DateTime.Now && a.Status != ESmSertStatus.Cancelled);

				return JResult.Success(new XPQuery<MSmSertificate>(uow).Where(a =>
							a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Deleted && a.FinishDate > DateTime.Now && a.Status != ESmSertStatus.Cancelled)
					.OrderBy(a => a.StartDate).Skip(start ?? 0).Take(limit ?? 1000)
					.Select(r => new ProxySmSertificate(r, false)), count);
			}
		}

		/// <summary> Выданные сертификаты СМ по выбранному ОПС за определённый период дат. </summary>
		public JResult LoadSmSerts(int? start, int? limit, DateTime? startDate, DateTime? finishDate, long? opsId) {
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				opsId = Int32.Parse(qwer["opsId"].ToString());
			}
			
			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			using(var uow = new FwXpoSession()) {
				MOpsUnit ops = null;
				switch ((ERoleType) CurrentUserRole.Role.Type) {
					case ERoleType.SmAuditor:
					case ERoleType.SmSecretary:
					case ERoleType.SmAdmin:
						ops = CurrentOps;
						break;
					default:
						ops = opsId == null ? null : new XPQuery<MOpsUnit>(uow).FirstOrDefault(q => q.Oid == opsId.Value);
						break;
				}

				if (ops == null)
					return JResult.Failure(Strings.Common_OpsNotFound);

				int count = new XPQuery<MSmSertificate>(uow).Count(a =>
					a.OpsUnit.Oid == ops.Oid && a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted);
				// new AuditManager().Write(string.Format("Отчёт. Заявки СМ за определённый период дат {0}-{1} [{2}]", startDate.Value.ToShortDateString(), finishDate.Value.ToShortDateString(), ops.Name), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);

				return JResult.Success(new XPQuery<MSmSertificate>(uow).Where(a =>
							a.OpsUnit.Oid == opsId && a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted).OrderBy(a => a.Status)
					.ThenByDescending(a => a.StartDate).Skip(start ?? 0).Take(limit ?? 100000)
					.Select(r => new ProxySmSertificate(r, false)), count);
			}
		}

		/// <summary> Заявки СМ по выбранному ОПС за определённый период дат. </summary>
		public JResult LoadSmEntryes(int? start, int? limit, DateTime? startDate, DateTime? finishDate, long? opsId) {
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				opsId = Int32.Parse(qwer["opsId"].ToString());
			}
			
			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			using(var uow = new FwXpoSession()) {
				MOpsUnit ops = null;
				switch ((ERoleType) CurrentUserRole.Role.Type) {
					case ERoleType.SmAuditor:
					case ERoleType.SmSecretary:
					case ERoleType.SmAdmin:
						ops = CurrentOps;
						break;
					default:
						ops = opsId == null ? null : new XPQuery<MOpsUnit>(uow).FirstOrDefault(q => q.Oid == opsId.Value);
						break;
				}

				if (ops == null)
					return JResult.Failure(Strings.Common_OpsNotFound);
				int count = new XPQuery<MSmEntry>(uow).Count(a => a.OpsUnit.Oid == opsId && a.EntryDate >= startDate.Value && a.EntryDate <= finishDate.Value.AddDays(1));
				// new AuditManager().Write(string.Format("Отчёт. Заявки СМ за определённый период дат {0}-{1} [{2}]", startDate.Value.ToShortDateString(), finishDate.Value.ToShortDateString(), ops.Name), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);

				return JResult.Success(new XPQuery<MSmEntry>(uow)
					.Where(a => a.OpsUnit.Oid == opsId && a.EntryDate >= startDate.Value && a.EntryDate <= finishDate.Value.AddDays(1))
					.OrderBy(a => a.Status)
					.ThenByDescending(a => a.EntryDate).Skip(start ?? 0).Take(limit ?? 100000)
					.Select(r => new ProxySmEntry(r)), count);
			}
		} 

		public JResult LoadSertFromType(int? start, int? limit, DateTime? startDate, DateTime? finishDate) {
			if (startDate == null && finishDate == null) {
				var par = Request.Params["json"];
				var pars = (JObject)JsonConvert.DeserializeObject(par);
				var qwer = pars["proxy"]["params"];
				startDate = DateTime.Parse(qwer["startDate"].ToString());
				finishDate = DateTime.Parse(qwer["finishDate"].ToString());
				start = 0;
				limit = 200;
			}

			if (startDate == null || finishDate == null)
				return JResult.Failure(Strings.EntryManager_SelectPeriodForFiltering);
			using(var uow = new FwXpoSession()) {
				int count = new XPQuery<MSmSertificate>(uow).Where(a =>
								a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted)
						.GroupBy(o => o.OpsUnit, a => a.SertificationStore)
						.ToList()
						.Count();
				;
				var res = new XPQuery<MSmSertificate>(uow).Where(a =>
							a.StartDate >= startDate.Value && a.StartDate <= finishDate.Value.AddDays(1) && a.Status != ESmSertStatus.Cancelled && a.Status != ESmSertStatus.Deleted)
					.ToList()
					.GroupBy(o => o.OpsUnit, a => a.SertificationStore)
					.OrderBy(o => o.Key.Name.ToString())
					.Skip(start ?? 0)
					.Take(limit ?? 1000)
					.Select(g => new {
						Ops = g.Key.Name.ToString(),
						Region = g.Key.Location.Name.ToString(),
						s18001 = GetVal(g.GetEnumerator(), 1),
						s9001 = GetVal(g.GetEnumerator(), 2),
						s14001 = GetVal(g.GetEnumerator(), 3),
						s22000 = GetVal(g.GetEnumerator(), 4),
						s29001 = GetVal(g.GetEnumerator(), 5),
						s50001 = GetVal(g.GetEnumerator(), 6),
						s27001 = GetVal(g.GetEnumerator(), 7),
						s1352 = GetVal(g.GetEnumerator(), 8),
						s1179 = GetVal(g.GetEnumerator(), 9),
						s1614 = GetVal(g.GetEnumerator(), 10),
						s1617 = GetVal(g.GetEnumerator(), 11),
						s120203 = GetVal(g.GetEnumerator(), 12),
						s20000 = GetVal(g.GetEnumerator(), 13),
						s14001_16 = GetVal(g.GetEnumerator(), 18),
						s9001_16 = GetVal(g.GetEnumerator(), 19),
						s27001_15 = GetVal(g.GetEnumerator(), 20),
						s20000_16 = GetVal(g.GetEnumerator(), 21),

						s13485_17 = GetVal(g.GetEnumerator(), 24),
						s45001_18 = GetVal(g.GetEnumerator(), 25),
						s45001_19 = GetVal(g.GetEnumerator(), 33),

						Count = g.Count()
					});

				return JResult.Success(res, count);
			}
		}

		private string GetVal(IEnumerator<MSertificationStore> store, int oid) {
			var stor = store.Current;
			int count = 0;
			while (store.MoveNext()) {
				stor = store.Current;
				if (stor.Oid == oid)
					count++;
			}

			return count.ToString();
		}

		public JResult LoadSmOps() {
			using(var uow = new FwXpoSession()) {
				if (CurrentUserRole.Role.Type == (int) ERoleType.Admin || CurrentUserRole.Role.Type == (int) ERoleType.GlobalAdmin)
					return
						JResult.Success(new XPQuery<MOpsTypes>(uow)
							.Where(a => a.OpsType == EOpsType.OpsSm)
							.OrderBy(a => a.OpsUnit.Name.L2)
							.ToList()
							.Select(r => new {Id = r.Oid, Name = r.OpsUnit.Name.ToString()}));
				return JResult.Success(new {Id = CurrentOps.Oid, Name = CurrentOps.Name.ToString()});
			}
		}
		
		public JResult SmMonitoringAuditors(int? start, int? limit, string sorting) {
			//new AuditManager().Write("Отчёт по Аудиторам.", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			using(var uow = new FwXpoSession()) {
				var auditors = new XPQuery<MEmploye>(uow).Where(a => a.Role.Type == (int)ERoleType.SmAuditor).OrderBy(a => a.User);
				var result = new List<KeyValuePair<string, string>?>();
					// auditors.ToDictionary(a => a.Key).Select(g => new KeyValuePair<string, string>(g.Key.FullName, g[1].ToString()));
				var problemAuditors = new Dictionary<long, KeyValuePair<string, string>?>();
				foreach (var employe in auditors.ToList()) {
					if (!problemAuditors.ContainsKey(employe.Oid)) {
						problemAuditors[employe.Oid] = new KeyValuePair<string, string>(employe.User.Person.ShortName, employe.OpsUnit.Name);
						break;
					}
					if (problemAuditors[employe.Oid] != null) {
						result.Add(problemAuditors[employe.Oid]);
						problemAuditors[employe.Oid] = null;
					}

					result.Add(new KeyValuePair<string, string>(employe.User.Person.ShortName, employe.OpsUnit.Name));
				}

				return JResult.Success(result.Select(a => new {Id = a.Value.Key, Name = a.Value.Value}));
			}
		}
	}
}