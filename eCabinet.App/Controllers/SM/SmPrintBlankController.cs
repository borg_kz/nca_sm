﻿using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Xpo.Extensions;
using App.Data.Xpo.Model.Entities.Auth;
using App.Framework.Web.Mvc.Results;
using App.Framework.Xpo;
using DevExpress.Xpo;
using ECabinet.Data.Model.Entities.System;
using ECabinet.Data.Properties;

namespace ECabinetApp.Controllers.SM {
	// [Privilege("SmPrintBlank", PrivilegeGroups.SmSertification, AccessType.AccessDenied | AccessType.Write | AccessType.Admin)]
	public class SmPrintBlankController : CommonController {
		public ActionResult Index() {
			return View("SmPrintBlank");
		}

//		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/OpsStore")
//	.IncludeDirectory("~/Resources/js/SM/OpsStore", "*.js", false);

		public JResult SavePrintSettings(string blank) {
			MUsersSettings set;
			using (var uow = new FwXpoSession()) {
				MAppUser curentUser = CurrentUser.GetUser(uow);
				set = new XPQuery<MUsersSettings>(uow).FirstOrDefault(a => a.User == curentUser && a.Key == "SmBlank");
				if (set == null)
					set = new MUsersSettings(uow);

				set.User = curentUser;
				set.Name = Strings.SmPrint_TestPrinterAtHome;
				set.Key = "SmBlank";
				set.Settings = blank;
				set.Save();
				uow.CommitChanges();
				return JResult.Success(Strings.Common_SettingsSaved);
			}
		}
	}
}