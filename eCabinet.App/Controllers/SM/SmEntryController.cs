﻿using System;
using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Xpo.Extensions;
using App.Framework.Utils;
using App.Framework.Web.Mvc.Results;
using ECabinet.Core;
using ECabinet.Core.Managers.Managment;
using ECabinet.Core.Managers.SM;
using ECabinet.Data;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Properties;
using Newtonsoft.Json;

namespace ECabinetApp.Controllers.SM {
	// [Privilege("SmEntry", PrivilegeGroups.SmSertification, AccessType.AccessDenied | AccessType.Read | AccessType.Write | AccessType.Admin)]
	public class SmEntryController : CommonController {
		public ActionResult Index() {
			return View("SmEntry");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/SmEntry")
			.IncludeDirectory("~/Resources/js/SM/SmEntry", "*.js", false);

		private SmEntryManager Manager { get { return Managers.Find<SmEntryManager>(); } }

		/// <summary>
		/// Загрузка заявок организации
		/// </summary>
		public JResult LoadEntryes(int entryType, int? start, int? limit) {
			MonitoringWrite("Загружен модуль: Заявки СМК");

			var result = Manager.GetUserEntryes(entryType, CurrentUserRole, CurrentOps, start, limit);
			return JResult.Success(result.Items.ToList().Select(a => new ProxySmEntry(a)), result.Count);
		}

		public JResult ReviewEntry(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);
			var obj = JsonConvert.DeserializeObject<ProxySmEntry>(json);

			Manager.ReviewEntry(Int32.Parse(obj.Id), obj.ReviewDescription, obj.Status);

			return JResult.Success(Strings.SmEntry_ApplicationProcessed);
		}

		public JResult LoadSmSertType() {
			return JResult.Success(Manager.GetSmTypes().Items.ToList().Select(a => new { Id = a.Oid, Name = a.ShortName }));
		}

		public JResult ResultEntry(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);
			var obj = JsonConvert.DeserializeObject<ProxySmEntry>(json);

			Manager.ResultEntry(Int32.Parse(obj.Id), obj.StartResearch, obj.FinishResearch, obj.ResultResearch);

			return JResult.Success(Strings.SmEntry_ApplicationReadyForCertIssue);
		}
		
		/// <summary>
		/// Загрузка заявок организации
		/// </summary>
		public JResult CheckRole() {
			var role = CurrentUser.GetUser(Manager.Uow).CurrentRole.Role;
			return JResult.Success(role.Type == (int)ERoleType.SmAdmin || role.Type == (int)ERoleType.SmAuditor || role.Type == (int)ERoleType.Admin);
		}

		/// <summary>
		/// Save new/edit client
		/// </summary>
		public JResult SaveEntry(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);
			var obj = JsonConvert.DeserializeObject<ProxySmEntry>(json);

			if ((isNew.HasValue & isNew.Value) & obj.EntryDate < DateTime.Now.AddDays(-(Settings.Instance.CountDayEntry + 1)))
				return JResult.Failure( string.Format(Strings.SmEntry_ApplicationprocessingDate,Settings.Instance.CountDayEntry));
			if (obj.Declarant == null || obj.Declarant == 0)
				return JResult.Failure(Strings.SmEntry_SelectDeclarantAndTryAgain);

			Manager.SaveEntry(isNew.HasValue & isNew.Value ? (long?)null : Int32.Parse(obj.Id), CabinetApp.GetCurrentOps(Manager.Uow), obj);

			return JResult.Success(Strings.SmEntry_ApplicationSuccessSaved);
		}

		public JResult LoadMyClients() {
			// Для админа системы загрузка всех клиентов
			if (CurrentOps == null)
				return JResult.Failure(Strings.SmEntry_OrgIsNotSpecified);

			var result = new SmClientManager().GetOrganizationClients(CurrentOps, false);
			var list = result.Items.ToList();
			return JResult.Success(list.Select(a => new { Id = a.Oid, Name = a.Name.ToString() }));
		}

		public JResult LoadAuditors() {
			var result = new OpsManager().GridLoadOpsEmployes(CurrentOps.Oid, 0, 50, ERoleType.SmAuditor);
			return JResult.Success(result.Items.ToList().Select(a => new { Id = a.User.Person.Oid, Name = a.User.Person.FullName }));
		}

		public JResult CheckIin(string iin) {
			var person = new OpsManager().CheckPersonIin(iin);
			return JResult.Success(new ProxySmTeamExpert(person) { Iin = iin });
		}
		
		public JResult LoadTeamResearch(long entryId, int? start, int? limit) {
			if (entryId == null)
				return JResult.Failure(Strings.EntryManager_ApplicationNotFound);
			// TODO MAX: Разобраться как брать ID по GUID
			var team = Manager.LoadTeam(entryId);
			return JResult.Success(team.Items.Select(t => new ProxySmTeamExpert(t)));
		}

		public JResult AddTeamEntry(bool? isNew, string json, string entryId) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);
			var obj = JsonConvert.DeserializeObject<ProxySmTeamExpert>(json);
			
//			if (!FwIinValidator.IsValid(obj.Iin))
//				return JResult.Failure(Strings.Msg_InvalidIinFormat);

			Manager.SaveTeam(Int64.Parse(entryId), obj);
			return JResult.Success(Strings.Common_DataSuccessSaved);
		}

		public JResult DeleteTeam(int? id, string entryId) {
			// return JResult.Success("Функция временно не доступна!");
			if (!id.HasValue)
				return JResult.Failure(Strings.Msg_JResultIsEmpty);

			Manager.DeleteTeamPerson(id.Value, Int64.Parse(entryId));
			return JResult.Success(Strings.SmEntry_TeamMemberDeleted);
		}

		public JResult LoadSertTypes(string entryId) {
			var entryTypes = Manager.GetEntryTypes(Int64.Parse(entryId));

			return JResult.Success(entryTypes.ToList().Select(a => new { Id = a.Oid, Name = a.SmType.ShortName }));
		}
	}
}