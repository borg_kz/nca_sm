﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Framework.Manager;
using App.Framework.Web.Mvc.Exceptions;
using App.Framework.Web.Mvc.Results;
using ECabinet.Core.Managers.SM;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Properties;
using Newtonsoft.Json;

namespace ECabinetApp.Controllers.SM {
	// [Privilege("SmInspection", PrivilegeGroups.SmSertification, AccessType.AccessDenied | AccessType.Read | AccessType.Write | AccessType.Admin)]
	public class SmInspectController : CommonController {
		public ActionResult Index() {
			return View("SmInspect");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/SmInspect")
			.IncludeDirectory("~/Resources/js/SM/SmInspect", "*.js", false);

		public SmSertificateManager Manager { get { return Managers.Find <SmSertificateManager>(); } }

		/// <summary>
		/// Загрузка заявок организации
		/// </summary>
		public JResult LoadSmInspections(int? start, int? limit, string searchId) {
			MonitoringWrite("Загружен модуль: Заявки СМК");

			var result = new List<ProxySmInspection>();
			if (!string.IsNullOrEmpty(searchId)) {
				int search = 0;
				try {
					search = Int32.Parse(searchId);
				} catch {
					throw new FwUserVisibleException(Strings.ErrMsg_WrongNumberSertKSS);
				}
				var ss = Manager.SearchSm(search, CurrentOps.Oid);
				foreach (var sert in ss.Items) {
					CreateInspItem(result, sert);
				}
				
				return JResult.Success(result);
			}

			FwQueryResult<MSmSertificate> serts = null;
			serts = Manager.GetNeedInspectionSerts(CurrentUserRole, CurrentOps, start, limit); 
			foreach (MSmSertificate sertificate in serts.Items)
				CreateInspItem(result, sertificate);

			return JResult.Success(result, serts.Count);
		}

		private static void CreateInspItem(List<ProxySmInspection> result, MSmSertificate sertificate) {
			if (sertificate == null) return;
			result.Add(new ProxySmInspection(sertificate.SmEntry, ESmEntryStatus.New, sertificate));
			result.Add(new ProxySmInspection(sertificate.SmEntry, ESmEntryStatus.Review, sertificate));
			result.Add(new ProxySmInspection(sertificate.SmEntry, ESmEntryStatus.Processed, sertificate));
			result.Add(new ProxySmInspection(sertificate));
			result.AddRange(sertificate.Inspections.Select(i => new ProxySmInspection(i)));
		}

		public JResult SaveInspection(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);

			var obj = JsonConvert.DeserializeObject<ProxySmInspection>(json);
	
			Manager.SaveInspection(obj, isNew.Value);

			return JResult.Success(Strings.SmInspect_InspectSaved);
		}
	}
}