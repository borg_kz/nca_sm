﻿using System;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Xpo;
using App.Data.Xpo.Model.Proxy;
using App.Framework.Web.Mvc.Results;
using App.Framework.Xpo;
using ECabinet.Core;
using ECabinet.Core.Managers.Managment;
using ECabinet.Data;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Properties;
using Newtonsoft.Json;

namespace ECabinetApp.Controllers.SM {
	// [Privilege("SmOpsProfile", PrivilegeGroups.ManageOps, AccessType.AccessDenied | AccessType.Read | AccessType.Write | AccessType.Admin)]
	public class SmOpsProfileController : CommonController {
		private OpsManager Manager { get { return Managers.Find <OpsManager>(); } }

		public ActionResult Index() {
			return View("SmOpsProfile");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/SmOpsProfile")
			.IncludeDirectory("~/Resources/js/SM/SmOpsProfile", "*.js", false);

		public JResult LoadOpsInfo() {
			return JResult.Success(new ProxyOpsUnit(CabinetApp.GetCurrentOps(new FwXpoSession()), EOpsType.OpsSm));
		}

		public JResult SaveOpsInfo(bool? isNew, string json) {
			var obj = JsonConvert.DeserializeObject<ProxyOpsUnit>(json, new MultiLanguageConverter());
			if (string.IsNullOrWhiteSpace(json))
				return JResult.Failure(Strings.Msg_JResultIsEmpty);

			Manager.SaveFullOpsInfo(Int32.Parse(obj.Id), obj);

			// new AuditManager().Write(string.Format("Изменены данные организации: {0}, {1}", obj.Name, obj.Bin), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			return JResult.Success(Strings.Common_DataSuccessChanged);
		}
	}
}