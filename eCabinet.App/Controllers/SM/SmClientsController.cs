﻿using System;
using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Data.Proxy;
using App.Framework.Manager;
using App.Framework.Web.Mvc.Results;
using DocumentFormat.OpenXml.Bibliography;
using ECabinet.Core;
using ECabinet.Core.Managers.SM;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Properties;
using Newtonsoft.Json;

namespace ECabinetApp.Controllers.SM {
	// [Privilege("SmClients", PrivilegeGroups.ManageOps, AccessType.AccessDenied | AccessType.Read | AccessType.Write | AccessType.Admin)]
	public class SmClientsController : CommonController {
		public ActionResult Index() {
			return View("SmClients");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/smClients")
			.IncludeDirectory("~/Resources/js/SM/smClients", "*.js", false);
		public SmClientManager Manager { get { return Managers.Find <SmClientManager>(); } }

		public JResult BtnCheckBinClick(string bin) {
			var org = Manager.CheckBin(bin);
			return JResult.Success(new ProxyClient(org) { Bin = bin });
		}

		/// <summary>
		/// Загрузка списка организаций
		/// </summary>
		public JResult LoadClients(int? start, int? limit, string sort) {
			MonitoringWrite("Загружен модуль: Заявки СМК");
			var sorting = ParseJsonSort<MClientUnit, ProxyClient>(sort);
			FwQueryResult<MClientUnit> clients = null;
			// TODO MAX Для админа надо сделать выбор организации, загружать всё не правильно
			if (CabinetApp.GetCurrentOps(Manager.Uow) == null)
				clients = Manager.GetAllClients(start, limit, sorting);
			else
				clients = Manager.GetOrganizationClients(CabinetApp.GetCurrentOps(Manager.Uow), true, start, limit, sorting);
			return JResult.Success(clients.Items.Select(a => new ProxyClient(a)), clients.Count);
		}
			
		/// <summary>
		/// Add new|save edit client
		/// </summary>
		public JResult SaveClient(bool? isNew, string json) {
			if (string.IsNullOrWhiteSpace(json)) return JResult.Failure(Strings.Common_InvalidParameters);
			var obj = JsonConvert.DeserializeObject<ProxyClient>(json);

			if (string.IsNullOrEmpty(obj.Bin))
				JResult.Failure(Strings.SmClients_BinIsRequiredField);

			//if (isNew.Value && Manager.CheckBin(obj.Bin) != null)
				//JResult.Failure("Организация с таким БИН уже существует. Проверте данные.");

			// TODO MAX: Для глобального админа надо свой обработчик так как у него нет своей Организации
			Manager.SaveClient(isNew, CurrentOps, obj);

			return JResult.Success(Strings.SmClients_ClientSuccessfullySaved);
		}

		public JResult DeleteClient(long clientId) {
			Manager.DeleteClient(CurrentOps, CurrentOps.Clients.First(a => a.Oid == clientId));
			return JResult.Success(Strings.SmClients_ClientDeleted);
		}
	}
}