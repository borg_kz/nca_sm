﻿using System;
using System.Linq;
using System.Web.Mvc;
using App.Bundle;
using App.Framework.Manager;
using App.Framework.Web.Mvc.Results;
using DevExpress.Xpo;
using ECabinet.Core.Managers.SM;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Properties;

namespace ECabinetApp.Controllers.SM {
	public class SmTransferController : CommonController {
		private SmTrancferManager Manager => Managers.Find<SmTrancferManager>();

		// GET: SmTransfer
		public ActionResult Index() {
			return View("SmTransfer");
		}

		public static AppScriptBundle Script = new AppScriptBundle(Plugin.Current, "~/res/js/SM/smTransfer")
			.IncludeDirectory("~/Resources/js/SM/smTransfer", "*.js", false);


		/// <summary> Загрузка списка организаций </summary>
		public JResult LoadTransfers(int? start, int? limit, string sort) {
			var sorting = ParseJsonSort<MSmTransfers, ProxySmTransfer>(sort);
			FwQueryResult<MSmTransfers> clients = Manager.GetAllTransfers(start, limit, sorting);
			return JResult.Success(clients.Items.Select(a => new ProxySmTransfer(a)), clients.Count);
		}

		public JResult LoadListSmOps() {
			return JResult.Success(new XPQuery<MOpsTypes>(CommonManager.Uow).Where(a => a.OpsType == EOpsType.OpsSm).OrderBy(a => a.OpsUnit.Name.L2).ToList()
				.Select(r => new { Id = r.Oid, Name = r.OpsUnit.Name.ToString() }));
		}

		public JResult ChangeOpsForBlank(int opsUnitId, int blankNumber, DateTime date) {
			if (blankNumber.ToString().Length > 7) {
				return JResult.Failure(Strings.EntryManager_InvalidEnteredSequence);
			}

			var sert = new XPQuery<MSmSertificate>(CommonManager.Uow).SingleOrDefault(a => a.BlankNumber == blankNumber);
			if (sert == null)
				return JResult.Failure(Strings.EntryManager_CertificateNotFoundInDB);

			var currentOps = CurrentUserRole.Role.Type == (int)ERoleType.Admin || CurrentUserRole.Role.Type == (int)ERoleType.GlobalAdmin ? sert.OpsUnit : CurrentOps;
			
			var ops = new XPQuery<MOpsUnit>(CommonManager.Uow).SingleOrDefault(a => a.Oid == opsUnitId);
			if (ops.Oid == currentOps.Oid)
				return JResult.Failure("Нельзя перевести сертификат в свой ОПС!");

			if (sert.OpsUnit.Oid != currentOps.Oid && (CurrentUserRole.Role.Type != (int)ERoleType.Admin || CurrentUserRole.Role.Type != (int)ERoleType.GlobalAdmin))
				return JResult.Failure(Strings.EntryManager_CertificateNotFoundInDB);

			sert.OpsUnit = ops;
			sert.SmEntry.OpsUnit = ops;
			sert.SmEntry.Declarant.OpsUnits.Add(ops);
			sert.Save();
			sert.SmEntry.Save();

			var transfer = new MSmTransfers(CommonManager.Uow) {
				Date = date,
				SmSertificate = sert,
				Recipient = ops,
				From = currentOps
			};

			var inspection = new MSmInspection(CommonManager.Uow) {
				SmSertificate = sert,
				InspectionDate = date,
				Status = ESmSertStatus.Transfered,
				Result = $"Выполнен трансфер {ops?.Name} => {currentOps.Name}"
			};
			inspection.Save();
			transfer.Save();

			CommonManager.Uow.CommitChanges();

			return JResult.Success(string.Format(Strings.EntryManager_OpsBlankChanged, blankNumber, ops?.Name));
		}
	}
}