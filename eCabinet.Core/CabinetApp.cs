﻿using System;
using System.Linq;
using System.Web;
using App;
using App.Data.Xpo;
using App.Data.Xpo.Extensions;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework.Xpo;
using DevExpress.Xpo;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Core {
	public class CabinetApp : AppManager {
		public const string CurrOpsId = "OpsId";

		// TODO: Нужен кеш который понимает для сесси какой надо возращать ОПС
		public static MOpsUnit GetCurrentOps(FwXpoSession uow) {
			if (HttpContext.Current != null && HttpContext.Current.Session != null) {
				if (HttpContext.Current.Session[CurrOpsId] == null) {
					var user = AppGlobal.CurrentUser.GetUser(uow);
					switch ((ERoleType)user.CurrentRole.Role.Type) {
						case ERoleType.GlobalAdmin:
						case ERoleType.Admin:
						case ERoleType.ReadOnly:
							var firstOps = new XPQuery<MOpsUnit>(uow).FirstOrDefault(a => a.Oid > 0 );
							if (firstOps != null) {
								SetSessionValue(CurrOpsId, firstOps.Oid);
								return firstOps;
							}
							break;
						case ERoleType.SmAdmin:
							var ops = new XPQuery<MOpsUnit>(uow).FirstOrDefault(a => a.Administrator.Oid == user.Oid);
							if (ops != null) {
								SetSessionValue(CurrOpsId, ops.Oid);
								return ops;
							}

							var empAdmin = new XPQuery<MEmploye>(uow).FirstOrDefault(a => a.User.Oid == user.Oid && a.Role.Type == (int)ERoleType.SmAdmin && a.OpsUnit != null);
							if (empAdmin == null)
								return null;
							SetSessionValue(CurrOpsId, empAdmin.OpsUnit.Oid);
							return empAdmin.OpsUnit;
						case ERoleType.SmSecretary:
						case ERoleType.SmAuditor:
						case ERoleType.SmExpert:
							// TODO MAX Надо брать выбранную организацию а не первую
							var emp = new XPQuery<MEmploye>(uow).FirstOrDefault(a => a.User.Oid == user.Oid && a.Role.Type == user.CurrentRole.Role.Type && a.OpsUnit != null);
							if (emp == null)
								return null;
							SetSessionValue(CurrOpsId, emp.OpsUnit.Oid);
							return emp.OpsUnit;
					}
				}

				if (HttpContext.Current.Session[CurrOpsId] != null) {
					int opsId = Int32.Parse(HttpContext.Current.Session[CurrOpsId].ToString());
					return new XPQuery<MOpsUnit>(uow).FirstOrDefault(a => a.Oid == opsId);
				}
			}

			return null;
		}

		public static void SetSessionValue(string key, object value) {
			HttpContext.Current.Session[key] = value;
		}
	}
}