﻿using App.Data.Proxy;
using App.Data.Xpo.Model;

namespace ECabinet.Core {
	public static class Extentions {
		public static bool Compare(this MultiString source, AppProxyMultiString str) {
			MultiString temp = str;
			return Compare(source, temp);
		}

		public static bool Compare(MultiString source, MultiString str) {
			return source.L1 == str.L1 && source.L2 == str.L2 && source.L3 == str.L3;
		}
	}
}