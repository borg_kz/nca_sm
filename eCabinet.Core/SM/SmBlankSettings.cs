﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Serialization;
using ECabinet.Data.Model.Entities.SM;

namespace ECabinet.Core.SM {
	[Serializable]
	public class SmBlankSettings {
		public Dictionary<string, string> Settings { get; set; }
		public SmBlankSettings() {
			Settings = new Dictionary<string, string>();
		}

		public string GetValue(string key) {
			string result;
			Settings.TryGetValue(key, out result);
			if (result == null) return "-100";

			return Settings[key];
		}

		public static HtmlString InitField(string field, string xy, string value) {
			string fieldTmpl = "this.initField(this.getEl('{0}'), '{1}', '{2}');";

			return new HtmlString(string.Format(fieldTmpl, field, xy, value == null ? string.Empty : value.Replace("'", "\"").Replace("\n", "<br>")) + Environment.NewLine);
		}
	}

	[Serializable]
	public class FieldSetting {
		public FieldSetting() {
			Top = -100;
			Left = -100;
		}

		[XmlAttribute]
		public string Name { get; set; }
		[XmlAttribute]
		public int Left { get; set; }
		[XmlAttribute]
		public int Top { get; set; }
	}

	[Serializable]
	public class FieldMultiLanguageSetting {
		public FieldSetting Rus { get; set; }
		public FieldSetting Kaz { get; set; }
		public FieldSetting Eng { get; set; }
	}
}
