﻿using App.Framework.Utils;
using ECabinet.Data;

namespace ECabinet.Core.Helpers {
	public static class LocalizeKazHelper {
		public static string Month(string month) {
			switch (month.Trim().ToLower()) {
				case "января": 
				case "янв":
				case "1":
					return Month(1);
				case "февраля":
				case "фев":
				case "2":
					return Month(2);
				case "марта":
				case "мар":
				case "3":
					return Month(3);
				case "апреля":
				case "апр":
				case "4":
					return Month(4);
				case "мая":
				case "5":
					return Month(5);
				case "июня":
				case "июн":
				case "6":
					return Month(6);
				case "июля":
				case "июл":
				case "7":
					return Month(7);
				case "августа":
				case "авг":
				case "8":
					return Month(8);
				case "сентября":
				case "сен":
				case "9":
					return Month(9);
				case "октября":
				case "окт":
				case "10":
					return Month(10);
				case "ноября":
				case "ноя":
				case "11":
					return Month(11);
				case "декабря":
				case "дек":
				case "12":
					return Month(12);
			}

			return string.Empty;
		}

		public static string Month(int month) {
			switch (month) {
				case 1: return "қаңтар";
				case 2: return "ақпан";
				case 3: return "наурыз";
				case 4: return "сәуір";
				case 5: return "мамыр";
				case 6: return "маусым";
				case 7: return "шілде";
				case 8: return "тамыз";
				case 9: return "қыркүйек";
				case 10: return "қазан";
				case 11: return "қараша";
				case 12: return "желтоқсан";
			}

			return string.Empty;
		}

		public static string MonthRus(int month) {
			switch (month) {
				case 1: return "января";
				case 2: return "февраля";
				case 3: return "марта";
				case 4: return "апреля";
				case 5: return "мая";
				case 6: return "июня";
				case 7: return "июля";
				case 8: return "августа";
				case 9: return "сентября";
				case 10: return "октября";
				case 11: return "ноября";
				case 12: return "декабря";
			}

			return string.Empty;
		}

		public static string MonthEng(int month) {
			switch (month) {
				case 1: return "january";
				case 2: return "february";
				case 3: return "march";
				case 4: return "april";
				case 5: return "may";
				case 6: return "june";
				case 7: return "july";
				case 8: return "august";
				case 9: return "september";
				case 10: return "october";
				case 11: return "november";
				case 12: return "december";
			}

			return string.Empty;
		}
	}
}
