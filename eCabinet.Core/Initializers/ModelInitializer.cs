﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using App.Data.Xpo.Initializer;
using App.Framework;
using ECabinet.Data;

namespace ECabinet.Core.Initializers {
	[Serializable]
	[XmlRoot("Init")]
	public class XmlInitializer : AppInitRootXml {
//		public XmlInitializer() {
//			Roles = new Collection<AppRole>();
//			Users = new Collection<AppUser>();
//		}

//		[XmlArray(Form = XmlSchemaForm.Unqualified)]
//		[XmlArrayItem("AppRole", Form = XmlSchemaForm.Unqualified)]
//		public Collection<AppRole> Roles { get; set; }

//		[XmlArray(Form = XmlSchemaForm.Unqualified)]
//		[XmlArrayItem("AppUser", Form = XmlSchemaForm.Unqualified)]
//		public Collection<AppUser> Users { get; set; }

		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("AreaUnit", Form = XmlSchemaForm.Unqualified)]
		public Collection<AppAreaUnit> AreaUnits { get; set; }

		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("Ops", Form = XmlSchemaForm.Unqualified)]
		public Collection<AppOpsUnit> OpsUnits { get; set; }

		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("Type", Form = XmlSchemaForm.Unqualified)]
		public Collection<AppSmSertTypes> SmSertTypes { get; set; }
	}

	[Serializable]
	public class AppUser {

		[XmlAttribute]
		public string FirstName { get; set; }

		[XmlAttribute]
		public string SecondName { get; set; }

		[XmlAttribute]
		public string ThirdName { get; set; }

		[XmlAttribute]
		public string Iin { get; set; }

		[XmlAttribute]
		public ERoleType RoleType { get; set; }
		
		[XmlAttribute]
		public EGender Gender { get; set; }

		[XmlAttribute]
		public string Login { get; set; }

		[XmlAttribute]
		public string Password { get; set; }

		[XmlAttribute]
		public Guid RoleGid { get; set; }
	}

	[Serializable]
	public class AppRole {

		[XmlAttribute]
		public Guid Gid { get; set; }

		[XmlAttribute]
		public string Name { get; set; }
		
		[XmlAttribute]
		public ERoleType Type { get; set; }

		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("AppPrivilege", Form = XmlSchemaForm.Unqualified)]
		public Collection<AppPrivilege> AppPrivileges { get; set; }
	}

	[Serializable]
	public class AppPrivilege {
		[XmlArray(Form = XmlSchemaForm.Unqualified)]
		[XmlArrayItem("Rule", Form = XmlSchemaForm.Unqualified)]
		public Collection<Rule> Rules { get; set; }
		
		[XmlAttribute]
		public string Id { get; set; }

		[XmlAttribute]
		public bool Checked { get; set; }
	}

	[Serializable]
	public class Rule {
		[XmlAttribute]
		public string Id { get; set; }

		[XmlAttribute]
		public bool Checked { get; set; }
	}

	[Serializable]
	public class AppAreaUnit {

		[XmlAttribute]
		public Guid Gid { get; set; }

		[XmlAttribute]
		public string NameKz { get; set; }

		[XmlAttribute]
		public string NameRu { get; set; }

		[XmlAttribute]
		public EAreaUnitType Type { get; set; }

		[XmlAttribute]
		public Guid Parent { get; set; }

		[XmlAttribute]
		public EAreaUnitStatus Status { get; set; }

		[XmlAttribute]
		public int Code { get; set; }
	}

	[Serializable]
	public class AppOpsUnit {
		[XmlAttribute]
		public string Name { get; set; }

		[XmlAttribute]
		public string Number { get; set; }

		[XmlAttribute]
		public string Login { get; set; }

		[XmlAttribute]
		public string PostCode { get; set; }

		[XmlAttribute]
		public string Code { get; set; }

		[XmlAttribute]
		public string Adress { get; set; }

		[XmlAttribute]
		public string Boss { get; set; }

		[XmlAttribute]
		public string Tel { get; set; }

		[XmlAttribute]
		public string Mail { get; set; }

		[XmlAttribute]
		public string Site { get; set; }

		[XmlAttribute]
		public DateTime DateFrom { get; set; }

		[XmlAttribute]
		public DateTime DateTo { get; set; }
	}

	[Serializable]
	public class AppSmSertTypes {
		[XmlAttribute]
		public string Code { get; set; }
		[XmlAttribute]
		public string Value { get; set; }
		[XmlAttribute]
		public string ShortName { get; set; }
		[XmlAttribute]
		public string System { get; set; }
	}
}
