﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Data.Xpo.Initializer;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using App.Data.Xpo.Model.Entities.Auth;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework;
using DevExpress.Xpo;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Reference;

namespace ECabinet.Core.Initializers {
	public class DataInitializer : DefaultDataInitializerBase<XmlInitializer> {
		public DataInitializer(string path) : base(path) { }

//		protected override void InitData(XmlInitializer obj) {
//			MyRolesInit(obj.Roles);
//			MyUsersInit(obj.Users);
//		}
		protected override void CustomInit() {
			AreaUnitsInit(CurrentData.AreaUnits);
			OpsUnitsInit(CurrentData.OpsUnits);
			SmSertTypesInit(CurrentData.SmSertTypes);
		}

		#region Initializiations

		/// <summary> Инициализация ролей </summary>
		/// <param name="appRoles">Роли</param>
		protected void MyRolesInit(ICollection<AppRole> appRoles) {
			foreach (var appRole in appRoles) {
				var role = Uow.Query<MAppRole>().FirstOrDefault(a => a.Type == (int)appRole.Type);
				// if (role == null && appRole.Gid == Guid.Empty) continue;
				if (role == null)
					role = new MAppRole(Uow) {
						Name = appRole.Name,
						Type = (int)appRole.Type,
						Gid = appRole.Gid != Guid.Empty ? appRole.Gid : Guid.NewGuid()
					};
				foreach (var appPrivilege in appRole.AppPrivileges) {
					var privilege = role.Privileges.FirstOrDefault(a => a.Key == appPrivilege.Id);
					if (privilege != null) continue;
					privilege = new MAppPrivilege(Uow) {
						Key = appPrivilege.Id,
						Role = role,
						Checked = true
					};

					foreach (var rule in appPrivilege.Rules) {
						privilege.Rules.Add(new MAppRule(Uow) {
							Privilege = privilege,
							Checked = rule.Checked,
							Key = rule.Id
						});
					}
					privilege.Save();
					role.Privileges.Add(privilege);
				}
				role.Save();
			}
			Uow.CommitChanges();
		}

		/// <summary> Инициализация пользователей </summary>
		/// <param name="users">Пользователи</param>
		protected void MyUsersInit(ICollection<AppUser> users) {
			if (Uow.Query<MAppUser>().Any()) return;
			foreach (var initUser in users) {
				var person = Uow.Query<MAppPerson>().FirstOrDefault(a => a.Iin == initUser.Iin) ?? new MAppPerson(Uow) {
					FirstName = initUser.FirstName,
					SecondName = initUser.SecondName,
					ThirdName = initUser.ThirdName,
					Iin = initUser.Iin,
					Gender = initUser.Gender
				};
				person.Save();
				var user = new MAppUser(Uow) {
					Person = person,
					UserName = initUser.Login
				};

				var role = Uow.Query<MAppRole>().FirstOrDefault(a => a.Type == (int)initUser.RoleType);
				if (role != null) {
					user.Roles.Add(new MAppUserRole(Uow) {
						Role = role,
						User = user
					});
				}

				user.SetPassword(initUser.Password);
				user.Save();
			}
			Uow.CommitChanges();
		}

		/// <summary> Инициализация Города и области </summary>
		/// <param name="units">Города и области</param>
		protected void AreaUnitsInit(ICollection<AppAreaUnit> units) {
			units = units.Where(a => a.Gid != Guid.Empty).ToArray();
			var list = Uow.Query<MAreaUnit>().ToList();
			foreach (var unit in units) {
				var u = list.FirstOrDefault(a => a.Gid == unit.Gid) ?? new MAreaUnit(Uow);
				u.Name = new MultiString(new MString(unit.NameKz, FwCultures.Kaz), new MString(unit.NameRu, FwCultures.Rus));
				u.Gid = unit.Gid;
				u.Type = unit.Type;
				u.Code = unit.Code;
				u.Status = unit.Status;
				if (u.IsNew)
					list.Add(u);
				u.Save();
			}
			foreach (var unit in units) {
				if (unit.Parent == Guid.Empty) continue;
				var u = list.First(a => a.Gid == unit.Gid);
				u.Parent = list.FirstOrDefault(a => a.Gid == unit.Parent);
				u.Save();
			}
			Uow.CommitChanges();
		}

		/// <summary>
		/// Инициализация ОПС СМК
		/// </summary>
		protected void OpsUnitsInit(ICollection<AppOpsUnit> units) {
			if (Uow.Query<MOpsUnit>().Any()) return;
			units = units.Where(a => a.Code != string.Empty).ToArray();
			var r = new Random(Environment.TickCount);
			foreach (var unit in units) {
				var u = new MOpsUnit(Uow) {
					Name = new MultiString(unit.Name),
					Location = Uow.Query<MAreaUnit>().FirstOrDefault(a => a.Code == int.Parse(unit.Code)),
					FioBoss = unit.Boss,
					Phone = unit.Tel,
					PostCode = unit.PostCode,
					AttestateNumber = unit.Number,
					SmSertNumber = "KZ." + unit.Code + unit.Number.Remove(0, 6) + ".07.03",
					Adress = unit.Adress,
					Email = unit.Mail,
					Site = unit.Site,
					SystemRegistrationDate = DateTime.Now,
					DateActivityFrom = unit.DateFrom,
					DateActivityTo = unit.DateTo,

					Administrator = new MAppUser(Uow) {
						UserName = unit.Login
					}
				};
				var opsType = new MOpsTypes(Uow) { OpsType = EOpsType.OpsSm, OpsUnit = u };
				u.OpsTypes.Add(opsType);
				u.Administrator.SetPassword("111111");
				u.Administrator.Person = new MAppPerson(Uow) { FirstName = "", SecondName = u.Name, Iin = (Environment.TickCount - r.Next(Int16.MaxValue)).ToString() };
				var role = Uow.Query<MAppRole>().FirstOrDefault(a => a.Type == (int)ERoleType.SmAdmin);
				var empl = new MEmploye(Uow) { User = u.Administrator, OpsUnit = u, Role = role };
				u.Employes.Add(empl);
				var uRole = new MAppUserRole(Uow) {
					User = u.Administrator,
					Role = role
				};

				empl.Save();
				opsType.Save();
				uRole.Save();
				u.Save();
			}

			Uow.CommitChanges();
		}

		private void SmSertTypesInit(IEnumerable<AppSmSertTypes> smTypes) {
			if (Uow.Query<MSertificationStore>().Any()) return;
			foreach (var type in smTypes) {
				var u = new MSertificationStore(Uow) {
					Name = new MultiString(type.Code),
					Equriments = new MultiString(type.Value),
					System = new MultiString(type.System),
					ShortName = type.ShortName,
					STRKType = ESTRKType.SM
				};

				u.Save();
			}

			Uow.CommitChanges();
		}

		#endregion Initializiations
	}
}
