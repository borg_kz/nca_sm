﻿using App.Data.Xpo.Manager;
using App.Framework.Manager;
using ECabinet.Data.Model.Entities.SM;

namespace ECabinet.Core.Managers.SM {
	public class SmTrancferManager : AppManager {
		public FwQueryResult<MSmTransfers> GetAllTransfers(int? start, int? limit, FwSortObject<MSmTransfers>[] sorting) {
			var ops = CabinetApp.GetCurrentOps(Uow);
			return Query(start, limit, a => a.From.Oid == ops.Oid || a.Recipient.Oid == ops.Oid, sorting);
		}
	}
}