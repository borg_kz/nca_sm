﻿using System;
using System.Linq.Expressions;
using App.Data.Xpo.Extensions;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using App.Data.Xpo.Model.Entities.Auth;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework;
using App.Framework.Manager;
using App.Framework.Utils.Sort;
using App.Framework.Web.Mvc.Exceptions;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Model.Reference;
using ECabinet.Data.Properties;

namespace ECabinet.Core.Managers.SM {
	public class SmSertificateManager : AppManager {
		public FwQueryResult<MSmSertificate> GetUserSertificates(MAppUserRole role, MOpsUnit ops, int sertType, int? findPatern, int? start = null, int? limit = null) {
			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);
			role = First<MAppUserRole>(r => r.Oid == role.Oid);

			if (role.Role.Type == (int)ERoleType.SmAuditor)
				if (findPatern.HasValue && findPatern.Value > 0)
					return Query<MSmSertificate>(start, limit, a => 
						a.OpsUnit == ops 
						&& a.Auditor.Oid == role.User.Person.Oid 
						&& a.Status == (ESmSertStatus) sertType 
						&& (a.BlankNumber == findPatern.Value || a.Counter == findPatern.Value), new FwSortObject<MSmSertificate>(a => a.StartDate, FwSortDirection.DESC));
				else
					return Query<MSmSertificate>(start, limit, a => a.OpsUnit == ops && a.Auditor.Oid == role.User.Person.Oid && a.Status == (ESmSertStatus) sertType, new FwSortObject<MSmSertificate>(a => a.StartDate, FwSortDirection.DESC));

			if (findPatern.HasValue && findPatern.Value > 0)
				return Query<MSmSertificate>(start, limit, a => a.OpsUnit == ops && a.Status == (ESmSertStatus)sertType && (a.BlankNumber == findPatern.Value || a.Counter == findPatern.Value), new FwSortObject<MSmSertificate>(a => a.StartDate, FwSortDirection.DESC));

			return Query<MSmSertificate>(start, limit, a => a.OpsUnit == ops && a.Status == (ESmSertStatus)sertType, new FwSortObject<MSmSertificate>(a => a.StartDate, FwSortDirection.DESC));
		}

		public FwQueryResult<MEntrySmTypeSert> GetUserDoneEntry(MAppUserRole role, MOpsUnit ops, int? start = null, int? limit = null) {
			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);
			role = First<MAppUserRole>(r => r.Oid == role.Oid);

			if (role.Role.Type == (int)ERoleType.SmAuditor)
				return Query<MEntrySmTypeSert>(start, limit, a => a.SmEntry.OpsUnit == ops && (a.SmEntry.Status == ESmEntryStatus.Processed || a.SmEntry.Status == ESmEntryStatus.Sertificate) && a.SmEntry.Auditor == role.User.Person && a.Sertificate == null);
			
			return Query<MEntrySmTypeSert>(start, limit, a => a.SmEntry.OpsUnit == ops && (a.SmEntry.Status == ESmEntryStatus.Processed || a.SmEntry.Status == ESmEntryStatus.Sertificate) && a.Sertificate == null);
		}

		/// <summary>
		/// Загрузка сертификатов которые не прошли инспекцию но дата инспекции уже близко
		/// </summary>
		public FwQueryResult<MSmSertificate> GetNeedInspectionSerts(MAppUserRole role, MOpsUnit ops, int? start = null, int? limit = null) {
			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);
			role = First<MAppUserRole>(r => r.Oid == role.Oid);

			if (role.Role.Type != (int)ERoleType.SmAuditor)
				return Query<MSmSertificate>(start, limit, a => a.OpsUnit == ops && a.SmEntry.Status == ESmEntryStatus.Sertificate
					&& a.InspectionDate > DateTime.Now.AddMonths(-1) && a.Status != ESmSertStatus.Dublicate);
			
			return Query<MSmSertificate>(start, limit, a => a.OpsUnit == ops && a.Auditor == role.User.Person && a.SmEntry.Status == ESmEntryStatus.Sertificate
				&& a.InspectionDate > DateTime.Now.AddMonths(-1) && a.Status != ESmSertStatus.Dublicate);
		}

		/// <summary>
		/// Загрузка списка проведённых инспекций
		/// </summary>
		public FwQueryResult<MSmInspection> GetSmInspection(MAppPerson auditor, MOpsUnit org, int? start = null, int? limit = null) {
			return Query<MSmInspection>(start, limit, a => a.Auditor == auditor && a.SmSertificate.OpsUnit == org);
		}

		public bool CheckBlancNumber(int number) {
			return Any<MSmSertificate>(a => a.BlankNumber == number && a.Status != ESmSertStatus.Deleted);
		}

		public MClientUnit GetClientProfile(int sertId) {
			var sertificate = First<MSmSertificate>(a => a.Oid == sertId && a.Status != ESmSertStatus.Deleted);
			sertificate.SmEntry.Declarant.RegistrationDate = sertificate.FirstRegisterDate ?? new DateTime();
			sertificate.SmEntry.Declarant.Oid = sertificate.Oid;
			return sertificate.SmEntry.Declarant;
		}

		public void SaveClientProfile(ProxyClient obj) {
			var sert = First<MSmSertificate>(a => a.Oid == Int32.Parse(obj.Id));
			var client = sert.SmEntry.Declarant;
			client.Adress = obj.Adress;
			client.Name = obj.Name;
			sert.FirstRegisterDate = obj.RegistrationDate;
			client.Save();
			Uow.CommitChanges();
		}
		
		public void SaveAuditor(ProxySmSertificate obj) {
			var sertificate = First<MSmSertificate>(a => a.Oid == Int32.Parse(obj.Id));
			sertificate.Auditor = First<MAppPerson>(a => a.Oid == obj.AuditorId);
			Uow.CommitChanges();
		}

		public void SaveSertificate(bool inNew, MOpsUnit ops, ProxySmSertificate obj, MAppUser user) {
			if (user.CurrentRole.Role.Type != (int)ERoleType.SmAuditor && user.CurrentRole.Role.Type != (int)ERoleType.SmAdmin)
				throw new FwUserVisibleException(Strings.SmSertificate_NoRightsForChange);

			if (false && inNew && Any<MSmSertificate>(a => a.Counter == ops.LastSertNumber + 1 && a.OpsUnit.Oid == ops.Oid))
				throw new FwUserVisibleException(Strings.SmSertificate_CertWithThatNumbAlreadyExists);

			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);

			if (obj.Status == ESmSertStatus.None)
				throw new FwUserVisibleException(Strings.SmSertificate_StatusNotDetected + obj.Id);

			MSmSertificate sert = null;
			if (obj.Status == ESmSertStatus.Valid || obj.Status == ESmSertStatus.Dublicate) {
				sert = First<MSmSertificate>(a => a.Oid == Int32.Parse(obj.Id));
				if (sert == null)
					throw new FwUserVisibleException(Strings.SmSertificate_SavingImpossible_CertNotFound + obj.Id);
			} else {
				sert = new MSmSertificate(Uow);
				var smType = First<MEntrySmTypeSert>(a => a.Oid == Int32.Parse(obj.Id));
				smType.Sertificate = sert;
				sert.SmEntry = smType.SmEntry;
				sert.SmEntry.Status = ESmEntryStatus.Sertificate;
				sert.Auditor = smType.SmEntry.Auditor;
				sert.OpsUnit = ops;
				ops.LastSertNumber++;
				sert.Counter = ops.LastSertNumber;
				sert.Status = ESmSertStatus.Valid;

				//new AuditManager().Write("Создан сертификат СМ. ОПС [" + ops.Name + "]", AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
			}

			sert.AreaSertification = new MultiString(new MString(obj.AreaSertification, FwCultures.Rus), new MString(obj.AreaSertificationKz, FwCultures.Kaz), new MString(obj.AreaSertificationEn, FwCultures.Eng));
			sert.AreaSertificationAppendix = new MultiString(new MString(obj.AreaSertificationAppendix, FwCultures.Rus), new MString(obj.AreaSertificationAppendixKz, FwCultures.Kaz), new MString(obj.AreaSertificationAppendixEn, FwCultures.Eng));
			sert.Requirements = new MultiString(new MString(obj.Requirements, FwCultures.Rus), new MString(obj.RequirementsKz, FwCultures.Kaz), new MString(obj.RequirementsEn, FwCultures.Eng));
			sert.System = new MultiString(new MString(obj.System, FwCultures.Rus), new MString(obj.SystemKz, FwCultures.Kaz), new MString(obj.SystemEn, FwCultures.Eng));
			
			sert.BlankNumber = Int32.Parse(obj.BlankNumber);
			sert.BlankNumberAppendix = obj.BlankNumberAppendix;
			sert.StartDate = obj.StartDate.Value;
			sert.FirstRegisterDate = obj.StartDate.Value;
			sert.FinishDate = obj.FinishDate.Value;
			sert.InspectionDate = obj.InspectionDate.Value;
			sert.Signatory = obj.Signatory;
			sert.SertificationStore = First<MSertificationStore>(a => a.Oid == obj.SmSertificationType);

			ops.Save();
			sert.Save();
			Uow.CommitChanges();
//			if (obj.Status == ESmSertStatus.Valid)
//				new AuditManager().Write(string.Format("Добавлен сертификат СМ - {0}/{1}, организация [{2}]", sert.Counter, sert.BlankNumber, ops.Name), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
//			else
//				new AuditManager().Write(string.Format("Изменён сертификат СМ - {0}/{1}, организация [{2}]", sert.Counter, sert.BlankNumber, ops.Name), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
		}

		public void DublicateSertificate(MOpsUnit ops, ProxySmSertificate obj, MAppUser user) {
			if (user.CurrentRole.Role.Type != (int)ERoleType.SmAuditor && user.CurrentRole.Role.Type != (int)ERoleType.SmAdmin)
				throw new FwUserVisibleException(Strings.SmSertificate_NoRightsForChange);

			if (obj.DublicateReason == null)
				throw new FwUserVisibleException(Strings.SmSertificate_ReasonForRedesignNotSpecified);

			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);

			var sert = First<MSmSertificate>(a => a.Oid == Int32.Parse(obj.Id));
			if (sert == null)
				throw new FwUserVisibleException(Strings.SmSertificate_SavingImpossible_CertNotFound + obj.Id);

			var dublicator = new MSmSertificate(Uow) {
				SmEntry = sert.SmEntry,
				OpsUnit = ops,
				AreaSertification = new MultiString(
					new MString(obj.AreaSertificationKz, FwCultures.Kaz),
					new MString(obj.AreaSertificationEn, FwCultures.Eng),
					new MString(obj.AreaSertification, FwCultures.Rus)),
				AreaSertificationAppendix = obj.AreaSertificationAppendix,
				Requirements = new MultiString(new MString(obj.Requirements, FwCultures.Rus), new MString(obj.RequirementsKz, FwCultures.Kaz), new MString(obj.RequirementsEn, FwCultures.Eng)),
				System = new MultiString(new MString(obj.System, FwCultures.Rus), new MString(obj.SystemEn, FwCultures.Eng)),
				BlankNumber = Int32.Parse(obj.BlankNumber),
				BlankNumberAppendix = obj.BlankNumberAppendix,
				StartDate = obj.StartDate.Value,
				FirstRegisterDate = sert.FirstRegisterDate ?? sert.StartDate,
				FinishDate = obj.FinishDate.Value,
				InspectionDate = obj.InspectionDate.Value,
				Signatory = obj.Signatory,
				Status = ESmSertStatus.Dublicate,
				Auditor = sert.Auditor,
				DublicateReason = First<MSmDublicateReason>(a => a.Oid == obj.DublicateReason),
				Counter = sert.Counter
			};

			dublicator.FinishDate = sert.FinishDate;
			if (dublicator.DublicateReason.Oid == 9)
				dublicator.FinishDate = obj.FinishDate.Value;
			
			switch (dublicator.DublicateReason.Oid) {
				case 10: dublicator.SertificationStore = First<MSertificationStore>(a => a.Oid == 19); break;
				case 11: dublicator.SertificationStore = First<MSertificationStore>(a => a.Oid == 18); break;
				case 12: dublicator.SertificationStore = First<MSertificationStore>(a => a.Oid == 20); break;
				case 10010: dublicator.SertificationStore = First<MSertificationStore>(a => a.Oid == 21); break;
				case 10013: dublicator.SertificationStore = First<MSertificationStore>(a => a.Oid == 33); break;
				case 10015: dublicator.SertificationStore = First<MSertificationStore>(a => a.Oid == 25); break;
				default:
					dublicator.SertificationStore = sert.SertificationStore; break;
			}

			if (dublicator.DublicateReason.Oid == 11 
				|| dublicator.DublicateReason.Oid == 10
				|| dublicator.DublicateReason.Oid == 12
				|| dublicator.DublicateReason.Oid == 10010
				|| dublicator.DublicateReason.Oid == 10013
				|| dublicator.DublicateReason.Oid == 10014
				|| dublicator.DublicateReason.Oid == 10015)
				sert.Status = ESmSertStatus.Cancelled;

			var insp = new MSmInspection(Uow);
			insp.Auditor = CurrentUser.GetUser(Uow).Person;
			insp.InspectionDate = DateTime.Now;
			insp.SmSertificate = dublicator;
			insp.Status = ESmSertStatus.Dublicate;
			insp.Result = string.Format(Strings.SmSertificate_KssCert_Numb_Reason, dublicator.BlankNumber, dublicator.Counter, dublicator.DublicateReason);

			sert.Inspections.Add(insp);
			sert.Save();
			insp.Save();
			dublicator.Save();
			Uow.CommitChanges();
			//new AuditManager().Write(string.Format("Переоформлен сертификат КСС: {0}, Номер: {1}, [{2}].", dublicator.BlankNumber, dublicator.Counter, user.Login), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
		}

		public void DeleteSert(MSmSertificate sert) {
			sert.BlankNumber = sert.BlankNumber * -1;
			if (sert.Status == ESmSertStatus.Valid)
				sert.SmEntry.Status = ESmEntryStatus.Processed;
			sert.Status = ESmSertStatus.Deleted;
			var stype = First<MEntrySmTypeSert>(a => a.SmEntry == sert.SmEntry && a.SmType == sert.SertificationStore);
			stype.Sertificate = null;
			if (sert.OpsUnit.LastSertNumber == sert.Counter)
				sert.OpsUnit.LastSertNumber--;
			sert.Save();
			sert.OpsUnit.Save();

			Uow.CommitChanges();
		}

		public void SaveInspection(ProxySmInspection obj, bool isNew) {
			// var ops = First<MOpsUnit>(o => o.Oid == 0);
			MSmInspection insp = null;
			if (isNew) {
				insp = new MSmInspection(Uow);
				insp.SmSertificate = First<MSmSertificate>(a => a.Oid == obj.SertId);
			} else {
				insp = First<MSmInspection>(a => a.Oid == Int32.Parse(obj.Id));
				// Если узнаём что статус инспекции поменялся то создаём новую инспекцию
				if (insp.SmSertificate.Status != obj.Status) {
					var temp = insp;
					insp = new MSmInspection(Uow);
					insp.SmSertificate = temp.SmSertificate;
				}
			}

			insp.InspectionDate = obj.DateInspection.Value;
			insp.Result = obj.Result;
			insp.SmSertificate.Status = obj.Status;
			insp.Status = obj.Status;
			insp.Auditor = insp.SmSertificate.Auditor;
			
			insp.Save();
			Uow.CommitChanges();

//			new AuditManager().Write(string.Format("Сохранена ИНСПЕКЦИЯ сертификата СМ КСС [{0}], организация [{1}]", insp.SmSertificate.BlankNumber,
//				insp.SmSertificate.OpsUnit.Name), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
		}

		public FwQueryResult<MSmSertificate> SearchSm(int sert, long ops, int? start = null, int? limit = null) {
			return Query<MSmSertificate>(start, limit, a => (a.BlankNumber == sert || a.Counter == sert) && a.OpsUnit.Oid == ops);
		}

		public FwQueryResult<MSmSertificate> FindSertificates(long? ops, int? sertType, DateTime? startdate, DateTime? finishDate, int? blank, int? sert, string declarant, int? start, int? limit) {
			Expression<Func<MSmSertificate, bool>> filter = a => a.Oid > 0;
			if (ops != null)
				filter = Combine(filter, x => x.OpsUnit.Oid == ops.Value, false);
			if (sertType != null)
				filter = Combine(filter, x => x.SertificationStore.Oid == sertType.Value, false);
			if (startdate != null && finishDate != null) {
				filter = Combine(filter, x => x.StartDate >= startdate.Value && x.StartDate <= finishDate.Value, false);
			} else {
				if (startdate != null)
					filter = Combine(filter, x => x.StartDate >= startdate.Value, false);
				if (finishDate != null)
					filter = Combine(filter, x => x.StartDate <= finishDate.Value, false);
			}
			if (blank != null)
				filter = Combine(filter, x => x.BlankNumber == blank.Value, false);
			if (sert != null)
				filter = Combine(filter, x => x.Counter == sert, false);
			
			if (!string.IsNullOrEmpty(declarant))
				filter = Combine(filter, x => x.SmEntry.Declarant.Name.L2.ToLower().Contains(declarant.ToLower()) || x.SmEntry.Declarant.Name.L1.ToLower().Contains(declarant.ToLower()), false);
			
			filter = Combine(filter, x => x.Status != ESmSertStatus.Deleted, false);
			return Query(start, limit, filter, new FwSortObject<MSmSertificate>(a => a.StartDate, FwSortDirection.DESC));
		}

		Expression<Func<T, bool>> Combine<T>(Expression<Func<T, bool>> filter1, Expression<Func<T, bool>> filter2, bool isOr = true) {
			// combine two predicates:
			// need to rewrite one of the lambdas, swapping in the parameter from the other
			var rewrittenBody1 = new ReplaceVisitor(
				filter1.Parameters[0], filter2.Parameters[0]).Visit(filter1.Body);
			var newFilter = isOr ? Expression.Lambda<Func<T, bool>>(
				Expression.Or(rewrittenBody1, filter2.Body), filter2.Parameters) : Expression.Lambda<Func<T, bool>>(
				Expression.And(rewrittenBody1, filter2.Body), filter2.Parameters);
			return newFilter;
		}
	}

	class ReplaceVisitor : ExpressionVisitor {
		private readonly Expression from, to;
		public ReplaceVisitor(Expression from, Expression to) {
			this.from = from;
			this.to = to;
		}
		public override Expression Visit(Expression node) {
			return node == from ? to : base.Visit(node);
		}
	}
}