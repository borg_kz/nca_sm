﻿using System;
using System.Linq;
using System.Threading;
using App.Data.Xpo.Manager;
using App.Framework.Manager;
using App.Framework.Utils.Sort;
using App.Framework.Web.Mvc.Exceptions;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Model.Reference;
using ECabinet.Data.Properties;

namespace ECabinet.Core.Managers.SM {
	public class SmClientManager : AppManager {
		public FwQueryResult<MClientUnit> GetOrganizationClients(MOpsUnit ops, bool allclients = true, int? start = null, int? limit = null, params FwSortObject<MClientUnit>[] sorting) {
			var curOps = First<MOpsUnit>(o => o.Oid == ops.Oid);
			//if (sorting.Length == 0) sorting.SetValue(, 0);
			if (!allclients)
				switch (Thread.CurrentThread.CurrentCulture.ToString().ToLower()) {
					case "kk-kz": return Query(start, limit, a => a.OpsUnits.Contains(curOps) && a.RegistrationDate != DateTime.Parse("01.01.2001"),
						new FwSortObject<MClientUnit>(a => a.Name.L1, FwSortDirection.ASC));
					case "en-us": return Query(start, limit, a => a.OpsUnits.Contains(curOps) && a.RegistrationDate != DateTime.Parse("01.01.2001"),
						new FwSortObject<MClientUnit>(a => a.Name.L3, FwSortDirection.ASC));
					default: return Query(start, limit, a => a.OpsUnits.Contains(curOps) && a.RegistrationDate != DateTime.Parse("01.01.2001"),
						new FwSortObject<MClientUnit>(a => a.Name.L2, FwSortDirection.ASC));
				}

			return Query(start, limit, a => a.OpsUnits.Contains(curOps), new FwSortObject<MClientUnit>(a => a.Name.ToString(Thread.CurrentThread.CurrentCulture), FwSortDirection.ASC));
		}

		public FwQueryResult<MClientUnit> GetAllClients(int? start, int? limit, params FwSortObject<MClientUnit>[] sorting) {
			return Query(start, limit, null, sorting);
		}

		public MClientUnit CheckBin(string bin, MOpsUnit ops = null) {
			if (ops == null)
				return First<MClientUnit>(a => a.Bin == bin);
			
			return First<MClientUnit>(a => a.Bin == bin && a.OpsUnits.Contains(ops));
		}

		public void SaveClient(bool? isNew, MOpsUnit ops, ProxyClient cln) {
			MClientUnit client;
			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);
			long id = long.Parse(cln.Id);

			if (isNew.HasValue && isNew.Value) {
				client = CheckBin(cln.Bin);
				if (client == null) {
					client = new MClientUnit(Uow);
					client.RegistrationDate = DateTime.Now;
				}
				if (!client.OpsUnits.Contains(ops))
					client.OpsUnits.Add(ops);
			} else {
				client = First<MClientUnit>(a => a.Oid == id);
				if (client == null)
					throw new FwUserVisibleException(Strings.SmClient_SavingImpossible_OrgNotFound + id);
				if (!client.OpsUnits.Contains(ops))
					client.OpsUnits.Add(ops);
			}
//			if (id != 0) {
//				client = First<MClientUnit>(a => a.Oid == id);
//				if (client == null)
//					throw new FwUserVisibleException("Сохранение невозможно. Организация не найдена: " + id);
//				if (!client.OpsUnits.Contains(ops))
//					client.OpsUnits.Add(ops);
//			} else {
//				
//				if (client == null) {
//					client = CreateNew<MClientUnit>();
//					client.RegistrationDate = DateTime.Now;
//					client.OpsUnits.Add(ops);
//				}
//			}

			if (!isNew.HasValue || !isNew.Value)
				if (!client.Name.Compare(cln.Name) || client.Bin != cln.Bin) {
					var history = new MClientHistory(Uow) {
						OpsUnit = ops,
						Bin = "-",
						Name = "-",
						BinNew = "-",
						NameNew = "-",
						RegistrationDate = DateTime.Now
					};

					if (!client.Name.Compare(cln.Name)) {
						history.Name = client.Name;
						history.NameNew = cln.Name;
					}

					if (client.Bin != cln.Bin) {
						history.Bin = client.Bin;
						history.BinNew = cln.Bin;
					}

					history.Save();
				}

			client.Name = cln.Name;
			client.Location = First<MAreaUnit>(a => a.Oid == cln.Location);
			client.Bin = cln.Bin;
			client.Adress = cln.Adress;
			client.Email = cln.Email;
			client.Phone = cln.Phone;
			client.FioBoss = cln.FioBoss;

			client.Save();
			Uow.CommitChanges();

			//new AppAuditManager().Write(isNew.HasValue ? string.Format("Добавлен клиент: {0}, организация: {1}", client.Name, ops.Name)
			//	: string.Format("Изменены данные клиента: {0}, {1}", client.Name, client.Bin), AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
		}

		public void DeleteClient(MOpsUnit ops, MClientUnit client) {
			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);
			client = First<MClientUnit>(o => o.Gid == client.Gid);
			ops.Clients.Remove(client);
			client.OpsUnits.Remove(ops);

			ops.Save();
			client.Save();

			Uow.CommitChanges();
		}

		public FwQueryResult<MClientHistory> GetClientsHistory(int? start, int? limit, FwSortObject<MClientHistory>[] sorting) {
			return Query(start, limit, null, sorting);
		}
	}
}
