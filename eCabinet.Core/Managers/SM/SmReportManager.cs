﻿using App.Data.Xpo.Manager;
using App.Framework.Manager;
using ECabinet.Common;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Core.Managers.SM {
	public class SmReportManager : AppManager {
		public FwQueryResult<MEmploye> MonitoringAuditors(int? start = null, int? limit = null, params FwSortObject<MEmploye>[] sorting) {
			return Query(start, limit, a => a.Role.Type == (int)ERoleType.SmAuditor && a.OpsUnit.Oid > 1, sorting);
		}
	}
}
