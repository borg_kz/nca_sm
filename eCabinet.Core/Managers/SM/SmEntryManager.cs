﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Data.Xpo.Extensions;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model.Entities;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Data.Xpo.Notification;
using App.Framework.Manager;
using App.Framework.Web.Mvc.Exceptions;
using DevExpress.Xpo;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Model.Reference;
using ECabinet.Data.Properties;

namespace ECabinet.Core.Managers.SM {
	public class SmEntryManager : AppManager {

		public FwQueryResult<MSmEntry> GetUserEntryes(int entryType, MAppUserRole role, MOpsUnit ops, int? start = null, int? limit = null) {
			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);
			if (role != null && (role.Role.Type == (int)ERoleType.SmAuditor)) {
				role = First<MAppUserRole>(r => r.Oid == role.Oid);
				return Query<MSmEntry>(start, limit, a => a.OpsUnit == ops && (a.Auditor == role.User.Person || a.Autor == role.User.Person) 
					&& a.Status == (ESmEntryStatus)entryType);
			}

			return Query<MSmEntry>(start, limit, a => a.OpsUnit == ops  && a.Status == (ESmEntryStatus)entryType); 
		}

		public FwQueryResult<MSertificationStore> GetSmTypes() {
			return Query<MSertificationStore>(null, null, a => a.Oid > -1);
		}

		public void SaveEntry(long? id, MOpsUnit ops, ProxySmEntry obj) {
			// ops = First<MOpsUnit>(o => o.Oid == ops.Oid);

			MSmEntry entry = id.HasValue ? First<MSmEntry>(a => a.Oid == id.Value) : new MSmEntry(Uow);
			
			if (entry == null)
				throw new NullReferenceException(string.Format(Strings.SmEntry_ApplicationWithIDNotFound, id.Value));

			if (!id.HasValue) {
				entry.RegistrationDate = DateTime.Now;
				foreach (var sertType in obj.SmTypeSert) {
					var type = new MEntrySmTypeSert(Uow);
					type.SmEntry = entry;
					type.SmType = First<MSertificationStore>(a => a.Oid == sertType);
					entry.SmTypeSert.Add(type);
				}
			} else {
				if (entry.Status != ESmEntryStatus.Sertificate) {
					List<MEntrySmTypeSert> needDelete = new List<MEntrySmTypeSert>();
					foreach (var sertType in entry.SmTypeSert) {
						if (!obj.SmTypeSert.Contains(sertType.Oid))
							needDelete.Add(sertType);
						else obj.SmTypeSert.Remove(sertType.Oid);
					}

					foreach (var smTypeSert in needDelete) {
						entry.SmTypeSert.Remove(smTypeSert);
					}

					foreach (var sert in obj.SmTypeSert) {
						var type = new MEntrySmTypeSert(Uow);
						type.SmEntry = entry;
						type.SmType = First<MSertificationStore>(a => a.Oid == sert);
						entry.SmTypeSert.Add(type);
					}
				}
			}

			// TODO MAX Надо тут проверять аудитора и в случаях если это назначение аудиттора то отправлять на мыло и на календарь
			if (obj.Auditor != 0)
				entry.Auditor = First<MAppPerson>(a => a.Oid == obj.Auditor);

			if (!id.HasValue) {
				entry.EntryDate = obj.EntryDate.Value;
				entry.Declarant = First<MClientUnit>(a => a.Oid == obj.Declarant);
			}
			entry.Autor = entry.Autor ?? CurrentUser.GetUser(Uow).Person;
			entry.DeclarantFio = obj.DeclarantFio;
			entry.DeclarantEmail = obj.DeclarantEmail;
			entry.DeclarantPhone = obj.DeclarantPhone;
			entry.AreaSertification = obj.AreaSertification;
			entry.Status = entry.Status == ESmEntryStatus.Unknown ? ESmEntryStatus.New : entry.Status;
			entry.OpsUnit = ops;
			entry.CountEmployes = obj.CountEmployes.Value;
			entry.CountYards = obj.CountYards.Value;

			entry.Save();
			Uow.CommitChanges();
			if (!id.HasValue) {
				new AppNotification("Новая заявка", string.Format("Уважаемый аудитор, на Ваше имя поступила новая заявка.Компания: {0}, тип сертификации: {1}", entry.Declarant.Name, 
				entry.SmTypeSert.Select(a => a.SmType.ShortName).Aggregate((typeSm, next) => typeSm + ", " + next)),
					ENotificationStatus.Information, DateTime.Now.AddDays(30)).AddPersons(entry.Auditor).Send();
			}
//			if (!id.HasValue)
//				new AuditManager().Write("Добавлена заявка. ОПС " + ops.Name, AuditMode.Critical, AuditType.User, CurrentUser.ShortName);
		}

		public void ReviewEntry(long? id, string description, ESmEntryStatus status) {
			var entry = First<MSmEntry>(a => a.Oid == id.Value);
			if (entry == null)
				throw new NullReferenceException(string.Format(Strings.SmEntry_ApplicationWithIDNotFound, id.Value));

			entry.ExpertMaterialsDate = entry.ExpertMaterialsDate ?? DateTime.Now;
			entry.ReviewDescription = description;
			if (entry.Status != ESmEntryStatus.Sertificate)
				entry.Status = status;
			entry.Save();
			Uow.CommitChanges();
		}

		public void ResultEntry(long? id, DateTime? startReseach, DateTime? finishReseach, string resultReseach) {
			var entry = First<MSmEntry>(a => a.Oid == id.Value);
			if (entry == null)
				throw new NullReferenceException(string.Format(Strings.SmEntry_ApplicationWithIDNotFound, id.Value));

			entry.StartResearch = startReseach.Value;
			entry.FinishResearch = finishReseach.Value;
			entry.ResultResearch = resultReseach;
			entry.ResultResearchDate = DateTime.Now;
			if (entry.Status != ESmEntryStatus.Sertificate)
				entry.Status = ESmEntryStatus.Processed;
			entry.Save();
			Uow.CommitChanges();
		}

		public FwQueryResult<MSmEntryTeam> LoadTeam(long entryId) {
			return Query<MSmEntryTeam>(null, null, a => a.Entry.Oid == entryId);
		}

		public void SaveTeam(long entryId, ProxySmTeamExpert obj) {
			var entry = First<MSmEntry>(a => a.Oid == entryId);
			if (entry == null) throw new FwUserVisibleException("Заявки с таким ID не существует.");

			var person = First<MAppPerson>(a => a.Oid == obj.Auditor);

			if (person == null) {
				person = new MAppPerson(Uow);
				person.FirstName = obj.FirstName;
				person.SecondName = obj.SecondName;
				person.ThirdName = obj.ThirdName;
				person.Iin = obj.Iin;
			}

			MSmEntryTeam team = obj.Id == 0 ? new MSmEntryTeam(Uow) : First<MSmEntryTeam>(t => t.Oid == obj.Id);
			team.Person = person;
			team.StartResearch = obj.StartResearch;
			team.FinishResearch = obj.FinishResearch;
			team.Type = First<MEntrySmTypeSert>(a => a.Oid == obj.SmTypeSertId);
			team.PositionType = EMemberType.Auditor;
			team.Entry = entry;
			entry.TeamResearch.Add(team);

			team.Save();
			entry.Save();
			Uow.CommitChanges();
		}

		public void DeleteTeamPerson(int id, long entryId) {
			var entry = First<MSmEntry>(a => a.Oid == entryId);
			if (entry == null) throw new FwUserVisibleException("Заявки с таким ID не существует.");

			var person = First<MSmEntryTeam>(a => a.Oid == id);
			entry.TeamResearch.Remove(person);
			entry.Save();
			person.Delete();
			person.Save();
			Uow.CommitChanges();
		}

		public XPCollection<MEntrySmTypeSert> GetEntryTypes(long entryId) {
			var entry = First<MSmEntry>(a => a.Oid == entryId);
			if (entry == null) throw new FwUserVisibleException("Заявки с таким ID не существует.");
			return entry.SmTypeSert;
			//return Query(null, null, a => a.OpsUnit == entry.OpsUnit && a.Role.Type == (int)ERoleType.SmAuditor, new FwSortObject<MEmploye>(a => a.User.Person.SecondName));
		}

		public MAppPerson CheckPersonIin(string iin) {
			return First<MAppPerson>(a => a.Iin == iin);
		}
	}
}
