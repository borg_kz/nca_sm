﻿using System;
using System.Threading;
using App.Data.Xpo.Extensions;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using App.Framework;
using App.Framework.Manager;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Reference;
using ECabinet.Data.Properties;

namespace ECabinet.Core.Managers {
	public class CommonManager : AppManager {
		public FwQueryResult<MAreaUnit> LoadCities(bool onlyLocal = true) {
			if (onlyLocal)
				return Query<MAreaUnit>(null, null, a => a.Type == EAreaUnitType.City && a.Code != 0);

			return Query<MAreaUnit>(null, null, a => a.Type == EAreaUnitType.City);
		}

		public FwQueryResult<MAreaUnit> LoadRegions(bool onlyLocal = true) {
			if (onlyLocal)
				return Query<MAreaUnit>(null, null, a => a.Status == EAreaUnitStatus.Region && a.Code != 0);

			return Query<MAreaUnit>(null, null, a => a.Status == EAreaUnitStatus.Region);
		}

		public FwQueryResult<MAreaUnit> LoadAreas(bool onlyLocal = true) {
			if (onlyLocal)
				return Query<MAreaUnit>(null, null, a => a.Code != 0);

			return Query<MAreaUnit>(null, null, a => a.Code > -1);
		}

		public FwQueryResult<MSmDublicateReason> RefSmDublicateReason() {
			return Query<MSmDublicateReason>(null, null, a => a.Oid > -1);
		}

		public void CreateSert(Sert sert) {
			var ops = First<MOpsUnit>(a => a.Old_ID == sert.KOD_OPS); 
			if (ops == null) return;
			if (Any<MSmSertificate>(a => a.BlankNumber == sert.NBLSERT)) return;

			MSmSertificate smSert = null; //First<MSmSertificate>(a => a.BlankNumber == sert.NBLSERT);

			if (smSert != null) {
				if (string.IsNullOrEmpty(smSert.SmEntry.DeclarantFio)) {
					smSert.SmEntry.DeclarantFio = Strings.Common_Unknown;
					smSert.SmEntry.Declarant.Name = sert.ZAYV;
					smSert.SmEntry.Declarant.Adress = sert.ADRES;
					smSert.SmEntry.Save();
					smSert.SmEntry.Declarant.Save();
					Uow.CommitChanges();
				}
				return;
			}
			

			var client = new MClientUnit(Uow);
			client.Adress = sert.ADRES;
			client.Name = new MultiString(new MString(sert.ZAYV_K, FwCultures.Kaz), new MString(sert.ZAYV, FwCultures.Rus));
			client.Bin = Environment.TickCount.ToString();
			client.OpsUnits.Add(ops);
			ops.Clients.Add(client);
			Thread.Sleep(30);
			client.Save();
			var entry = new MSmEntry(Uow);
			entry.OpsUnit = ops;
			entry.Declarant = client;
			entry.DeclarantFio = Strings.Common_Unknown;
			entry.EntryDate = DateTime.MinValue;
			entry.Autor = CurrentUser.GetUser(Uow).Person;
			entry.Auditor = entry.Autor;
			entry.AreaSertification = sert.NM;
			entry.Status = ESmEntryStatus.Sertificate;
			var type = new MEntrySmTypeSert(Uow);
			type.SmEntry = entry;
			type.SmType = First<MSertificationStore>(a => a.Oid == sert.TREB_ID); 
			entry.SmTypeSert.Add(type);
			smSert = new MSmSertificate(Uow);
			type.Sertificate = smSert;
			smSert.AreaSertification = new MultiString(new MString(sert.NM_K, FwCultures.Kaz), new MString(sert.NM, FwCultures.Rus)); 
			smSert.BlankNumber = sert.NBLSERT;
			smSert.Counter = sert.NSERT;
			smSert.StartDate = sert.DATE_R;
			smSert.FirstRegisterDate = sert.DATE_R;
			smSert.FinishDate = sert.DATE_O;
			smSert.InspectionDate = smSert.StartDate.AddYears(1);
			smSert.Signatory = sert.EXP;
			smSert.OpsUnit = ops;
			smSert.SmEntry = entry;
			smSert.Auditor = First<MAppPerson>(a => a.Oid == 1);
			smSert.System = sert.SYS;
			smSert.Requirements = new MultiString(new MString(type.SmType.Name.ToString(FwCultures.Kaz) + " " + type.SmType.Equriments.ToString(FwCultures.Kaz), FwCultures.Kaz),
				new MString(type.SmType.Name.ToString(FwCultures.Rus) + " " + type.SmType.Equriments.ToString(FwCultures.Rus), FwCultures.Rus));
			smSert.Status = ESmSertStatus.Valid;
			smSert.SertificationStore = type.SmType;
			Uow.CommitChanges();
		}
	}

	public class Sert {
		public int KOD_OPS { get; set; }
		public string NAME_OPS { get; set; }
		public DateTime DATE_R { get; set; }
		public DateTime DATE_O { get; set; }
		public int NSERT { get; set; }
		public int NBLSERT { get; set; }
		public string NM { get; set; }
		public string NM_K { get; set; }
		public string ZAYV { get; set; }
		public string ZAYV_K { get; set; }
		public string ADRES { get; set; }
		public string SYS { get; set; }
		public string TREB { get; set; }
		public string TREB_K { get; set; }
		public int TREB_ID { get; set; }
		public string EXP { get; set; }

		public override string ToString() {
			return NSERT.ToString();
		}
	}

}