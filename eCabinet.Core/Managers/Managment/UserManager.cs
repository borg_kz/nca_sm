﻿using System.Linq;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model.Entities.Auth;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework.Manager;


namespace ECabinet.Core.Managers.Managment {
	/// <summary>
	/// Менеджер для работы с пользователями
	/// </summary>
	public class UserManager : AppManager {
		/// <summary> Найти пользователя по имени </summary>
		public MAppUser GetUserByLogin(string login) {
			return First<MAppUser>(a => a.UserName == login);
		}

		/// <summary> Все пользователи </summary>
		public FwQueryResult<MAppUser> GetAllUsers(int? start = null, int? limit = null, params FwSortObject<MAppUser>[] sorting) {
			return Query<MAppUser>(start, limit, a => a.Person.Oid != 1, sorting);
		}

		/// <summary> Список пользователей без роли </summary>
		public FwQueryResult<MAppUser> GetUsersWithoutRole(int? start = null, int? limit = null, params FwSortObject<MAppUser>[] sorting) {
			return Query<MAppUser>(start, limit, a => a.Person.Oid != 1 && a.Roles.Count == 0, sorting);
		}

		/// <summary> Список пользователей с 1 и более ролями </summary>
		public FwQueryResult<MAppUser> GetUsersOneOrMoreRole(int? start = null, int? limit = null, params FwSortObject<MAppUser>[] sorting) {
			return Query<MAppUser>(start, limit, a => a.Person.Oid != 1 && a.Roles.Count > 0, sorting);
		}

		/// <summary> Пользователи указанной роли </summary>
		public FwQueryResult<MAppUser> GetUsersByRole(int role, int? start = null, int? limit = null, params FwSortObject<MAppUser>[] sorting) {
			return Query<MAppUser>(start, limit, a => a.Person.Oid != 1 && a.Roles.Any(b => b.Role.Oid == role), sorting);
		}

		/// <summary> Пользователи указанной роли </summary>
		public FwQueryResult<MAppUserRole> GetUsersByRole(int? start = null, int? limit = null, int? roleId = null) {
			return Query<MAppUserRole>(start, limit, a => a.Role.Oid == roleId, new FwSortObject<MAppUserRole>(a => a.User.Person.SecondName));
		}

		public void SetPassword(long id, string password) {
			var user = First<MAppUser>(a => a.Oid == id);
			user.SetPassword(password);
			user.Save();
			Uow.CommitChanges();
		}
	}
}
