﻿using System;
using System.Linq.Expressions;
using System.Linq;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model.Entities;
using App.Data.Xpo.Model.Entities.Auth;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework;
using App.Framework.Manager;
using App.Framework.Utils.Sort;
using App.Framework.Web.Mvc.Exceptions;
using ECabinet.Data;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Model.Proxy.OrgStructure;
using ECabinet.Data.Model.Proxy.SM;
using ECabinet.Data.Model.Reference;
using ECabinet.Data.Properties;

namespace ECabinet.Core.Managers.Managment {
	public class OpsManager : AppManager {
		/// <summary> Список всех организаций </summary>
		public FwQueryResult<MOpsUnit> GridLoadOpsUnits(int? start = null, int? limit = null, int? opsType = null, params FwSortObject<MOpsUnit>[] sorting) {
			Expression<Func<MOpsUnit, bool>> expr = a => a.OpsTypes.Contains(First<MOpsTypes>(t => t.OpsType == (EOpsType)opsType));
			return Query(start, limit, opsType.HasValue ? expr : null, sorting);
		}

		/// <summary> Создание нового ОПС </summary>
		public void AddOpsUnit(long? id, ProxyOpsUnit ops) {
			if (string.IsNullOrWhiteSpace(ops.Login) || string.IsNullOrWhiteSpace(ops.Password))
				throw new FwUserVisibleException(Strings.OpsManager_NeedToSpecifyLogPass);
			if (ops.Password.Length < 6)
				throw new FwUserVisibleException(string.Format(Strings.Msg_MinPasswordLength, Settings.Instance.MinRequiredPasswordLength));

			var area = First<MAreaUnit>(a => a.Oid == ops.Location);
			string smSertNumber = "KZ." + area.Code + ops.AttestateNumber.Remove(0, 6) + ".07.03";

			if (Any<MOpsUnit>(a => a.SmSertNumber == smSertNumber))
				throw new FwUserVisibleException(string.Format(Strings.OpsManager_OrgWithNumberAlreadyExists, smSertNumber));

			var org = new MOpsUnit(Uow);
			org.SystemRegistrationDate = DateTime.Now;
			org.Name = ops.Name;
			org.Location = area;
			org.Email = ops.Email;
			org.AttestateNumber = ops.AttestateNumber;
			org.SmSertNumber = smSertNumber;

			var opsType = new MOpsTypes(Uow);
			opsType.OpsType = (EOpsType)ops.OpsType;
			opsType.OpsUnit = org;
			opsType.RegistrationDate = DateTime.Now;
			org.OpsTypes.Add(opsType);
			org.Administrator = new MAppUser(Uow);
			org.Administrator.UserName = ops.Login;
			org.Administrator.SetPassword(ops.Password);

			if (org.Administrator.IsNew) {
				org.Administrator.Person = new MAppPerson(Uow);
				org.Administrator.Person.FirstName = "";
				org.Administrator.Person.SecondName = org.Name;
				org.Administrator.Person.Iin = Environment.TickCount.ToString();

				var role = new MAppUserRole(Uow);
				role.User = org.Administrator;
				// TODO MAX Тут надо как то выбирать тип организации и такой и давать доступ админу
				role.Role = First<MAppRole>(a => a.Type == (int)ERoleType.SmAdmin);
				role.Save();
			}
			opsType.Save();
			org.Save();
			Uow.CommitChanges();
		}

		public void SaveFullOpsInfo(long? id, ProxyOpsUnit ops, DateTime? activateDate = null) {
			MOpsUnit org = id.HasValue ? First<MOpsUnit>(a => a.Oid == id.Value) : null;
			if (org == null) throw new NullReferenceException(string.Format(Strings.OpsManager_OpsNotFound, id.Value));

			org.Name = ops.Name;
			org.Bin = ops.Bin;
			org.Adress = ops.Adress;
			org.SmSertNumber = ops.SmSertNumber;
			org.AttestateNumber = ops.AttestateNumber;
			org.FioBoss = ops.FioBoss;
			org.Singer = ops.Singer;
			org.Phone = ops.Phone;
			org.Email = ops.Email;
			if (ops.LastSertNumber != null && ops.LastSertNumber.Value != 0)
				org.LastSertNumber = ops.LastSertNumber.Value;
			org.Site = ops.Site;
			org.PostCode = ops.PostCode;
			if (org.Location.Oid != ops.Location)
				org.Location = First<MAreaUnit>(a => a.Oid == ops.Location);
			if (activateDate != null && org.ActivateDate == null)
				org.ActivateDate = activateDate;

			org.Save();
			Uow.CommitChanges();
		}

		/// <summary>
		/// Удаление организации
		/// </summary>
		public void DeleteOpsUnit(int id) {
			var org = First<MOpsUnit>(a => a.Oid == id);
			if (org == null) throw new NullReferenceException(string.Format(Strings.OpsManager_OpsNotFound, id));
			org.Blocked = true;

			org.Save();
			Uow.CommitChanges();
		}

		public FwQueryResult<MEmploye> GridLoadOpsEmployes(long opsId, int? start = null, int? limit = null, ERoleType? role = null, params FwSortObject<MEmploye>[] sorting) {
			var ops = First<MOpsUnit>(o => o.Oid == opsId);
			Expression<Func<MEmploye, bool>> expr = a => a.OpsUnit == ops && a.Role.Type == (int)role.Value;
			if (role != null)
				// @Review4 Max почемутo если для пейджинга приходят null валится с ошибкой "Нулевой объект должен иметь значение."
				return Query(start, limit, expr, new FwSortObject<MEmploye>(a => a.User.Person.SecondName));

			return Query(start, limit, a => a.OpsUnit == ops, new FwSortObject<MEmploye>(a => a.User.Person.SecondName));
		}

		public MAppPerson CheckEmployeIin(string iin) {
			return First<MAppPerson>(a => a.Iin == iin);
		}

		public MAppPerson CheckPersonIin(string iin) {
			return First<MAppPerson>(a => a.Iin == iin);
		}

		public void AddSaveEmploye(MOpsUnit ops, bool isNew, ProxyEmploye obj) {
			var role = First<MAppRole>(a => a.Oid == obj.Role);
			ops = First<MOpsUnit>(o => o.Oid == ops.Oid);
			MEmploye emp;
			if (isNew) {
				emp = new MEmploye(Uow);
				emp.Role = role;

				if (Int64.Parse(obj.Id) == 0) {
					emp.User = new MAppUser(Uow);
					emp.User.UserName = obj.Iin;
					emp.User.Person = new MAppPerson(Uow);
				} else {
					emp.User = First<MAppUser>(e => e.Person.Oid == Int64.Parse(obj.Id));
					if (emp.User == null) {
						emp.User = new MAppUser(Uow);
						emp.User.UserName = obj.Iin;
						emp.User.Person = First<MAppPerson>(e => e.Oid == Int64.Parse(obj.Id));
					}
				}
			} else {
				emp = First<MEmploye>(a => a.Oid == Int64.Parse(obj.Id) && a.OpsUnit.Oid == ops.Oid);
				// Значит пользователь меняет роль
				if (emp == null) {
					
				}
			}

			if (isNew) {
				if (!Any<MAppUserRole>(a => a.User == emp.User && a.Role == emp.Role)) {
					var userRole = new MAppUserRole(Uow);
					userRole.Role = emp.Role;
					userRole.User = emp.User;
					emp.User.Roles.Add(userRole);
				}
				
				emp.OpsUnit = ops;
				ops.Employes.Add(emp);
			}

			if (!obj.Password.Contains("..."))
				emp.User.SetPassword(obj.Password);

			emp.User.Person.FirstName = obj.FirstName;
			emp.User.Person.SecondName = obj.SecondName;
			emp.User.Person.ThirdName = obj.ThirdName;
			emp.User.Person.Email = obj.Email;
			emp.User.Person.Iin = obj.Iin;
			emp.User.Person.MobileTel = obj.MobTel;
			emp.User.Person.HomeTel = obj.HomeTel;
			emp.User.Person.Gender = (EGender)obj.Gender;

			emp.User.Person.Save();
			emp.User.Save();
			emp.Save();
			ops.Save();
			Uow.CommitChanges();
		}

		public void DeleteEmploye(long id, MOpsUnit ops) {
			MEmploye emp = First<MEmploye>(a => a.Oid == id);
			if (emp.User.Person.Iin.Length < 12)
				throw new FwUserVisibleException(Strings.OpsManager_UserCanNotDeleted);
			ops.Employes.Remove(emp);
			emp.OpsUnit = null;
			ops.Save();
			emp.Save();
			Uow.CommitChanges();
		}
	}
}