﻿using App.Framework.Localization;
using App.Properties;
using ECabinet.Data.Properties;

namespace ECabinet.Data {
	/// <summary> Типы организаций сертификации </summary>
	public enum EOpsType {
		[FwCultureLocalize("EOpsType_OpsSm", typeof(Strings))]
		OpsSm = 1
	}

	/// <summary> Статус населенног пункта </summary>
	public enum EAreaUnitStatus {
		[FwCultureLocalize("Common_Region")]
		Region = 1,
		[FwCultureLocalize("EAreaUnitType_City")]
		City = 2
	}

	/// <summary> Тип территориального деления </summary>
	public enum EAreaUnitType {
		/// <summary> область </summary>
		[FwCultureLocalize("Common_Region")]
		Region = 0,
		/// <summary> город </summary>
		[FwCultureLocalize("EAreaUnitType_City")]
		City = 1,
		/// <summary> Деревня </summary>
		[FwCultureLocalize("EAreaUnitType_Village")]
		Village = 2,
		/// <summary> район </summary>
		[FwCultureLocalize("EAreaUnitType_District")]
		District = 3
	}

	public enum ESmEntryStatus {
		/// <summary> Неизвестный </summary>
		[FwCultureLocalize("Common_Unknown", typeof(Strings))]
		Unknown = 0,
		/// <summary> Новая </summary>
		[FwCultureLocalize("New", typeof(AppStrings))]
		New = 1,
		/// <summary> Расмотренная </summary>
		[FwCultureLocalize("Common_Adopted", typeof(Strings))]
		Review = 2,
		/// <summary> Отказано </summary>
		[FwCultureLocalize("Common_Denied", typeof(Strings))]
		Reject = 3,
		/// <summary> Обработана заявка </summary>
		[FwCultureLocalize("Common_Processed", typeof(Strings))]
		Processed = 4,
		/// <summary> Выдан сертификат </summary>
		[FwCultureLocalize("Common_Certificate", typeof(Strings))]
		Sertificate = 5,
		/// <summary> Отозвана </summary>
		[FwCultureLocalize("Common_Revoked", typeof(Strings))]
		CancelClient = 6,
		/// <summary> Ошибочный </summary>
		[FwCultureLocalize("Common_Erroneous", typeof(Strings))]
		Mistake = 7
	}

	public enum ESmSertStatus {
		[FwCultureLocalize("NoStatus", typeof(Strings))]
		None = 0,
		[FwCultureLocalize("Common_Applications", typeof(Strings))]
		Entry = 1,
		[FwCultureLocalize("Common_Certificate", typeof(Strings))]
		Valid = 2,
		[FwCultureLocalize("Common_Undo", typeof(Strings))]
		Cancelled = 3,
		[FwCultureLocalize("Common_Paused", typeof(Strings))]
		Suspended = 4,
		[FwCultureLocalize("Common_Renewed", typeof(Strings))]
		Renewed = 5,
		[FwCultureLocalize("Common_Reassigned", typeof(Strings))]
		Dublicate = 6,
		[FwCultureLocalize("Common_Removed", typeof(Strings))]
		Deleted = 7,
		[FwCultureLocalize("Common_Transfered", typeof(Strings))]
		Transfered = 8
	}

	/// <summary> Тип роли </summary>
	public enum ERoleType {
		[FwCultureLocalize("Common_NoRole", typeof(Strings))]
		None = 0,
		[FwCultureLocalize("Common_GAdmin", typeof(Strings))]
		GlobalAdmin = 1,
		[FwCultureLocalize("Common_Admin", typeof(Strings))]
		Admin = 2,
		[FwCultureLocalize("Common_OnlyReading", typeof(Strings))]
		ReadOnly = 3,
		[FwCultureLocalize("Common_SmkAdmin", typeof(Strings))]
		SmAdmin = 100,
		[FwCultureLocalize("Common_SmkSecretary", typeof(Strings))]
		SmSecretary = 101,
		[FwCultureLocalize("Common_SmkAuditor", typeof(Strings))]
		SmAuditor = 102,
		[FwCultureLocalize("Common_NcaUser", typeof(Strings))]
		SmExpert = 201
	}

	public enum EMemberType {
		Auditor = 0,
		Expert = 1
	}

	/// <summary>
	/// Типы стандартов которые имеются в систмете, приввязаны к 
	/// </summary>
	public enum ESTRKType {
		/// <summary> Сертификация Менеджмента </summary>
		SM = 1
	}
}