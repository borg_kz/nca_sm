﻿using System.Collections.Generic;
using System.Linq;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using ECabinet.Common;

namespace ECabinet.Data.Model.Reference {
	/// <summary>
	/// Область, город
	/// </summary>
	[Persistent("Ref_AreaUnits")]
	public class MAreaUnit : MAppDbObject {
		public MAreaUnit() {}
		public MAreaUnit(Session session) : base(session) {}
		public MAreaUnit(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MultiString _fName;
		/// <summary>
		/// Наименование
		/// </summary>
		[Persistent("Name")]
		[AppSorting(FwSortDirection.ASC, 0)]
//		[Column("Name", typeof(Common.Properties.Common), IsDisplayColumn = true, IsRequired = true)]
		public MultiString Name {
			get { return _fName; }
			set { SetPropertyValue("Name", ref _fName, value); }
		}

		MAreaUnit _fParent;
		/// <summary>
		/// Родительский элемент
		/// </summary>
//		[Column("Reference_AreaUnit_Parent")]
		public MAreaUnit Parent {
			get { return _fParent; }
			set { SetPropertyValue("Parent", ref _fParent, value); }
		}

		EAreaUnitType _fType;
		/// <summary>
		/// Тип
		/// </summary>
//		[Column("Reference_AreaUnit_Type", IsRequired = true)]
		public EAreaUnitType Type {
			get { return _fType; }
			set { SetPropertyValue("Type", ref _fType, value); }
		}

		EAreaUnitStatus _fStatus;
		/// <summary>
		/// Тип
		/// </summary>
//		[Column("Status", IsRequired = true)]
		public EAreaUnitStatus Status {
			get { return _fStatus; }
			set { SetPropertyValue("Status", ref _fStatus, value); }
		}

		int _fCode;
		/// <summary>
		/// Тип
		/// </summary>
//		[Column("Code", IsDisplayColumn = true, IsRequired = true)]
		public int Code {
			get { return _fCode; }
			set { SetPropertyValue("Code", ref _fCode, value); }
		}

		IEnumerable<MAreaUnit> _fChildren;
		/// <summary>
		/// Внутренняя территориальная единица
		/// </summary>
		[NonPersistent]
		public IEnumerable<MAreaUnit> Children {
			get {
				if (_fChildren != null) return _fChildren;
				return (_fChildren = Session.Query<MAreaUnit>().Where(a => a.Parent != null && a.Parent.Oid == Oid));
			}
		}

	}
}
