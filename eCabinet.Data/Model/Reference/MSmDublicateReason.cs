﻿using App.Data.Model;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Reference {
	[Persistent("Ref_SmDublicateReason")]
//	[Reference("Reference_SmDublicateReason")]
	public class MSmDublicateReason : MAppDbObject {
		public MSmDublicateReason() {}
		public MSmDublicateReason(Session session) : base(session) {}
		public MSmDublicateReason(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MultiString _fName;
		/// <summary> Наименование </summary>
		[Persistent("Name")]
		[AppSorting(FwSortDirection.ASC, 0)]
		public MultiString Name {
			get { return _fName; }
			set { SetPropertyValue("Name", ref _fName, value); }
		}

		int _fCode;
		/// <summary> Код статуса </summary>
		public int Code {
			get { return _fCode; }
			set { SetPropertyValue("Code", ref _fCode, value); }
		}

	}
}
