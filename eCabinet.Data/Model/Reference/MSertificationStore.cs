﻿using App.Data.Xpo.Model;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Reference {
	[Persistent("Ref_Sertification_Store")]
//	[Reference("Reference_Sertification_Store")]
	public class MSertificationStore : MAppDbObject {
		public MSertificationStore() { }
		public MSertificationStore(Session session) : base(session) {}
		public MSertificationStore(Session session, XPClassInfo classInfo) : base(session, classInfo) { }

		MultiString _fName;
		/// <summary> Наименование </summary>
		[Persistent("Name")]
//		[Column("Name", typeof(Common.Properties.Common), IsDisplayColumn = true, IsRequired = true)]
		public MultiString Name {
			get { return _fName; }
			set { SetPropertyValue("Name", ref _fName, value); }
		}

		MultiString _fEquriments;
		/// <summary> Соответствует требованиям </summary>
		[Persistent("Equriments")]
//		[Column("Value", typeof(Common.Properties.Common), IsDisplayColumn = true, IsRequired = false)]
		public MultiString Equriments {
			get { return _fEquriments; }
			set { SetPropertyValue("Equriments", ref _fEquriments, value); }
		}

		string _fShortName;
		/// <summary> Короткое название </summary>
//		[Column("Короткое название", typeof(Common.Properties.Common), IsDisplayColumn = true, IsRequired = false)]
		public string ShortName {
			get { return _fShortName; }
			set { SetPropertyValue("ShortName", ref _fShortName, value); }
		}

		string _fPrintLabel;
		/// <summary> PrintLabel </summary>
//		[Column("Короткое название", typeof(Common.Properties.Common), IsDisplayColumn = true, IsRequired = false)]
		public string PrintLabel {
			get { return _fPrintLabel; }
			set { SetPropertyValue("PrintLabel", ref _fPrintLabel, value); }
		}

		MultiString _fSystem;
		/// <summary> Система </summary>
		[Persistent("System")]
//		[Column("Система", typeof(Common.Properties.Common), IsDisplayColumn = true, IsRequired = false)]
		public MultiString System {
			get { return _fSystem; }
			set { SetPropertyValue("System", ref _fSystem, value); }
		}

		ESTRKType _fSTRKType;
		/// <summary> Тип сертификации </summary>
		[Persistent("STRKType")]
//		[Column("Тип сертификации", typeof(Common.Properties.Common), IsDisplayColumn = true, IsRequired = false)]
		public ESTRKType STRKType {
			get { return _fSTRKType; }
			set { SetPropertyValue("STRKType", ref _fSTRKType, value); }
		}
	}
}
