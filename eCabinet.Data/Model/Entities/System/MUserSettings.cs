﻿using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities.Auth;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Entities.System {
	/// <summary>
	/// Настройки пользователя в виде Key - Settings
	/// </summary>
	[Persistent("SYS_UsersSettings")]
	public class MUsersSettings : MAppDbObject {
		public MUsersSettings() { }
		public MUsersSettings(Session session) : base(session) { }
		public MUsersSettings(Session session, XPClassInfo classInfo) : base(session, classInfo) { }

		MAppUser _fUser;
		/// <summary>
		/// Пользователь
		/// </summary>
		public MAppUser User {
			get { return _fUser; }
			set { SetPropertyValue("User", ref _fUser, value); }
		}

		string _fKey;
		/// <summary>
		/// Ключ идентификатор настроек
		/// </summary>
		public string Key {
			get { return _fKey; }
			set { SetPropertyValue("Key", ref _fKey, value); }
		}

		string _fName;
		/// <summary>
		/// Название настроек
		/// </summary>
		public string Name {
			get { return _fName; }
			set { SetPropertyValue("Name", ref _fName, value); }
		}

		string _fSettings;
		/// <summary>
		/// Настройки
		/// </summary>
		[DbType("nvarchar(MAX)")]
		public string Settings {
			get { return _fSettings; }
			set { SetPropertyValue("Settings", ref _fSettings, value); }
		}
	}
}
