﻿using System;
using App.Data.Model;
using App.Data.Xpo.Model;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Entities.System {
	/// <summary>
	/// Сообщение аудита
	/// </summary>
	[Persistent("Sys_Audit_Messages")]
	public class MAuditMessage1 : MAppDbObject {
		public MAuditMessage1() { }
		public MAuditMessage1(Session session) : base(session) { }
		public MAuditMessage1(Session session, XPClassInfo classInfo) : base(session, classInfo) { }

		DateTime _fDate;
		/// <summary>
		/// Дата и время
		/// </summary>
		public DateTime Date {
			get { return _fDate; }
			set { SetPropertyValue("Date", ref _fDate, value); }
		}

		string _fLogin;
		/// <summary>
		/// Логин пользователя
		/// </summary>
		public string Login {
			get { return _fLogin; }
			set { SetPropertyValue("Login", ref _fLogin, value); }
		}

		string _fClientAddress;
		/// <summary>
		/// Адрес клиента
		/// </summary>
		public string ClientAddress {
			get { return _fClientAddress; }
			set { SetPropertyValue("ClientAddress", ref _fClientAddress, value); }
		}

		string _fClassName;
		/// <summary>
		/// Имя класса или контроллера
		/// </summary>
		public string ClassName {
			get { return _fClassName; }
			set { SetPropertyValue("ClassName", ref _fClassName, value); }
		}

		string _fMethod;
		/// <summary>
		/// Метод в котором вызывается запись в аудит
		/// </summary>
		public string Method {
			get { return _fMethod; }
			set { SetPropertyValue("Method", ref _fMethod, value); }
		}

		string _fMessage;
		/// <summary>
		/// Сообщение
		/// </summary>
		[DbType("nvarchar(MAX)")]
		public string Message {
			get { return _fMessage; }
			set { SetPropertyValue("Message", ref _fMessage, value); }
		}

		string _fException;
		/// <summary>
		/// Детали ошибки
		/// </summary>
		[DbType("nvarchar(MAX)")]
		public string Exception {
			get { return _fException; }
			set { SetPropertyValue("Exception", ref _fException, value); }
		}

		TimeSpan _fRuntime;
		/// <summary>
		/// Время выполнения метода контроллера
		/// </summary>
		public TimeSpan Runtime {
			get { return _fRuntime; }
			set { SetPropertyValue("Runtime", ref _fRuntime, value); }
		}

		string _fDescription;

		/// <summary>
		/// Дополнительное описание
		/// </summary>
		public string Description {
			get { return _fDescription; }
			set { SetPropertyValue("Description", ref _fDescription, value); }
		}
	}
}
