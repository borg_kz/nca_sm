﻿using System;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Entities.SM {
	[Persistent("SM_Inspection")]
	public class MSmInspection : MAppDbObject {
		public MSmInspection() {}
		public MSmInspection(Session session) : base(session) {}
		public MSmInspection(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		DateTime _fDateDeparture;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime DateDeparture {
			get { return _fDateDeparture; }
			set { SetPropertyValue("DateDeparture", ref _fDateDeparture, value); }
		}

		DateTime _fInspectionDate;
		/// <summary> Дата инспекции </summary>
		public DateTime InspectionDate {
			get { return _fInspectionDate; }
			set { SetPropertyValue("InspectionDate", ref _fInspectionDate, value); }
		}

		MAppPerson _fAuditor;
		/// <summary> Дата регистрации в системе </summary>
		public MAppPerson Auditor {
			get { return _fAuditor; }
			set { SetPropertyValue("Auditor", ref _fAuditor, value); }
		}

		[DbType("nvarchar(MAX)")]
		string _fResult;
		/// <summary> Дата регистрации в системе </summary>
		public string Result {
			get { return _fResult; }
			set { SetPropertyValue("Result", ref _fResult, value); }
		}
		
		ESmSertStatus _fStatus;
		/// <summary> Статус проведения инспекции </summary>
		public ESmSertStatus Status {
			get { return _fStatus; }
			set { SetPropertyValue("Status", ref _fStatus, value); }
		}

		MSmSertificate _fSmSertificate;
		/// <summary> Дата регистрации в системе </summary>
		[Association("Sert-to-Inspect")]
		public MSmSertificate SmSertificate {
			get { return _fSmSertificate; }
			set { SetPropertyValue("SmSertificate", ref _fSmSertificate, value); }
		}
	}
}
