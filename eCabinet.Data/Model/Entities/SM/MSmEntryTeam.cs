﻿using System;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Entities.SM {
	[Persistent("SM_EntryTeam")]
	public class MSmEntryTeam : MAppDbObject {
		public MSmEntryTeam() {}
		public MSmEntryTeam(Session session) : base(session) {}
		public MSmEntryTeam(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MSmEntry _fEntry;
		/// <summary> Статус заявки </summary>
		[Association("Employes")]
		public MSmEntry Entry {
			get { return _fEntry; }
			set { SetPropertyValue("Entry", ref _fEntry, value); }
		}

		MAppPerson _fPerson;
		/// <summary> Person </summary>
		public MAppPerson Person {
			get { return _fPerson; }
			set { SetPropertyValue("Person", ref _fPerson, value); }
		}

		EMemberType _fPositionType;
		/// <summary> Должность </summary>
		public EMemberType PositionType {
			get { return _fPositionType; }
			set { SetPropertyValue("PositionType", ref _fPositionType, value); }
		}

		private MEntrySmTypeSert _fType;
		/// <summary> ТИп сертификации/система </summary>
		public MEntrySmTypeSert Type {
			get { return _fType; }
			set { SetPropertyValue("Type", ref _fType, value); }
		}

		DateTime? _fStartResearch;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime? StartResearch {
			get { return _fStartResearch; }
			set { SetPropertyValue("StartResearch", ref _fStartResearch, value); }
		}

		DateTime? _fFinishResearch;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime? FinishResearch {
			get { return _fFinishResearch; }
			set { SetPropertyValue("FinishResearch", ref _fFinishResearch, value); }
		}
	}
}
