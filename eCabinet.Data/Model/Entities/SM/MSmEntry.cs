﻿using System;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Data.Model.Entities.SM {
	/// <summary> Заявка для организации СМК </summary>
	[Persistent("SM_Entry")]
	public class MSmEntry : MAppDbObject {
		public MSmEntry() {}
		public MSmEntry(Session session) : base(session) {}
		public MSmEntry(Session session, XPClassInfo classInfo) : base(session, classInfo) { }

		ESmEntryStatus _fStatus;
		/// <summary> Статус заявки </summary>
		public ESmEntryStatus Status {
			get { return _fStatus; }
			set { SetPropertyValue("Status", ref _fStatus, value); }
		}		
		
		MClientUnit _fDeclarant;
		/// <summary> Декларант </summary>
		public MClientUnit Declarant {
			get { return _fDeclarant; }
			set { SetPropertyValue("Declarant", ref _fDeclarant, value); }
		}

		MOpsUnit _fOpsUnit;
		/// <summary> ОПС </summary>
		public MOpsUnit OpsUnit {
			get { return _fOpsUnit; }
			set { SetPropertyValue("OpsUnit", ref _fOpsUnit, value); }
		}
		
		MAppPerson _fAuditor;
		/// <summary> Аудитор </summary>
		public MAppPerson Auditor {
			get { return _fAuditor; }
			set { SetPropertyValue("Auditor", ref _fAuditor, value); }
		}

		MAppPerson _fAutor;
		/// <summary> Email </summary>
		public MAppPerson Autor {
			get { return _fAutor; }
			set { SetPropertyValue("Autor", ref _fAutor, value); }
		}

		string _fAreaSertification;
		/// <summary> Область сертификации </summary>
		[DbType("nvarchar(MAX)")]
		public string AreaSertification {
			get { return _fAreaSertification; }
			set { SetPropertyValue("AreaSertification", ref _fAreaSertification, value); }
		}

		string _fDeclarantFio;
		/// <summary> Email </summary>
		public string DeclarantFio {
			get { return _fDeclarantFio; }
			set { SetPropertyValue("DeclarantFIO", ref _fDeclarantFio, value); }
		}

		string _fDeclarantEmail;
		/// <summary> Email </summary>
		public string DeclarantEmail {
			get { return _fDeclarantEmail; }
			set { SetPropertyValue("DeclarantEmail", ref _fDeclarantEmail, value); }
		}

		string _fDeclarantPhone;
		/// <summary> Email </summary>
		public string DeclarantPhone {
			get { return _fDeclarantPhone; }
			set { SetPropertyValue("DeclarantPhone", ref _fDeclarantPhone, value); }
		}

		int _fCountYards;
		/// <summary> Email </summary>
		public int CountYards {
			get { return _fCountYards; }
			set { SetPropertyValue("CountYards", ref _fCountYards, value); }
		}

		int _fCountEmployes;
		/// <summary> Email </summary>
		public int CountEmployes {
			get { return _fCountEmployes; }
			set { SetPropertyValue("CountEmployes", ref _fCountEmployes, value); }
		}

		DateTime _fRegistrationDate;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime RegistrationDate {
			get { return _fRegistrationDate; }
			set { SetPropertyValue("RegistrationDate", ref _fRegistrationDate, value); }
		}

		DateTime? _fExpertMaterialsDate;
		/// <summary> Реальная Дата экспертизы материалов </summary>
		public DateTime? ExpertMaterialsDate {
			get { return _fExpertMaterialsDate; }
			set { SetPropertyValue("ExpertMaterialsDate", ref _fExpertMaterialsDate, value); }
		}

		DateTime _fEntryDate;
		/// <summary> Дата заявки </summary>
		[AppSorting(FwSortDirection.DESC, 0)]
		public DateTime EntryDate {
			get { return _fEntryDate; }
			set { SetPropertyValue("EntryDate", ref _fEntryDate, value); }
		}

		DateTime? _fStartResearch;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime? StartResearch {
			get { return _fStartResearch; }
			set { SetPropertyValue("StartResearch", ref _fStartResearch, value); }
		}

		DateTime? _fFinishResearch;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime? FinishResearch {
			get { return _fFinishResearch; }
			set { SetPropertyValue("FinishResearch", ref _fFinishResearch, value); }
		}

		string _fResultResearch;
		/// <summary> Результат обследования </summary>
		[DbType("nvarchar(MAX)")]
		public string ResultResearch {
			get { return _fResultResearch; }
			set { SetPropertyValue("ResultResearch", ref _fResultResearch, value); }
		}

		DateTime? _fResultResearchDate;
		/// <summary> Реальная дата занесения результата обследования </summary>
		public DateTime? ResultResearchDate {
			get { return _fResultResearchDate; }
			set { SetPropertyValue("ResultResearchDate", ref _fResultResearchDate, value); }
		}

		string _fReviewDescription;
		/// <summary> Описание рассмотрения заявки </summary>
		[DbType("nvarchar(MAX)")]
		public string ReviewDescription {
			get { return _fReviewDescription; }
			set { SetPropertyValue("ReviewDescription", ref _fReviewDescription, value); }
		}

		/// <summary>
		/// Команда которая ездила на обследование
		/// </summary>
		[Association("Employes"), Aggregated]
		public XPCollection<MSmEntryTeam> TeamResearch { get { return GetCollection<MSmEntryTeam>("TeamResearch"); } }

		/// <summary>
		/// Типы сертификатов
		/// </summary>
		[Association("SmTypeSert"), Aggregated]
		public XPCollection<MEntrySmTypeSert> SmTypeSert { get { return GetCollection<MEntrySmTypeSert>("SmTypeSert"); } }
	}
}
