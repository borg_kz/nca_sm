﻿using System;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Data.Model.Entities.SM {
	[Persistent("SM_Transfers")]
	public class MSmTransfers : MAppDbBaseObject {
		public MSmTransfers() { }
		public MSmTransfers(Session session) : base(session) {}

		MSmSertificate _fSmSertificate;
		/// <summary> Дата регистрации в системе </summary>
		public MSmSertificate SmSertificate {
			get { return _fSmSertificate; }
			set { SetPropertyValue("SmSertificate", ref _fSmSertificate, value); }
		}

		DateTime _fDate;
		/// <summary> Начало действия сертификата </summary>
		[AppSorting(FwSortDirection.ASC, 0)]
		public DateTime Date {
			get { return _fDate; }
			set { SetPropertyValue("StartDate", ref _fDate, value); }
		}

		MAppPerson _fAuditor;
		/// <summary> Email </summary>
		public MAppPerson Auditor {
			get { return _fAuditor; }
			set { SetPropertyValue("Auditor", ref _fAuditor, value); }
		}

		string _fDescription;
		/// <summary> Результат обследования </summary>
		[DbType("nvarchar(MAX)")]
		public string Description {
			get { return _fDescription; }
			set { SetPropertyValue("Description", ref _fDescription, value); }
		}

		MOpsUnit _fFrom;
		/// <summary> Организация выдавшая сертификат </summary>
		public MOpsUnit From {
			get { return _fFrom; }
			set { SetPropertyValue("From", ref _fFrom, value); }
		}

		MOpsUnit _fRecipient;
		/// <summary> Организация выдавшая сертификат </summary>
		public MOpsUnit Recipient {
			get { return _fRecipient; }
			set { SetPropertyValue("Recipient", ref _fRecipient, value); }
		}
	}
}