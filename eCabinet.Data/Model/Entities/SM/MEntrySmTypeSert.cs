﻿using App.Data.Xpo.Model;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using ECabinet.Data.Model.Reference;

namespace ECabinet.Data.Model.Entities.SM {
	public class MEntrySmTypeSert : MAppDbObject {
		public MEntrySmTypeSert() {}
		public MEntrySmTypeSert(Session session) : base(session) {}
		public MEntrySmTypeSert(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MSmEntry _fSmEntry;
		/// <summary> Заявка </summary>
		[Association("SmTypeSert")]
		public MSmEntry SmEntry {
			get { return _fSmEntry; }
			set { SetPropertyValue("Enrty", ref _fSmEntry, value); }
		}

		MSertificationStore _fSmType;
		/// <summary> Тип сертификации </summary>
		public MSertificationStore SmType {
			get { return _fSmType; }
			set { SetPropertyValue("SmType", ref _fSmType, value); }
		}

		MSmSertificate _fSertificate;
		/// <summary> Сертификат </summary>
		public MSmSertificate Sertificate {
			get { return _fSertificate; }
			set { SetPropertyValue("Sertificate", ref _fSertificate, value); }
		}
	}
}