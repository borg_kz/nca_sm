﻿using System;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Reference;

namespace ECabinet.Data.Model.Entities.SM {
	[Persistent("SM_Sertificate")]
	public class MSmSertificate : MAppDbBaseObject {
		public MSmSertificate() {}
		public MSmSertificate(Session session) : base(session) {}
		public MSmSertificate(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MSmEntry _fSmEntry;
		/// <summary> Заявка </summary>
		public MSmEntry SmEntry {
			get { return _fSmEntry; }
			set { SetPropertyValue("Enrty", ref _fSmEntry, value); }
		}

		MultiString _fSystem;
		/// <summary> System </summary>
		[Persistent("System")]
		public MultiString System {
			get { return _fSystem; }
			set { SetPropertyValue("System", ref _fSystem, value); }
		}

		MultiString _fAreaSertification;
		/// <summary> Область сертификации </summary>
		[Persistent("AreaSertification")]
		public MultiString AreaSertification {
			get { return _fAreaSertification; }
			set { SetPropertyValue("AreaSertification", ref _fAreaSertification, value); }
		}

		MultiString _fRequirements;
		/// <summary> Область сертификации </summary>
		[Persistent("Requirements")]
		public MultiString Requirements {
			get { return _fRequirements; }
			set { SetPropertyValue("Requirements", ref _fRequirements, value); }
		}

		string _fSignatory;
		/// <summary> Email </summary>
		public string Signatory {
			get { return _fSignatory; }
			set { SetPropertyValue("Signatory", ref _fSignatory, value); }
		}

		DateTime? _fFirstRegisterDate;
		/// <summary> Начало действия сертификата </summary>
		public DateTime? FirstRegisterDate {
			get { return _fFirstRegisterDate; }
			set { SetPropertyValue("FirstRegisterDate", ref _fFirstRegisterDate, value); }
		}

		DateTime _fStartDate;
		/// <summary> Начало действия сертификата </summary>
		[AppSorting(FwSortDirection.ASC, 0)]
		public DateTime StartDate {
			get { return _fStartDate; }
			set { SetPropertyValue("StartDate", ref _fStartDate, value); }
		}

		DateTime _fFinishDate;
		/// <summary> Окончание действия сертификата </summary>
		public DateTime FinishDate {
			get { return _fFinishDate; }
			set { SetPropertyValue("FinishDate", ref _fFinishDate, value); }
		}

		DateTime _fInspectionDate;
		/// <summary> Планируемая дата инспекции </summary>
		public DateTime InspectionDate {
			get { return _fInspectionDate; }
			set { SetPropertyValue("InspectionDate", ref _fInspectionDate, value); }
		}

		int _fBlankNumber;
		/// <summary> Номер бланка </summary>
		[Indexed(Unique = true)]
		public int BlankNumber {
			get { return _fBlankNumber; }
			set { SetPropertyValue("BlankNumber", ref _fBlankNumber, value); }
		}

		string _fBlankSeries;
		/// <summary> Серия бланка по умолчанию КСС </summary>
		public string BlankSeries {
			get { return _fBlankSeries; }
			set { SetPropertyValue("BlankSeries", ref _fBlankSeries, value); }
		}

		int _fCounter;
		/// <summary> Порядковый номер сертификата внутри ОПС </summary>
		public int Counter {
			get { return _fCounter; }
			set { SetPropertyValue("Counter", ref _fCounter, value); }
		}

		MultiString _fAreaSertificationAppendix;
		/// <summary> Область сертификации </summary>
		[Persistent("AreaSertificationAppendix")]
		public MultiString AreaSertificationAppendix {
			get { return _fAreaSertificationAppendix; }
			set { SetPropertyValue("AreaSertificationAppendix", ref _fAreaSertificationAppendix, value); }
		}

		int _fBlankNumberAppendix;
		/// <summary> Номер бланка </summary>
		public int BlankNumberAppendix {
			get { return _fBlankNumberAppendix; }
			set { SetPropertyValue("BlankNumberAppendix", ref _fBlankNumberAppendix, value); }
		}

		string _fBlankSeriesAppendix;
		/// <summary> Серия бланка по умолчанию КССП </summary>
		public string BlankSeriesAppendix {
			get { return _fBlankSeriesAppendix; }
			set { SetPropertyValue("BlankSeriesAppendix", ref _fBlankSeriesAppendix, value); }
		}

		MOpsUnit _fOpsUnit;
		/// <summary> Организация выдавшая сертификат </summary>
		public MOpsUnit OpsUnit {
			get { return _fOpsUnit; }
			set { SetPropertyValue("OpsUnit", ref _fOpsUnit, value); }
		}

		MSertificationStore _fSertificationStore;
		/// <summary> Тип сертификации </summary>
		public MSertificationStore SertificationStore {
			get { return _fSertificationStore; }
			set { SetPropertyValue("SmSertificationType", ref _fSertificationStore, value); }
		}

		ESmSertStatus _fStatus;
		/// <summary> Статус сертификата СМК </summary>
		public ESmSertStatus Status {
			get { return _fStatus; }
			set { SetPropertyValue("Status", ref _fStatus, value); }
		}		
		
		bool _fBlocked;
		/// <summary> Если сертификат не актуален, у него статус блокирован </summary>
		public bool Blocked {
			get { return _fBlocked; }
			set { SetPropertyValue("Blocked", ref _fBlocked, value); }
		}

		MSmDublicateReason _fDublicateReason;
		/// <summary> Причина выдачи дубликата </summary>
		public MSmDublicateReason DublicateReason {
			get { return _fDublicateReason; }
			set { SetPropertyValue("DublicateReason", ref _fDublicateReason, value); }
		}

		MAppPerson _fAuditor;
		/// <summary> Email </summary>
		public MAppPerson Auditor {
			get { return _fAuditor; }
			set { SetPropertyValue("Auditor", ref _fAuditor, value); }
		}

		[Association("Sert-to-Inspect"), Aggregated]
		public XPCollection<MSmInspection> Inspections { get { return GetCollection<MSmInspection>("Inspections"); } }
	}
}