﻿using App.Data.Model;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Data.Xpo.Model.Entities.Auth;
using App.Data.Xpo.Model.Entities.Auth.Security;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Entities.OrgStructure {
	[Persistent("Org_Employes")]
	public class MEmploye : MAppDbBaseObject {
		public MEmploye() {}
		public MEmploye(Session session) : base(session) {}
		public MEmploye(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MAppUser _fUser;
		public MAppUser User {
			get { return _fUser; }
			set { SetPropertyValue("User", ref _fUser, value); }
		}

		MAppRole _fRole;
		public MAppRole Role {
			get { return _fRole; }
			set { SetPropertyValue("Role", ref _fRole, value); }
		}
		
		int _fStatus;
		[AppSorting(FwSortDirection.ASC, 0)]
		public int Status {
			get { return _fStatus; }
			set { SetPropertyValue("Status", ref _fStatus, value); }
		}

		MOpsUnit _fOpsUnit;
		[Association("Employes")]
		public MOpsUnit OpsUnit {
			get { return _fOpsUnit; }
			set { SetPropertyValue("OpsUnit", ref _fOpsUnit, value); }
		}
//
//		[Association("Employe-to-OpsUnit", typeof(MOpsUnit)), Aggregated]
//		public XPCollection<MOpsUnit> OpsUnits { get { return GetCollection<MOpsUnit>("OpsUnits"); } }
	}
}