﻿using System;
using App.Data.Xpo.Model;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Entities.OrgStructure {
	[Persistent("Org_OpsTypes")]
	public class MOpsTypes : MAppDbObject {
		public MOpsTypes() {}
		public MOpsTypes(Session session) : base(session) {}

		public MOpsTypes(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MOpsUnit _fOpsUnit;
		/// <summary> ОПС </summary>
		[Association("OpsTypes"), Aggregated]
		public MOpsUnit OpsUnit {
			get { return _fOpsUnit; }
			set { SetPropertyValue("OpsUnit", ref _fOpsUnit, value); }
		}

		EOpsType _fOpsType;
		/// <summary> Тип ОПС </summary>
		public EOpsType OpsType {
			get { return _fOpsType; }
			set { SetPropertyValue("OpsType", ref _fOpsType, value); }
		}

		DateTime _fRegistrationDate;
		/// <summary> Дата регистрации </summary>
		public DateTime RegistrationDate {
			get { return _fRegistrationDate; }
			set { SetPropertyValue("RegistrationDate", ref _fRegistrationDate, value); }
		}


	}
}
