﻿using System;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace ECabinet.Data.Model.Entities.OrgStructure {
	[Persistent("Org_ClientHistory")]
	public class MClientHistory : MAppDbBaseObject {
		public MClientHistory() {}
		public MClientHistory(Session session) : base(session) {}
		public MClientHistory(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MultiString _fName;
		/// <summary> Наименование </summary>
		[Persistent("Name")]
		public MultiString Name {
			get { return _fName; }
			set { SetPropertyValue("Name", ref _fName, value); }
		}

		MultiString _fNameNew;
		/// <summary> Наименование </summary>
		[Persistent("NameNew")]
		public MultiString NameNew {
			get { return _fNameNew; }
			set { SetPropertyValue("NameNew", ref _fNameNew, value); }
		}

		string _fBin;
		/// <summary> БИН Организации </summary>
		public string Bin {
			get { return _fBin; }
			set { SetPropertyValue("Bin", ref _fBin, value); }
		}

		string _fBinNew;
		/// <summary> БИН Организации </summary>
		public string BinNew {
			get { return _fBinNew; }
			set { SetPropertyValue("BinNew", ref _fBinNew, value); }
		}

		DateTime _fRegistrationDate;
		/// <summary> Дата регистрации в системе </summary>
		[AppSorting(FwSortDirection.DESC, 0)]
		public DateTime RegistrationDate {
			get { return _fRegistrationDate; }
			set { SetPropertyValue("RegistrationDate", ref _fRegistrationDate, value); }
		}

		MOpsUnit _fOps;
		/// <summary> Дата регистрации в системе </summary>
		public MOpsUnit OpsUnit {
			get { return _fOps; }
			set { SetPropertyValue("OpsUnit", ref _fOps, value); }
		}

	}
}