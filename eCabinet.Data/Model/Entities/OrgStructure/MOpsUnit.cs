﻿using System;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using ECabinet.Data.Model.Reference;
using App.Data.Xpo.Model.Entities.Auth;

namespace ECabinet.Data.Model.Entities.OrgStructure {
	[Persistent("Org_OpsStore")]
	public class MOpsUnit : MAppDbBaseObject {
		public MOpsUnit(Session session) : base(session) {}
		public MOpsUnit(Session session, XPClassInfo classInfo) : base(session, classInfo) {}
		public MOpsUnit() {}

		int _fOld_ID;
		/// <summary> Старый идентификатор </summary>
		public int Old_ID {
			get { return _fOld_ID; }
			set { SetPropertyValue("Old_ID", ref _fOld_ID, value); }
		}

		MultiString _fName;
		/// <summary> Наименование </summary>
		[Persistent("Name")]
		[AppSorting(FwSortDirection.ASC, 0)]
		public MultiString Name {
			get { return _fName; }
			set { SetPropertyValue("Name", ref _fName, value); }
		}

		DateTime _fSystemRegistrationDate;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime SystemRegistrationDate {
			get { return _fSystemRegistrationDate; }
			set { SetPropertyValue("SystemRegistrationDate", ref _fSystemRegistrationDate, value); }
		}

		DateTime? _fActivateDate;
		/// <summary> Дата активации </summary>
		public DateTime? ActivateDate {
			get { return _fActivateDate; }
			set { SetPropertyValue("ActivateDate", ref _fActivateDate, value); }
		}

		MAreaUnit _fLocation;
		/// <summary> Местоположение </summary>
		public MAreaUnit Location {
			get { return _fLocation; }
			set { SetPropertyValue("Location", ref _fLocation, value); }
		}		
		
		string _fPostCode;
		/// <summary> Почтовый код </summary>
		public string PostCode {
			get { return _fPostCode; }
			set { SetPropertyValue("PostCode", ref _fPostCode, value); }
		}

		string _fEmail;
		/// <summary> Email </summary>
		public string Email {
			get { return _fEmail; }
			set { SetPropertyValue("Email", ref _fEmail, value); }
		}
		
		string _fPhone;
		/// <summary> Телефонны </summary>
		public string Phone {
			get { return _fPhone; }
			set { SetPropertyValue("Phone", ref _fPhone, value); }
		}

		MultiString _fAdress;
		/// <summary> Адрес </summary>
		[Persistent("Adress")]
		public MultiString Adress {
			get { return _fAdress; }
			set { SetPropertyValue("Adress", ref _fAdress, value); }
		}

		string _fSite;
		/// <summary> Сайт </summary>
		public string Site {
			get { return _fSite; }
			set { SetPropertyValue("Site", ref _fSite, value); }
		}

		string _fFioBoss;
		/// <summary> ФИО руководителя </summary>
		public string FioBoss {
			get { return _fFioBoss; }
			set { SetPropertyValue("FioBoss", ref _fFioBoss, value); }
		}

		string _fSinger;
		/// <summary> ФИО с правом подписи </summary>
		public string Singer {
			get { return _fSinger; }
			set { SetPropertyValue("Singer", ref _fSinger, value); }
		}

		string _fBin;
		/// <summary> БИН Организации </summary>
		public string Bin {
			get { return _fBin; }
			set { SetPropertyValue("Bin", ref _fBin, value); }
		}

		string _fSmSertNumber;
		/// <summary> Номер сертификата аккредитации </summary>
		[Indexed(Unique = true)]
		public string SmSertNumber {
			get { return _fSmSertNumber; }
			set { SetPropertyValue("SmSertNumber", ref _fSmSertNumber, value); }
		}

		string _fAttestateNumber;
		/// <summary> Регистрационный номер </summary>
		public string AttestateNumber {
			get { return _fAttestateNumber; }
			set { SetPropertyValue("AttestateNumber", ref _fAttestateNumber, value); }
		}

		DateTime _fDateSertAkreditation;
		/// <summary> Дата регистрации сертификата аккредитации </summary>
		public DateTime SertAkreditationDate {
			get { return _fDateSertAkreditation; }
			set { SetPropertyValue("DateSertAkreditation", ref _fDateSertAkreditation, value); }
		}

		MAppUser _fAdministrator;
		/// <summary> Администратор </summary>
		public MAppUser Administrator {
			get { return _fAdministrator; }
			set { SetPropertyValue("Administrator", ref _fAdministrator, value); }
		}

		bool _fBlocked;
		/// <summary> Статус блокировки </summary>
		public bool Blocked {
			get { return _fBlocked; }
			set { SetPropertyValue("Blocked", ref _fBlocked, value); }
		}

		int _fLastSertNumber;
		/// <summary> Порядковый номер последнего выданного сертификата </summary>
		public int LastSertNumber {
			get { return _fLastSertNumber; }
			set { SetPropertyValue("LastSertNumber", ref _fLastSertNumber, value); }
		}

		DateTime _fDateActivityFrom;
		/// <summary> Дата регистрации от </summary>
		public DateTime DateActivityFrom {
			get { return _fDateActivityFrom; }
			set { SetPropertyValue("DateActivityFrom", ref _fDateActivityFrom, value); }
		}

		DateTime _fDateActivityTo;
		/// <summary> Дата регистрации до </summary>
		public DateTime DateActivityTo {
			get { return _fDateActivityTo; }
			set { SetPropertyValue("DateActivityTo", ref _fDateActivityTo, value); }
		}

		[Association("Employes"), Aggregated]
		public XPCollection<MEmploye> Employes { get { return GetCollection<MEmploye>("Employes"); } }
		
		[Association("OpsTypes"), Aggregated]
		public XPCollection<MOpsTypes> OpsTypes { get { return GetCollection<MOpsTypes>("OpsTypes"); } }

		[Association("OpsUnit-to-Client", typeof(MClientUnit))]
		public XPCollection<MClientUnit> Clients { get { return GetCollection<MClientUnit>("Clients"); } }
	}
}
