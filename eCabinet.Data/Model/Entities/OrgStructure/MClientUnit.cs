﻿using System;
using App.Data.Xpo.Manager;
using App.Data.Xpo.Model;
using App.Framework.Utils.Sort;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using ECabinet.Data.Model.Reference;

namespace ECabinet.Data.Model.Entities.OrgStructure {
	[Persistent("Org_ClientStore")]
	public class MClientUnit : MAppDbBaseObject {
		public MClientUnit() {}
		public MClientUnit(Session session) : base(session) {}
		public MClientUnit(Session session, XPClassInfo classInfo) : base(session, classInfo) {}

		MultiString _fName;
		/// <summary> Наименование </summary>
		[Persistent("Name")]
		[AppSorting(FwSortDirection.ASC, 0)]
		public MultiString Name {
			get { return _fName; }
			set { SetPropertyValue("Name", ref _fName, value); }
		}

		DateTime _fRegistrationDate;
		/// <summary> Дата регистрации в системе </summary>
		public DateTime RegistrationDate {
			get { return _fRegistrationDate; }
			set { SetPropertyValue("RegistrationDate", ref _fRegistrationDate, value); }
		}

		MAreaUnit _fLocation;
		/// <summary> Местоположение </summary>
		public MAreaUnit Location {
			get { return _fLocation; }
			set { SetPropertyValue("Location", ref _fLocation, value); }
		}

		string _fEmail;
		/// <summary> Email </summary>
		public string Email {
			get { return _fEmail; }
			set { SetPropertyValue("Email", ref _fEmail, value); }
		}

		string _fPhone;
		/// <summary> Телефонны </summary>
		public string Phone {
			get { return _fPhone; }
			set { SetPropertyValue("Phone", ref _fPhone, value); }
		}

		MultiString _fAdress;
		/// <summary> Адрес </summary>
		[Persistent("Adress")]
		public MultiString Adress {
			get { return _fAdress; }
			set { SetPropertyValue("Adress", ref _fAdress, value); }
		}

		string _fFioBoss;
		/// <summary> ФИО руководителя </summary>
		public string FioBoss {
			get { return _fFioBoss; }
			set { SetPropertyValue("FioBoss", ref _fFioBoss, value); }
		}

		string _fBin;
		/// <summary> БИН Организации </summary>
		[Indexed(Unique = true)]
		public string Bin {
			get { return _fBin; }
			set { SetPropertyValue("Bin", ref _fBin, value); }
		}

		bool _fBlocked;
		/// <summary> Статус блокировки </summary>
		public bool Blocked {
			get { return _fBlocked; }
			set { SetPropertyValue("Blocked", ref _fBlocked, value); }
		}

		[Association("OpsUnit-to-Client", typeof(MOpsUnit))]
		public XPCollection<MOpsUnit> OpsUnits { get { return GetCollection<MOpsUnit>("OpsUnits"); } }

	}
}
