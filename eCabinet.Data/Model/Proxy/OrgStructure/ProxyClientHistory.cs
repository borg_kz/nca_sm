﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using App.Data.Proxy;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Data.Model.Proxy.OrgStructure {
	public class ProxyClientHistory : AppProxyBase, IAppProxySort {
		public ProxyClientHistory() {}

		public ProxyClientHistory(MClientHistory client) : base(client?.Oid ?? 0) {
			if (client == null) return;

			Name = new AppProxyMultiString(client.Name);
			NameNew = new AppProxyMultiString(client.NameNew);
			Bin = client.Bin;
			BinNew = client.BinNew;
			OpsTypeDisplay = client.OpsUnit.Name.ToString();
			RegistrationDate = client.RegistrationDate;
		}

		public AppProxyMultiString Name { get; set; }
		public AppProxyMultiString NameNew { get; set; }
		public string Bin { get; set; }
		public string BinNew { get; set; }
		public DateTime? RegistrationDate { get; set; }
		public string OpsTypeDisplay { get; set; }

		public Expression<Func<T, object>> GetExpression<T>(PropertyInfo property) {
			return BuildExpression(property) as Expression<Func<T, object>>;
		}

		public Expression<Func<MClientUnit, object>> BuildExpression(PropertyInfo property) {
			switch (property.Name) {
				case "Name":
					return a => a.Name;
				case "LocationDisplay":
					return a => a.Location.Name;
				case "SystemRegistrationDate":
					return a => a.RegistrationDate;
			}

			return null;
		}
	}
}