﻿using System;
using App.Data.Xpo.Model.Entities.Auth;
namespace ECabinet.Data.Model.Proxy.OrgStructure {
	public class ProxyUser {
		public ProxyUser() {}

		public ProxyUser(MAppUser user) {
			if (user == null) return;
			Id = user.Oid;
			ShortName = string.Format("{0} {1}.", user.Person.SecondName, (string.IsNullOrEmpty(user.Person.FirstName) ? "" : user.Person.FirstName[0].ToString()));
			Iin = user.Person.Iin;
			LastActivityDate = user.LastActivity;
			Login = user.UserName;
			// Ops = opsName;
			// OnLine = user.IsOnline ? SysStrings.Msg_Online : string.Empty;
		}

		public long Id { get; set; }
		public string ShortName { get; set; }
		public string Iin { get; set; }
		public string Password { get; set; }
		public DateTime? LastActivityDate { get; set; }
		public string Login { get; set; }
		public string OnLine { get; set; }
	}
}
