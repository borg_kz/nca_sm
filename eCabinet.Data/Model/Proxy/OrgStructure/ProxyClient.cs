﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using App.Data.Proxy;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Data.Model.Proxy.OrgStructure {
	public class ProxyClient : AppProxyBase, IAppProxySort {
		public ProxyClient() {}

		public ProxyClient(MClientUnit client) : base(client == null ? 0 : client.Oid) {
			if (client == null) return;

			Name = new AppProxyMultiString(client.Name);
			Location = client.Location != null ? client.Location.Oid : 0;
			LocationDisplay = client.Location != null ? client.Location.Name.ToString() : string.Empty;
			Bin = client.Bin;
			RegistrationDate = client.RegistrationDate;
			Adress = client.Adress;
			Email = client.Email;
			Phone = client.Phone;
			FioBoss = client.FioBoss;
		}

		public AppProxyMultiString Name { get; set; }
		public long? Location { get; set; }
		public string LocationDisplay { get; set; }
		public string Bin { get; set; }
		public DateTime? RegistrationDate { get; set; }
		public AppProxyMultiString Adress { get; set; }
		public string Email { get; set; }
		public string Login { get; set; }
		public string Phone { get; set; }
		public string FioBoss { get; set; }

		public Expression<Func<T, object>> GetExpression<T>(PropertyInfo property) {
			return BuildExpression(property) as Expression<Func<T, object>>;
		}

		public Expression<Func<MClientUnit, object>> BuildExpression(PropertyInfo property) {
			switch (property.Name) {
				case "Name":
					return a => a.Name;
				case "LocationDisplay":
					return a => a.Location.Name;
				case "SystemRegistrationDate":
					return a => a.RegistrationDate;
			}

			return null;
		}
	}
}