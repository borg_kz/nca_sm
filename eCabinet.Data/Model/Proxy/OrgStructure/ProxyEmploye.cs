﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using App.Data.Proxy;
using App.Data.Xpo.Model;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Data.Model.Proxy.OrgStructure {
	public class ProxyEmploye : AppProxyBase, IAppProxySort {
		public ProxyEmploye() {}

		public ProxyEmploye(MEmploye emp) : base(emp.Oid) {
			Role = emp.Role.Oid;
			RoleDisplay = emp.Role.Name;
			LastActivity = emp.User.LastActivity;
			FullName = String.Format("{0} {1} {2}", emp.User.Person.SecondName, emp.User.Person.FirstName, emp.User.Person.ThirdName);
			FirstName = emp.User.Person.FirstName;
			SecondName = emp.User.Person.SecondName;
			ThirdName = emp.User.Person.ThirdName;
			Email = emp.User.Person.Email;
			HomeTel = emp.User.Person.HomeTel;
			MobTel = emp.User.Person.MobileTel;
			Iin = emp.User.Person.Iin;
			Login = emp.User.UserName;
			Gender = (int)emp.User.Person.Gender;
			Password = "...........";
		}

		public DateTime? LastActivity { get; set; }
		public long? Role { get; set; }
		// public int Id { get; set; }
		public string RoleDisplay { get; set; }
		public string FullName { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
		public string ThirdName { get; set; }
		public string Email { get; set; }
		public string HomeTel { get; set; }
		public string MobTel { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string Iin { get; set; }
		public int? Gender { get; set; }

		public Expression<Func<T, object>> GetExpression<T>(PropertyInfo property) {
			return BildExpression(property) as Expression<Func<T, object>>;
		}

		public Expression<Func<MEmploye, object>> BildExpression(PropertyInfo property) {
			throw new NotImplementedException();
		}
	}
}