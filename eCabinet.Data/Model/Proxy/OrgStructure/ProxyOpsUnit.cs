﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using App.Data.Proxy;
using App.Framework.Utils;
using ECabinet.Data.Model.Entities.OrgStructure;

namespace ECabinet.Data.Model.Proxy.OrgStructure {
	public class ProxyOpsUnit : AppProxyBase, IAppProxySort {
		public ProxyOpsUnit() { }

		public ProxyOpsUnit(MOpsUnit ops, EOpsType opsType) : base(ops.Oid) {
			if (ops == null) return;
			Name = ops.Name;
			SmSertNumber = ops.SmSertNumber;
			Location = ops.Location != null ? ops.Location.Oid : 0;
			LocationDisplay = ops.Location != null ? ops.Location.Name.ToString() : string.Empty;
			OpsType = (int)opsType;
			OpsTypeDisplay = opsType.GetLocalizedField();
			SystemRegistrationDate = ops.SystemRegistrationDate;
			Email = ops.Email;
			Login = ops.Administrator != null ? ops.Administrator.UserName : string.Empty;
			ActivateDate = ops.ActivateDate;
			AttestateNumber = ops.AttestateNumber;
			PostCode = ops.PostCode;

			Bin = ops.Bin;
			LastSertNumber = ops.LastSertNumber;
			Site = ops.Site;
			Adress = ops.Adress;
			Phone = ops.Phone;
			FioBoss = ops.FioBoss;
			Singer = ops.Singer;
		}

		public AppProxyMultiString Name { get; set; }
		public AppProxyMultiString Adress { get; set; }
		public int? LastSertNumber { get; set; }
		public string Bin { get; set; }
		public string Site { get; set; }
		public string PostCode { get; set; }
		public string AttestateNumber { get; set; }
		public string SmSertNumber { get; set; }
		public string Phone { get; set; }
		public long? Location { get; set; }
		public string LocationDisplay { get; set; }
		public int? OpsType { get; set; }
		public string OpsTypeDisplay { get; set; }
		public DateTime? SystemRegistrationDate { get; set; }
		public DateTime? ActivateDate { get; set; }
		public string Email { get; set; }
		public string FioBoss { get; set; }
		public string Singer { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }

		public Expression<Func<T, object>> GetExpression<T>(PropertyInfo property) {
			return BuildExpression(property) as Expression<Func<T, object>>;
		}

		public Expression<Func<MOpsUnit, object>> BuildExpression(PropertyInfo property) {
			switch (property.Name) {
				case "Name":
					return a => a.Name;
				case "LocationDisplay":
					return a => a.Location.Name;
			}

			return null;
		}
	}
}
