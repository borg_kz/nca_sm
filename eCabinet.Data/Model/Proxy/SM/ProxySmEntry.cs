﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using App.Data.Proxy;
using App.Framework.Utils;
using ECabinet.Data.Model.Entities.SM;

namespace ECabinet.Data.Model.Proxy.SM {
	public class ProxySmEntry : AppProxyBase, IAppProxySort {
		public ProxySmEntry() {}

		public ProxySmEntry(MSmEntry obj) : base(obj.Oid) {
			Status = obj.Status;
			StatusDisplay = obj.Status.GetLocalizedField();
			Declarant = obj.Declarant.Oid;
			DeclarantDisplay = obj.Declarant.Name;
			Autor = string.Format("{0} {1}.", obj.Autor.SecondName, (string.IsNullOrEmpty(obj.Autor.FirstName) ? "" : obj.Autor.FirstName[0].ToString()));
			if (obj.Auditor != null) {
				Auditor = obj.Auditor.Oid;
				AuditorDisplay = string.Format("{0} {1}.", obj.Auditor.SecondName, (string.IsNullOrEmpty(obj.Auditor.FirstName) ? "" : obj.Auditor.FirstName[0].ToString()));
			}
			AreaSertification = obj.AreaSertification;
			DeclarantFio = obj.DeclarantFio;
			DeclarantEmail = obj.DeclarantEmail;
			DeclarantPhone = obj.DeclarantPhone;
			CountEmployes = obj.CountEmployes;
			CountYards = obj.CountYards;
			RegistrationDate = obj.RegistrationDate;
			EntryDate = obj.EntryDate;
			StartResearch = obj.StartResearch;
			FinishResearch = obj.FinishResearch;
			ResultResearch = obj.ResultResearch;
			ResultResearchDate = obj.ResultResearchDate;
			ReviewDescription = obj.ReviewDescription;
			SmTypeSert = obj.SmTypeSert.Select(a => a.SmType.Oid).ToList();
			if (obj.SmTypeSert.Count != 0)
				SmTypes = obj.SmTypeSert.Select(a => a.SmType.ShortName).Aggregate((typeSm, next) =>
					typeSm + ", " + next);
		}

		public ESmEntryStatus Status  { get; set; }
		public string StatusDisplay { get; set; }
		public long? Declarant  { get; set; }
		public string DeclarantDisplay  { get; set; }
		public long? Auditor { get; set; }
		public string AuditorDisplay { get; set; }
		public string Autor { get; set; }
		public string AreaSertification { get; set; }
		public string DeclarantFio  { get; set; }
		public string DeclarantEmail  { get; set; }
		public string DeclarantPhone  { get; set; }
		public List<long> SmTypeSert { get; set; }
		public string SmTypes { get; set; }
		public int? CountYards  { get; set; }
		public int? CountEmployes  { get; set; }
		public DateTime? RegistrationDate  { get; set; }
		public DateTime? EntryDate  { get; set; }
		public DateTime? StartResearch  { get; set; }
		public DateTime? FinishResearch  { get; set; }
		public DateTime? ResultResearchDate { get; set; }
		public string ResultResearch { get; set; }
		public string ReviewDescription { get; set; }

		public Expression<Func<T, object>> GetExpression<T>(PropertyInfo property) {
			throw new NotImplementedException();
		}
	}
}
	