﻿using System;
using System.Diagnostics;
using System.Linq;
using App.Data.Proxy;
using App.Framework;
using App.Framework.Utils;
using ECabinet.Data.Model.Entities.OrgStructure;
using ECabinet.Data.Model.Entities.SM;
using ECabinet.Data.Properties;

namespace ECabinet.Data.Model.Proxy.SM {
	public class ProxySmSertificate : AppProxyBase {
		public ProxySmSertificate() {}

		/// <summary> Используется в отчётах по бланкам </summary>
		public ProxySmSertificate(MSmSertificate sert, bool needFirstYear) : base(sert.Oid) {
			SertifikateSmNumber = sert.OpsUnit.SmSertNumber + "." + NornalizaNumber(sert.Counter, 5);

			SertifikateSmNumber = needFirstYear
				? (DateTime.Now.Year - 2010) + SertifikateSmNumber
				: SertifikateSmNumber + Environment.NewLine + Strings.Common_KSS + NornalizaNumber(sert.BlankNumber, 7);

			AreaSertification = sert.AreaSertification.ToString(FwCultures.Rus);
			AreaSertificationKz = sert.AreaSertification.ToString(FwCultures.Kaz);
			AreaSertificationEn = sert.AreaSertification.ToString(FwCultures.Eng);
			CounterDisplay = sert.StartDate.ToString("dd.MM.yyyy") + " - " + Environment.NewLine + sert.FinishDate.ToString("dd.MM.yyyy");
			Declarant = sert.SmEntry.Declarant.Name;
			if (!needFirstYear)
				Declarant += Environment.NewLine + sert.SmEntry.Declarant.Adress.ToString(FwCultures.Rus);

			StatusDisplay = sert.Status.GetLocalizedField();
			BlankNumber = NornalizaNumber(sert.BlankNumber, 7);
			StartDate = sert.StartDate;
			FinishDate = sert.FinishDate;
			RegistrationEntryDate = sert.SmEntry.EntryDate;
			EntryDate = sert.SmEntry.RegistrationDate;
			ResultReseachDate = sert.SmEntry.ResultResearchDate;
			Auditor = sert.Auditor?.ShortName;
			AuditorId = sert.Auditor?.Oid;
			Signatory = sert.OpsUnit.Name;
			Counter = needFirstYear ? sert.OpsUnit.Old_ID : sert.Counter;
			System = sert.System.ToString(FwCultures.Rus);
			Requirements = sert.Requirements.ToString(FwCultures.Rus);
			SmSertificationTypeDisplay = sert.SertificationStore.ShortName;
		}

		public ProxySmSertificate(MSmSertificate sert) : base(sert.Oid) {
			SertifikateSmNumber = sert.OpsUnit.SmSertNumber + "." + NornalizaNumber(sert.Counter, 5);
			AreaSertification = sert.AreaSertification.ToString(FwCultures.Rus);
			AreaSertificationKz = sert.AreaSertification.ToString(FwCultures.Kaz);
			AreaSertificationEn = sert.AreaSertification.ToString(FwCultures.Eng);

			Declarant = sert.SmEntry.Declarant.Name;
			Adress = sert.SmEntry.Declarant.Adress;
			StatusDisplay = sert.Status.GetLocalizedField();

			if (sert.Status == ESmSertStatus.Valid)
				StatusDisplay = "Действует";

			if (sert.DublicateReason != null)
				StatusDisplay += " [" + sert.DublicateReason.Name + "]";

			if (sert.Status == ESmSertStatus.Renewed || sert.Status == ESmSertStatus.Suspended)
				StatusDisplay += " [" + sert.Inspections.Last(a => a.Status == sert.Status).Result + "]";

			if (sert.FinishDate < DateTime.Now)
				StatusDisplay = Strings.Common_Expired;

			BlankNumber = NornalizaNumber(sert.BlankNumber, 7);
			StartDate = sert.StartDate;
			FinishDate = sert.FinishDate;
			InspectionDate = sert.SmEntry.EntryDate;
			StartReseachDate = sert.SmEntry.StartResearch;
			ResultReseachDate = sert.SmEntry.FinishResearch;
			Auditor = sert.Auditor?.ShortName;
			AuditorId = sert.Auditor?.Oid;
			Signatory = sert.OpsUnit.Name;
			Counter = sert.Counter;
			CounterDisplay = sert.OpsUnit.Name;
			System = sert.System.ToString(FwCultures.Rus);
			Requirements = sert.Requirements.ToString(FwCultures.Rus);
			SmSertificationTypeDisplay = sert.SertificationStore.ShortName;
		}

		public ProxySmSertificate(MSmSertificate sert, MOpsUnit ops) : base(sert.Oid) {
			Debug.Assert(sert != null);

			Status = sert.Status;
			StatusDisplay = sert.Status.GetLocalizedField();
			Counter = sert.Counter;
			CounterDisplay = ops.SmSertNumber + "." + NornalizaNumber(sert.Counter, 5);

			System = sert.System.ToString(FwCultures.Rus);
			AreaSertification = sert.AreaSertification.ToString(FwCultures.Rus);
			Requirements = sert.Requirements.ToString(FwCultures.Rus);
			AreaSertificationAppendix = sert.AreaSertificationAppendix.ToString(FwCultures.Rus);
			
			SystemKz = sert.System.ToString(FwCultures.Kaz);
			AreaSertificationKz = sert.AreaSertification.ToString(FwCultures.Kaz);
			RequirementsKz = sert.Requirements.ToString(FwCultures.Kaz);
			AreaSertificationAppendixKz = sert.AreaSertificationAppendix.ToString(FwCultures.Kaz);
			
			SystemEn = sert.System.ToString(FwCultures.Eng);
			AreaSertificationEn = sert.AreaSertification.ToString(FwCultures.Eng);
			RequirementsEn = sert.Requirements.ToString(FwCultures.Eng);
			AreaSertificationAppendixEn = sert.AreaSertificationAppendix.ToString(FwCultures.Eng);

			Auditor = sert.Auditor?.ShortName;
			AuditorId = sert.Auditor?.Oid;
			Signatory = sert.Signatory;
			StartDate = sert.StartDate;
			FinishDate = sert.FinishDate;
			InspectionDate = sert.InspectionDate;
			BlankNumber = NornalizaNumber(sert.BlankNumber, 7);
			BlankNumberAppendix = sert.BlankNumberAppendix;
			Declarant = sert.SmEntry.Declarant.Name.ToString();
			SmSertificationType = sert.SertificationStore.Oid;
			SmSertificationTypeDisplay = sert.SertificationStore.ShortName;
			ResultReseachDate = sert.SmEntry.ResultResearchDate;
			DublicateReason = sert.DublicateReason == null ? -1 : sert.DublicateReason.Oid;
		}

		public ProxySmSertificate(MEntrySmTypeSert entry, MOpsUnit ops) : base(entry.Oid) {
			Debug.Assert(entry != null);

			Status = ESmSertStatus.Entry;
			StatusDisplay = ESmSertStatus.Entry.GetLocalizedField();
			Counter = ops.LastSertNumber;
			CounterDisplay = ops.SmSertNumber + "." + NornalizaNumber(0, 5);
			Auditor = entry.SmEntry.Auditor.ShortName;
			Declarant = entry.SmEntry.Declarant.Name;
			ResultReseachDate = entry.SmEntry.FinishResearch;
			AreaSertificationKz = entry.SmEntry.AreaSertification;
			AreaSertification = entry.SmEntry.AreaSertification;
			SmSertificationType = entry.SmType.Oid;
			SmSertificationTypeDisplay = entry.SmType.ShortName;
			Signatory = ops.FioBoss;
		}

		public static string NornalizaNumber(int number, int couner) {
			string result = string.Empty;
			for (int i = 0; i < couner - number.ToString().Length; i++)
				result += "0";

			return result + number;
		}

		public long? DublicateReason { get; set; }
		public ESmSertStatus Status { get; set; }
		public string StatusDisplay { get; set; }
		public string SertifikateSmNumber { get; set; }
		public string AreaSertification { get; set; }
		public string Requirements { get; set; }
		public string AreaSertificationAppendix { get; set; }
		public string System { get; set; }
		public string SystemKz { get; set; }
		public string AreaSertificationKz { get; set; }
		public string RequirementsKz { get; set; }
		public string AreaSertificationAppendixKz { get; set; }
		public string SystemEn { get; set; }
		public string AreaSertificationEn { get; set; }
		public string RequirementsEn { get; set; }
		public string AreaSertificationAppendixEn { get; set; }
		public string Signatory { get; set; }
		public DateTime? FirstRegDate { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? FinishDate { get; set; }
		public DateTime? StartReseachDate { get; set; }
		public DateTime? ResultReseachDate { get; set; }
		public DateTime? InspectionDate { get; set; }
		public DateTime? EntryDate { get; set; }
		public DateTime? RegistrationEntryDate { get; set; }
		public string BlankNumber { get; set; }
		public long SmSertificationType { get; set; }
		public string SmSertificationTypeDisplay { get; set; }
		public int Counter { get; set; }
		public string CounterDisplay { get; set; }
		public string Auditor { get; set; }
		public long? AuditorId { get; set; }
		public string Declarant { get; set; }
		public string Adress { get; set; }
		public int DuplicateType { get; set; }
		public int BlankNumberAppendix { get; set; }
	}
}
