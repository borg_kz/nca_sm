﻿using System;
using App.Data.Xpo.Model.Entities;
using ECabinet.Data.Model.Entities.SM;

namespace ECabinet.Data.Model.Proxy.SM {
	public class ProxySmTeamExpert {
		public ProxySmTeamExpert() {}

		public ProxySmTeamExpert(MAppPerson person) {
			if (person == null) return;
			Iin = person.Iin;
			FirstName = person.FirstName;
			SecondName = person.SecondName;
			ThirdName = person.ThirdName;
			TeamFIO = person.ShortName;
		}

		public ProxySmTeamExpert(MSmEntryTeam team) {
			if (team == null) return;
			Id = team.Oid;
			SmTypeSertId = team.Type.Oid;
			SmTypeSert = team.Type.SmType.ShortName;
			Auditor = team.Person.Oid;
			StartResearch = team.StartResearch;
			FinishResearch = team.FinishResearch;
			Iin = team.Person.Iin;
			FirstName = team.Person.FirstName;
			SecondName = team.Person.SecondName;
			ThirdName = team.Person.ThirdName;
			TeamFIO = team.Person.ShortName;

			Status = team.PositionType.ToString();
		}

		public long Id { get; set; }
		public string Iin { get; set; }
		public long Auditor { get; set; }
		public long SmTypeSertId { get; set; }
		public string SmTypeSert { get; set; }
		public long Entry { get; set; }
		public DateTime? StartResearch { get; set; }
		public DateTime? FinishResearch { get; set; }
		public string TeamFIO { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
		public string ThirdName { get; set; }
		public string Status { get; set; }

	}
}
