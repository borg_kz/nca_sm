﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using App.Data.Proxy;
using App.Data.Xpo.Model;
using App.Framework.Utils;
using ECabinet.Common;
using ECabinet.Data.Model.Entities.SM;

namespace ECabinet.Data.Model.Proxy.SM {
	public class ProxySmInspection : AppProxyBase, IAppProxySort {
		public ProxySmInspection() {}

		public ProxySmInspection(MSmInspection insp) : base(insp.Oid) {
			SertId = insp.SmSertificate.Oid;
			SertNumber = insp.SmSertificate.OpsUnit.SmSertNumber + ProxySmSertificate.NornalizaNumber(insp.SmSertificate.Counter, 5);
			BlankNumber = ProxySmSertificate.NornalizaNumber(insp.SmSertificate.BlankNumber, 7);
			Declarant = insp.SmSertificate.SmEntry.Declarant.Name;
			StartDate = insp.SmSertificate.StartDate.ToString("dd.MM.yy");
			FinishDate = insp.SmSertificate.FinishDate.ToString("dd.MM.yy");
			StatusDisplay = insp.Status.GetLocalizedField();
			Status = insp.Status;

			Auditor = insp.Auditor.ShortName;
			DateInspection = insp.InspectionDate;
			StatusInsp = (int)insp.Status;
			StatusInspDisplay = insp.Status.GetLocalizedField();
			Result = insp.Result;
		}

		public ProxySmInspection(MSmSertificate sert) : base(sert.Oid) {
			SertId = sert.Oid;

			StatusDisplay = ESmSertStatus.Valid.GetLocalizedField();
			Status = sert.Status;
			SertNumber = sert.OpsUnit.SmSertNumber + ProxySmSertificate.NornalizaNumber(sert.Counter, 5);
			BlankNumber = ProxySmSertificate.NornalizaNumber(sert.BlankNumber, 7);
			Declarant = sert.SmEntry.Declarant.Name;
			StartDate = sert.StartDate.ToString("dd.MM.yy");
			FinishDate = sert.FinishDate.ToString("dd.MM.yy");
			Result = sert.AreaSertification;
			Auditor = sert.Auditor.ShortName;
			DateInspection = sert.CreateTime;
		}

		public ProxySmInspection(MSmEntry entry, MSmSertificate sert) : base(entry.Oid) {
			SertId = sert.Oid;
			Id = Guid.NewGuid().ToString();

			StatusDisplay = entry.Status.GetLocalizedField();
			Status = ESmSertStatus.Valid;
			SertNumber = sert.OpsUnit.SmSertNumber + ProxySmSertificate.NornalizaNumber(sert.Counter, 5);
			BlankNumber = ProxySmSertificate.NornalizaNumber(sert.BlankNumber, 7);
			Declarant = entry.Declarant.Name;
			StartDate = sert.StartDate.ToString("dd.MM.yy");
			FinishDate = sert.FinishDate.ToString("dd.MM.yy");
			Result = sert.AreaSertification;
			Auditor = entry.Auditor.ShortName;
			DateInspection = entry.EntryDate;
		}

		public ProxySmInspection(MSmEntry entry, ESmEntryStatus status, MSmSertificate sert) : base(entry.Oid) {
			SertId = sert.Oid;
			Id = Guid.NewGuid().ToString();

			if (status == ESmEntryStatus.New) {
				StatusDisplay = ESmEntryStatus.New.GetLocalizedField();
				Status = sert.Status;
				SertNumber = sert.OpsUnit.SmSertNumber + ProxySmSertificate.NornalizaNumber(sert.Counter, 5);
				BlankNumber = ProxySmSertificate.NornalizaNumber(sert.BlankNumber, 7);
				StartDate = sert.StartDate.ToString("dd.MM.yy");
				FinishDate = sert.FinishDate.ToString("dd.MM.yy");
				Declarant = entry.Declarant.Name;
				DateInspection = entry.EntryDate;
				Auditor = entry.Autor.ShortName;
				Result = entry.AreaSertification;
				return;
			}
			if (status == ESmEntryStatus.Review) {
				StatusDisplay = ESmEntryStatus.Review.GetLocalizedField();
				Status = sert.Status;
				SertNumber = sert.OpsUnit.SmSertNumber + ProxySmSertificate.NornalizaNumber(sert.Counter, 5);
				BlankNumber = ProxySmSertificate.NornalizaNumber(sert.BlankNumber, 7);
				StartDate = sert.StartDate.ToString("dd.MM.yy");
				FinishDate = sert.FinishDate.ToString("dd.MM.yy");
				Declarant = entry.Declarant.Name;
				DateInspection = entry.ExpertMaterialsDate;
				Auditor = entry.Auditor.ShortName;
				Result = entry.ResultResearch;
				return;
			}
			if (status == ESmEntryStatus.Processed) {
				StatusDisplay = ESmEntryStatus.Processed.GetLocalizedField();
				Status = sert.Status;
				SertNumber = sert.OpsUnit.SmSertNumber + ProxySmSertificate.NornalizaNumber(sert.Counter, 5);
				BlankNumber = ProxySmSertificate.NornalizaNumber(sert.BlankNumber, 7);
				StartDate = sert.StartDate.ToString("dd.MM.yy");
				FinishDate = sert.FinishDate.ToString("dd.MM.yy");
				Declarant = entry.Declarant.Name;
				DateInspection = entry.FinishResearch;
				Auditor = entry.Auditor.ShortName;
				Result = entry.ReviewDescription;
			}
		}

		public long? SertId { get; set; }
		public string StartDate { get; set; }
		public string FinishDate { get; set; }
		public DateTime? DateInspection { get; set; }
		public ESmSertStatus Status { get; set; }
		public string StatusDisplay { get; set; }
		public int? StatusInsp { get; set; }
		public string StatusInspDisplay { get; set; }
		public string Auditor { get; set; }
		public string Declarant { get; set; }
		public string Result { get; set; }
		public string SertNumber { get; set; }
		public string BlankNumber { get; set; }

		public Expression<Func<T, object>> GetExpression<T>(PropertyInfo property) {
			throw new NotImplementedException();
		}
	}
}
