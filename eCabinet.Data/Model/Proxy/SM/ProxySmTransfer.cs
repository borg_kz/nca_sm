﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using App.Data.Proxy;
using ECabinet.Data.Model.Entities.SM;

namespace ECabinet.Data.Model.Proxy.SM {
	public class ProxySmTransfer  : AppProxyBase, IAppProxySort {
		public ProxySmTransfer() { }

		public ProxySmTransfer(MSmTransfers obj) : base(obj.Oid) {
			Date = obj.Date;
			Auditor = obj.Auditor?.Oid;
			AuditorFio = obj.Auditor?.ShortName;
			Sertificate = $"№ сертификата {obj.SmSertificate.Counter}, действие {obj.SmSertificate.StartDate.ToShortDateString()}-{obj.SmSertificate.FinishDate.ToShortDateString()}";
			BlankNumber = obj.SmSertificate.BlankNumber;
			Recipient = obj.Recipient.Name;
		}

		public DateTime Date { get; set; }
		public long? Auditor { get; set; }
		public string AuditorFio { get; set; }
		public string Sertificate { get; set; }
		public int BlankNumber { get; set; }
		public string Recipient { get; set; }
		public string Description { get; set; }

		public Expression<Func<T, object>> GetExpression<T>(PropertyInfo property) {
			return null;
		}
	}
}