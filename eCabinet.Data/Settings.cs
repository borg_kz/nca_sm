﻿using System.Reflection;
using System.Web.Mvc;
using App.Data.Xpo.Authentification;
using App.Data.Xpo.Model;
using App.Framework;
using App.Framework.Localization.Data;
using App.Framework.Logs;
using App.Framework.Web.Autentification;
using App.Settings;
using App.Settings.Auth;

namespace ECabinet.Data {
	public class Settings : AppSettingsBase<Settings>, IAppSystemSettings {
		public override Assembly ExtensionAssembly => null;

		public bool AuthorizeUserBySession => true;

		public IFwMultiLanguageString AppFullName {
			get { return new MultiString(new MString("NCA eCabinet", FwCultures.Rus), new MString("NCA eCabinet", FwCultures.Kaz)); }
		}
		public string AppLoginBackgroundImagePath { get; private set; }

		IFwMultiLanguageString IAppSystemSettings.AppShortName {
			get { return new MultiString(new MString("eCabinet", FwCultures.Rus), new MString("eCabinet", FwCultures.Kaz)); }
		}

		public IFwMultiLanguageString AppTitle => new MultiString(new MString("eCabinet", FwCultures.Rus), new MString("eCabinet", FwCultures.Kaz));

		public string AppVersion => "v2.2.10";
		public FwLogMode AuditMode => FwLogMode.Important;
		public FwLogMode MonitoringMode => FwLogMode.Important;
		public string ChangesXmlPath => Get(FwGlobal.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\Changes.xml");
		public string Icon => "fa-yelp";
		public string DefaultTheme => "Crisp";

		private IAppAuthentificationSettings _appAuthentificationSettings;

		public IAppAuthentificationSettings AuthentificationSettings {
			get { return _appAuthentificationSettings ?? (_appAuthentificationSettings = new AuthentificationSettings()); }
		}

		public int MinRequiredPasswordLength => 6;
		public int CountDayEntry => 1;
	}

	public class AuthentificationSettings : IAppAuthentificationSettings {
		public IFwFormsAutentification GetAuthentificationService(Controller controller) {
			return new AppFormsAutentificationService();
		}

		public string MembershipProviderName => "LocalMembershipProvider";
	}
}